Hello, and welcome to the Boot Sequence Readme.

Boot Sequence is a roguelike dungeon crawler. It is made with Unity 2018.3.2f1. 

Please try to commit to your hyperdev branch. It's okay to commit to develop, but do your best to avoid that.

And above all else, have fun.

If anything breaks, stop whatever you're doing, and message Jimmy.
