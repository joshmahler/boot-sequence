﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using UnityEngine.UI;


public class GemScript : MonoBehaviour
{
    public enum GemType
    {
        INVALID_TYPE = -1,
        LIGHTNING,
        SPEED,
        POWER,
        ENERGY,
        FIRE,
        WIND,
        ICE,
        NUM_TYPES
    };

    public GemType type;
    public LayerMask layerMask;
    [TextArea]
    public string description;

    /*
    All different gem types inherit from GemScript
    Each child class should have overrides for all nessecary functions
    */
    [HideInInspector] public MovementScript movementScript = null;
    //public float decayTime;

     public string soundSwitch;

    //lightning gem
    [HideInInspector] public float kickStunTimeBonus = 0f;
    [HideInInspector] public float contactStunTime = 0f;

    //speed gem
    [HideInInspector] public float hoverSpeedMult = 0f;
    [HideInInspector] public float stunKickWindupMod = 0f;
    [HideInInspector] public float dashSpeedMult = 0f;

    //power gem
    [HideInInspector] public float attackDamageMult = 0f;
    [HideInInspector] public float dashDamageMult = 0f;
    [HideInInspector] public float kickDamageMult = 0f;

    //energy gem
    [HideInInspector] public float hoverEnergyMod = 0f;
    [HideInInspector] public float dashEnergyMod = 0f;
    [HideInInspector] public float kickEnergyMod = 0f;

    //fire gem
    [HideInInspector] public float burnTimeSeconds = 0f;
    [HideInInspector] public float burnDamage = 0f;
    [HideInInspector] public float burnTickSeconds = 0f;
    [HideInInspector] public float burnImmediateDamage = 0f;
    [HideInInspector] public float firePoolRadius = 0f;

    //wind gem
    [HideInInspector] public float pushForce = 0f;

    //Ice gem
    [HideInInspector] public float slowPercent = 0f;
    [HideInInspector] public float slowDurationHover = 0f;
    [HideInInspector] public float slowDurationKick = 0f;
    [HideInInspector] public float icePoolRadius = 0f;

   public bool isPickedUp = false;

    [HideInInspector] public GameObject currentHoverPool;
    [HideInInspector] public GameObject currentDashBox;
    [HideInInspector] public GameObject groundPool;


    private Coroutine goCommitDie = null;
    private bool isDestructing;
    private int collidersLength = 0;
    private bool isPlayerOver = false;

    private void Awake()
    {
        //check which type we are
        switch (type)
        {
            case GemType.LIGHTNING:
                {
                    string filename = "/Data/Gem/LightningGem.txt";
                    StreamReader reader = new StreamReader(Application.streamingAssetsPath + filename);

                    soundSwitch = "electric";

                    while (!reader.EndOfStream)
                    {
                        reader.ReadLine();
                        float bonus;
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            kickStunTimeBonus = bonus;
                        }

                        reader.ReadLine();
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            contactStunTime = bonus;
                        }
                    }
                }
                break;

            case GemType.SPEED:
                {
                    string filename = "/Data/Gem/SpeedGem.txt";
                    StreamReader reader = new StreamReader(Application.streamingAssetsPath + filename);

                    while (!reader.EndOfStream)
                    {
                        reader.ReadLine();
                        float bonus;
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            hoverSpeedMult = bonus;
                        }

                        reader.ReadLine();
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            stunKickWindupMod = bonus;
                        }

                        reader.ReadLine();
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            dashSpeedMult = bonus;
                        }
                    }
                }
                break;

            case GemType.POWER:
                {
                    string filename = "/Data/Gem/PowerGem.txt";
                    StreamReader reader = new StreamReader(Application.streamingAssetsPath + filename);

                    while (!reader.EndOfStream)
                    {
                        reader.ReadLine();
                        float bonus;
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            attackDamageMult = bonus;
                        }

                        reader.ReadLine();
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            dashDamageMult = bonus;
                        }

                        reader.ReadLine();
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            kickDamageMult = bonus;
                        }
                    }
                }
                break;

            case GemType.ENERGY:
                {
                    string filename = "/Data/Gem/EnergyGem.txt";
                    StreamReader reader = new StreamReader(Application.streamingAssetsPath + filename);

                    while (!reader.EndOfStream)
                    {
                        reader.ReadLine();
                        float bonus;
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            hoverEnergyMod = bonus;
                        }

                        reader.ReadLine();
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            dashEnergyMod = bonus;
                        }

                        reader.ReadLine();
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            kickEnergyMod = bonus;
                        }
                    }
                }
                break;

            case GemType.FIRE:
                {
                    string filename = "/Data/Gem/FireGem.txt";
                    StreamReader reader = new StreamReader(Application.streamingAssetsPath + filename);

                    soundSwitch = "fire";

                    while (!reader.EndOfStream)
                    {
                        reader.ReadLine();
                        float bonus;
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            burnTimeSeconds = bonus;
                        }

                        reader.ReadLine();
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            burnDamage = bonus;
                        }

                        reader.ReadLine();
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            burnTickSeconds = bonus;
                        }

                        reader.ReadLine();
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            burnImmediateDamage = bonus;
                        }

                        reader.ReadLine();
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            firePoolRadius = bonus;
                        }
                    }
                }
                break;

            case GemType.WIND:
                {
                    string filename = "/Data/Gem/WindGem.txt";
                    StreamReader reader = new StreamReader(Application.streamingAssetsPath + filename);

                    soundSwitch = "wind";

                    while (!reader.EndOfStream)
                    {
                        reader.ReadLine();
                        float bonus;
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            pushForce = bonus;
                        }
                    }
                }
                break;

            case GemType.ICE:
                {
                    string filename = "/Data/Gem/IceGem.txt";
                    StreamReader reader = new StreamReader(Application.streamingAssetsPath + filename);

                    soundSwitch = "ice";

                    while (!reader.EndOfStream)
                    {
                        reader.ReadLine();
                        float bonus;
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            slowPercent = bonus;
                        }

                        reader.ReadLine();
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            slowDurationHover = bonus;
                        }

                        reader.ReadLine();
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            slowDurationKick = bonus;
                        }

                        reader.ReadLine();
                        if (!float.TryParse(reader.ReadLine(), out bonus))
                        {
                            Utility.Log("Not a valid windup value");
                        }
                        else
                        {
                            icePoolRadius = bonus;
                        }
                    }
                }
                break;
        }
        isDestructing = false;
    }

    public virtual void Update()
    {
        /*if (movementScript == null && !isDestructing)
        {
            goCommitDie = StartCoroutine(SelfDestruct());
        }
        else if (movementScript != null && isDestructing)
        {
            StopCoroutine(goCommitDie);
            isDestructing = false;
        }*/
    }

    private void FixedUpdate()
    {
        if (!isPickedUp)
        {
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, GetComponent<SphereCollider>().radius, layerMask);
            foreach (Collider other in hitColliders)
            {
                if (other.tag == "Player")
                {
                    other.transform.Find("ScreenSpaceCanvas").Find("Background").Find("GemDescription").GetComponent<Text>().text = description;
                    isPlayerOver = true;
                }
            }
            collidersLength = hitColliders.Length;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            if (collidersLength <= 0 || isPickedUp)
            {
                other.transform.Find("ScreenSpaceCanvas").Find("Background").Find("GemDescription").GetComponent<Text>().text = "";
                isPlayerOver = false;
            }
        }
    }

    public virtual void OnHover()
    {
        Debug.LogWarning("Virtual function called!");
    }

    public virtual void OnDash()
    {
        Debug.LogWarning("Virtual function called!");
    }

    public virtual void OnKick(GameObject kicked)
    {
        Debug.LogWarning("Virtual function called!");
    }

    public virtual void OnDamageTaken(GameObject attacker)
    {
        Debug.LogWarning("Virtual function called!");
    }

    public virtual void OnCollision(GameObject collidedWith)
    {

        Debug.LogWarning("Virtual function called!");
    }

    /*private IEnumerator SelfDestruct()
    {
        isDestructing = true;
        yield return new WaitForSecondsRealtime(decayTime);
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, GetComponent<SphereCollider>().radius, layerMask);

        foreach (Collider other in hitColliders)
        {
            if (other.tag == "Player")
            {
                other.transform.Find("ScreenSpaceCanvas").Find("Background").Find("GemDescription").GetComponent<Text>().text = "";
                isPlayerOver = false;
            }
        }
        Destroy(gameObject);
    }*/
}
