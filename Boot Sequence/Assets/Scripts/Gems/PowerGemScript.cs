﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerGemScript : GemScript
{
    void Start()
    {

    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();

    }

    public override void OnHover()
    {

    }

    public override void OnDash()
    {

    }

    public override void OnKick(GameObject kicked)
    {
        //stun kicked
    }

    public override void OnDamageTaken(GameObject attacker)
    {

    }

    public override void OnCollision(GameObject collidedWith)
    {
        if (movementScript.isDashing)
        {
            if (collidedWith.tag == "EnemyHitbox")
            {
                collidedWith.GetComponentInParent<EnemyScript>().TakeDamage(1 * dashDamageMult);
            }
        }
    }
}
