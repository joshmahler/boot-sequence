﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(MovementScript))]
[RequireComponent(typeof(HeroScript))]
public class PlayerGemScript : MonoBehaviour
{
    public float gemGrabRadius = 5;

    public GameObject shockGemImage;
    public GameObject fireGemImage;
    public GameObject iceGemImage;
    public GameObject windGemImage;

    public GameObject shockGemCircuit;
    public GameObject fireGemCircuit;
    public GameObject iceGemCircuit;
    public GameObject windGemCircuit;

    public GameObject fireKickParticle;
    public GameObject iceKickParticle;
    public GameObject shockKickParticle;
    public GameObject windKickParticle;
    [HideInInspector] public GameObject origKickParticle;

    [HideInInspector] public float stunKickTimeBonus = 0;

    [HideInInspector] public float stunKickWindupMod = 0;
    [HideInInspector] public float dashSpeedMod = 1;
    [HideInInspector] private float hoverSpeedMult = 1;

    [HideInInspector] public float attackDamageMult = 1f;
    [HideInInspector] public float dashDamageMult = 1f;
    [HideInInspector] public float kickDamageMult = 1f;

    [HideInInspector] public float hoverEnergyMod = 0f;
    [HideInInspector] public float dashEnergyMod = 0f;
    [HideInInspector] public float kickEnergyMod = 0f;

    [HideInInspector] public float burnDamage = 0f;
    [HideInInspector] public float burnTime = 0f;
    [HideInInspector] public float burnTickTime = 0f;

    [HideInInspector] public float frostSlow = 0f;
    [HideInInspector] public float frostDuration = 0f;

 


    [HideInInspector] public GemScript gem;

    private MovementScript movement;
    private FadeUIScript background;
    [HideInInspector] public Transform gemHolder;
    [HideInInspector] public Transform circuitHolder;
    private HeroScript hero;
    private List<GameObject> gemsToDestroy;

    private float oldHoverSpeed = 0.0f;


    // Start is called before the first frame update
    void Start()
    {
        //gem script GetComponent
        movement = GetComponent<MovementScript>();
        hero = GetComponent<HeroScript>();
        origKickParticle = hero.kickParticle;
        dashSpeedMod = 1;
        gem = null;
        background = transform.Find("ScreenSpaceCanvas").Find("Background").GetComponent<FadeUIScript>();
        gemHolder = background.transform.Find("GemHolder");
        circuitHolder = background.transform.Find("CircuitHolder");
        gemsToDestroy = new List<GameObject>();

        SceneManager.activeSceneChanged += SceneChanged;
    }

    // Update is called once per frame
    void Update()
    {
        //!gem.isPickedUp &&
        if ( InputManager.input.PickUpButton(GetComponent<HeroScript>().id))
        {
            //get an array of all the existing gems
            GemScript[] gemList = GameObject.FindObjectsOfType<GemScript>();

            //for each gem in the list

            float minDist = 10000;
            GemScript newGem = null;
            foreach (GemScript g in gemList)
            {
                //if its not the one we already have equipped
                if (g != gem)
                { 
                //get the distance to it
                float dist = Vector3.Distance(transform.position, g.transform.position);
                    //if that gem is within the grab radius
                    if (dist < gemGrabRadius)
                    {
                        //and if its the closest gem we've found so far
                        if (dist < minDist)
                        {
                            //set it as our current gem and update minDist
                            newGem = g;
                            minDist = dist;
                            if (newGem.soundSwitch != null)
                            {
                                AkSoundEngine.SetSwitch("element", newGem.soundSwitch, gameObject);
                            }
                            AkSoundEngine.PostEvent(2464846003, gameObject); //gem swap sound
                        }
                    }
                }
            }

            if (gem != null)
            {
                //reset the gem
                gem.movementScript = null;
                gem.transform.parent = null;
                gem.isPickedUp = false;
                gem.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                Destroy(gem.currentHoverPool);
                Destroy(gem.currentDashBox);
                gemsToDestroy.Add(gem.gameObject);
                hero.kickParticle = origKickParticle;

                if (gemHolder.childCount > 0)
                {
                    for (int i = 0; i < gemHolder.childCount; ++i)
                    {
                        //maybe?
                        background.images.Remove(gemHolder.GetChild(i).GetComponent<Image>());
                        background.images.Remove(circuitHolder.GetChild(i).GetComponent<Image>());

                        Destroy(gemHolder.GetChild(i).gameObject);
                        Destroy(circuitHolder.GetChild(i).gameObject);
                    }
                }

                gem = null;
                //reset the values from the gem to their defaults
                ResetVals();
            }
            if (newGem != null)
            {
                //set the gem and the gem's movementscript
                gem = newGem;
                gem.movementScript = movement;
                //log the old hover speed
                oldHoverSpeed = gem.movementScript.hoverSpeed;

                //dummy code to show we're holding it
                gem.transform.position = transform.position;
                gem.transform.parent = transform;
                gem.isPickedUp = true;
                gem.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;

                if (gemHolder.childCount > 0)
                {
                    for(int i = 0; i < gemHolder.childCount; ++i)
                    {
                        //maybe?
                        background.images.Remove(gemHolder.GetChild(i).GetComponent<Image>());
                        background.images.Remove(circuitHolder.GetChild(i).GetComponent<Image>());

                        Destroy(gemHolder.GetChild(i).gameObject);
                        Destroy(circuitHolder.GetChild(i).gameObject);
                    }
                }
                switch(gem.type)
                {
                    case GemScript.GemType.LIGHTNING:
                        {
                            GameObject gemUI = Instantiate(shockGemImage, Vector3.zero, Quaternion.identity, gemHolder);
                            gemUI.transform.localPosition = Vector3.zero;
                            gemUI.transform.localRotation = Quaternion.identity;
                            background.images.Add(gemUI.GetComponent<Image>());

                            GameObject circuitUI = Instantiate(shockGemCircuit, Vector3.zero, Quaternion.identity, circuitHolder);
                            circuitUI.transform.localPosition = Vector3.zero;
                            circuitUI.transform.localRotation = Quaternion.identity;
                            background.images.Add(circuitUI.GetComponent<Image>());

                            hero.kickParticle = shockKickParticle;
                        }
                        break;
                    case GemScript.GemType.FIRE:
                        {
                            GameObject gemUI = Instantiate(fireGemImage, Vector3.zero, Quaternion.identity, gemHolder);
                            gemUI.transform.localPosition = Vector3.zero;
                            gemUI.transform.localRotation = Quaternion.identity;
                            background.images.Add(gemUI.GetComponent<Image>());

                            GameObject circuitUI = Instantiate(fireGemCircuit, Vector3.zero, Quaternion.identity, circuitHolder);
                            circuitUI.transform.localPosition = Vector3.zero;
                            circuitUI.transform.localRotation = Quaternion.identity;
                            background.images.Add(circuitUI.GetComponent<Image>());

                            hero.kickParticle = fireKickParticle;
                        }
                        break;
                    case GemScript.GemType.ICE:
                        {
                            GameObject gemUI = Instantiate(iceGemImage, Vector3.zero, Quaternion.identity, gemHolder);
                            gemUI.transform.localPosition = Vector3.zero;
                            gemUI.transform.localRotation = Quaternion.identity;
                            background.images.Add(gemUI.GetComponent<Image>());

                            GameObject circuitUI = Instantiate(iceGemCircuit, Vector3.zero, Quaternion.identity, circuitHolder);
                            circuitUI.transform.localPosition = Vector3.zero;
                            circuitUI.transform.localRotation = Quaternion.identity;
                            background.images.Add(circuitUI.GetComponent<Image>());

                            hero.kickParticle = iceKickParticle;
                        }
                        break;
                    case GemScript.GemType.WIND:
                        {
                            GameObject gemUI = Instantiate(windGemImage, gemHolder);
                            background.images.Add(gemUI.GetComponent<Image>());

                            GameObject circuitUI = Instantiate(windGemCircuit, Vector3.zero, Quaternion.identity, circuitHolder);
                            circuitUI.transform.localPosition = Vector3.zero;
                            circuitUI.transform.localRotation = Quaternion.identity;
                            background.images.Add(circuitUI.GetComponent<Image>());

                            hero.kickParticle = windKickParticle;
                        }
                        break;
                }

                //get the values from the gem
                SetVals();
            }
        }
    }

    private void SceneChanged(Scene current, Scene next)
    {
        for(int i = 0; i < gemsToDestroy.Count; ++i)
        {
            if (gemsToDestroy[i] && !gemsToDestroy[i].GetComponent<GemScript>().isPickedUp)
            {
                Destroy(gemsToDestroy[i]);
            }
        }
        gemsToDestroy.Clear();
    }

    public void OnDash()
    {
        if (gem != null)
        {
            gem.OnDash();
        }
    }

    
    public void OnHover()
    {
        if (gem != null)
        {
            gem.OnHover();
        }
    }

    public void OnKick(GameObject kicked)
    {
        if (gem != null)
        {
            gem.OnKick(kicked);
        }
    }

    public void OnDamageTaken(GameObject attacker)
    {
        if (gem != null)
        {
            gem.OnDamageTaken(attacker);
        }
    }

    public void OnCollission(GameObject collidedWith)
    {
        if (gem != null)
        {
            gem.OnCollision(collidedWith);
        }
    }

    private void SetVals()
    {
        burnDamage = gem.burnDamage;
        burnTime = gem.burnTimeSeconds;
        burnTickTime = gem.burnTickSeconds;

        frostDuration = gem.slowDurationHover;
        frostSlow = gem.slowPercent;

        stunKickTimeBonus = gem.kickStunTimeBonus;
        kickEnergyMod = gem.kickEnergyMod;
        dashEnergyMod = gem.dashEnergyMod;
        hoverEnergyMod = gem.hoverEnergyMod;
  
        

        stunKickWindupMod = gem.stunKickWindupMod;

        if (gem.dashSpeedMult != 0)
        {
            dashSpeedMod = gem.dashSpeedMult;
        }

        if (gem.hoverSpeedMult != 0)
        {
            gem.movementScript.hoverSpeed *= gem.hoverSpeedMult;
        }

        if (gem.attackDamageMult != 0)
        {
            attackDamageMult = gem.attackDamageMult;
        }

        if (gem.kickDamageMult != 0)
        {
            kickDamageMult = gem.kickDamageMult;
        }

        if (gem.dashDamageMult != 0)
        {
            dashDamageMult = gem.dashDamageMult;
        }
    }

    private void ResetVals()
    {
        burnDamage = 0;
        burnTickTime = 0;
        burnTime = 0;

        frostSlow = 0;
        frostDuration = 0;

        stunKickTimeBonus = 0;
        movement.hoverSpeed = oldHoverSpeed;
        stunKickWindupMod = 0;
        dashSpeedMod = 1;

        kickDamageMult = 1;
        dashDamageMult = 1;
        attackDamageMult = 1;
        kickEnergyMod = 0;
        hoverEnergyMod = 0;
        dashEnergyMod = 0;
    }
}
