﻿using UnityEngine;

public class ShockGemScript : GemScript
{
    public GameObject shockPool;
    public GameObject shockBox;

    void Start()
    {
        
    }

    public override void Update()
    {
        base.Update();
        if (movementScript != null)
        {
            if (!movementScript.isHovering)
            {
                if (currentHoverPool != null)
                {
                    Destroy(currentHoverPool);
                }
            }

            if (!movementScript.isDashing)
            {
                if (currentDashBox != null)
                {
                    Destroy(currentDashBox);
                }
            }
        }
    }

    public override void OnHover()
    {
        if (currentHoverPool == null)
        {
            currentHoverPool = Instantiate(shockPool, transform.parent);
            currentHoverPool.transform.localPosition = new Vector3(0, -1f, 0);
            //currentHoverPool.transform.localScale = new Vector3(firePoolRadius*0.75, 1.5f, firePoolRadius*0.75);
            currentHoverPool.GetComponent<PoolScript>().parent = gameObject;
        }
    }

    public override void OnDash()
    {
        if (currentDashBox == null)
        {
            currentDashBox = Instantiate(shockBox, transform.parent);
            currentDashBox.transform.localPosition = new Vector3(0, -1f, 0);
            //currentHoverPool.transform.localScale = new Vector3(firePoolRadius*0.75, 1.5f, firePoolRadius*0.75);
            currentDashBox.GetComponent<PoolScript>().parent = gameObject;
        }
    }

    public override void OnKick(GameObject kicked)
    {
        //stun kicked
    }

    public override void OnDamageTaken(GameObject attacker)
    {
        if (base.movementScript.isHovering)
        {
            Entity et = attacker.GetComponent<Entity>();
            
            et.StartCoroutine(et.Stun(contactStunTime));
        }
    }

    public override void OnCollision(GameObject collidedWith)
    {
        if (base.movementScript.isDashing)
        {
            if (collidedWith.tag == "Enemy")
            {
                Entity et = collidedWith.GetComponent<Entity>();
                et.StartCoroutine(et.Stun(contactStunTime));
            }
        }
    }
}
