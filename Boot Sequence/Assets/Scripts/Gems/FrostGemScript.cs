﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrostGemScript : GemScript
{
    public GameObject frostPool;
    public GameObject frostDrop;
    public GameObject frostBox;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (movementScript != null)
        {
            if (!movementScript.isHovering)
            {
                if (currentHoverPool != null)
                {
                    Destroy(currentHoverPool);
                }
            }

            if (!movementScript.isDashing)
            {
                if (currentDashBox != null)
                {
                    Destroy(currentDashBox);
                }
            }
        }
    }

    public override void OnHover()
    {
        if (currentHoverPool == null)
        {
            currentHoverPool = Instantiate(frostPool, transform.parent);
            currentHoverPool.transform.localPosition = new Vector3(0, -1f, 0);
            currentHoverPool.transform.localScale = new Vector3(icePoolRadius * 2, 1.5f, icePoolRadius * 2);
            currentHoverPool.GetComponent<PoolScript>().parent = gameObject;
        }
    }

    public override void OnDash()
    {
        Utility.Log(icePoolRadius);
        groundPool = Instantiate(frostDrop, new Vector3(transform.position.x, -0.5f, transform.position.z), transform.rotation);
        groundPool.transform.localScale = new Vector3(icePoolRadius * 2, 0.5f, icePoolRadius * 2);
        PoolScript fp = groundPool.GetComponent<PoolScript>();
        fp.parent = gameObject;
        fp.StartCoroutine(fp.SelfDestruct(5.0f));

        if (currentDashBox == null)
        {
            currentDashBox = Instantiate(frostBox, transform.parent);
            currentDashBox.transform.localPosition = new Vector3(0, -1f, 0);
            //currentHoverPool.transform.localScale = new Vector3(firePoolRadius*0.75, 1.5f, firePoolRadius*0.75);
            currentDashBox.GetComponent<PoolScript>().parent = gameObject;
        }
    }

    public override void OnKick(GameObject kicked)
    {
        GroundSteeringScript gs = kicked.GetComponent<GroundSteeringScript>();
        gs.StartCoroutine(gs.GetSlowed(slowPercent, slowDurationKick));
    }

    public override void OnDamageTaken(GameObject attacker)
    {

    }

    public override void OnCollision(GameObject collidedWith)
    {

    }
}
