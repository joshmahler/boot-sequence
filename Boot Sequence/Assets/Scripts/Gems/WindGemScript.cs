﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindGemScript : GemScript
{

    public GameObject shockPool;
    public GameObject windBox;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (movementScript != null)
        {
            if (!movementScript.isHovering)
            {
                if (currentHoverPool != null)
                {
                    Destroy(currentHoverPool);
                }
            }

            if (!movementScript.isDashing)
            {
                if (currentDashBox != null)
                {
                    Destroy(currentDashBox);
                }
            }
        }
    }

    public override void OnHover()
    {
        if (currentHoverPool == null)
        {
            currentHoverPool = Instantiate(shockPool, transform.parent);
            currentHoverPool.transform.localPosition = new Vector3(0, -1f, 0);
            //currentHoverPool.transform.localScale = new Vector3(firePoolRadius*0.75, 1.5f, firePoolRadius*0.75);
            currentHoverPool.GetComponent<PoolScript>().parent = gameObject;
        }
    }

    public override void OnDash()
    {
        if (currentDashBox == null)
        {
            currentDashBox = Instantiate(windBox, transform.parent);
            currentDashBox.transform.localPosition = new Vector3(0, -1f, 0);
            //currentHoverPool.transform.localScale = new Vector3(firePoolRadius*0.75, 1.5f, firePoolRadius*0.75);
            currentDashBox.GetComponent<PoolScript>().parent = gameObject;
        }
    }

    public override void OnKick(GameObject kicked)
    {
        Rigidbody rb = kicked.GetComponent<Rigidbody>();
        Vector3 pushDir = kicked.transform.position - transform.position;
        rb.velocity += pushDir * pushForce;
    }

    public override void OnDamageTaken(GameObject attacker)
    {
        if (movementScript.isHovering)
        {
            Rigidbody rb = attacker.GetComponent<Rigidbody>();
            Vector3 pushDir = attacker.transform.position - transform.position;
            rb.velocity += pushDir * pushForce;
        }
        
    }

    public override void OnCollision(GameObject collidedWith)
    { 
        if (movementScript.isDashing && collidedWith.tag == "Enemy")
        {
            Rigidbody rb = collidedWith.GetComponent<Rigidbody>();
            Vector3 pushDir = collidedWith.transform.position - transform.position;
            rb.velocity += pushDir * pushForce;
        }
    }
}
