﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireGemScript : GemScript
{
    public GameObject firePool;
    public GameObject fireDrop;
    public GameObject fireBox;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (movementScript != null)
        {
            if (!movementScript.isHovering)
            {
                if (currentHoverPool != null)
                {
                    Destroy(currentHoverPool);
                }
            }

            if (!movementScript.isDashing)
            {
                if (currentDashBox != null)
                {
                    Destroy(currentDashBox);
                }
            }
        }
    }

    public override void OnHover()
    {
        if (currentHoverPool == null)
        {
            currentHoverPool = Instantiate(firePool, transform.parent);
            currentHoverPool.transform.localPosition = new Vector3(0, -1f, 0);
            currentHoverPool.transform.localScale = new Vector3(firePoolRadius*2, 1.5f, firePoolRadius*2);
            currentHoverPool.GetComponent<PoolScript>().parent = gameObject;
        }
    }

    public override void OnDash()
    {
        groundPool = Instantiate(fireDrop, new Vector3(transform.position.x, -0.5f, transform.position.z), transform.rotation);
        groundPool.transform.localScale = new Vector3(firePoolRadius*2, 0.5f, firePoolRadius*2);
        PoolScript fp = groundPool.GetComponent<PoolScript>();
        fp.parent = gameObject;
        fp.StartCoroutine(fp.SelfDestruct(5.0f));

        if (currentDashBox == null)
        {
            currentDashBox = Instantiate(fireBox, transform.parent);
            currentDashBox.transform.localPosition = new Vector3(0, -1f, 0);
            //currentHoverPool.transform.localScale = new Vector3(firePoolRadius*0.75, 1.5f, firePoolRadius*0.75);
            currentDashBox.GetComponent<PoolScript>().parent = gameObject;
        }
    }

    public override void OnKick(GameObject kicked)
    {
        EnemyScript es = kicked.GetComponent<EnemyScript>();
        es.TakeDamage(burnImmediateDamage);
        es.burn(burnTimeSeconds * 60, burnDamage, burnTickSeconds * 60);
    }

    public override void OnDamageTaken(GameObject attacker)
    {

    }

    public override void OnCollision(GameObject collidedWith)
    {

    }
}