﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public struct Node
{
    public List<Node> parent;
    public List<Node> children;
    public Vector2 position;
    public float cost;

}

public class RRTPathfinderScript : MonoBehaviour
{
    public float minDist = 0.2f;
    public float maxDist = 0.5f;
    public float distance = 50f;
    public float xRad = 10;
    public float yRad = 10;
    public float neighborhoodRad = 4.0f;
    public int numOptomizationLoops = 30;

    private bool endFound;
    private float unitPosY;
    private Vector2 currentEndPos;
    private Vector2 center;
    private Vector3 currentEndPos3D;
    private Vector3 startPos;
    private List<Node> nodeList;
    private List<Node> path;
    private Node endNode;
    public LayerMask obstacleMask;

    float checkFor = 0.0175f;
    // Use this for initialization
    void Start()
    {
        //obstacleMask = LayerMask.GetMask("PathObstacle");
    }

    // Update is called once per frame
    void Update()
    {
      
    }

    public List<Node> Pathfind(Vector3 start, Vector3 end, Vector3 unitPos)
    {
        
        
        //set up some variables
        float tempXRad = xRad;
        float tempYRad = yRad;
        unitPosY = unitPos.y;
        center = new Vector2(unitPos.x, unitPos.z);
        currentEndPos = new Vector2(end.x, end.z);
        currentEndPos3D = new Vector3(end.x, unitPosY, end.z);
        endNode = createNode(currentEndPos, 0);
        startPos = start;
        nodeList = new List<Node>();
        path = new List<Node>();
        //add the player's starting position to the path
        Node startNode = createNode(new Vector2(start.x, start.z), 0f);
        nodeList.Add(startNode);
        //set the first node's parent to itself
        nodeList[0].parent.Add(nodeList[0]);

        endFound = false;

        //check if the end is inside an object. it is just try a straight line. 
        if (!EndIsValid())
        {
            path.Add(startNode);
            path.Add(endNode);
            return path;
        }
        //adds a raycast to runtime, but likely significantly cuts down on average runtime.
        if (CanReachEnd(startNode))
        {
            path.Add(startNode);
            path.Add(endNode);
            return path;
        }

        while (!endFound)
        {

            if (nodeList.Count > 100)
            {
                path.Add(startNode);
                path.Add(endNode);
                Utility.Log("ur fucked lmao");
                return path;
            }
            //create a temp node
            Node toNode = createNode(new Vector2(start.x, start.z), -1);
            //loop until we have a valid node form explore()
            do
            {
                toNode = Explore();

            } while (toNode.cost == -1);
            //once we do, add it to the list
            nodeList.Add(toNode);

            //check if we can reach the end
            if (CanReachEnd(toNode))
            {
                //tell the loop to stop
                endFound = true;
                //set the end nodes values
                endNode.cost = toNode.cost + Vector2.Distance(endNode.position, toNode.position);
                //adopt it 
                endNode.parent.Add(toNode);
                toNode.children.Add(endNode);
                //add it to the lists
                nodeList.Add(endNode);
            }

        }

        ////loop through n more times to optimize path
        for (int i = 0; i < numOptomizationLoops; i++)
        {
            //create a temp node
            Node toNode = createNode(new Vector2(0, 0), -1);
            //expore until it has a valid value
            do
            {
                toNode = Explore();

            } while (toNode.cost == -1);
            //add it to the list
            nodeList.Add(toNode);
            //check if we can reach the end
            if (CanReachEnd(toNode))
            {
                //if ew can, calculate the new cost to the end node
                float newCost = toNode.cost + Vector2.Distance(endNode.position, toNode.position);
                //if its less than the old cost to the end node
                if (newCost < endNode.cost)
                {
                    ///orphan the end node
                    foreach (Node n in endNode.parent[0].children)
                    {
                        if (n.position == endNode.position)
                        {
                            endNode.parent.Remove(n);
                        }
                    }
                    //continue to orphan the end node
                    endNode.parent.Clear();
                    //adopt it in accordance with the new shorter path
                    endNode.parent.Add(toNode);
                    toNode.children.Add(endNode);
                    //set its cost to the new cost
                    endNode.cost = newCost;
                }

            }
            //generate the current path
            MakePath(endNode);
            //find the average position of points and set that as the center of the oval
            center = FindAvg();
            //find the maximum distances from the center of points on the path
            Vector2 rad = FindMax();
            //and set the x and y diameters the oval
            xRad = rad.x;
            yRad = rad.y;

        }
        //uncomment below to draw tree in editor
        //VisualizeTree(nodeList);

        //reset radii to starting values
        xRad = tempXRad;
        yRad = tempYRad;
        MakePath(endNode);
        return path;
    }

    Node createNode(Vector2 pos, float cost)
    {
        //set a bunch of defaults
        Node n = new Node();
        n.position = pos;
        n.cost = cost;
        n.parent = new List<Node>();
        n.children = new List<Node>();

        return n;
    }

    Node Explore()
    {
        //generate a random positions until one is not inside an object
        Vector3 randPos = startPos;
        bool hit = true;
        int i = 0;
        
        while (hit == true)
        {
            ++i;
            randPos = new Vector3(Random.Range(center.x - xRad, center.x + xRad), unitPosY, Random.Range(center.y - yRad, center.y + yRad));
            RaycastHit hitThing = new RaycastHit();
            hit = Physics.Linecast(new Vector3(randPos.x, -10, randPos.z), randPos, out hitThing, obstacleMask);


            if (i > 1000)
            {
                
                Utility.Log("oof");
                break;
            }
        }
       
        Vector2 randPos2D = new Vector2(randPos.x, randPos.z);

        //find the lowest cost
        Node parent = FindLowestCost(randPos2D);
        Vector3 parentPos3D = new Vector3(parent.position.x, unitPosY, parent.position.y);
        float dist = Vector2.Distance(randPos2D, parent.position);

        //linecast to the nearest node
        hit = Physics.Linecast(randPos, parentPos3D, obstacleMask);

        //create the node to return and set its defaults
        Node created = createNode(new Vector2(1, 1), -1);

        //if we didn't hit anything that means we have a clear path to it
        if (!hit)
        {
            //calculate the cost to the end
            created.cost = parent.cost + dist;
            //set parents and children
            created.parent.Add(parent);
            //security check
            if (parent.children == null)
            {
                parent.children = new List<Node>();
            }
            parent.children.Add(created);
            created.position = randPos2D;
        }


        return created;

    }
    bool EndIsValid()
    {
        Vector3 pos = new Vector3(currentEndPos3D.x, unitPosY + 5, currentEndPos3D.z);
        //linecst from pos to the endpos world position
        bool hit = Physics.Linecast(pos, currentEndPos3D, obstacleMask);
        //if we hit anything along the way
        if (hit)
        {
            //return false
            return false;
        }
        //otherwise, return true
        return true;
    }

    bool CanReachEnd(Node checkFrom)
    {
        //convert the node position into world position
        Vector3 pos = new Vector3(checkFrom.position.x, unitPosY, checkFrom.position.y);
        //linecst from there to the endpos world position
        bool hit = Physics.Linecast(pos, currentEndPos3D, obstacleMask);
        //if we didn't hit anything along the way
        if (!hit)
        {
            //return true
            return true;
        }
        //otherwise, return false
        return false;
    }

    Node FindLowestCost(Vector2 pos)
    {
        //get the start node
        Node lowest = nodeList[0];
        float cost = 1000;
        //set cost to a default value
        //loop over the node list
        foreach (Node n in nodeList)
        {
            //calculate the distance between them
            float dist = Vector2.Distance(pos, n.position);
            //if its lower than the neighborhood range
            if (dist < neighborhoodRad)
            {
                //calculate the cost to it from n
                float tempCost = n.cost + dist;
                //if that cost is lower than the old lowest
                if (tempCost < cost)
                {
                    //it's the new lowests
                    lowest = n;
                    cost = lowest.cost;
                }
            }
        }
        return lowest;

    }

    void VisualizeTree(List<Node> tree)
    {
        //loop over the passed tree
        foreach (Node n in tree)
        {
            //if n isn't the root
            if (n.parent[0].position != n.position)
            {
                //calculate the world positions of n and it's parent
                Vector3 startPos = new Vector3(n.position.x, unitPosY, n.position.y);
                Vector3 endPos = new Vector3(n.parent[0].position.x, unitPosY, n.parent[0].position.y);
                //if n is in the path to the end
                if (IsInPath(n))
                {
                    //draw a blue line
                    Debug.DrawLine(startPos, endPos, Color.blue, 5);
                }
                else
                {
                    //otherwise draw a red line
                    Debug.DrawLine(startPos, endPos, Color.red, 5);
                }

            }
        }
    }

    void MakePath(Node check)
    {
        path.Clear();
        //if it's not the root node
        if (check.position != nodeList[0].position)
        {
            //recur
            MakePath(check.parent[0]);
        }
        //once the stack resolves, add it to the list
        path.Add(check);

    }

    bool IsInPath(Node check)
    {
        //loop over the path
        foreach (Node n in path)
        {
            //if check is n
            if (n.position == check.position)
            {
                //check is in the path
                return true;
            }
        }
        //if we haven't found it, then return false
        return false;
    }

    Vector2 FindAvg()
    {
        //set defaults
        Vector2 avg = new Vector2();
        float x = 0;
        float y = 0;
        int count = 0;
        //loop over path
        foreach (Node n in path)
        {
            //sum x and y positions, keep track of how many we've added
            x += n.position.x;
            y += n.position.y;
            count++;
        }
        //take averages
        avg.x = x / count;
        avg.y = y / count;
        return avg;
    }

    Vector2 FindMax()
    {
        //set a default
        Vector2 max = new Vector2(0, 0);
        ///loop over the path
        foreach (Node n in path)
        {
            //if the difference between n's x and the center's x is greater than the old max
            if (Mathf.Abs(n.position.x - center.x) > max.x)
            {
                //n's x is the new max x
                max.x = Mathf.Abs(n.position.x - center.x);
            }
            //if the difference between n's y and the center's y is greater than the old max
            if (Mathf.Abs(n.position.y - center.y) > max.y)
            {
                //n's y is the new max y
                max.y = Mathf.Abs(n.position.y - center.y);
            }
        }

        return max;
    }
}
