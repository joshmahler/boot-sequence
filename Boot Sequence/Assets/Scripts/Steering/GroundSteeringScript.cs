﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class GroundSteeringScript : MonoBehaviour
{
    public float maxSpeed;
    public float maxAccel;

    private List<Node> path = new List<Node>();
    private Rigidbody rb;
    private int currentIndex;
    public float hoverHeightInterval;
    public float maxHoverHeight;
    [HideInInspector] public bool shouldRotate = true;
    [HideInInspector] public bool isHovering;
    private int pathResetMin = 45;
    private int pathResetCount;
    private void Start()
    {
        pathResetCount = 0;
        rb = GetComponent<Rigidbody>();
    }

    public void SetPath(List<Node> nodes)
    {

        if (nodes.Count != 0)
        {
            //only reset the path every 45 frames
            if (pathResetCount <= 0)
            {
                path.Clear();
                path = nodes;
                currentIndex = 0;
                pathResetCount = pathResetMin;
            }
            else if (path.Count != 0)
            {
               
                //otherwise just update the target position.
                path[path.Count-1] = nodes[nodes.Count - 1];
            }
        }
    }

    public void CrankSteering(Animator anim)
    {
        if (pathResetCount > 0)
        {
            pathResetCount--;
        }
        bool isGrounded = GetIsGrounded(anim);
        bool shouldMove = true;

        if (path.Count > 0)
        {
            Vector3 targetPos = new Vector3(path[currentIndex].position.x, 1f, path[currentIndex].position.y);
            Vector3 adjustedCurrentPos = new Vector3(transform.position.x, 1, transform.position.z);

            Vector3 targetVel = Vector3.zero;
            if (Vector3.Distance(adjustedCurrentPos, targetPos) < .5f)
            {

                //TODO: CHANGE THIS LATER

                if (currentIndex != path.Count-1)
                {
                    ++currentIndex;
                }
                shouldMove = false;
            }
            else
            {

            }
            if (shouldMove)
            {

                targetPos = new Vector3(path[currentIndex].position.x, 1f, path[currentIndex].position.y);


                Vector3 diff = targetPos - transform.position;
                diff.Normalize();
                diff *= maxSpeed;
                diff.y = rb.velocity.y;
                targetVel = diff;

                if (shouldRotate)
                {
                    //look at
                    transform.rotation = Quaternion.LookRotation(new Vector3(diff.x, 0f, diff.z));
                }
            }

            if (rb == null)
                rb = GetComponent<Rigidbody>();

            Vector3 velDiff = targetVel - rb.velocity;
            velDiff.Normalize();
            velDiff *= maxAccel;
            rb.velocity += velDiff;
   
            rb.angularVelocity = Vector3.zero;

            if (!isGrounded)
            {
                rb.useGravity = false;
                float yPos = transform.position.y;

                yPos = transform.position.y + hoverHeightInterval;
                if (yPos >= maxHoverHeight - hoverHeightInterval)
                {
                    yPos = maxHoverHeight;
                }


                transform.position = new Vector3(transform.position.x, yPos, transform.position.z);
                isHovering = true;
            }
            else
            {
                isHovering = false;
                rb.useGravity = true;
            }
        }
    }
    bool GetIsGrounded(Animator anim)
    {
        Physics.Raycast(transform.position, Vector3.down, out RaycastHit hit, 3f);
        if (hit.collider != null)
        {
            anim.SetBool("hovering", false);
            return true;
        }
        anim.SetBool("hovering", true);
        return false;
    }
    public IEnumerator GetSlowed(float slowMod, float duration)
    {
        float baseMS = maxSpeed;
        maxSpeed = maxSpeed * slowMod;
        yield return new WaitForSeconds(duration);
        maxSpeed = baseMS;
    }
}
