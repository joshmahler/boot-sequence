﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBoxScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerHitboxScript>())
        {
            Drop();
            
        }
       
    }

    public void Drop()
    {
        if (GetComponent<ItemDropScript>())
        {
            
            GetComponent<ItemDropScript>().drop();
            
            Destroy(gameObject);
        }
        else
        {
            if(!gameObject.GetComponent<Rigidbody>())
                gameObject.AddComponent<Rigidbody>();
            transform.parent = null;
            Rigidbody rb = GetComponent<Rigidbody>();
            AkSoundEngine.PostEvent("railing_break", gameObject);
            rb.AddExplosionForce(1f, transform.position, 1f);
            rb.useGravity = true;
            rb.mass = 0.001f;
            Destroy(gameObject, 1);
        }
        
    }
}
