﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider), typeof(Rigidbody))]
public class GeneratorScript : MonoBehaviour
{
    public bool Testing;

    public float healthPercentage;
    public float health = 100f;
    public float healthMaximum = 100f;
    public GameObject beamStart;
    public GameObject breakSmoke;
    public string generatorDestroyEvent;
    public List<GameObject> SmokeHolder;
    
    private LineRenderer exitBeam;
    private GameObject exitPortal;

    void Start()
    {
        health += GameControl.control.generatorHealth;
        exitPortal = GameObject.FindGameObjectWithTag("ExitDoor");
        healthMaximum = health;
        AkSoundEngine.PostEvent("generator_hum", gameObject);
        if(beamStart != null && Testing == false)
        {
            exitBeam = GetComponent<LineRenderer>();
            exitBeam.SetPosition(0, beamStart.transform.position);
            exitBeam.SetPosition(1, exitPortal.transform.position);  
        }
    }

    public void TakeDamage(float damage)
    {
        health -= damage;
        AkSoundEngine.PostEvent("generator_hit", gameObject);
        if (health <= 0f)
        {
            health = 0f;
            AkSoundEngine.PostEvent("generator_destroy", gameObject);
            GameControl.control.DestroyGenerator();
            GetComponent<ItemDropScript>().drop();
            
            Explode();
            Destroy(gameObject);
        }

        GameObject newBreakSmoke = Instantiate(breakSmoke, new Vector3(transform.position.x, transform.position.y, transform.position.z), (transform.rotation * Quaternion.Euler(0, Random.Range(0, 360), 0)), this.gameObject.transform) as GameObject;
        SmokeHolder.Add(newBreakSmoke);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (health > 0)
        {
            //cannot OR it because sounds
            if (other.tag == "PlayerHitbox")
            {
                TakeDamage(other.GetComponentInParent<HeroScript>().outgoingAttack.damage);
            }
            else if (other.tag == "KnightBasicHitbox")
            {
                TakeDamage(other.GetComponentInParent<HeroScript>().outgoingAttack.damage);
            }
            else if (other.tag == "KickHitbox")
            {
                TakeDamage(other.GetComponentInParent<HeroScript>().outgoingAttack.damage);
            }
            else if (other.tag == "MageProjectile")
            {
                TakeDamage(other.GetComponent<ProjectileScript>().dmg);
            }
        }

    }
    private void Explode()
    {
        GetComponent<Rigidbody>().AddExplosionForce(1f, transform.position, 0.1f);
        Transform meshParent = transform.GetChild(2);
        for (int i = 0; i < meshParent.childCount; ++i)
        {
            Transform current = meshParent.GetChild(i);
            current.gameObject.AddComponent<Rigidbody>();
            current.gameObject.GetComponent<Rigidbody>().mass = 0.001f;
            current.gameObject.AddComponent<BoxCollider>();
            current.gameObject.AddComponent<DestroyAfterOneSecondScript>();
            current.parent = null;
        }
    }
}
