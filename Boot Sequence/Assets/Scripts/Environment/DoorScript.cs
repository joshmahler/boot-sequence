﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    public GameObject beamPoints;
    public float ySpeed = 0.01f;

    private float currentY;
    private float curX, curZ;
    private float origY;
    private bool doTheDoor;

    private void Start()
    {
        currentY = transform.position.y;
        curX = transform.position.x;
        curZ = transform.position.z;
        origY = currentY;
    }

    private void Update()
    {
        if (GameControl.control.isAllGeneratorsDestroyed)
        {
            doTheDoor = true;
            GameControl.control.isAllGeneratorsDestroyed = false;
        }

        if (doTheDoor)
        {
            if (beamPoints != null)
            {
                Destroy(beamPoints);
            }
            float x = Random.Range(-0.1f, 0.1f);
            float z = Random.Range(-0.1f, 0.1f);
            currentY -= ySpeed;
            transform.position = new Vector3(curX + x, currentY,  curZ + z);
            if (origY - currentY >= transform.localScale.y / 2.0f)
            {
                Destroy(gameObject);
            }
        }
    }
}
