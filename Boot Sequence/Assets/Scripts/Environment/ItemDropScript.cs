﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDropScript : MonoBehaviour
{
    //GameControl.control.controllersAmount
    public bool dropsAny;
    public bool dropCenter;
    public float itemDropHeight;
    public enum dropType
    {
        Default,
        PlayerCount,
        SpawnRate
    }
    public dropType dropsystem;
    public GameObject defaultItemDrop;

    [Header("Player Number Decider")]
    public int onePlayerItemDropCount;
    public int twoPlayerItemDropCount;
    public int threePlayerItemDropCount;
    public int fourPlayerItemDropCount;
 
    [Header("Rate Decider")]
    public int nullChance;
    
    public int healthSpawnChance;
    public int healthSpawnCount;
    public GameObject healthItem;
    
    public int gemSpawnChance;
    public List<GameObject> gems;

    /*public int upgradeSpawnChance;
    public List<GameObject> upgradeItems;*/


    public void drop()
    {
        if (dropsAny == true)
        {
            if (dropsystem == dropType.Default)
            {
                DefaultDrop();
            }
            else if (dropsystem == dropType.PlayerCount)
            {
                PlayerCountDrop();
            }
            else if (dropsystem == dropType.SpawnRate)
            {
                SpawnRateDrop();
            }
            else
            {
                DefaultDrop();
            }
        }
    }

    void DefaultDrop()
    {
        if (defaultItemDrop)
        {
            Instantiate(defaultItemDrop, new Vector3 (transform.position.x, -0.5f, transform.position.z), transform.rotation);
        }
    }

    void PlayerCountDrop()
    {
        if (GameControl.control.controllersAmount <= 1)
        {
            Instantiate(defaultItemDrop, new Vector3(transform.position.x, -0.5f, transform.position.z), transform.rotation);
        }
        else if (GameControl.control.controllersAmount == 2)
        {
            if (defaultItemDrop != null)
            {
                Instantiate(defaultItemDrop, new Vector3(transform.position.x - 1f, -0.5f, transform.position.z), transform.rotation);
                Instantiate(defaultItemDrop, new Vector3(transform.position.x + 1f, -0.5f, transform.position.z), transform.rotation);
            }
        }
        else if (GameControl.control.controllersAmount == 3)
        {
            if (defaultItemDrop != null)
            {
                Instantiate(defaultItemDrop, new Vector3(transform.position.x, -0.5f, transform.position.z), transform.rotation);
                Instantiate(defaultItemDrop, new Vector3(transform.position.x, -0.5f, transform.position.z + 1f), transform.rotation);
                Instantiate(defaultItemDrop, new Vector3(transform.position.x - 1f, -0.5f, transform.position.z), transform.rotation);
            }
        }
        else if (GameControl.control.controllersAmount == 4)
        {
            if (defaultItemDrop != null)
            {
                Instantiate(defaultItemDrop, new Vector3(transform.position.x, -0.5f, transform.position.z), transform.rotation);
                Instantiate(defaultItemDrop, new Vector3(transform.position.x + 1f, -0.5f, transform.position.z + 1f), transform.rotation);
                Instantiate(defaultItemDrop, new Vector3(transform.position.x, -0.5f, transform.position.z - 1f), transform.rotation);
                Instantiate(defaultItemDrop, new Vector3(transform.position.x - 1f, -0.5f, transform.position.z), transform.rotation);
                
            }
        }
    }

    void SpawnRateDrop()
    {
        int result = Random.Range(1, (nullChance + healthSpawnChance + gemSpawnChance));

        if (result > nullChance && result <= nullChance + healthSpawnChance)
        {
            Instantiate(healthItem, new Vector3(transform.position.x, -0.5f, transform.position.z), transform.rotation);
        }
        else if (result > nullChance + healthSpawnChance && result <= nullChance + healthSpawnChance + gemSpawnChance)
        {
            int gemToSpawn = Random.Range(0, gems.Count);
            Instantiate(gems[gemToSpawn], new Vector3(transform.position.x, -0.5f, transform.position.z), transform.rotation);
        }
        /*else if (result > healthSpawnChance + gemSpawnChance && result <= healthSpawnChance + gemSpawnChance + upgradeSpawnChance)
        {
            int upgradeToSpawn = Random.Range(0, upgradeItems.Count);
            Instantiate(upgradeItems[upgradeToSpawn], transform.position, transform.rotation);
        }*/
    }
}


