﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPackScript : MonoBehaviour
{
    public float heal = 25f;

    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == ("Player"))
        {
            AkSoundEngine.PostEvent("HealthPickup", gameObject);
            other.gameObject.GetComponent<HeroScript>().TakeDamage(-heal);
            Destroy(gameObject);
        }
    }
}
