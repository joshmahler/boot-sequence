﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(GroundSteeringScript))]
public class BasicEnemyStateMachine : StateMachine
{
    public float chaseRadius;   // The radius which the enemy will start chasing the player.
    public float attackRadius;  // The radius which the enemy will start hitting the player.
    public GameObject hitbox;   // The hitbox object that will be instantiated each time an attack is played.

    private Vector3 targetPos = Vector3.zero;           // The position this object will move towards in chase state.
    private int targetIndex = -1;                       // The index of the player array this object will move towards.
    private GroundSteeringScript groundSteering;        // A reference to the script attached to this object.
    private BasicEnemyStateMachine enemyStateMachine;   // A reference to this object for the StateMachine.
    private RRTPathfinderScript pathfinder;             // A reference to the steering script.
    private Rigidbody rb;                               // A reference to the rigidbody.
    private State currentState;                         // The current state this object is in.

    //need to declare a "starting state" here, IdleState is a temporary thing.
    private IdleState idleState;            // The starting state of this object.
    private int countdown = 0;              // A counter to pathfind not every frame.

    /*
     * Must override Start to call the base class Start before calling AddSkill.
     * This makes sure the List exists.
     */
    public override void Start()
    {
        base.Start();
        enemyStateMachine = GetComponent<BasicEnemyStateMachine>();
        AddSkill(SkillPoolScript.Skill.SkillType.ENEMY_BASIC);
        AddSkill(SkillPoolScript.Skill.SkillType.ENEMY_BASIC_SPOT);
        pathfinder = GameObject.FindGameObjectWithTag("PathfindingManager").GetComponent<RRTPathfinderScript>();
        rb = GetComponent<Rigidbody>();
        groundSteering = GetComponent<GroundSteeringScript>();

        idleState = new IdleState();
        currentState = idleState;
        currentState.OnEnter(enemyStateMachine);
        health += GameControl.control.walkerHealth;
        SkillPoolScript.Skill skill = skills[0];
        skill.damage += GameControl.control.dmgToAdd;
        skills[0] = skill;
    }

    public override void Update()
    {
        base.Update();

        if (!isStunned)
            CheckState();
    }

    private void CheckState()
    {
        for (int i = 0; i < currentState.transitions.Count; ++i)
        {         
            if (currentState.transitions[i].CheckCondition(enemyStateMachine))
            {
                State temp = currentState;
                currentState = currentState.transitions[i].GetNextState();
                temp.OnExit();
                currentState.OnEnter(enemyStateMachine);
                break;
            }
        }

        currentState.OnUpdate();
    }

    /// <summary>
    /// The starting state of most enemies.
    /// </summary>
    public class IdleState : State
    {
        private BasicEnemyStateMachine sm;
        private MobTransition mobTransition;
        private ChaseTransition chaseTransition;

        /// <summary>
        /// Gets called once every time this object enters this state.
        /// </summary>
        /// <param name="stateMachine">The stateMachine object to reference the GameObject.</param>
        public override void OnEnter(StateMachine stateMachine)
        {
            sm = (BasicEnemyStateMachine)stateMachine;
            transitions = new List<Transition>();

            mobTransition = new MobTransition();
            chaseTransition = new ChaseTransition();

            mobTransition.SetNextState(new MobState());
            chaseTransition.SetNextState(new ChaseState());

            transitions.Add(mobTransition);
            transitions.Add(chaseTransition);
            sm.ResetEye();
        }

        /// <summary>
        /// Gets called every Update() when this object is in this state.
        /// </summary>
        public override void OnUpdate()
        {
        }

        /// <summary>
        /// Gets called once when this object is exitting this state.
        /// </summary>
        public override void OnExit()
        {
            transitions.Clear();
        }
    };

    public class ChaseState : State
    {
        private BasicEnemyStateMachine sm;
        private MobTransition mobTransition;
        private AttackTransition attackTransition;

        /// <summary>
        /// Gets called once every time this object enters this state.
        /// </summary>
        /// <param name="stateMachine">The stateMachine object to reference the GameObject.</param>
        public override void OnEnter(StateMachine stateMachine)
        {
            sm = (BasicEnemyStateMachine)stateMachine;
            transitions = new List<Transition>();

            mobTransition = new MobTransition();
            attackTransition = new AttackTransition();

            mobTransition.SetNextState(new MobState());
            attackTransition.SetNextState(new AttackState());

            transitions.Add(mobTransition);
            transitions.Add(attackTransition);

            List<Node> path = sm.pathfinder.Pathfind(sm.transform.position, sm.targetPos, sm.transform.position);
            sm.groundSteering.SetPath(path);

            sm.anim.SetBool("running", true);
        }

        /// <summary>
        /// Gets called every Update() when this object is in this state.
        /// </summary>
        public override void OnUpdate()
        {
            if (!sm.isSpot && ++sm.countdown >= 20)
            {
                sm.countdown = 0;
                sm.targetPos = GameControl.control.players[sm.targetIndex].transform.position;
                List<Node> path = sm.pathfinder.Pathfind(sm.transform.position, sm.targetPos, sm.transform.position);
                sm.groundSteering.SetPath(path);
            }
            sm.groundSteering.CrankSteering(sm.anim);
        }

        /// <summary>
        /// Gets called once when this object is exitting this state.
        /// </summary>
        public override void OnExit()
        {
            sm.anim.SetBool("running", false);
            transitions.Clear();
        }
    };

    public class AttackState : State
    {
        private BasicEnemyStateMachine sm;
        private MobTransition mobTransition;
        private AttackToIdleTransition attackToIdleTransition;

        /// <summary>
        /// Gets called once every time this object enters this state.
        /// </summary>
        /// <param name="stateMachine">The stateMachine object to reference the GameObject.</param>
        public override void OnEnter(StateMachine stateMachine)
        {
            sm = (BasicEnemyStateMachine)stateMachine;
            transitions = new List<Transition>();

            mobTransition = new MobTransition();
            attackToIdleTransition = new AttackToIdleTransition();

            mobTransition.SetNextState(new MobState());
            attackToIdleTransition.SetNextState(new IdleState());

            transitions.Add(mobTransition);
            transitions.Add(attackToIdleTransition);
        }

        /// <summary>
        /// Gets called every Update() when this object is in this state.
        /// </summary>
        public override void OnUpdate()
        {
            //face the enemy
            Vector3 diff = GameControl.control.players[sm.closestPlayerIndex].transform.position - sm.transform.position;
            sm.transform.rotation = Quaternion.LookRotation(new Vector3(diff.x, 0, diff.z));
            if (!sm.isSpot && !sm.isAttacking)
            {
                sm.StartCoroutine(sm.PlayingAttack());
                sm.outgoingAttack = sm.skills[0];
                sm.coroutine = sm.StartCoroutine(sm.Attack(sm.outgoingAttack.windupFrames, sm.outgoingAttack.activeFrames, sm.outgoingAttack.cooldownFrames,
                    sm.hitbox, sm.transform, null, null));
            }
        }

        /// <summary>
        /// Gets called once when this object is exitting this state.
        /// </summary>
        public override void OnExit()
        {
            sm.anim.SetBool("attacking", false);
            transitions.Clear();
        }
    };

    public class MobState : State
    {
        private BasicEnemyStateMachine sm;
        private AttackTransition attackTransition;
        private NoMoreMobTransition noMoreMobTransition;

        /// <summary>
        /// Gets called once every time this object enters this state.
        /// </summary>
        /// <param name="stateMachine">The stateMachine object to reference the GameObject.</param>
        public override void OnEnter(StateMachine stateMachine)
        {
            sm = (BasicEnemyStateMachine)stateMachine;
            transitions = new List<Transition>();

            attackTransition = new AttackTransition();
            noMoreMobTransition = new NoMoreMobTransition();

            attackTransition.SetNextState(new AttackState());
            noMoreMobTransition.SetNextState(new IdleState());

            transitions.Add(attackTransition);
            transitions.Add(noMoreMobTransition);

            sm.anim.SetBool("running", true);
        }

        /// <summary>
        /// Gets called every Update() when this object is in this state.
        /// </summary>
        public override void OnUpdate()
        {
            sm.groundSteering.CrankSteering(sm.anim);
        }

        /// <summary>
        /// Gets called once when this object is exitting this state.
        /// </summary>
        public override void OnExit()
        {
            sm.anim.SetBool("running", false);
            transitions.Clear();
        }
    };

    public class ChaseTransition : Transition
    {
        private BasicEnemyStateMachine sm;

        /// <summary>
        /// When the distance to a player is less than the chaseRadius, this will return true.
        /// </summary>
        /// <param name="stateMachine">The stateMachine object to reference the GameObject.</param>
        /// <returns>True when this object should enter the next state,
        /// false when this object should stay in the same state.</returns>
        public override bool CheckCondition(StateMachine stateMachine)
        {
            sm = (BasicEnemyStateMachine)stateMachine;
            sm.targetIndex = -1;
            

            if (sm.closestPlayerDist < sm.chaseRadius)
            {
                sm.targetPos = GameControl.control.players[sm.closestPlayerIndex].transform.position;
                sm.targetIndex = sm.closestPlayerIndex;
            }
                
            
            if (sm.targetIndex != -1)
            {
                sm.outgoingAttack = sm.skills[1];
                sm.StartCoroutine(sm.Spot(sm.outgoingAttack.activeFrames));
                sm.anim.SetBool("inRange", true);
                return true;
            }

            sm.anim.SetBool("inRange", false);
            return false;
        }
    };

    public class AttackTransition : Transition
    {
        private BasicEnemyStateMachine sm;

        /// <summary>
        /// When the distance to a player is less than the attackRadius, this will return true.
        /// </summary>
        /// <param name="stateMachine">The stateMachine object to reference the GameObject.</param>
        /// <returns>True when this object should enter the next state,
        /// false when this object should stay in the same state.</returns>
        public override bool CheckCondition(StateMachine stateMachine)
        {
            sm = (BasicEnemyStateMachine)stateMachine;

            

            if (sm.closestPlayerDist < sm.attackRadius)
            {
                sm.targetIndex = sm.closestPlayerIndex;
                return true;
            }

            return false;
        }
    };

    public class AttackToIdleTransition : Transition
    {
        private BasicEnemyStateMachine sm;
        private EnemyScript enemy;

        /// <summary>
        /// When the distance to a player is greater than the attackRadius and
        /// the enemy is not attacking, this will return true.
        /// </summary>
        /// <param name="stateMachine">The stateMachine object to reference the GameObject.</param>
        /// <returns>True when this object should enter the next state,
        /// false when this object should stay in the same state.</returns>
        public override bool CheckCondition(StateMachine stateMachine)
        {
            sm = (BasicEnemyStateMachine)stateMachine;
            enemy = sm.GetComponent<EnemyScript>();


            if (enemy.isAttacking || sm.closestPlayerDist < sm.attackRadius)
                return false;
            
            return true;
        }
    };

    public class MobTransition : Transition
    {
        private BasicEnemyStateMachine sm;
        private EnemyScript enemy;

        /// <summary>
        /// When the distance to a player is greater than the attackRadius and
        /// the enemy is not attacking, this will return true.
        /// </summary>
        /// <param name="stateMachine">The stateMachine object to reference the GameObject.</param>
        /// <returns>True when this object should enter the next state,
        /// false when this object should stay in the same state.</returns>
        public override bool CheckCondition(StateMachine stateMachine)
        {
            sm = (BasicEnemyStateMachine)stateMachine;
            enemy = sm.GetComponent<EnemyScript>();

            if (enemy.isMobbed && !enemy.isAttacking)
                return true;
            return false;
        }
    };

    public class NoMoreMobTransition : Transition
    {
        private BasicEnemyStateMachine sm;
        private EnemyScript enemy;

        /// <summary>
        /// When the distance to a player is greater than the attackRadius and
        /// the enemy is not attacking, this will return true.
        /// </summary>
        /// <param name="stateMachine">The stateMachine object to reference the GameObject.</param>
        /// <returns>True when this object should enter the next state,
        /// false when this object should stay in the same state.</returns>
        public override bool CheckCondition(StateMachine stateMachine)
        {
            sm = (BasicEnemyStateMachine)stateMachine;
            enemy = sm.GetComponent<EnemyScript>();

            if (!enemy.isMobbed)
                return true;
            return false;
        }
    };
}
