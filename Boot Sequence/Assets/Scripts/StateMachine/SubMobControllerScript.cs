﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubMobControllerScript : MonoBehaviour
{
    public List<EnemyScript> enemies;
    public int maxEnemies = 4;
    public float flankDistance = 7.5f;
    public float aggroRange = 20f;

    private List<Vector3> targetPos;
    private RRTPathfinderScript pathfinder;
    private Vector3 center;
    private float bufferRange = 1.5f;
    private State currentState = State.CLUMPING;
    private SuperState currentSuperState = SuperState.FLANKING;
    private Vector3 oldCenter;
    private int bombIndex;
    private bool lockedSuperState = false;

    private List<int> notBombs = new List<int>();
    private int updateAt = 3;
    private int updateTick = 0;

    enum SuperState
    {
        INVALID_STATE = -1,
        FLANKING,
        HAMMER_ANVIL
    }

    enum State
    {
        INVALID_STATE = -1,
        FLANKING,
        COLLAPSING,
        CLUMPING,
        HAMMER,
        ANVIL
    }

    private void Start()
    {
        notBombs.Add(0);
        notBombs.Add(1);
        notBombs.Add(2);
        notBombs.Add(3);

        targetPos = new List<Vector3>();
        pathfinder = FindObjectOfType<RRTPathfinderScript>();
    }

    private void Update()
    {
        
        if (updateTick < updateAt)
        {
            ++updateTick;
        }
        else
        {
            updateTick = 0;
            for (int i = 0; i < enemies.Count; ++i)
            {
                if (enemies[i] == null)
                {
                    enemies.Remove(enemies[i]);
                    currentState = State.CLUMPING;
                }
            }
            if (enemies.Count < 2)
            {
                foreach (EnemyScript e in enemies)
                {
                    e.isMobbed = false;
                }
                Destroy(gameObject);
            }
            else
            {
                DecideSuperState();
                center = Vector3.zero;
                foreach (EnemyScript e in enemies)
                {

                    center += e.transform.position;
                    e.isMobbed = true;
                }


                if (targetPos.Count != enemies.Count)
                {
                    targetPos.Clear();
                    foreach (EnemyScript e in enemies)
                    {
                        targetPos.Add(new Vector3());
                    }
                }



                center /= enemies.Count;

                DecideActions();
            }
        }
    }

    private void DecideActions()
    {
        float min = aggroRange;
        int index = -1;
        for (int i = 0; i < GameControl.control.players.Length; ++i)
        {
            if (GameControl.control.players[i].activeInHierarchy)
            {
                Vector2 center2D = new Vector2(center.x, center.z);
                Vector2 player2D = new Vector2(GameControl.control.players[i].transform.position.x, GameControl.control.players[i].transform.position.z);
                float distance = Vector2.Distance(player2D, center2D);
                if (distance < min)
                {
                    min = distance;
                    index = i;
                }
            }
        }
        if (currentSuperState == SuperState.FLANKING)
        {
            if (index != -1)
            {
                if (currentState == State.FLANKING)
                {
                    int counter = TargetsReached();
                    if (counter > enemies.Count / 2)
                    {
                        Collapse(index);
                    }
                    else
                    {
                        Flank(index);
                    }
                }
                else if (currentState == State.COLLAPSING)
                {
                    Collapse(index);
                }
                else
                {
                    oldCenter = center;
                    Flank(index);
                }
            }
            else
            {
                Clump();
            }
        }
        else if (currentSuperState == SuperState.HAMMER_ANVIL)
        {
            if (index != -1)
            {
                //first anvil
                if (currentState == State.CLUMPING)
                {
                    oldCenter = center;
                    Anvil(index);
                }
                //check if we've reached our targets
                int counter = TargetsReached();
                if (counter == enemies.Count)
                {
                    if (currentState == State.ANVIL)
                    {
                        Hammer(index);
                    }
                    else if (currentState == State.HAMMER)
                    {
                        Collapse(index);
                    }
                }
                else
                {
                    if (currentState == State.ANVIL)
                    {
                        Anvil(index);
                    }
                    else if (currentState == State.HAMMER)
                    {
                        Hammer(index);
                    }
                    else if (currentState == State.COLLAPSING)
                    {
                        Collapse(index);
                    }
                }

            }
            else
            {
                Clump();
            }
        }
        else
        {
            Clump();
        }
       
    }

    private int TargetsReached()
    {
        int targetsReached = 0;

        for (int i = 0; i < targetPos.Count; ++i)
        {
            Vector2 enemy2D = new Vector2(enemies[i].transform.position.x, enemies[i].transform.position.z);
            Vector2 target2D = new Vector2(targetPos[i].x, targetPos[i].z);
            float distance = Vector2.Distance(target2D, enemy2D);
            if (distance < bufferRange)
            {
                enemies[i].anim.SetBool("running", false);
                ++targetsReached;
            }
        }
        return targetsReached;
    }

    private void Clump()
    {
        currentState = State.CLUMPING;
        //set one target for all targetPos to be set to
        //center of all enemies
        for (int i = 0; i < enemies.Count; ++i)
        {
            enemies[i].ResetEye();
            //    targetPos[i] = center;
            targetPos[i] = enemies[i].transform.position;
        }
        //StartCoroutine(DelayPathfind());
    }

    private void Flank(int playerIndex)
    {

        currentState = State.FLANKING;

        Vector3 directionToCenter = oldCenter - GameControl.control.players[playerIndex].transform.position;

        directionToCenter.Normalize();
        directionToCenter *= flankDistance;

        Quaternion lRot = Quaternion.AngleAxis(90f, Vector3.up);
        Quaternion rRot = Quaternion.AngleAxis(-90f, Vector3.up);
        Vector3 lTarget = GameControl.control.players[playerIndex].transform.position + (lRot * directionToCenter);
        Vector3 rTarget = GameControl.control.players[playerIndex].transform.position + (rRot * directionToCenter);

        for (int i = 0; i < enemies.Count; ++i)
        {
            enemies[i].ActivateEye();
            if (i % 2 == 0)
            {
                targetPos[i] = lTarget;
            }
            else
            {
                targetPos[i] = rTarget;
            }
        }

        StartCoroutine(DelayPathfind());
    }

    private void Collapse(int playerIndex)
    {
        currentState = State.COLLAPSING;

        for (int i = 0; i < enemies.Count; ++i)
        {
            enemies[i].ActivateEye();

            enemies[i].anim.SetBool("running", true);
            targetPos[i] = GameControl.control.players[playerIndex].transform.position;
        }
        StartCoroutine(DelayPathfind());
    }

    private void Anvil(int playerIndex)
    {
        currentState = State.ANVIL;

        Vector3 directionToCenter = oldCenter - GameControl.control.players[playerIndex].transform.position;

        directionToCenter.Normalize();
        directionToCenter *= flankDistance;

        Quaternion lRot = Quaternion.AngleAxis(45f, Vector3.up);
        Quaternion rRot = Quaternion.AngleAxis(-45f, Vector3.up);
        Quaternion bRot = Quaternion.AngleAxis(90f, Vector3.up);

        Vector3 lTarget = GameControl.control.players[playerIndex].transform.position + (lRot * directionToCenter);
        Vector3 rTarget = GameControl.control.players[playerIndex].transform.position + (rRot * directionToCenter);
        Vector3 bTarget = GameControl.control.players[playerIndex].transform.position + (bRot * directionToCenter);
        Vector3 cTarget = GameControl.control.players[playerIndex].transform.position + directionToCenter;

        for (int i = 0; i < enemies.Count; ++i)
        {
            enemies[i].ActivateEye();
        }

        targetPos[notBombs[0]] = rTarget;
        targetPos[notBombs[1]] = lTarget;
        targetPos[notBombs[2]] = cTarget;
        targetPos[bombIndex] = bTarget;

        StartCoroutine(DelayPathfind());
    }

    private void Hammer(int playerIndex)
    {
        currentState = State.HAMMER;

        Vector3 directionToCenter = oldCenter - GameControl.control.players[playerIndex].transform.position;

        directionToCenter.Normalize();
        directionToCenter *= flankDistance;

        Quaternion lRot = Quaternion.AngleAxis(45f, Vector3.up);
        Quaternion rRot = Quaternion.AngleAxis(-45f, Vector3.up);
        Quaternion bRot = Quaternion.AngleAxis(180f, Vector3.up);

        Vector3 lTarget = GameControl.control.players[playerIndex].transform.position + (lRot * directionToCenter);
        Vector3 rTarget = GameControl.control.players[playerIndex].transform.position + (rRot * directionToCenter);
        Vector3 bTarget = GameControl.control.players[playerIndex].transform.position + (bRot * directionToCenter);
        Vector3 cTarget = GameControl.control.players[playerIndex].transform.position + directionToCenter;

        for (int i = 0; i < enemies.Count; ++i)
        {
            enemies[i].ActivateEye();
        }
        targetPos[notBombs[0]] = rTarget;
        targetPos[notBombs[1]] = lTarget;
        targetPos[notBombs[2]] = cTarget;
        targetPos[bombIndex] = bTarget;

        StartCoroutine(DelayPathfind());
    }

    void DecideSuperState()
    {
        if (enemies.Count == maxEnemies)
        {
            int count = 0;
            int index = 0;
            for (int i = 0; i < maxEnemies; ++i)
            {
                if (enemies[i].gameObject.GetComponent<BombEnemyStateMachine>())
                {
                    ++count;
                    index = i;
                }
            }

            if (count == 1)
            {
                currentSuperState = SuperState.HAMMER_ANVIL;
                bombIndex = index;
                notBombs.Remove(index);

            }
            else
            {
                currentSuperState = SuperState.FLANKING;
            }
        }
        else
        {
            currentSuperState = SuperState.FLANKING;
        }

    }

    //where should enemy go?
    //give it a path
    private IEnumerator DelayPathfind()
    {
        for (int i = 0; i < enemies.Count; ++i)
        {
            //because things can die before this gets to them
            if (i < enemies.Count)
            {
                if (enemies[i] != null)
                {
                    enemies[i].GetComponent<GroundSteeringScript>().SetPath(
                        pathfinder.Pathfind(enemies[i].transform.position, targetPos[i], enemies[i].transform.position));

                    yield return null;
                }
            }
        }
    }
}
