﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StateMachine : EnemyScript
{
    public override void Start()
    {
        base.Start();
    }

    public class State
    {
        public virtual void OnEnter(StateMachine sm) { }
        public virtual void OnUpdate() { }
        public virtual void OnExit() { }

        public List<Transition> transitions;
    };

    public class Transition
    {
        public virtual bool CheckCondition(StateMachine sm) { return false; }

        public State GetNextState()
        {
            return nextState;
        }

        public void SetNextState(State next)
        {
            nextState = next;
        }

        private State nextState;
    };
}