﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class TurretEnemyStateMachine : StateMachine
{
    public float attackRadius;
    public Transform projectileSpawner;
    public GameObject projectile;
    public GameObject projectileWindup;

    private Vector3 targetPos = Vector3.zero;
    private int targetIndex = -1;
    private TurretEnemyStateMachine enemyStateMachine;
    private RRTPathfinderScript pathfinder;
    private Rigidbody rb;
    private State currentState;
    private Coroutine spotCoroutine;

    //need to declare a "starting state" here, IdleState is a temporary thing
    private IdleState idleState;
    [HideInInspector]
    public static bool incDamage;

    public override void Start()
    {
        base.Start();
        enemyStateMachine = GetComponent<TurretEnemyStateMachine>();
        AddSkill(SkillPoolScript.Skill.SkillType.ENEMY_TURRET);
        AddSkill(SkillPoolScript.Skill.SkillType.ENEMY_TURRET_SPOT);
        rb = GetComponent<Rigidbody>();

        idleState = new IdleState();
        currentState = idleState;
        currentState.OnEnter(enemyStateMachine);
        health += GameControl.control.turretHealth;

        if (incDamage)
        {
            incDamage = false;
            projectile.GetComponent<ProjectileScript>().dmg += GameControl.control.dmgToAdd;
        }
    }

    public override void Update()
    {
        base.Update();
        if (!isStunned)
        {
            CheckState();
        }
    }


    private void CheckState()
    {
        for (int i = 0; i < currentState.transitions.Count; ++i)
        { 
            if (currentState.transitions[i].CheckCondition(enemyStateMachine))
            {
                State temp = currentState;
                currentState = currentState.transitions[i].GetNextState();
                temp.OnExit();
                currentState.OnEnter(enemyStateMachine);
                break;
            }
        }

        currentState.OnUpdate();
    }

    public class IdleState : State
    {
        private TurretEnemyStateMachine sm;
        private AttackTransition attackTransition;

        public override void OnEnter(StateMachine stateMachine)
        {
            sm = (TurretEnemyStateMachine)stateMachine;
            transitions = new List<Transition>();

            attackTransition = new AttackTransition();
            attackTransition.SetNextState(new AttackState());

            transitions.Add(attackTransition);
            sm.ResetEye();
        }

        public override void OnUpdate()
        {
        }

        public override void OnExit()
        {
            transitions.Clear();
        }
    };

    public class AttackState : State
    {
        private TurretEnemyStateMachine sm;
        private IdleTransition idleTransition;

        public override void OnEnter(StateMachine stateMachine)
        {
            sm = (TurretEnemyStateMachine)stateMachine;
            transitions = new List<Transition>();

            idleTransition = new IdleTransition();
            idleTransition.SetNextState(new IdleState());

            transitions.Add(idleTransition);
        }

        public override void OnUpdate()
        {
            Vector3 targetPos = GameControl.control.players[sm.targetIndex].transform.position - sm.transform.position;
            sm.transform.rotation = Quaternion.LookRotation(new Vector3(targetPos.x, 0f, targetPos.z));

            if(!sm.isSpot && !sm.isAttacking)
            {
                sm.StartCoroutine(sm.PlayingAttack());
                sm.outgoingAttack = sm.skills[0];
                sm.coroutine = sm.StartCoroutine(sm.AttackProjectile(sm.outgoingAttack.windupFrames, sm.outgoingAttack.activeFrames, sm.outgoingAttack.cooldownFrames,
                    sm.projectile, sm.projectileSpawner, sm.projectileWindup, null, "turret_gunshot"));
            }
        }

        public override void OnExit()
        {
            sm.anim.SetBool("attacking", false);
            transitions.Clear();
        }
    };

    public class IdleTransition : Transition
    {
        private TurretEnemyStateMachine sm;

        public override bool CheckCondition(StateMachine stateMachine)
        {
            sm = (TurretEnemyStateMachine)stateMachine;

            if (sm.closestPlayerDist < sm.attackRadius)
            {
                sm.anim.SetBool("inRange", true);
                return false;
            }
            
            sm.anim.SetBool("inRange", false);
            return true;
        }
    };

    public class AttackTransition : Transition
    {
        private TurretEnemyStateMachine sm;

        public override bool CheckCondition(StateMachine stateMachine)
        {
            sm = (TurretEnemyStateMachine)stateMachine;

         
            if (sm.closestPlayerDist < sm.attackRadius)
            {
                if (sm.spotCoroutine != null)
                {
                    sm.StopCoroutine(sm.spotCoroutine);
                    sm.isSpot = false;
                }
                sm.outgoingAttack = sm.skills[1];
                sm.spotCoroutine = sm.StartCoroutine(sm.Spot(sm.outgoingAttack.activeFrames));
                sm.targetIndex = sm.closestPlayerIndex;
                sm.anim.SetBool("inRange", true);
                return true;
            }
                
            
            sm.anim.SetBool("inRange", false);
            return false;
        }
    }
}
