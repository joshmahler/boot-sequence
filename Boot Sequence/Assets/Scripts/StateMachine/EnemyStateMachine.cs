﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStateMachine : StateMachine
{
    public float attackRadius;
    public float chaseRadius;

    private GameObject[] playerArray;
    private Vector3 targetPos = Vector3.zero;
    private int targetIndex = -1;
    private EnemyStateMachine enemyStateMachine;
    private RRTPathfinderScript pathfinder;
    private Rigidbody rb;
    private State currentState;
    //need to declare a "starting state" here, IdleState is a temporary thing
    IdleState idleState;
    

    void Start()
    {
        enemyStateMachine = GetComponent<EnemyStateMachine>();
        pathfinder = GameObject.FindGameObjectWithTag("PathfindingManager").GetComponent<RRTPathfinderScript>();
        rb = GetComponent<Rigidbody>();
        playerArray = GameObject.FindGameObjectsWithTag("Player");
        idleState = new IdleState();
        currentState = idleState;
        currentState.OnEnter(enemyStateMachine);
    }

    void Update()
    {
        CheckState();
    }

    private void CheckState()
    {
        for (int i = 0; i < currentState.transitions.Count; ++i)
        {
            if (currentState.transitions[i].CheckCondition(enemyStateMachine))
            {
                State temp = currentState;
                currentState = currentState.transitions[i].GetNextState();
                temp.OnExit();
                currentState.OnEnter(enemyStateMachine);
                break;
            }
        }

        currentState.OnUpdate();
    }

    public class IdleState : State
    {
        private EnemyStateMachine sm;
        private ChaseTransition chaseTransition;

        public override void OnEnter(StateMachine stateMachine)
        {
            sm = (EnemyStateMachine)stateMachine;
            transitions = new List<Transition>();

            chaseTransition = new ChaseTransition();
            chaseTransition.SetNextState(new ChaseState());

            transitions.Add(chaseTransition);
        }

        public override void OnUpdate()
        {
        }

        public override void OnExit()
        {
            transitions.Clear();
        }
    };

    public class ChaseState : State
    {
        private EnemyStateMachine sm;

        public override void OnEnter(StateMachine stateMachine)
        {
            sm = (EnemyStateMachine)stateMachine;
            transitions = new List<Transition>();

            List<Node> path = sm.pathfinder.Pathfind(sm.transform.position, sm.targetPos, sm.transform.position);
        }

        public override void OnUpdate()
        {

        }

        public override void OnExit()
        {
            transitions.Clear();
        }
    };

    public class ChaseTransition : Transition
    {
        private EnemyStateMachine sm;

        public override bool CheckCondition(StateMachine stateMachine)
        {
            sm = (EnemyStateMachine)stateMachine;

            for (int i = 0; i < sm.playerArray.Length; ++i)
            {
                float distance = Vector3.Distance(sm.transform.position, sm.playerArray[i].transform.position);
                if (distance < sm.chaseRadius)
                {
                    sm.targetPos = sm.playerArray[i].transform.position;
                    sm.targetIndex = i;
                    return true;
                }
            }
            return false;
        }
    };
}
