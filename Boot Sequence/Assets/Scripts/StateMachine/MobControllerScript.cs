﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobControllerScript : MonoBehaviour
{
    public GameObject subMobPrefab;

    private List<SubMobControllerScript> mobs;


    private void Start()
    {
        mobs = new List<SubMobControllerScript>();
    }

    public void MobRequest(EnemyScript thisEnemy, EnemyScript otherEnemy)
    {
        SubMobControllerScript subMobA = null;
        SubMobControllerScript subMobB = null;
        foreach(SubMobControllerScript sc in mobs)
        {
            if(sc.enemies.Contains(thisEnemy))
            {
                subMobA = sc;
            }

            if (sc.enemies.Contains(otherEnemy))
            {
                subMobB = sc;
            }
        }

        if (subMobA && subMobB)
        {
            //do nothing
        }
        else if (!subMobA && !subMobB)
        {
            //make new mob
            GameObject newSubMob = Instantiate(subMobPrefab, transform);
            SubMobControllerScript subMob = newSubMob.GetComponent<SubMobControllerScript>();
            subMob.enemies = new List<EnemyScript>();

            subMob.enemies.Add(thisEnemy);
            subMob.enemies.Add(otherEnemy);
            mobs.Add(subMob);
        }
        else if (!subMobA)
        {
            //add A to B
            if (subMobB.enemies.Count < subMobB.maxEnemies)
            {
                subMobB.enemies.Add(thisEnemy);
            }
        }
        else
        {
            //add B to A
            if (subMobA.enemies.Count < subMobA.maxEnemies)
            {
                subMobA.enemies.Add(otherEnemy);
            }
        }
        
        
    }
}
