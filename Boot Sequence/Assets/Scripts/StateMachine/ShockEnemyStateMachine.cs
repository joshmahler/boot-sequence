﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShockEnemyStateMachine : StateMachine
{
    public float maxPatrolPoints = 4f;
    public float attackRadius = 6f;          // The radius which the enemy will start hitting the player.
    public float dashSpeed = 575f;
    public GameObject hitbox;
    public Transform impactSpawn;
    public GameObject stunnedVFX;
    public GameObject shockingVFX;

    private GameObject stunnedVFXPlaying;
    private GameObject shockingVFXPlaying;

    private GameObject WFCarea;

    private Vector3 targetPos = Vector3.zero;           // The position this object will move towards in chase state.
    private int targetIndex = -1;                       // The index of the player array this object will move towards.
    private GroundSteeringScript groundSteering;        // A reference to the script attached to this object.
    private ShockEnemyStateMachine enemyStateMachine;   // A reference to this object for the StateMachine.
    private RRTPathfinderScript pathfinder;             // A reference to the steering script.
    private Rigidbody rb;                               // A reference to the rigidbody.
    private State currentState;                         // The current state this object is in.
    private Coroutine spotCoroutine;


    //need to declare a "starting state" here, IdleState is a temporary thing.
    private WanderState wanderState;            // The starting state of this object.
    private int countdown = 0;              // A counter to pathfind not every frame.

    private List<Vector3> waypoints = new List<Vector3>();
    private int patrolPoints = 0;
    private float rotatePerFrame = 3f;

    /*
     * Must override Start to call the base class Start before calling AddSkill.
     * This makes sure the List exists.
     */
    public override void Start()
    {

        WFCarea = GameObject.Find("ConversionArea");
        base.Start();
        enemyStateMachine = GetComponent<ShockEnemyStateMachine>();
        AddSkill(SkillPoolScript.Skill.SkillType.ENEMY_SHOCK);
        AddSkill(SkillPoolScript.Skill.SkillType.ENEMY_SHOCK_SPOT);
        pathfinder = GameObject.FindGameObjectWithTag("PathfindingManager").GetComponent<RRTPathfinderScript>();
        rb = GetComponent<Rigidbody>();
        rb.velocity += new Vector3(0.5f, 0, 0);
        groundSteering = GetComponent<GroundSteeringScript>();

        wanderState = new WanderState();
        currentState = wanderState;
        currentState.OnEnter(enemyStateMachine);
        health += GameControl.control.shockHealth;
        // Have to do this to actually modify the value.
        SkillPoolScript.Skill skill = skills[0];
        skill.damage += GameControl.control.dmgToAdd;
        skills[0] = skill;
    }

    public override void Update()
    {
        base.Update();
        if (!isStunned)
        {
            CheckState();
        }

        if (isStunned)
        {
            if (stunnedVFXPlaying == null)
            {
                stunnedVFXPlaying = Instantiate(stunnedVFX, impactSpawn);
            }
        }
        else
        {
            if (stunnedVFXPlaying != null)
            {
                Destroy(stunnedVFXPlaying);
            }
        }
    }

    private void CheckState()
    {

        for (int i = 0; i < currentState.transitions.Count; ++i)
        {

            if (currentState.transitions[i].CheckCondition(enemyStateMachine))
            {

                State temp = currentState;
                currentState = currentState.transitions[i].GetNextState();
                temp.OnExit();
                currentState.OnEnter(enemyStateMachine);
                
                break;
            }
        }

        currentState.OnUpdate();
    }


    public class WanderState : State
    {
        private ShockEnemyStateMachine sm;
        private AttackTransition attackTransition;
        private PatrolTransition patrolTransition;

        private Vector3 targetPos;
        private float radius = 2.5f;
        private float offset = 3f;
        private float bufferRange = .50f;
        private float maxRaycastDistance = 3.5f;
        private float waypointSpread = 5.1f;
        private Coroutine turnAround = null;
        private bool shouldSteer = true;
        private float raycastOffset = 0.5f;


        public override void OnEnter(StateMachine stateMachine)
        {
            sm = (ShockEnemyStateMachine)stateMachine;
            transitions = new List<Transition>();

            attackTransition = new AttackTransition();
            patrolTransition = new PatrolTransition();

            attackTransition.SetNextState(new AttackState());
            patrolTransition.SetNextState(new PatrolState());

            transitions.Add(attackTransition);
            transitions.Add(patrolTransition);

            //width * height * convertTextureToMesh.mSizeOfModule
            if (sm.patrolPoints <= 0)
            {
                WFCOutput output = FindObjectOfType<WFCOutput>();
                Vector3 min = sm.WFCarea.transform.position;
                Vector3 max = new Vector3(min.x + output.width * 7f, min.y, min.z + output.height * 7f);
                min = new Vector3(min.x + output.width * -7f, min.y, min.z + output.height * -7f);
                targetPos = new Vector3(Random.Range(min.x, max.x), 1.5f, Random.Range(min.z, max.z));
            }
            else
            {
                targetPos = sm.transform.position + sm.transform.forward * offset;
                float angle = Random.Range(0f, 359f);
                Quaternion rot = Quaternion.AngleAxis(angle, Vector3.up);
                targetPos += (rot * sm.transform.forward) * radius;
            }
            sm.groundSteering.SetPath(sm.pathfinder.Pathfind(sm.transform.position, targetPos, sm.transform.position));

            sm.anim.SetBool("running", true);
        }
        
        public override void OnUpdate()
        {


            //whisker'd steering
            Vector2 pos = new Vector2(sm.transform.position.x, sm.transform.position.z);
            Vector2 targetPos2D = new Vector2(targetPos.x, targetPos.z);

            if (Vector2.Distance(pos, targetPos2D) < bufferRange)
            {
                
                if (sm.patrolPoints <= 0)
                {

                    targetPos = new Vector3(50, 1.5f, -10);
                }
                else
                {

                    targetPos = sm.transform.position + sm.transform.forward * offset;
                    float angle = Random.Range(0f, 359f);
                    Quaternion rot = Quaternion.AngleAxis(angle, Vector3.up);
                    targetPos += (rot * sm.transform.forward) * radius;
        
                }
                sm.groundSteering.SetPath(sm.pathfinder.Pathfind(sm.transform.position, targetPos, sm.transform.position));
                sm.groundSteering.shouldRotate = true;
            }
        

            Vector3 origin = sm.transform.position + (sm.transform.forward * raycastOffset);
            RaycastHit hit;
            if(sm.patrolPoints <= 0)
            {

                Physics.Raycast(origin, Vector3.down, out hit, maxRaycastDistance);
                if (!hit.collider)
                {
                    raycastOffset += 0.75f;
                    ++sm.patrolPoints;
                    sm.waypoints.Add(origin);

                    targetPos = sm.transform.position + sm.transform.forward * offset;
                    float angle = Random.Range(0f, 359f);
                    Quaternion rot = Quaternion.AngleAxis(angle, Vector3.up);
                    targetPos += (rot * sm.transform.forward) * radius;
                    sm.groundSteering.SetPath(sm.pathfinder.Pathfind(sm.transform.position, targetPos, sm.transform.position));
                }
            }
            else
            {
                if (Physics.Raycast(origin, Vector3.down, out hit, maxRaycastDistance))
                {
                    bool good = true;
                    foreach (Vector3 vec in sm.waypoints)
                    {
                        float distance = Vector3.Distance(sm.transform.position, vec);
                        if(distance < waypointSpread)
                        {
                            good = false;
                        }
                    }
                    if(good)
                    {
                        ++sm.patrolPoints;
                        sm.waypoints.Add(sm.transform.position);
                    }

                    if (turnAround == null)
                    {
                        turnAround = sm.StartCoroutine(TurnAround());
                    }
                }
            }
            if (shouldSteer)
            {
                sm.groundSteering.CrankSteering(sm.anim);
            }
        }

        public override void OnExit()
        {
            sm.anim.SetBool("running", false);
            transitions.Clear();
        }

        private IEnumerator TurnAround()
        {
            sm.groundSteering.shouldRotate = false;
            Quaternion target = Quaternion.AngleAxis(sm.transform.rotation.y + 190f, Vector3.up);
            shouldSteer = false;
            sm.rb.velocity = Vector3.zero;

            targetPos = sm.transform.position + (target * sm.transform.forward) * offset / 1.33333f;

            sm.groundSteering.SetPath(sm.pathfinder.Pathfind(sm.transform.position, targetPos, sm.transform.position));
           
            target = target * sm.transform.rotation;

            for (float i = 0; i <= 1; i += 0.05f)
            {
                sm.transform.rotation = Quaternion.Lerp(sm.transform.rotation, target, i);
                yield return null;
            }

           
            turnAround = null;
            shouldSteer = true;
        }
    };

    public class PatrolState : State
    {
        private ShockEnemyStateMachine sm;
        private AttackTransition attackTransition;
        private int index = 0;
        private float bufferRange = 1f;

        public override void OnEnter(StateMachine stateMachine)
        {
            sm = (ShockEnemyStateMachine)stateMachine;
            transitions = new List<Transition>();

            attackTransition = new AttackTransition();
            attackTransition.SetNextState(new AttackState());

            transitions.Add(attackTransition);

            sm.groundSteering.shouldRotate = true;
            sm.groundSteering.SetPath(sm.pathfinder.Pathfind(sm.transform.position, sm.waypoints[index], sm.transform.position));

            sm.anim.SetBool("hovering", true);
        }

        public override void OnUpdate()
        {
            if(Vector3.Distance(sm.transform.position, sm.waypoints[index]) < bufferRange)
            {
                if(++index >= sm.waypoints.Count)
                {
                    index = 0;
                }
                sm.groundSteering.SetPath(sm.pathfinder.Pathfind(sm.transform.position, sm.waypoints[index], sm.transform.position));
            }

            sm.groundSteering.CrankSteering(sm.anim);
        }

        public override void OnExit()
        {
            sm.anim.SetBool("hovering", false);
            transitions.Clear();
        }
    };

    public class AttackState : State
    {
        private ShockEnemyStateMachine sm;
        private AttackToWanderTransition attackToWanderTransition;
        private AttackToPatrolTransition attackToPatrolTransition;
        private bool isDashEnding = false;


        public override void OnEnter(StateMachine stateMachine)
        {
            sm = (ShockEnemyStateMachine)stateMachine;

            transitions = new List<Transition>();

            attackToWanderTransition = new AttackToWanderTransition();
            attackToPatrolTransition = new AttackToPatrolTransition();

            attackToWanderTransition.SetNextState(new WanderState());
            attackToPatrolTransition.SetNextState(new PatrolState());

            transitions.Add(attackToWanderTransition);
            transitions.Add(attackToPatrolTransition);

            sm.patrolPoints = 0;
            sm.waypoints.Clear();
        }

        public override void OnUpdate()
        {
            if (!sm.isSpot && !sm.isAttacking)
            {
                sm.StartCoroutine(sm.PlayingAttack());
                sm.outgoingAttack = sm.skills[0];
                sm.coroutine = sm.StartCoroutine(sm.Attack(sm.outgoingAttack.windupFrames, sm.outgoingAttack.activeFrames, sm.outgoingAttack.cooldownFrames,
                    sm.hitbox, sm.transform, null, null, "WalkerAggro"));
            }
            if (sm.isAttackWindup)
            {
                Vector3 target = GameControl.control.players[sm.targetIndex].transform.position - sm.transform.position;
                Quaternion targetRot = Quaternion.LookRotation(target);
                sm.transform.rotation = Quaternion.RotateTowards(sm.transform.rotation, targetRot, sm.rotatePerFrame);
            }
            if(!sm.isSpot && !isDashEnding && sm.isAttackActive)
            {
                sm.rb.AddForce(sm.transform.forward * sm.dashSpeed);
                sm.StartCoroutine(DashEnding(.75f));
            }
            if (sm.isAttackWindup || isDashEnding)
            {
                if (sm.shockingVFXPlaying == null)
                {
                    sm.shockingVFXPlaying = Instantiate(sm.shockingVFX, sm.transform);
                }
            }
            else
            {
                if(sm.shockingVFXPlaying != null)
                {
                    Destroy(sm.shockingVFXPlaying);
                }
            }
        }

        public override void OnExit()
        {
            sm.anim.SetBool("attacking", false);
            transitions.Clear();
        }

        IEnumerator DashEnding(float t)
        {
            isDashEnding = true;
            yield return new WaitForSeconds(t);
            isDashEnding = false;
            sm.rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            sm.transform.rotation = Quaternion.Euler(new Vector3(0f, sm.transform.eulerAngles.y, 0f));
            sm.rb.angularVelocity = Vector3.zero;
            sm.ResetEye();
        }
    };




    public class AttackTransition : Transition
    {
        private ShockEnemyStateMachine sm;

        /// <summary>
        /// When the distance to a player is less than the attackRadius, this will return true.
        /// </summary>
        /// <param name="stateMachine">The stateMachine object to reference the GameObject.</param>
        /// <returns>True when this object should enter the next state,
        /// false when this object should stay in the same state.</returns>
        public override bool CheckCondition(StateMachine stateMachine)
        {
            sm = (ShockEnemyStateMachine)stateMachine;


            if (sm.closestPlayerDist < sm.attackRadius)
            {
                if (sm.spotCoroutine != null)
                {
                    sm.StopCoroutine(sm.spotCoroutine);
                    sm.isSpot = false;
                }
                sm.outgoingAttack = sm.skills[1];
                sm.spotCoroutine = sm.StartCoroutine(sm.Spot(sm.outgoingAttack.activeFrames));
                sm.targetIndex = sm.closestPlayerIndex;
                return true;
            }
             
            return false;
        }
    };

    public class PatrolTransition : Transition
    {
        private ShockEnemyStateMachine sm;

        /// <summary>
        /// When the distance to a player is less than the attackRadius, this will return true.
        /// </summary>
        /// <param name="stateMachine">The stateMachine object to reference the GameObject.</param>
        /// <returns>True when this object should enter the next state,
        /// false when this object should stay in the same state.</returns>
        public override bool CheckCondition(StateMachine stateMachine)
        {
            sm = (ShockEnemyStateMachine)stateMachine;

            if(sm.patrolPoints >= 4)
            {
                return true;
            }
            return false;
        }
    };

    public class AttackToWanderTransition : Transition
    {
        private ShockEnemyStateMachine sm;

        /// <summary>
        /// When the distance to a player is less than the attackRadius, this will return true.
        /// </summary>
        /// <param name="stateMachine">The stateMachine object to reference the GameObject.</param>
        /// <returns>True when this object should enter the next state,
        /// false when this object should stay in the same state.</returns>
        public override bool CheckCondition(StateMachine stateMachine)
        {
            sm = (ShockEnemyStateMachine)stateMachine;

           

            if (sm.closestPlayerDist < sm.attackRadius)
            {
                return false;
            }
            
            if (sm.patrolPoints < sm.maxPatrolPoints)
            {
                return true;
            }
            return false;
        }
    };

    public class AttackToPatrolTransition : Transition
    {
        private ShockEnemyStateMachine sm;

        /// <summary>
        /// When the distance to a player is less than the attackRadius, this will return true.
        /// </summary>
        /// <param name="stateMachine">The stateMachine object to reference the GameObject.</param>
        /// <returns>True when this object should enter the next state,
        /// false when this object should stay in the same state.</returns>
        public override bool CheckCondition(StateMachine stateMachine)
        {
            sm = (ShockEnemyStateMachine)stateMachine;

            
            if (sm.closestPlayerDist < sm.attackRadius)
            {
                return false;
            }
            
            if (sm.patrolPoints >= sm.maxPatrolPoints)
            {
                return true;
            }
            return false;
        }
    };
}
