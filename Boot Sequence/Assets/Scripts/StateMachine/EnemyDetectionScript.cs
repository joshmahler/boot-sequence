﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetectionScript : MonoBehaviour
{
    public float radius = 7.5f;

    private MobControllerScript mob;
    private EnemyScript enemy;

    private void Start()
    {
        mob = FindObjectOfType<MobControllerScript>();
        enemy = GetComponent<EnemyScript>();
    }

    private void FixedUpdate()
    {
        if (enemy.isMobbed)
            return;

        //LayerMask 12 is EnemyDetect
        LayerMask layerMask = 1 << 12;
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius, layerMask);

        foreach (Collider c in hitColliders)
            if (c.gameObject != gameObject)
                mob.MobRequest(enemy, c.GetComponentInParent<EnemyScript>());
    }
}
