﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(GroundSteeringScript))]
public class BombEnemyStateMachine : StateMachine
{ 
    public float chaseRadius;
    public float attackRadius;
    public GameObject hitbox;

    private Vector3 targetPos = Vector3.zero;
    private int targetIndex = -1;
    private GroundSteeringScript groundSteering;
    private BombEnemyStateMachine enemyStateMachine;
    private RRTPathfinderScript pathfinder;
    private Rigidbody rb;
    private State currentState;

    //need to declare a "starting state" here, IdleState is a temporary thing
    private IdleState idleState;
    private int countdown = 0;

    public override void Start()
    {
        base.Start();
        enemyStateMachine = GetComponent<BombEnemyStateMachine>();
        AddSkill(SkillPoolScript.Skill.SkillType.ENEMY_BOMB);
        AddSkill(SkillPoolScript.Skill.SkillType.ENEMY_BOMB_SPOT);
        pathfinder = GameObject.FindGameObjectWithTag("PathfindingManager").GetComponent<RRTPathfinderScript>();
        rb = GetComponent<Rigidbody>();
        groundSteering = GetComponent<GroundSteeringScript>();

        idleState = new IdleState();
        currentState = idleState;
        currentState.OnEnter(enemyStateMachine);
        health += GameControl.control.bombHealth;
        SkillPoolScript.Skill skill = skills[0];
        skill.damage += GameControl.control.dmgToAdd;
        skills[0] = skill;
    }

    public override void Update()
    {
        base.Update();
        if (!isStunned)
        {
            CheckState();
        }
    }

    private void CheckState()
    {
        for (int i = 0; i < currentState.transitions.Count; ++i)
        {
            if (currentState.transitions[i].CheckCondition(enemyStateMachine))
            {
                State temp = currentState;
                currentState = currentState.transitions[i].GetNextState();
                temp.OnExit();
                currentState.OnEnter(enemyStateMachine);
                break;
            }
    }

    currentState.OnUpdate();
    }

    private IEnumerator BombSound(float windupFrames, string chargeEvent, string explodeEvent)
    {
        windupFrames /= 60f;
        AkSoundEngine.PostEvent(chargeEvent, gameObject);
        yield return new WaitForSeconds(windupFrames);
        AkSoundEngine.PostEvent(explodeEvent, gameObject);

    }

    public class IdleState : State
    {
        private BombEnemyStateMachine sm;
        private MobTransition mobTransition;
        private ChaseTransition chaseTransition;

        public override void OnEnter(StateMachine stateMachine)
        {
            sm = (BombEnemyStateMachine)stateMachine;
            transitions = new List<Transition>();

            mobTransition = new MobTransition();
            chaseTransition = new ChaseTransition();

            mobTransition.SetNextState(new MobState());
            chaseTransition.SetNextState(new ChaseState());

            transitions.Add(mobTransition);
            transitions.Add(chaseTransition);
        }

        public override void OnUpdate()
        {
        }

        public override void OnExit()
        {
            transitions.Clear();
        }
    };

    public class ChaseState : State
    {
        private BombEnemyStateMachine sm;
        private MobTransition mobTransition;
        private AttackTransition attackTransition;

        public override void OnEnter(StateMachine stateMachine)
        {
            sm = (BombEnemyStateMachine)stateMachine;
            transitions = new List<Transition>();

            mobTransition = new MobTransition();
            attackTransition = new AttackTransition();

            mobTransition.SetNextState(new MobState());
            attackTransition.SetNextState(new AttackState());

            transitions.Add(mobTransition);
            transitions.Add(attackTransition);

            List<Node> path = sm.pathfinder.Pathfind(sm.transform.position, sm.targetPos, sm.transform.position);
            sm.groundSteering.SetPath(path);

            sm.anim.SetBool("running", true);
        }

        public override void OnUpdate()
        {
            if (!sm.isSpot && ++sm.countdown >= 20f)
            {
                sm.countdown = 0;
                sm.targetPos = GameControl.control.players[sm.targetIndex].transform.position;
                List<Node> path = sm.pathfinder.Pathfind(sm.transform.position, sm.targetPos, sm.transform.position);
                sm.groundSteering.SetPath(path);
            }
            sm.groundSteering.CrankSteering(sm.anim);
        }

        public override void OnExit()
        {
            sm.anim.SetBool("running", false);
            transitions.Clear();
        }
    };

    public class AttackState : State
    {
        private BombEnemyStateMachine sm;

        public override void OnEnter(StateMachine stateMachine)
        {
            
            sm = (BombEnemyStateMachine)stateMachine;
            
            transitions = new List<Transition>();
        }

        public override void OnUpdate()
        {
            if (!sm.isSpot && !sm.isAttacking)
            {
                sm.anim.SetBool("attacking", true);
                sm.outgoingAttack = sm.skills[0];
                sm.coroutine = sm.StartCoroutine(sm.AttackParticle(sm.outgoingAttack.windupFrames, sm.outgoingAttack.activeFrames, sm.outgoingAttack.cooldownFrames,
                    sm.hitbox, sm.gameObject.transform, sm, ParticlesScript.HitboxType.INVALID));
                sm.StartCoroutine(sm.BombSound(sm.outgoingAttack.windupFrames, "BomberCharge", "BomberExplosion"));
                
                /*
                 * Bomb explodes after windupframes + activeFrames. Since active frames is so long, 
                 * there is a noticeable delay from when it deals damage and when it destroys itself
                 */
                //sm.bomberExplodeEvent.Post(sm.gameObject); //Bomber explosion sound. 

                //TODO: make that destroy delay below into a coroutine so that we can play the charge sound at the beginning and the explosion sound at the end of the delay
                
                Destroy(sm.gameObject, (sm.outgoingAttack.windupFrames / 60f) + (sm.outgoingAttack.activeFrames / 60f));
            }

            
        }

        public override void OnExit()
        {
            sm.anim.SetBool("attacking", false);
            transitions.Clear();
        }
    };

    public class MobState : State
    {
        private BombEnemyStateMachine sm;
        private AttackTransition attackTransition;
        private NoMoreMobTransition noMoreMobTransition;

        /// <summary>
        /// Gets called once every time this object enters this state.
        /// </summary>
        /// <param name="stateMachine">The stateMachine object to reference the GameObject.</param>
        public override void OnEnter(StateMachine stateMachine)
        {
            sm = (BombEnemyStateMachine)stateMachine;
            transitions = new List<Transition>();

            attackTransition = new AttackTransition();
            noMoreMobTransition = new NoMoreMobTransition();

            attackTransition.SetNextState(new AttackState());
            noMoreMobTransition.SetNextState(new IdleState());

            transitions.Add(attackTransition);
            transitions.Add(noMoreMobTransition);
        }

        /// <summary>
        /// Gets called every Update() when this object is in this state.
        /// </summary>
        public override void OnUpdate()
        {
            sm.groundSteering.CrankSteering(sm.anim);
        }

        /// <summary>
        /// Gets called once when this object is exitting this state.
        /// </summary>
        public override void OnExit()
        {
            transitions.Clear();
        }
    };

    public class ChaseTransition : Transition
    {
        private BombEnemyStateMachine sm;

        public override bool CheckCondition(StateMachine stateMachine)
        {
            sm = (BombEnemyStateMachine)stateMachine;
            sm.targetIndex = -1;
            float minDist = sm.chaseRadius;
          
            if (sm.closestPlayerDist < minDist)
            {
                sm.targetPos = GameControl.control.players[sm.closestPlayerIndex].transform.position;
                sm.targetIndex = sm.closestPlayerIndex;
            }
               
            if (sm.targetIndex != -1)
            {
                sm.outgoingAttack = sm.skills[1];
                sm.StartCoroutine(sm.Spot(sm.outgoingAttack.activeFrames));
                return true;
            }

            return false;
        }
    };

    public class AttackTransition : Transition
    {
        private BombEnemyStateMachine sm;

        public override bool CheckCondition(StateMachine stateMachine)
        {
            sm = (BombEnemyStateMachine)stateMachine;

            if (sm.closestPlayerDist < sm.attackRadius)
            {
                sm.targetIndex = sm.closestPlayerIndex;
                return true;
            }
            
            return false;
        }
    };

    public class MobTransition : Transition
    {
        private BombEnemyStateMachine sm;
        private EnemyScript enemy;

        /// <summary>
        /// When the distance to a player is greater than the attackRadius and
        /// the enemy is not attacking, this will return true.
        /// </summary>
        /// <param name="stateMachine">The stateMachine object to reference the GameObject.</param>
        /// <returns>True when this object should enter the next state,
        /// false when this object should stay in the same state.</returns>
        public override bool CheckCondition(StateMachine stateMachine)
        {
            sm = (BombEnemyStateMachine)stateMachine;
            enemy = sm.GetComponent<EnemyScript>();

            if (enemy.isMobbed && !enemy.isAttacking)
            {
                return true;
            }
            return false;
        }
    };

    public class NoMoreMobTransition : Transition
    {
        private BombEnemyStateMachine sm;
        private EnemyScript enemy;

        /// <summary>
        /// When the distance to a player is greater than the attackRadius and
        /// the enemy is not attacking, this will return true.
        /// </summary>
        /// <param name="stateMachine">The stateMachine object to reference the GameObject.</param>
        /// <returns>True when this object should enter the next state,
        /// false when this object should stay in the same state.</returns>
        public override bool CheckCondition(StateMachine stateMachine)
        {
            sm = (BombEnemyStateMachine)stateMachine;
            enemy = sm.GetComponent<EnemyScript>();

            if (!enemy.isMobbed)
            {
                return true;
            }
            return false;
        }
    };
}

