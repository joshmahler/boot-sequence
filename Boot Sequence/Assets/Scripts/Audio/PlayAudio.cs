﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAudio : MonoBehaviour
{
    public string eventName;
    public enum AudioCallTime {Awake, Start, Destroy};
    public AudioCallTime calledOn;

    private void Awake()
    {
        if (calledOn == AudioCallTime.Awake)
        {
            AkSoundEngine.PostEvent(eventName, gameObject);
        }
    }
    void Start()
    {
        if(calledOn == AudioCallTime.Start)
        {
            AkSoundEngine.PostEvent(eventName, gameObject);
        }
    }

    private void OnDestroy()
    {
        if (calledOn == AudioCallTime.Destroy)
        {
            AkSoundEngine.PostEvent(eventName, gameObject);
        }
    }
}
