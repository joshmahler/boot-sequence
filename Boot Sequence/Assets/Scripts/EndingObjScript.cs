﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndingObjScript : MonoBehaviour
{
    void Start()
    {
        GameControl.control.playersFound = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
            SceneManager.LoadScene("CharacterSelect");
    }
}
