﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashScript : Energy
{
    private MovementScript movement;
    private PlayerGemScript gemScript;

    private float restoration;
    private float restorationTickRateSeconds;
    private float drain;
    private float drainTickRateSeconds;
    private float rechargeDelay;
    private bool isStarted = false;
    private bool isRecharging = false;


    [Header("DashUI")]
    public Image dashCornerUI;
    public Image dashAboveHeadUI;
    public Image dashBorder;


    public override void Start()
    {
        movement = GetComponentInParent<MovementScript>();
        gemScript = GetComponentInParent<PlayerGemScript>();

        base.Start();
        AddEnergy(EnergyDataScript.Energy.EnergyType.HOVER_BAR);
        AddEnergy(EnergyDataScript.Energy.EnergyType.DASH_BAR);
        AddEnergy(EnergyDataScript.Energy.EnergyType.SHARED_HEALTH_BAR);

        // Get the struct variables in this scripts variables to prevent me from going crazy

        restoration = energies[(int)EnergyDataScript.Energy.EnergyType.DASH_BAR].restorationIncrement;
        restorationTickRateSeconds = energies[(int)EnergyDataScript.Energy.EnergyType.DASH_BAR].restorationTickRateSeconds;

        drain = energies[(int)EnergyDataScript.Energy.EnergyType.DASH_BAR].drainDecrement;
        drainTickRateSeconds = energies[(int)EnergyDataScript.Energy.EnergyType.DASH_BAR].drainTickRateSeconds;

        rechargeDelay = energies[(int)EnergyDataScript.Energy.EnergyType.DASH_BAR].rechargeDelaySeconds;

        energyLeft = energies[(int)EnergyDataScript.Energy.EnergyType.DASH_BAR].maxValue;
    }

    public IEnumerator StartDelay(float t)
    {
        yield return new WaitForSeconds(t);
        isStarted = true;
    }

    public void Update()
    {
        if (isStarted)
        {
            if (!isRecharging && energyLeft >= drain)
            {
                movement.canDash = true;
            }
            else
            {
                movement.canDash = false;
            }
            if (movement.isDashing)
            {
                if (!activeStarted && energyLeft >= drain)
                {
                    StartCoroutine(Active(drainTickRateSeconds));
                }
            }
            else if(energyLeft < energies[(int)EnergyDataScript.Energy.EnergyType.DASH_BAR].maxValue)
            {
                if (!inactiveStarted)
                {
                    StartCoroutine(Inactive(restorationTickRateSeconds));
                }
            }
            else
            {
                if (!isFaded && !startedFade)
                {
                    StartCoroutine(FadeAfterMaxed(fadeAfterSeconds));
                }
            }

            
            if (isFaded)
            {
                dashAboveHeadUI.enabled = false;
                dashBorder.enabled = false;
            }
            else
            {
                dashAboveHeadUI.enabled = true;
                dashBorder.enabled = true;
            }
        }
    }

    /// <summary>
    /// This will decrement the energy every parameter seconds seconds.
    /// </summary>
    /// <param name="seconds">Time to wait in seconds to decrement the energy.</param>
    /// <returns></returns>
    public override IEnumerator Active(float seconds)
    {
        isFaded = false;
        energyLeft -= drain;
        dashAboveHeadUI.fillAmount = energyLeft / 100f;
        dashCornerUI.fillAmount = energyLeft / 100f;

        activeStarted = true;
        yield return new WaitForSeconds(seconds);
        activeStarted = false;

        if (energyLeft <= 0f)
        {
            energyLeft = 0f;
            StartCoroutine(RechargeTime(rechargeDelay));
        }
    }

    /// <summary>
    /// This will increment the energy every parameter seconds seconds.
    /// </summary>
    /// <param name="seconds">Time to wait in seconds to increment the energy.</param>
    /// <returns></returns>
    public override IEnumerator Inactive(float seconds)
    {
        isFaded = false;
        inactiveStarted = true;
        yield return new WaitForSeconds(seconds);
        inactiveStarted = false;

        energyLeft += restoration;
        if (energyLeft >= 100f - restoration)
        {
            energyLeft = 100f;
        }

        dashAboveHeadUI.fillAmount = energyLeft / 100f;
        dashCornerUI.fillAmount = energyLeft / 100f;
    }

    /// <summary>
    /// Waits for seconds defined in parameter.
    /// </summary>
    /// <param name="seconds">Waits this amount of seconds.</param>
    /// <returns></returns>
    public override IEnumerator RechargeTime(float seconds)
    {
        movement.canDash = false;
        isRecharging = true;
        dashAboveHeadUI.color = Color.red;
        dashCornerUI.color = Color.red;
        yield return new WaitForSeconds(seconds);
        movement.canDash = true;
        dashAboveHeadUI.color = Color.white;
        dashCornerUI.color = Color.white;
        isRecharging = false;
    }
}
