﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Energy : MonoBehaviour
{
    public List<EnergyDataScript.Energy> energies;      // List of energy types

    private EnergyDataScript energyData;            // Reference to the script

    [HideInInspector] public bool activeStarted = false;
    [HideInInspector] public bool inactiveStarted = false;
    [HideInInspector] public float energyLeft;
    [HideInInspector] public bool startedFade = false;
    [HideInInspector] public bool isFaded = false;
    [HideInInspector] public float fadeAfterSeconds = .75f;

    public virtual void Start()
    {
        energyData = GameObject.FindGameObjectWithTag("EnergyData").GetComponent<EnergyDataScript>();
        energies = new List<EnergyDataScript.Energy>();
    }

    /// <summary>
    /// This will decrement the energy every parameter seconds seconds.
    /// </summary>
    /// <param name="seconds">Time to wait in seconds to decrement the energy.</param>
    /// <returns></returns>
    public virtual IEnumerator Active(float seconds)
    {
        activeStarted = true;
        yield return new WaitForSeconds(seconds);
        activeStarted = false;
    }

    /// <summary>
    /// This will increment the energy every parameter seconds seconds.
    /// </summary>
    /// <param name="seconds">Time to wait in seconds to increment the energy.</param>
    /// <returns></returns>
    public virtual IEnumerator Inactive(float seconds)
    {
        inactiveStarted = true;
        yield return new WaitForSeconds(seconds);
        inactiveStarted = false;
    }

    /// <summary>
    /// Waits for seconds defined in parameter.
    /// </summary>
    /// <param name="seconds">Waits this amount of seconds.</param>
    /// <returns></returns>
    public virtual IEnumerator RechargeTime(float seconds)
    {
        yield return new WaitForSeconds(seconds);
    }

    public IEnumerator FadeAfterMaxed(float seconds)
    {
        startedFade = true;
        yield return new WaitForSeconds(seconds);
        startedFade = false;
        isFaded = true;
    }

    /// <summary>
    /// Adds the energy type to the dictionary.
    /// </summary>
    /// <param name="theEnergy">The index of the dictionary.</param>
    public void AddEnergy(EnergyDataScript.Energy.EnergyType theEnergy)
    {
        energies.Add(energyData.energyDictionary[(int)theEnergy]);
    }
}
