﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeUIScript : MonoBehaviour
{
    
    [Range(0, 1)] public float fadedAlpha = .4f;
    public List<Image> images;


    private enum UIPosition
    {
        INVALID = -1,
        TOP_LEFT,
        BOTTOM_LEFT,
        BOTTOM_RIGHT,
        TOP_RIGHT,
        SIZE
    };

    private UIPosition posUI;
    private bool isFaded = false;
    private bool isOpaque = true;
    private bool isPlayerIn = false;
    private int playerInIndex = -1;


    private void Start()
    {
        StartCoroutine(LateStart());
    }

    private void Update()
    {
        for(int i = 0; i < GameControl.control.players.Length; ++i)
        {
            //Get the players' position in screen space
            Vector3 playerPos = Camera.main.WorldToScreenPoint(GameControl.control.players[i].transform.position);

            //Switch to only update the corner position this UI is in
            switch(posUI)
            {
                case UIPosition.TOP_LEFT:
                    {
                        //Get the top left corner position in screen space
                        Vector3 topLeft = new Vector3(Screen.width / 5f, Screen.height - (Screen.height / 5f), 0);

                        //If the player is in the UI, check to see if said player exits
                        if(isPlayerIn && i == playerInIndex)
                        {
                            if(playerPos.x > topLeft.x || playerPos.y < topLeft.y)
                                isPlayerIn = false;
                        }

                        //If UI is not faded yet, and player is in the UI, then fade it. Set flags accordingly.
                        if (!isFaded && playerPos.x < topLeft.x && playerPos.y > topLeft.y)
                        {
                            foreach(Image image in images)
                            {
                                image.color = new Color(image.color.r, image.color.g, image.color.b, fadedAlpha);
                            }
                            isFaded = true;
                            isOpaque = false;
                            isPlayerIn = true;
                            playerInIndex = i;
                        }
                        //If UI is faded, and player exits the UI, then make it opaque. Set flags accordingly.
                        else if(!isOpaque && !isPlayerIn && (playerPos.x > topLeft.x || playerPos.y < topLeft.y))
                        {
                            foreach (Image image in images)
                            {
                                image.color = new Color(image.color.r, image.color.g, image.color.b, 1f);
                            }
                            isFaded = false;
                            isOpaque = true;
                        }
                    }
                    break;
                case UIPosition.BOTTOM_LEFT:
                    {
                        //Get the bottom left corner position in screen space
                        Vector3 bottomLeft = new Vector3(Screen.width / 5f, Screen.height / 5f, 0);

                        //If the player is in the UI, check to see if said player exits
                        if (isPlayerIn && i == playerInIndex)
                        {
                            if (playerPos.x > bottomLeft.x || playerPos.y > bottomLeft.y)
                                isPlayerIn = false;
                        }

                        //If UI is not faded yet, and player is in the UI, then fade it. Set flags accordingly.
                        if (!isFaded && playerPos.x < bottomLeft.x && playerPos.y < bottomLeft.y)
                        {
                            foreach (Image image in images)
                            {
                                image.color = new Color(image.color.r, image.color.g, image.color.b, fadedAlpha);
                            }
                            isFaded = true;
                            isOpaque = false;
                            isPlayerIn = true;
                            playerInIndex = i;
                        }
                        //If UI is faded, and player exits the UI, then make it opaque. Set flags accordingly.
                        else if (!isOpaque && !isPlayerIn && (playerPos.x > bottomLeft.x || playerPos.y > bottomLeft.y))
                        {
                            foreach (Image image in images)
                            {
                                image.color = new Color(image.color.r, image.color.g, image.color.b, 1f);
                            }
                            isFaded = false;
                            isOpaque = true;
                        }
                    }
                    break;
                case UIPosition.BOTTOM_RIGHT:
                    {
                        //Get the bottom right corner position in screen space
                        Vector3 bottomRight = new Vector3(Screen.width - (Screen.width / 5f), Screen.height / 5f, 0);

                        //If the player is in the UI, check to see if said player exits
                        if (isPlayerIn && i == playerInIndex)
                        {
                            if (playerPos.x < bottomRight.x || playerPos.y > bottomRight.y)
                                isPlayerIn = false;
                        }

                        //If UI is not faded yet, and player is in the UI, then fade it. Set flags accordingly.
                        if (!isFaded && playerPos.x > bottomRight.x && playerPos.y < bottomRight.y)
                        {
                            foreach (Image image in images)
                            {
                                image.color = new Color(image.color.r, image.color.g, image.color.b, fadedAlpha);
                            }
                            isFaded = true;
                            isOpaque = false;
                            isPlayerIn = true;
                            playerInIndex = i;
                        }
                        //If UI is faded, and player exits the UI, then make it opaque. Set flags accordingly.
                        else if (!isOpaque && !isPlayerIn && (playerPos.x < bottomRight.x || playerPos.y > bottomRight.y))
                        {
                            foreach (Image image in images)
                            {
                                image.color = new Color(image.color.r, image.color.g, image.color.b, 1f);
                            }
                            isFaded = false;
                            isOpaque = true;
                        }
                    }
                    break;
                case UIPosition.TOP_RIGHT:
                    {
                        //Get the top right corner position in screen space
                        Vector3 topRight = new Vector3(Screen.width - (Screen.width / 5f), Screen.height - (Screen.height / 5f), 0);

                        //If the player is in the UI, check to see if said player exits
                        if (isPlayerIn && i == playerInIndex)
                        {
                            if (playerPos.x < topRight.x || playerPos.y < topRight.y)
                                isPlayerIn = false;
                        }

                        //If UI is not faded yet, and player is in the UI, then fade it. Set flags accordingly.
                        if (!isFaded && playerPos.x > topRight.x && playerPos.y > topRight.y)
                        {
                            foreach (Image image in images)
                            {
                                image.color = new Color(image.color.r, image.color.g, image.color.b, fadedAlpha);
                            }
                            isFaded = true;
                            isOpaque = false;
                            isPlayerIn = true;
                            playerInIndex = i;
                        }
                        //If UI is faded, and player exits the UI, then make it opaque. Set flags accordingly.
                        else if (!isOpaque && !isPlayerIn && (playerPos.x < topRight.x || playerPos.y < topRight.y))
                        {
                            foreach (Image image in images)
                            {
                                image.color = new Color(image.color.r, image.color.g, image.color.b, 1f);
                            }
                            isFaded = false;
                            isOpaque = true;
                        }
                    }
                    break;
            }
        }
    }

    private IEnumerator LateStart()
    {
        yield return new WaitForSeconds(1f);

        //Assign the UI position based on each player's id
        switch (transform.parent.parent.GetComponent<HeroScript>().id)
        {
            case 1:
                posUI = UIPosition.TOP_LEFT;
                break;
            case 2:
                posUI = UIPosition.BOTTOM_LEFT;
                break;
            case 3:
                posUI = UIPosition.BOTTOM_RIGHT;
                break;
            case 4:
                posUI = UIPosition.TOP_RIGHT;
                break;
        }
    }
}
