﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoRotateScript : MonoBehaviour
{
    private Quaternion rotation;

    void Awake()
    {
        rotation = transform.localRotation;
    }
    void LateUpdate()
    {
        transform.rotation = rotation;
    }
}
