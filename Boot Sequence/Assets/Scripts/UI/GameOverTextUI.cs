﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameOverTextUI : MonoBehaviour
{
    private Text goText;
    private Text flavText;
    private string[] mFlavor =
    {
        "MORE DUST. MORE ASHES. MORE DISAPPOINTMENT.", "TIME FOR A NAP.", "IS THIS REALLY THE END?",
        "THERE CAN BE NO HOPE IN THIS HELL. NO HOPE AT ALL.", "OUTPUT OPTIMIZED FOR MASS BUG PRODUCTION.",
        "REMIND YOURSELF THAT OVERCONFIDENCE IS A SLOW AND INSIDIOUS KILLER",
        "BEEP BOOP... HELLO WORLD?", "IS THIS YOUR TRUE POTENTIAL?",
        "KNIGHTS STILL FOOLISHLY CONSIDER THEMSELVES ENTITIES SEPARATE FROM THE WHOLE."
    };

    // Start is called before the first frame update
    void Start()
    {
        System.Random rand = new System.Random(Guid.NewGuid().GetHashCode());
        goText = GameObject.Find("ScoreText").GetComponent<Text>();
        goText.text = "YOU SURVIVED " + GameControl.control.holder.mOldFloorsTraveresed + " FLOORS.";
        flavText = GameObject.Find("FlavorText").GetComponent<Text>();
        flavText.text = mFlavor[rand.Next(mFlavor.Length)];
        
    }
}
