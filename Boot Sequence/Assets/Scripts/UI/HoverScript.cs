﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HoverScript : Energy
{
    private MovementScript movement;
    private PlayerGemScript gemScript;
    private HeroScript hero;

    private float restoration;
    private float restorationTickRateSeconds;
    private float drain;
    private float drainTickRateSeconds;
    private float rechargeDelay;

    [Header("HoverUI")]
    public Image hoverCornerUI;
    public Image hoverAboveHeadUI;
    public Image hoverBorder;



    public override void Start()
    {
        movement = GetComponentInParent<MovementScript>();
        gemScript = GetComponentInParent<PlayerGemScript>();
        hero = GetComponentInParent<HeroScript>();

        base.Start();
        AddEnergy(EnergyDataScript.Energy.EnergyType.HOVER_BAR);
        AddEnergy(EnergyDataScript.Energy.EnergyType.DASH_BAR);
        AddEnergy(EnergyDataScript.Energy.EnergyType.SHARED_HEALTH_BAR);

        // Get the struct variables in this scripts variables to prevent me from going crazy
        restoration = energies[(int)EnergyDataScript.Energy.EnergyType.HOVER_BAR].restorationIncrement;
        restorationTickRateSeconds = energies[(int)EnergyDataScript.Energy.EnergyType.HOVER_BAR].restorationTickRateSeconds;

        drain = energies[(int)EnergyDataScript.Energy.EnergyType.HOVER_BAR].drainDecrement;
        drainTickRateSeconds = energies[(int)EnergyDataScript.Energy.EnergyType.HOVER_BAR].drainTickRateSeconds;

        rechargeDelay = energies[(int)EnergyDataScript.Energy.EnergyType.HOVER_BAR].rechargeDelaySeconds;

        energyLeft = energies[(int)EnergyDataScript.Energy.EnergyType.HOVER_BAR].maxValue;
    }

    public void Update()
    {
        if (movement.isHovering)
        {
            if (!activeStarted)
            {
                StartCoroutine(Active(drainTickRateSeconds));
            }
        }
        else if (energyLeft < energies[(int)EnergyDataScript.Energy.EnergyType.HOVER_BAR].maxValue)
        {
            if (!inactiveStarted /*&& hero.GetIsGrounded()*/)
            {
                StartCoroutine(Inactive(restorationTickRateSeconds));
            }
        }
        else
        {
            if (!isFaded && !startedFade)
            {
                StartCoroutine(FadeAfterMaxed(fadeAfterSeconds));
            }
        }

        if (isFaded)
        {
            hoverAboveHeadUI.enabled = false;
            hoverBorder.enabled = false;
        }
        else
        {
            hoverAboveHeadUI.enabled = true;
            hoverBorder.enabled = true;
        }
        
    }

    /// <summary>
    /// This will decrement the energy every parameter seconds seconds.
    /// </summary>
    /// <param name="seconds">Time to wait in seconds to decrement the energy.</param>
    /// <returns></returns>
    public override IEnumerator Active(float seconds)
    {
        isFaded = false;
        activeStarted = true;
        yield return new WaitForSeconds(seconds);
        activeStarted = false;

        energyLeft -= drain + gemScript.hoverEnergyMod;
        if (energyLeft <= 0f)
        {
            energyLeft = 0f;
            movement.isHovering = false;
            StartCoroutine(RechargeTime(rechargeDelay));
        }

        hoverAboveHeadUI.fillAmount = energyLeft / 100f;
        hoverCornerUI.fillAmount = energyLeft / 100f;
    }

    /// <summary>
    /// This will increment the energy every parameter seconds seconds.
    /// </summary>
    /// <param name="seconds">Time to wait in seconds to increment the energy.</param>
    /// <returns></returns>
    public override IEnumerator Inactive(float seconds)
    {
        isFaded = false;
        inactiveStarted = true;
        yield return new WaitForSeconds(seconds);
        inactiveStarted = false;

        energyLeft += restoration;
        if (energyLeft >= 100f - restoration)
        {
            energyLeft = 100f;
        }

        hoverAboveHeadUI.fillAmount = energyLeft / 100f;
        hoverCornerUI.fillAmount = energyLeft / 100f;
    }

    /// <summary>
    /// Waits for seconds defined in parameter.
    /// </summary>
    /// <param name="seconds">Waits this amount of seconds.</param>
    /// <returns></returns>
    public override IEnumerator RechargeTime(float seconds)
    {
        movement.canHover = false;
        hoverAboveHeadUI.color = Color.red;
        hoverCornerUI.color = Color.red;
        yield return new WaitForSeconds(seconds);
        movement.canHover = true;
        hoverAboveHeadUI.color = Color.white;
        hoverCornerUI.color = Color.white;
    }
}
