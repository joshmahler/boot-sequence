﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class EnergyDataScript : MonoBehaviour
{
    public Dictionary<int, Energy> energyDictionary;

    public struct Energy
    {
        public enum EnergyType
        {
            INVALID = -1,
            HOVER_BAR,
            DASH_BAR,
            SHARED_HEALTH_BAR,
            ENERGY_SIZE
        };

        public EnergyType energyType;               // Index of the referenced thing
        public float maxValue;                      // Max value of the bar
        public float restorationIncrement;          // Increment when not in use
        public float restorationTickRateSeconds;    // How often to increment
        public float drainDecrement;                // Decrement when in use
        public float drainTickRateSeconds;          // How often to decrement
        public float rechargeDelaySeconds;          // When at 0 energy, how long it takes to be able to use again
    }

    private Energy energy;
    private int id = -1;

    private void Awake()
    {
        energyDictionary = new Dictionary<int, Energy>();
        PopulateDictionary();
    }

    private void PopulateDictionary()
    {
        string filename = "/Data/energyInfo.txt";
        StreamReader reader = new StreamReader(Application.streamingAssetsPath + filename);

        while (!reader.EndOfStream)
        {
            string name = reader.ReadLine();
            int energyIndex;
            if (!int.TryParse(reader.ReadLine(), out energyIndex))
            {
                Utility.Log("Not a valid energy index." + energyIndex);
            }
            else if ((Energy.EnergyType)energyIndex < Energy.EnergyType.INVALID || (Energy.EnergyType)energyIndex > Energy.EnergyType.ENERGY_SIZE)
            {
                Utility.Log("The skill index is too small or too large.");
            }
            else
            {
                energy.energyType = (Energy.EnergyType)energyIndex;
            }

            reader.ReadLine();
            float maximum;
            if (!float.TryParse(reader.ReadLine(), out maximum))
            {
                Utility.Log("Not a valid max value");
            }
            else
            {
                energy.maxValue = maximum;
            }


            reader.ReadLine();
            float increment;
            if (!float.TryParse(reader.ReadLine(), out increment))
            {
                Utility.Log("Not a valid active value");
            }
            else
            {
                energy.restorationIncrement = increment;
            }

            reader.ReadLine();
            float restorationSeconds;
            if (!float.TryParse(reader.ReadLine(), out restorationSeconds))
            {
                Utility.Log("Not a valid active value");
            }
            else
            {
                energy.restorationTickRateSeconds = restorationSeconds;
            }

            reader.ReadLine();
            float decrement;
            if (!float.TryParse(reader.ReadLine(), out decrement))
            {
                Utility.Log("Not a valid active value");
            }
            else
            {
                energy.drainDecrement = decrement;
            }

            reader.ReadLine();
            float drainSeconds;
            if (!float.TryParse(reader.ReadLine(), out drainSeconds))
            {
                Utility.Log("Not a valid active value");
            }
            else
            {
                energy.drainTickRateSeconds = drainSeconds;
            }

            reader.ReadLine();
            float recharge;
            if (!float.TryParse(reader.ReadLine(), out recharge))
            {
                Utility.Log("Not a valid active value");
            }
            else
            {
                energy.rechargeDelaySeconds = recharge;
            }

            energyDictionary.Add(++id, energy);
        }

        reader.Close();
    }
}
