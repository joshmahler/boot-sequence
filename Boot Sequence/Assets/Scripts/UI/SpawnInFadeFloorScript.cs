﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnInFadeFloorScript : MonoBehaviour
{
    public Text floorText, flavorText, persistentText;
    private bool fadedUp;
    private bool fadeDown;
    private bool finalFadeUp;

    private void Start()
    {

    }

    private void Update()
    {
        if (!fadedUp)
        {
            fadedUp = true;
            floorText.text = "FLOOR          " + GameControl.control.holder.mFloorsTraveresed;
            flavorText.text = GameControl.control.GetAdj() + " " + GameControl.control.GetNoun();
            persistentText.text = "FLOOR " + GameControl.control.holder.mFloorsTraveresed + " - " + flavorText.text;
            floorText.canvasRenderer.SetAlpha(0f);
            flavorText.canvasRenderer.SetAlpha(0f);
            persistentText.canvasRenderer.SetAlpha(0f);
            floorText.CrossFadeAlpha(1.0f, 0.5f, true);
            flavorText.CrossFadeAlpha(1.0f, 0.5f, true);
            StartCoroutine(WaitFadeDown(2.5f));
        }

        if (fadeDown)
        {
            fadeDown = false;
            //floorText.canvasRenderer.SetAlpha(1f);
            //flavorText.canvasRenderer.SetAlpha(1f);
            floorText.CrossFadeAlpha(0.0f, 0.5f, true);
            flavorText.CrossFadeAlpha(0.0f, 0.5f, true);
            StartCoroutine(WaitFinalFadeUp(0.5f));
        }

        if (finalFadeUp)
        {
            finalFadeUp = false;
            persistentText.CrossFadeAlpha(1.0f, 0.5f, true);
        }
    }

    IEnumerator WaitFadeDown(float t)
    {
        yield return new WaitForSeconds(t);
        fadeDown = true;
    }

    IEnumerator WaitFinalFadeUp(float t)
    {
        yield return new WaitForSeconds(t);
        finalFadeUp = true;
    }
}
