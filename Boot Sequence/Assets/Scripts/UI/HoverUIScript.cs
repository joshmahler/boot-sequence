﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HoverUIScript : MonoBehaviour
{
    public float hoverDecrement;
    public float hoverIncrement;
    public float dashDecrement;
    public float dashIncrement;
    public float hoverRechargeSeconds;
    public float dashRechargeSeconds;
    public float hoverAtZeroRechargeSeconds;
    public float dashAtZeroRechargeSeconds;
    public MeshRenderer dashBar;
    public MeshRenderer hoverBar;

    private Vector3 dashBarScale;
    private Vector3 hoverBarScale;
    private Color originalDashBarColor;
    private Color originalHoverBarColor;
    private float hoverLeft;
    private float dashLeft;
    private MovementScript movement;
    private bool hoverActiveStarted = false;
    private bool hoverInactiveStarted = false;
    private bool dashActiveStarted = false;
    private bool dashInactiveStarted = false;

    private PlayerGemScript gemScript;


    void Start()
    {
        gemScript = GetComponent<PlayerGemScript>();
        hoverLeft = 100f;
        dashLeft = 100f;
       // dashBarScale = dashBar.transform.localScale;
        //hoverBarScale = hoverBar.transform.localScale;
       // originalDashBarColor = dashBar.material.color;
       // originalHoverBarColor = hoverBar.material.color;
        movement = GetComponent<MovementScript>();
    }

    void Update()
    {
        if(dashLeft >= dashDecrement)
        {
            dashBar.material.color = originalDashBarColor;
            movement.canDash = true;
        }
        else
        {
            dashBar.material.color = Color.red;
            movement.canDash = false;
        }
        if(!movement.canHover)
        {
            hoverBar.material.color = Color.red;
        }
        else if (hoverLeft >= hoverDecrement * 8f)
        {
            hoverBar.material.color = originalHoverBarColor;
        }
        else
        {
            hoverBar.material.color = Color.Lerp(originalHoverBarColor, Color.red, 1 - hoverLeft * 0.01f);
        }

        if (movement.isHovering)
        {
            if(!hoverActiveStarted)
            {
                StartCoroutine(HoverActive(hoverRechargeSeconds / 2f));
            }
        }
        else
        {
            if(!hoverInactiveStarted)
            {
                StartCoroutine(HoverInactive(hoverRechargeSeconds / 2f));
            }
        }
        
        if(movement.isDashing)
        {
            if(!dashActiveStarted && hoverLeft >= dashDecrement)
            {
                StartCoroutine(DashActive(dashRechargeSeconds));
            }
        }
        else
        {
            if (!dashInactiveStarted)
            {
                StartCoroutine(DashInactive(hoverRechargeSeconds / 2f));
            }
        }
    }

    public void UpdateHoverBar()
    {
        hoverBar.transform.localScale = new Vector3(hoverBarScale.x * hoverLeft * 0.01f, hoverBarScale.y, hoverBarScale.z);
        AkSoundEngine.SetRTPCValue("HoverEnergy", hoverLeft);
    }

    public void UpdateDashBar()
    {
        dashBar.transform.localScale = new Vector3(dashBarScale.x * dashLeft * 0.01f, dashBarScale.y, dashBarScale.z);
        AkSoundEngine.SetRTPCValue("DashEnergy", dashLeft);
    }

    IEnumerator HoverActive(float t)
    {
        hoverActiveStarted = true;
        yield return new WaitForSeconds(t);
        hoverActiveStarted = false;
        hoverLeft -= hoverDecrement + gemScript.hoverEnergyMod;
        if(hoverLeft <= 0f + hoverDecrement)
        {
            hoverLeft = 0f;
            movement.isHovering = false;
            StartCoroutine(HoverRechargeTime(hoverAtZeroRechargeSeconds));
        }
        UpdateHoverBar();
    }

    IEnumerator HoverInactive(float t)
    {
        hoverInactiveStarted = true;
        yield return new WaitForSeconds(t);
        hoverInactiveStarted = false;
        hoverLeft += hoverIncrement;
        if (hoverLeft >= 100f - hoverIncrement)
        {
            hoverLeft = 100f;
        }
        UpdateHoverBar();
    }

    IEnumerator DashActive(float t)
    {
        dashActiveStarted = true;
        dashLeft -= dashDecrement;
        UpdateDashBar();
        yield return new WaitForSeconds(t);
        dashActiveStarted = false;
        
        if(dashLeft <= 0f + dashDecrement + gemScript.dashEnergyMod)
        {
            dashLeft = 0f;
            StartCoroutine(DashRechargeTime(dashAtZeroRechargeSeconds));
        }
    }

    IEnumerator DashInactive(float t)
    {
        dashInactiveStarted = true;
        yield return new WaitForSeconds(t);
        dashInactiveStarted = false;
        dashLeft += dashIncrement;
        if (dashLeft >= 100f - dashIncrement)
        {
            dashLeft = 100f;
        }
        UpdateDashBar();
    }

    IEnumerator HoverRechargeTime(float t)
    {
        movement.canHover = false;
        yield return new WaitForSeconds(t);
        movement.canHover = true;
    }

    IEnumerator DashRechargeTime(float t)
    {
        movement.canDash = false;
        yield return new WaitForSeconds(t);
        movement.canDash = true;
    }
}
