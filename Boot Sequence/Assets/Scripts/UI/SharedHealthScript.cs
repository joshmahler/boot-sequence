﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SharedHealthScript : Energy
{
    public Image health0;
    public Image health1;
    public Image health2;

    private HeroScript hero;
    private float restoration;
    private float restorationTickRateSeconds;
    private float drain;
    private float drainTickRateSeconds;
    private float rechargeDelay;

    public Text livesLeft;
    private bool doFade = false;
    private bool fadeDown;

    public override void Start()
    {
        hero = GetComponentInParent<HeroScript>();

        base.Start();
        AddEnergy(EnergyDataScript.Energy.EnergyType.HOVER_BAR);
        AddEnergy(EnergyDataScript.Energy.EnergyType.DASH_BAR);
        AddEnergy(EnergyDataScript.Energy.EnergyType.SHARED_HEALTH_BAR);

        // Get the struct variables in this scripts variables to prevent me from going crazy
        restoration = energies[(int)EnergyDataScript.Energy.EnergyType.SHARED_HEALTH_BAR].restorationIncrement;
        restorationTickRateSeconds = energies[(int)EnergyDataScript.Energy.EnergyType.SHARED_HEALTH_BAR].restorationTickRateSeconds;

        drain = energies[(int)EnergyDataScript.Energy.EnergyType.SHARED_HEALTH_BAR].drainDecrement;
        drainTickRateSeconds = energies[(int)EnergyDataScript.Energy.EnergyType.SHARED_HEALTH_BAR].drainTickRateSeconds;

        rechargeDelay = energies[(int)EnergyDataScript.Energy.EnergyType.SHARED_HEALTH_BAR].rechargeDelaySeconds;

        energyLeft = energies[(int)EnergyDataScript.Energy.EnergyType.SHARED_HEALTH_BAR].maxValue;

        GameControl.control.currentSharedHealth = (int)energies[(int)EnergyDataScript.Energy.EnergyType.SHARED_HEALTH_BAR].maxValue;
        livesLeft.canvasRenderer.SetAlpha(0f);
    }

    public void DecreaseSharedHealth()
    {
        GameControl.control.currentSharedHealth -= (int)energies[(int)EnergyDataScript.Energy.EnergyType.SHARED_HEALTH_BAR].drainDecrement;

        switch(GameControl.control.currentSharedHealth)
        {
            case 0:
                {
                    health0.enabled = false;
                    health1.enabled = false;
                    health2.enabled = false;
                    livesLeft.text = "0 Lives left!";
                    doFade = true;
                }
                break;
            case 1:
                {
                    health0.enabled = true;
                    health1.enabled = false;
                    health2.enabled = false;
                    livesLeft.text = "1 Life left!";
                    doFade = true;
                }
                break;
            case 2:
                {
                    health0.enabled = true;
                    health1.enabled = true;
                    health2.enabled = false;
                    livesLeft.text = "2 Lives left!";
                    doFade = true;
                }
                break;
            case 3:
                {
                    health0.enabled = true;
                    health1.enabled = true;
                    health2.enabled = true;
                }
                break;
        }
    }

    private void Update()
    {
        if (doFade)
        {
            livesLeft.CrossFadeAlpha(1f, 0.5f, true);
            StartCoroutine(LivesFade(1.5f));
            doFade = false;
        }

        if (fadeDown)
        {
            livesLeft.CrossFadeAlpha(0f, 0.5f, true);
            fadeDown = false;
        }
    }

    public override IEnumerator Active(float seconds)
    {
        activeStarted = true;
        yield return new WaitForSeconds(seconds);
        activeStarted = false;
    }

    public override IEnumerator Inactive(float seconds)
    {
        inactiveStarted = true;
        yield return new WaitForSeconds(seconds);
        inactiveStarted = false;
    }

    public override IEnumerator RechargeTime(float seconds)
    {
        yield return new WaitForSeconds(seconds);
    }

    private IEnumerator LivesFade(float wait)
    {
        yield return new WaitForSeconds(wait);
        fadeDown = true;
    }
}
