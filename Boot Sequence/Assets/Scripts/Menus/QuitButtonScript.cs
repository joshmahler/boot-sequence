﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitButtonScript : ButtonScript
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public override void Press()
    {
        base.Press();
        Application.Quit();
    }
}
