﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIButtonsScript : MonoBehaviour
{
    public GameObject[] buttons = new GameObject[3];
    int index;
    float lastY;
    bool stickReset = true;
    // Start is called before the first frame update
    void Start()
    {
        index = 0;
    }

    // Update is called once per frame
    void Update()
    {
        CheckInput();   
    }

    void CheckInput()
    {
        if (InputManager.input.DashButton(1))
        {
            buttons[index].GetComponent<ButtonScript>().Press();
        }

        float yVal = InputManager.input.MainVertical(1);

        if (stickReset)
        {
            if (yVal < -0.3f)
            {
                IncrementFromCurrent();
            }
            else if (yVal > 0.3f)
            {
                DecrementFromCurrent();
            }
        }
        else if (yVal == 0)
        {
            stickReset = true;
        }
    }

    void IncrementFromCurrent()
    {
        ++index;

        if (index == buttons.Length)
        {
            index = 0;
        }
        ResetColors();
        buttons[index].GetComponent<Image>().color = new Vector4(1f, 1f, 1f, 1f);
    }
    void DecrementFromCurrent()
    {
        --index;

        if (index < 0)
        {
            index = buttons.Length - 1;
        }
        ResetColors();
        buttons[index].GetComponent<Image>().color = new Vector4(1f, 1f, 1f, 1f);
    }
    void ResetColors()
    {
        foreach (GameObject g in buttons)
        {
            Image i = g.GetComponent<Image>();

            i.color = new Vector4(1f, 1f, 1f, 0);
        }
        stickReset = false;
    }
}
