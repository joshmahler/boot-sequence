﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveOffScreenCameraWithPlayerScript : MonoBehaviour
{
    [HideInInspector] public GameObject player;

    private Color32 red = new Color32(0xC2, 0x17, 0x16, 0xFF);
    private Color32 blue = new Color32(0x18, 0x27, 0xBF, 0xFF);
    private Color32 green = new Color32(0x4A, 0xC9, 0x1E, 0xFF);
    private Color32 purple = new Color32(0x8B, 0x00, 0xC2, 0xFF);

    private RectTransform theCircle;

    private void Start()
    {
        theCircle = GetComponent<RectTransform>();

        switch (player.GetComponent<HeroScript>().id)
        {
            case 1:
                transform.Find("Border").GetComponent<Image>().color = red;
                break;
            case 2:
                transform.Find("Border").GetComponent<Image>().color = blue;
                break;
            case 3:
                transform.Find("Border").GetComponent<Image>().color = green;
                break;
            case 4:
                transform.Find("Border").GetComponent<Image>().color = purple;
                break;
        }
}

    private void LateUpdate()
    {
        //Clamp the x and y to the edges of main camera
        Vector3 newPosition = Camera.main.WorldToScreenPoint(player.transform.position);

        float x = Mathf.Clamp(newPosition.x, 0f + (theCircle.rect.width / 2f), (Screen.width) - (theCircle.rect.width / 2f));
        float y = Mathf.Clamp(newPosition.y, 0f + (theCircle.rect.height / 2f), (Screen.height) - (theCircle.rect.height / 2f));
        newPosition = new Vector3(x, y, 0f);

        theCircle.position = newPosition;
    }
}
