﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffScreenCameraFollowScript : MonoBehaviour
{
    public Vector3 offset;

    [HideInInspector] public GameObject player;
    [HideInInspector] public GameObject mask;
    [HideInInspector] public int index;
    [HideInInspector] public bool isOffScreen = true;


    private void Start()
    {
        isOffScreen = true;    
    }

    private void LateUpdate()
    {
        if (!isOffScreen)
        {
            Camera.main.GetComponent<CameraFollow>().ResetInstantiated(index, mask);
            Camera.main.GetComponent<CameraFollow>().RemoveFromDictionary(index);
            Destroy(gameObject);
        }
        isOffScreen = Camera.main.GetComponent<CameraFollow>().CheckPlayerOffScreen(player);
        if(isOffScreen)
        {
            transform.position = player.transform.position + offset;
        }
        else
        {
            Camera.main.GetComponent<CameraFollow>().ResetInstantiated(index, mask);
            Destroy(gameObject);
        }
    }
}
