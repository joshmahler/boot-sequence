﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Camera))]
public class CameraFollow : MonoBehaviour
{ 
    public Vector3 offset;
    public float smoothTime = .5f;

    public float minZoom = 40f;
    public float maxZoom = 10f;
    public float zoomLimiter = 50f;

    public GameObject offScreenCamera;
    public GameObject cameraMask;

    public RenderTexture renderTex1;
    public RenderTexture renderTex2;
    public RenderTexture renderTex3;
    public RenderTexture renderTex4;

    private Dictionary<int, bool> playersOffScreenDictionary;  // <index of players, isPlayerOffScreen>
    private Dictionary<int, bool> playerCameraInstantiated;    // parallel to playersOffScreenDictionary before instantiation 
    private Vector3 velocity;
    private Camera cam;

    [HideInInspector] public bool found = false;

    private void Start()
    {
        cam = GetComponent<Camera>();
        playersOffScreenDictionary = new Dictionary<int, bool>();
        playerCameraInstantiated = new Dictionary<int, bool>();
        found = false;
    }

    private void Update()
    {
        if (!found)
        {
            playersOffScreenDictionary.Clear();
            playerCameraInstantiated.Clear();

            if (GameControl.control.players.Length != 0)
            {
                for (int i = 0; i < GameControl.control.players.Length; ++i)
                {
                    if (!playersOffScreenDictionary.ContainsKey(i))
                    {
                        playersOffScreenDictionary.Add(i, false);
                    }
                    if (!playerCameraInstantiated.ContainsKey(i))
                    {
                        playerCameraInstantiated.Add(i, false);
                    }
                }
                found = true;
            }
        }
    }

    private void FixedUpdate()
    {
        if (GameControl.control.players.Length == 0f)
            return;

        Move();
        Zoom();
    }

    private void LateUpdate()
    {
        CheckPlayersOffScreen();
        for (int i = 0; i < playersOffScreenDictionary.Count; ++i)
        {
            if (!playerCameraInstantiated[i] && playersOffScreenDictionary[i] && i < GameControl.control.players.Length)
            {
                GameObject cameraForOffScreenPlayer = Instantiate(offScreenCamera, GameControl.control.players[i].transform.position, Quaternion.Euler(45f, 0f, 0f), null);
                cameraForOffScreenPlayer.GetComponent<OffScreenCameraFollowScript>().player = GameControl.control.players[i];
                cameraForOffScreenPlayer.GetComponent<OffScreenCameraFollowScript>().index = i;

                GameObject offScreenCameraCanvas = Instantiate(cameraMask, Vector3.zero, Quaternion.identity, null);
                cameraForOffScreenPlayer.GetComponent<OffScreenCameraFollowScript>().mask = offScreenCameraCanvas;
                offScreenCameraCanvas.transform.GetChild(0).GetComponent<MoveOffScreenCameraWithPlayerScript>().player = GameControl.control.players[i];

                switch (GameControl.control.players[i].GetComponent<HeroScript>().id)
                {
                    case 1:
                        {
                            cameraForOffScreenPlayer.GetComponent<Camera>().targetTexture = renderTex1;
                            offScreenCameraCanvas.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<RawImage>().texture = renderTex1;
                        }
                        break;
                    case 2:
                        {
                            cameraForOffScreenPlayer.GetComponent<Camera>().targetTexture = renderTex2;
                            offScreenCameraCanvas.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<RawImage>().texture = renderTex2;
                        }
                        break;
                    case 3:
                        {
                            cameraForOffScreenPlayer.GetComponent<Camera>().targetTexture = renderTex3;
                            offScreenCameraCanvas.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<RawImage>().texture = renderTex3;
                        }
                        break;
                    case 4:
                        {
                            cameraForOffScreenPlayer.GetComponent<Camera>().targetTexture = renderTex4;
                            offScreenCameraCanvas.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<RawImage>().texture = renderTex4;
                        }
                        break;
                }

                RectTransform theCircle = offScreenCameraCanvas.transform.GetChild(0).GetComponent<RectTransform>();
                Canvas canvas = offScreenCameraCanvas.GetComponent<Canvas>();
                float scaleFactor = canvas.scaleFactor;

                //Clamp the x and y to the edges of main camera
                Vector3 newPosition = Camera.main.WorldToScreenPoint(GameControl.control.players[i].transform.position);

                float x = Mathf.Clamp(newPosition.x, 0f + (theCircle.rect.width / 2f), (Screen.width) - (theCircle.rect.width / 2f));
                float y = Mathf.Clamp(newPosition.y, 0f + (theCircle.rect.height / 2f), (Screen.height) - (theCircle.rect.height / 2f));
                newPosition = new Vector3(x / scaleFactor, y / scaleFactor, 0f);

                theCircle.position = newPosition;

                playerCameraInstantiated[i] = true;
            }
        }
    }

    public void ResetInstantiated(int index, GameObject mask)
    {
        playerCameraInstantiated[index] = false;
        Destroy(mask);
    }

    public void RemoveFromDictionary(int index)
    {
        playerCameraInstantiated.Remove(index);
        playersOffScreenDictionary.Remove(index);
    }

    public bool CheckPlayerOffScreen(GameObject player)
    {
        Vector3 screenPos = Camera.main.WorldToScreenPoint(player.transform.position);

        if (screenPos.x < 0 ||
            screenPos.y < 0 ||
            screenPos.x > Screen.width ||
            screenPos.y > Screen.height)
        {
            return true;
        }
        return false;
    }

    private void CheckPlayersOffScreen()
    {
        for (int i = 0; i < GameControl.control.players.Length; ++i)
        {
            if(CheckPlayerOffScreen(GameControl.control.players[i]))
            { 
                if(playersOffScreenDictionary.ContainsKey(i))
                    playersOffScreenDictionary[i] = true;
            }
            else
            {
                if (playersOffScreenDictionary.ContainsKey(i))
                    playersOffScreenDictionary[i] = false;
            }
        }
    }



    private void Zoom()
    {
        float newZoom = Mathf.Lerp(maxZoom, minZoom, GetGreatestDistance() / zoomLimiter);
        cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, newZoom, Time.deltaTime);
    }

    private void Move()
    {
        Vector3 centerPoint = GetCenterPoint();
        Vector3 newPosition = centerPoint + offset;
        transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref velocity, smoothTime);
    }

    private float GetGreatestDistance()
    {
        if (GameControl.control.players[0] != null)
        {
            Bounds bounds = new Bounds(GameControl.control.players[0].transform.position, Vector3.zero);
            for (int i = 0; i < GameControl.control.players.Length; ++i)
            {
                if (GameControl.control.players[i] != null)
                {
                    bounds.Encapsulate(GameControl.control.players[i].transform.position);
                }
            }

            if (bounds.size.x > bounds.size.z)
                return bounds.size.x;
            return bounds.size.z;
        }
        return 0f;
    }

    private Vector3 GetCenterPoint()
    {
        if (GameControl.control.players.Length == 1)
        {
            return GameControl.control.players[0].transform.position;
        }
        if (GameControl.control.players[0] != null)
        {
            Bounds bounds = new Bounds(GameControl.control.players[0].transform.position, Vector3.zero);

            for (int i = 0; i < GameControl.control.players.Length; ++i)
            {
                if (GameControl.control.players[i] != null)
                {
                    bounds.Encapsulate(GameControl.control.players[i].transform.position);
                }
            }
            return bounds.center;
        }
        return Vector3.zero;
    }
}
