﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
public class InputManager : MonoBehaviour
{
    //Reference this script from anywhere with "InputManager.input.'functionName'"
    public static InputManager input;
    private Player player0;
    private Player player1;
    private Player player2;
    private Player player3;
    void Awake()
    {
        player0 = ReInput.players.GetPlayer("Player0");
        player1 = ReInput.players.GetPlayer("Player1");
        player2 = ReInput.players.GetPlayer("Player2");
        player3 = ReInput.players.GetPlayer("Player3");
        if (input == null)
        {
            DontDestroyOnLoad(gameObject);
            input = this;
        }
        else if (input != this)
        {
            Destroy(gameObject);
        }
    }

    /*
     * MainHorizontal(string axis) 
     *      - axis: name of the horizontal setting in ProjectSettings
     *      - returns: A range from -1 to 1, -1 means "Go Left" and 1 means "Go Right"
     *                  0 (or deadzone range on controllers) means "No Movement"
     * Important: This may be used on controllers or keyboards,
     *              with controllers you must set a deadzone in ProjectSettings
     *              with keyboard everything is a -1, 0, or 1
     */
    public float MainHorizontal(int playerNum)
    {
        float r = 0.0f;

        switch (playerNum)
        {
            case 1:
                {
                    r += player0.GetAxis("Move Horizontal");
                }
                break;
            case 2:
                {
                    r += player1.GetAxis("Move Horizontal");
                }
                break;
            case 3:
                {
                    r += player2.GetAxis("Move Horizontal");
                }
                break;
            case 4:
                {
                    r += player3.GetAxis("Move Horizontal");
                }
                break;
        }

        return Mathf.Clamp(r, -1.0f, 1.0f);
    }

    /*
     * MainVertical(string axis) 
     *      - axis: name of the vertical setting in ProjectSettings
     *      - returns: A range from -1 to 1, -1 means "Go Down" and 1 means "Go Up"
     *                  0 (or deadzone range on controllers) means "No Movement"
     * Important: This may be used on controllers or keyboards,
     *              with controllers you must set a deadzone in ProjectSettings
     *              with keyboard everything is a -1, 0, or 1
     */
    public float MainVertical(int playerNum)
    {
        float r = 0.0f;
        switch (playerNum)
        {
            case 1:
                {
                    r += player0.GetAxis("Move Vertical");
                }
                break;
            case 2:
                {
                    r += player1.GetAxis("Move Vertical");
                }
                break;
            case 3:
                {
                    r += player2.GetAxis("Move Vertical");
                }
                break;
            case 4:
                {
                    r += player3.GetAxis("Move Vertical");
                }
                break;
        }

        return Mathf.Clamp(r, -1.0f, 1.0f);
    }

    /*
     * MainMovement(string horizAxis, string vertAxis) 
     *      - horizAxis: name of the horizontal setting in ProjectSettings
     *      - vertAxis: name of the vertical setting in ProjectSettings
     *      - returns: Vector3 where x is the MainHorizontal, y is 0, and z is the MainVertical
     */
    public Vector3 MainMovement(int playerNum)
    {
        float x = MainHorizontal(playerNum);
        float z = MainVertical(playerNum);


        return new Vector3(MainHorizontal(playerNum), 0f, MainVertical(playerNum));
    }

    /*
    * MeleeButton(string key) 
    *      - key: name of the key to be pressed in ProjectSettings
    *      - returns: bool of whether or not the melee button was pressed down
    *  Important: The key is NOT the keycode, it is the string name of the setting in ProjectSettings
    */
    public bool MeleeButton(int playerNum)
    {
        switch (playerNum)
        {
            case 1:
                {
                    return player0.GetButtonDown("Attack");
                }
                
            case 2:
                {
                    return player1.GetButtonDown("Attack");
                }
                
            case 3:
                {
                    return player2.GetButtonDown("Attack");
                }
                
            case 4:
                {
                    return player3.GetButtonDown("Attack");
                }
                
            default:
                return false;
        }
    }

    /*
    * KickButton(string key) 
    *      - key: name of the key to be pressed in ProjectSettings
    *      - returns: bool of whether or not the kick button was pressed down
    *  Important: The key is NOT the keycode, it is the string name of the setting in ProjectSettings
*/
    public bool KickButton(int playerNum)
    {
        switch (playerNum)
        {
            case 1:
                {
                    return player0.GetButtonDown("Kick");
                }
                
            case 2:
                {
                    return player1.GetButtonDown("Kick");
                }
                
            case 3:
                {
                    return player2.GetButtonDown("Kick");
                }
                
            case 4:
                {
                    return player3.GetButtonDown("Kick");
                }
                
            default:
                return false;
        }
    }

    /*
    * SpecialAttackButton(string key) 
    *      - key: name of the key to be pressed in ProjectSettings
    *      - returns: bool of whether or not the special attack button was pressed down
    *  Important: The key is NOT the keycode, it is the string name of the setting in ProjectSettings
*/
    public bool SpecialAttackButton(int playerNum)
    {
        switch (playerNum)
        {
            case 1:
                {
                    return player0.GetButtonDown("Special");
                }
                
            case 2:
                {
                    return player1.GetButtonDown("Special");
                }
                
            case 3:
                {
                    return player2.GetButtonDown("Special");
                }
                
            case 4:
                {
                    return player3.GetButtonDown("Special");
                }
                
            default:
                return false;
        }
    }

    /*
    * HoverButton(string key) 
    *      - key: name of the key to be pressed in ProjectSettings
    *      - returns: bool of whether or not the hover button is being pressed
    *  Important: The key is NOT the keycode, it is the string name of the setting in ProjectSettings
*/
    public bool HoverButton(int playerNum)
    {
        switch (playerNum)
        {
            case 1:
                {
                    return player0.GetButton("Hover");
                }
                
            case 2:
                {
                    return player1.GetButton("Hover");
                }
                
            case 3:
                {
                    return player2.GetButton("Hover");
                }
                
            case 4:
                {
                    return player3.GetButton("Hover");
                }
                
            default:
                return false;
        }
    }

    public bool HoverButtonUp(int playerNum)
    {
        switch (playerNum)
        {
            case 1:
                {
                    return player0.GetButtonUp("Hover");
                }
                
            case 2:
                {
                    return player1.GetButtonUp("Hover");
                }
                
            case 3:
                {
                    return player2.GetButtonUp("Hover");
                }
                
            case 4:
                {
                    return player3.GetButtonUp("Hover");
                }
                
            default:
                return false;
        }
    }

    public bool HoverButtonDown(int playerNum)
    {
        switch (playerNum)
        {
            case 1:
                {
                    return player0.GetButtonDown("Hover");
                }
                
            case 2:
                {
                    return player1.GetButtonDown("Hover");
                }
                
            case 3:
                {
                    return player2.GetButtonDown("Hover");
                }
                
            case 4:
                {
                    return player3.GetButtonDown("Hover");
                }
                
            default:
                return false;
        }
    }

    /*
    * DashButton(string key) 
    *      - key: name of the key to be pressed in ProjectSettings
    *      - returns: bool of whether or not the dash button was pressed down
    *  Important: The key is NOT the keycode, it is the string name of the setting in ProjectSettings
*/
    public bool DashButton(int playerNum)
    {
        switch (playerNum)
        {
            case 1:
                {
                    return player0.GetButtonDown("Dash");
                }
                
            case 2:
                {
                    return player1.GetButtonDown("Dash");
                }
                
            case 3:
                {
                    return player2.GetButtonDown("Dash");
                }
                
            case 4:
                {
                    return player3.GetButtonDown("Dash");
                }
                
            default:
                return false;
        }
    }

    /*
    * SelectionButton(string key) 
    *      - key: name of the key to be pressed in ProjectSettings
    *      - returns: bool of whether or not the selection button was pressed down
    *  Important: The key is NOT the keycode, it is the string name of the setting in ProjectSettings
*/
    public bool SelectionButton(int playerNum)
    {
        switch (playerNum)
        {
            case 1:
                {
                    return player0.GetButtonDown("Dash");
                }
                
            case 2:
                {
                    return player1.GetButtonDown("Dash");
                }
                
            case 3:
                {
                    return player2.GetButtonDown("Dash");
                }
                
            case 4:
                {
                    return player3.GetButtonDown("Dash");
                }
                
            default:
                return false;
        }

    }

    /*
    *  duration - how many seconds the rumble lasts
    *  intensity (0-1 range) - how strong the rumble is
    *  playerNum - which player should get the rumble
    */

    public bool Rumble(float duration, float intensity, int playerNum)
    {
        switch (playerNum)
        {
            case 1:
                {
                    player0.SetVibration(0, intensity, duration);
                    return true;
                }
            case 2:
                {
                    player1.SetVibration(0, intensity, duration);
                    return true;
                }
            case 3:
                {
                    player2.SetVibration(0, intensity, duration);
                    return true;
                }
            case 4:
                {
                    player3.SetVibration(0, intensity, duration);
                    return true;
                }
        }
        return false;
    }
    public bool PickUpButton(int playerNum)
    {

        switch (playerNum)
        {
            case 1:
                {
      
                    return player0.GetButtonDown("Gem Grab");
                }
                 
            case 2:
                {
                    return player1.GetButtonDown("Gem Grab");
                }
                
            case 3:
                {
                    return player2.GetButtonDown("Gem Grab");
                }
                
            case 4:
                {
                    return player3.GetButtonDown("Gem Grab");
                }
                
            default:
                return false;
        }
    }
}
