﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    //Reference this script from anywhere with "SceneLoader.loader.'functionName'"
    public static SceneLoader loader;

    void Awake()
    {
        if (loader == null)
        {
            DontDestroyOnLoad(gameObject);
            loader = this;
        }
        else if (loader != this)
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Make sure the scene that you want to load is in the build settings!
    /// </summary>
    /// <param name="sceneName">Name of the scene that will be loaded.</param>
    public void LoadScene(string sceneName)
    {
        if(sceneName == "CharacterSelect")
        {
            AkSoundEngine.SetState("music_state", "cs");
        }
        else if (sceneName == "_StartMenu")
        {
            AkSoundEngine.SetState("music_state", "cs");
        }
       
        SceneManager.LoadScene(sceneName);
    }
}
