﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = System.Random;
using UnityEngine.UI;

public class GameControl : MonoBehaviour
{
    //Reference this script from anywhere with "GameControl.control.'variableName/functionName'"
    public static GameControl control;

    //These objects may be referenced from any script at any time, see above.
    public string player1Prefab = "Knight";
    public string player2Prefab = "Mage";
    public string player3Prefab = "Knight";
    public string player4Prefab = "Mage";
    [HideInInspector] public int lockedAmount = 0;
    [HideInInspector] public int controllersAmount = 0;

    [HideInInspector] public bool isAllGeneratorsDestroyed = false;
    private int generatorsAlive;

    [HideInInspector]
    public bool doneSpawn;

    public int playersAlive;
    public GameObject[] players;

    [HideInInspector]
    public bool playersFound;

    [Tooltip("These are the base values of the type that will spawn in the level. For example, baseEnemies is the absolute minimum amount of enemies." +
             " Later modifiers will be added to them.")]
    [Range(0, 100)]
    public int baseEnemies, baseGenerators, baseTurrets;

    [Tooltip("These are the values that are added when we go further in the game. Whenever we load up a level, we add these values.")]
    [Range(0, 100)]
    public int enemiesAddPerLevel, generatorsAddPerLevel, turretsAddPerLevel;

    [Tooltip("These are the values that we add per player.")]
    [Range(0, 100)]
    public int enemiesAddPerPlayer, generatorsAddPerPlayer, turretsAddPerPlayer;

    [Tooltip("These are the weighted values for certain enemies. We will attempt to spawn that many enemies out of the total.")]
    [Range(0, 20)]
    public int basicEnemyWeight = 3, bomberEnemyWeight = 2, shockerEnemyWeight = 1;

    [HideInInspector] public int currentSharedHealth = 3;

    [HideInInspector] public SharedHealthScript shs;

    [HideInInspector] public bool firstLevel = true;
    private PlayerSpawnerScript playerSpawner;

    public List<GameObject> completelyDeadPlayers;
    public GameObject respawnHexagon;

    private int playersNeedRespawn = 0;

    //[HideInInspector]
    public int totalEnemies = 0, totalGenerators = 0, totalTurrets = 0;

    private bool firstScaleCalculated = false;

    [Tooltip("These are the absolute maximum values of the specific type. So, during gameplay, the other values may not go over its max of its type.")]
    [Range(0, 100)]
    public int MAX_ENEMIES = 0, MAX_GENERATORS = 0, MAX_TURRETS = 0;

    [Tooltip("This is how much HP is added to each enemy when a new floor is reached.")] [Range(0, 25)]
    public int walkerHealthAdd, bombHealthAdd, shockHealthAdd, turretHealthAdd;

    [Tooltip("This is how much HP is added to a generator per floor.")] [Range(0, 25)]
    public int genHealthAdd;

    [Tooltip("How many floors the players have to go through before enemies gain extra damage.")]
    [Range(0, 10)]
    public int floorToAddDmg;

    // These are the final amounts that will be added to each enemy at the start of a new level.
    [HideInInspector]
    public int walkerHealth, bombHealth, shockHealth, turretHealth, generatorHealth;

    [HideInInspector]
    public int dmgToAdd;

    [Tooltip("Describes how many floors players must go through before the size of the floor increases.")]
    [Range(0, 10)]
    public int whenIncFloorSize;

    [Tooltip("Describes how the floor size increases. ")]
    [Range(0, 5)]
    public int floorSizeFactor;

    [HideInInspector]
    public int xInc, yInc;

    //This is for flavor text.
    private List<String> adjectives = new List<String>();
    private List<String> nouns = new List<string>();

    /// <summary>
    /// Wrapper class to hold how far a player has gone. 
    /// </summary>
    public struct ScoreHolder
    {
        public int mFloorsTraveresed { get; set; }
        public int mOldFloorsTraveresed { get; set; }

        public void ResetValues()
        {
            mFloorsTraveresed = 0;
        }
    }

    [HideInInspector]
    public ScoreHolder holder;

    private void Awake()
    {
        Application.targetFrameRate = 60;
        //OnValidate();
        if (control == null)
        {
            DontDestroyOnLoad(gameObject);
            control = this;
        }
        else if (control != this)
        {
            Destroy(gameObject);
        }

        holder.mFloorsTraveresed = 1;
        completelyDeadPlayers = new List<GameObject>();
        GetFlavorText();
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    /// <summary>
    /// Do stuff every time a level is loaded. Such as cleaning up certain DDOL objects, re-finding objects in the
    /// scene, and so on.
    /// </summary>
    private void OnLoadLevel()
    {
        playersFound = false;
        for (int i = 0; i < completelyDeadPlayers.Count; ++i)
        {
            Utility.Log("Reactivating player " + i);
            completelyDeadPlayers[i].SetActive(true);
            RemoveGem(completelyDeadPlayers[i]);

            SharedHealthScript sharedHealth = GameObject.FindGameObjectWithTag("ScreenSpaceUI").transform.GetChild(0).GetComponent<SharedHealthScript>();
            sharedHealth.energyLeft = sharedHealth.energies[(int)EnergyDataScript.Energy.EnergyType.SHARED_HEALTH_BAR].maxValue;

            HeroScript hero = completelyDeadPlayers[i].GetComponent<HeroScript>();
            hero.TakeDamage(-9999);
            hero.isStunned = false;

            FindPlayers();
        }
        completelyDeadPlayers.Clear();

        shs = GameObject.FindGameObjectWithTag("ScreenSpaceUI").transform.GetChild(0).GetComponent<SharedHealthScript>();
        playerSpawner = FindObjectOfType<PlayerSpawnerScript>();
        playerSpawner.Spawn();
        StartCoroutine(playerSpawner.LetPlayersMoveAgain(3));
        firstLevel = false;
        AkSoundEngine.SetState("music_state", "dungeon");
        GameObject[] generators = GameObject.FindGameObjectsWithTag("Generator");
        generatorsAlive = generators.Length;

        doneSpawn = false;
        FindPlayers();

        foreach (GameObject g in players)
        {
            Transform background = g.transform.Find("ScreenSpaceCanvas").GetChild(0);

            DashScript dash = background.Find("DashHolder").GetComponent<DashScript>();
            dash.StartCoroutine(dash.StartDelay(0.2f));
        }
    }

    private void Update()
    {
        if(doneSpawn)
            OnLoadLevel();

        if(!playersFound)
            FindPlayers();

        /*
        if(SceneManager.GetActiveScene().name == "WFC_Test")
            CheckPlayerDead();*/
    }

    /// <summary>
    /// Find all of the players in the game.
    /// </summary>
    private void FindPlayers()
    {
        players = GameObject.FindGameObjectsWithTag("Player");
        if (Camera.main.GetComponent<CameraFollow>())
        {
            Camera.main.GetComponent<CameraFollow>().found = false;
        }

        playersAlive = players.Length;

        Utility.Log("<color=red> There are " + playersAlive + " players alive.</color>");
        if(playersAlive != 0)
            playersFound = true;
    }

    /// <summary>
    /// Notify GC that a player has died, and do stuff accordingly.
    /// </summary>
    /// <param name="player">The player who was just killed.</param>
    public void KillPlayer(GameObject player)
    {
        HeroScript hero = player.GetComponent<HeroScript>();

        if (currentSharedHealth > 0)
        {
            hero.TakeDamage(-9999f);
            shs.DecreaseSharedHealth();
            hero.isStunned = false;

            RemoveGem(player);

            player.transform.position = new Vector3(hero.ground.transform.position.x, 1f, hero.ground.transform.position.z);

            //StartCoroutine(DeactivatePlayer(player));
        }
        else
        {
            completelyDeadPlayers.Add(player);
            player.GetComponent<MovementScript>().canMove = false;
            player.GetComponent<HeroScript>().isAttacking = false;
            player.SetActive(false);

            if (--playersAlive <= 0)
            {
                SceneManager.LoadScene("GameOverMenu");
                completelyDeadPlayers.Clear();

                foreach (GameObject g in players)
                {
                    Destroy(g);
                }
                firstLevel = true;
            }

            players = GameObject.FindGameObjectsWithTag("Player");
            Camera.main.GetComponent<CameraFollow>().found = false;

            GameObject[] offScreenCameras = GameObject.FindGameObjectsWithTag("OffScreenCamera");
            for (int i = 0; i < offScreenCameras.Length; ++i)
            {
                if (offScreenCameras[i].GetComponent<OffScreenCameraFollowScript>().player == player)
                {
                    offScreenCameras[i].GetComponent<OffScreenCameraFollowScript>().isOffScreen = false;
                }
            }

            Camera.main.GetComponent<CameraFollow>().found = false;
        }
    }

    private void RemoveGem(GameObject player)
    {
        HeroScript hero = player.GetComponent<HeroScript>();
        Transform background = player.transform.Find("ScreenSpaceCanvas").GetChild(0);
        FadeUIScript fadeUI = background.GetComponent<FadeUIScript>();

        DashScript dash = background.Find("DashHolder").GetComponent<DashScript>();
        dash.energyLeft = dash.energies[(int)EnergyDataScript.Energy.EnergyType.DASH_BAR].maxValue;
        dash.StartCoroutine(dash.Inactive(0.01f));
        HoverScript hover = background.Find("HoverHolder").GetComponent<HoverScript>();
        hover.energyLeft = hover.energies[(int)EnergyDataScript.Energy.EnergyType.HOVER_BAR].maxValue;

        PlayerGemScript playerGem = player.GetComponent<PlayerGemScript>();
        GemScript gem = playerGem.gem;

        if (gem != null)
        {
            fadeUI.images.Remove(playerGem.gemHolder.GetChild(0).GetComponent<Image>());
            fadeUI.images.Remove(playerGem.circuitHolder.GetChild(0).GetComponent<Image>());

            if (gem.currentHoverPool != null)
                Destroy(gem.currentHoverPool);
            if (gem.currentDashBox != null)
                Destroy(gem.currentDashBox);

            if (playerGem.gemHolder.childCount > 0)
                Destroy(playerGem.gemHolder.GetChild(0).gameObject);
            if (playerGem.circuitHolder.childCount > 0)
                Destroy(playerGem.circuitHolder.GetChild(0).gameObject);

            hero.kickParticle = playerGem.origKickParticle;

            Destroy(gem.gameObject);
        }
        gem = null;

        AkSoundEngine.SetSwitch("element", "null", player);
    }

    /*
    private IEnumerator DeactivatePlayer(GameObject player)
    {
        foreach(Transform mesh in player.transform.Find("g_mesh"))
        {
            mesh.GetComponent<SkinnedMeshRenderer>().enabled = false;
        }
        HeroScript hero = player.GetComponent<HeroScript>();
        player.transform.position = new Vector3(hero.ground.transform.position.x, 1f, hero.ground.transform.position.z);
        MovementScript move = player.GetComponent<MovementScript>();
        move.canHover = true;
        move.canDash = true;
        move.canMove = true;
        move.enabled = false;

        hero.enabled = false;
        player.GetComponent<Rigidbody>().useGravity = false;
        GameObject canvas = player.transform.Find("Canvas").gameObject;
        canvas.SetActive(false);
        Transform cornerBackground = player.transform.Find("ScreenSpaceCanvas").GetChild(0);

        Energy dash = cornerBackground.Find("DashHolder").GetComponent<Energy>();
        dash.activeStarted = false;
        dash.inactiveStarted = false;
        dash.energyLeft = dash.energies[1].maxValue;
        dash.startedFade = false;

        Energy hover = cornerBackground.Find("HoverHolder").GetComponent<Energy>();
        hover.activeStarted = false;
        hover.inactiveStarted = false;
        hover.energyLeft = hover.energies[0].maxValue;
        hover.startedFade = false;
        

        yield return new WaitForSeconds(1.1f);

        foreach (Transform mesh in player.transform.Find("g_mesh"))
        {
            mesh.GetComponent<SkinnedMeshRenderer>().enabled = true;
        }
        player.GetComponent<MovementScript>().enabled = true;
        hero.enabled = true;
        canvas.SetActive(true);
        player.GetComponent<Rigidbody>().useGravity = true;
        player.SetActive(false);
    }
    */

    /// <summary>
    /// Check whether a player is dead or not.
    /// </summary>
    private void CheckPlayerDead()
    {
        if (playersAlive <= 0)
            SceneManager.LoadScene("GameOverMenu");
    }

    /// <summary>
    /// Do something when a generator is destroyed so that we can modify the ending script.
    /// </summary>
    public void DestroyGenerator()
    {
        if (--generatorsAlive <= 0)
        {
            isAllGeneratorsDestroyed = true;
        }
    }

    /// <summary>
    /// Calculate the first scales for scaling things. Total enemies, generators, turrets, etc. This will not be called
    /// again until the players retry.
    /// </summary>
    public void CalculateFirstScale()
    {
        if (firstScaleCalculated)
            return;

        totalEnemies = baseEnemies + enemiesAddPerPlayer * controllersAmount;
        totalGenerators = baseGenerators + generatorsAddPerPlayer * controllersAmount;
        totalTurrets = baseTurrets + turretsAddPerPlayer * controllersAmount;
        firstScaleCalculated = true;
    }

    /// <summary>
    /// Scale things when the players complete a level accordingly.
    /// </summary>
    public void RecalcScales()
    {
        if (totalEnemies < MAX_ENEMIES)
            totalEnemies += enemiesAddPerLevel;

        if (totalGenerators < MAX_GENERATORS)
            totalGenerators += generatorsAddPerLevel;

        if (totalTurrets < MAX_TURRETS)
            totalTurrets += turretsAddPerLevel;

        walkerHealth += walkerHealthAdd;
        bombHealth += bombHealthAdd;
        shockHealth += shockHealthAdd;
        turretHealth += turretHealthAdd;
        generatorHealth += genHealthAdd;

        // If the floor is divisable by 5, then add one point of damage, otherwise, add none.
        if (holder.mFloorsTraveresed % floorToAddDmg == 0)
        {
            dmgToAdd += 25;
            TurretEnemyStateMachine.incDamage = true;
        }
        //dmgToAdd += (holder.mFloorsTraveresed) % floorToAddDmg == 0 ? 25 : 0;

        ++holder.mFloorsTraveresed;

        if(holder.mFloorsTraveresed % whenIncFloorSize == 0)
            CalcFloorSize();
    }

    /// <summary>
    /// Reset scaled values to bases when all players die.
    /// </summary>
    private void resetVals()
    {
        totalEnemies = baseEnemies;
        totalGenerators = baseGenerators;
        totalTurrets = baseTurrets;
        firstScaleCalculated = false;
        holder.mOldFloorsTraveresed = holder.mFloorsTraveresed;
        holder.ResetValues();
        holder.mFloorsTraveresed = 1;
        walkerHealth = 0;
        bombHealth = 0;
        shockHealth = 0;
        turretHealth = 0;
        generatorHealth = 0;
        dmgToAdd = 0;
        xInc = 0;
        yInc = 0;
    }

    /// <summary>
    /// Do things when a scene is loaded. Generally just changing the scaling values.
    /// </summary>
    /// <param name="scene">The scene that is being loaded.</param>
    /// <param name="mode">The way we are loading the new scene.</param>
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if(scene.name == "WFC_Test")
        {
            if(firstScaleCalculated)
                RecalcScales();
            CalculateFirstScale();
        }
        else if (scene.name == "CharacterSelect")
        {
            AkSoundEngine.SetState("music_state", "cs");
        }
        else
            resetVals();
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    /// <summary>
    /// Read the files for flavor text.
    /// </summary>
    private void GetFlavorText()
    {
        string path = "/Data/FlavorText/";
        string nPath = path + "Nouns.txt";
        string aPath = path + "Adjectives.txt";
        StreamReader nReader = new StreamReader(Application.streamingAssetsPath + nPath);
        StreamReader aReader = new StreamReader(Application.streamingAssetsPath + aPath);

        while (!nReader.EndOfStream)
        {
            nouns.Add(nReader.ReadLine());
        }

        while (!aReader.EndOfStream)
        {
            adjectives.Add(aReader.ReadLine());
        }

        nReader.Close();
        aReader.Close();
    }

    public string GetNoun()
    {
        return nouns[UnityEngine.Random.Range(0, nouns.Count)];
    }

    public string GetAdj()
    {
        return adjectives[UnityEngine.Random.Range(0, adjectives.Count)];
    }

    private void CalcFloorSize()
    {
        for (int i = 0; i < floorSizeFactor; ++i)
        {
            int which = UnityEngine.Random.Range(0, 2);
            if (which == 0)
                ++xInc;
            else
                ++yInc;
        }
    }
}
