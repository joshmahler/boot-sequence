﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerIndicatorScript : MonoBehaviour
{
   public GameObject player;
   private Image img;
    // Start is called before the first frame update
    void Start()
    {
      int playerNum = player.GetComponent<HeroScript>().id;
      img = GetComponent<Image>();

    if (playerNum == 1)
    {
         img.color = Color.blue;
    }
    if (playerNum == 2)
    {
        img.color = Color.red;
    }
     if (playerNum == 3)
     {
       img.color = Color.yellow;
     }
      if (playerNum == 4)
      {
         img.color = Color.green;
      }
      else if (playerNum == 0)
      {
         img.color = new Vector4(0, 0, 0, 0);
      }

}

// Update is called once per frame
void Update()
    {
        
    }
}
