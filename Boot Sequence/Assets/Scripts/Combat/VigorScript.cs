﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class VigorScript : ProjectileScript
{
    public float heal = 20f;
    public float healWaitFrames = 15f;
    public LayerMask playerLayerMask;

    private Dictionary<Collider, bool> hitDictionary; /* Dictionary so that when player A walks into the vigor sphere with 
                                                       * player B already in the vigor, player A starts the dot effect immediately
                                                       */

    void Start()
    {
        Destroy(gameObject, lifetime);
        hitDictionary = new Dictionary<Collider, bool>();
    }

    private void Update()
    {
        Vigor();
    }

    /// <summary>
    /// Vigor gets called from Update().
    /// If multiple objects are being hit,
    /// then go through each object's collider 
    /// and either heal players or damage enemies
    /// that enter the collider.
    /// </summary>
    void Vigor()
    {
        Collider[] hit = Physics.OverlapSphere(transform.position, GetComponent<SphereCollider>().radius, playerLayerMask);
        for (int i = 0; i < hit.Length; ++i)
        {
            if (!hitDictionary.ContainsKey(hit[i]))
            {
                hitDictionary.Add(hit[i], false);
            }

            if (!hitDictionary[hit[i]] && hit[i].gameObject.tag == "Player")
            {
                StartCoroutine(PlayerHit(hit, i, healWaitFrames));
            }
        }
    }

    /// <summary>
    /// Heal players that are hit as soon as
    /// they enter the radius of the vigor.
    /// </summary>
    /// <param name="hit">The collider array set in Vigor().</param>
    /// <param name="index">The index of the current collider.</param>
    /// <param name="t">The amount of seconds to wait for the dot heal.</param>
    /// <returns></returns>
    IEnumerator PlayerHit(Collider[] hit, int index, float t)
    {
        t /= 60f;
        hitDictionary[hit[index]] = true;
        Utility.Log("healed " + heal);
        HeroScript hero = hit[index].gameObject.GetComponent<HeroScript>();
        hero.TakeDamage(-heal);
        yield return new WaitForSeconds(t);
        hitDictionary[hit[index]] = false;
    }
}
