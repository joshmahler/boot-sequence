﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyScript : Entity
{
    [HideInInspector] public bool isMobbed = false;
    protected bool isSpot = false;

    private Vector3 pos;        // The position of the player character closest to this enemy

    public string enemyDeathEvent;

    public Animator anim;
    public GameObject mesh;
    private MaterialPropertyBlock[] propertyBlock;
    private Transform[] meshParts;

    [HideInInspector] public float closestPlayerDist;
    [HideInInspector] public int closestPlayerIndex;

    public override void Start()
    {
        base.Start();
        isStunned = false;
        isSpot = false;
        propertyBlock = new MaterialPropertyBlock[mesh.transform.childCount];
        meshParts = new Transform[mesh.transform.childCount];
        for(int i = 0; i < mesh.transform.childCount; ++i)
        {
            Transform current = mesh.transform.GetChild(i);
            meshParts[i] = current;
            propertyBlock[i] = new MaterialPropertyBlock();
            if (current.GetComponent<Renderer>())
            {
                current.GetComponent<Renderer>().GetPropertyBlock(propertyBlock[i]);
            }

            propertyBlock[i].SetFloat("_Alpha", 0f);
            propertyBlock[i].SetColor("_EmissiveCustom", Color.cyan);

            current.GetComponent<Renderer>().SetPropertyBlock(propertyBlock[i]);
        }  
    }

    public override void Update()
    {
        base.Update();
        closestPlayerDist = 99999f;
        for (int i = 0; i < GameControl.control.players.Length; ++i)
        {
            if (GameControl.control.players[i] != null)
            {
                if (GameControl.control.players[i].activeInHierarchy)
                {
                    float distance = Vector3.Distance(transform.position, GameControl.control.players[i].transform.position);
                    if (distance < closestPlayerDist)
                    {
                        closestPlayerDist = distance;
                        closestPlayerIndex = i;
                    }
                }
            }
        }
    }
    /// <summary>
    /// This decreases the health of enemy 
    /// by the amount defined in damage.
    /// </summary>
    /// <param name="damage">Amount of damage to deal to enemy health.</param>
    public override void TakeDamage(float damage)
    {
        health -= damage;
        if (health <= 0f)
        {
            health = 0f;
            GetComponent<ItemDropScript>().drop();
            if(enemyDeathEvent != "")
            {
                AkSoundEngine.PostEvent(enemyDeathEvent, gameObject);
            }

            StartCoroutine(OnDeath());
        }
        else
        {
            StartCoroutine(DamageFlash(5));
        }
    }

    public override void burn(float durationFrames, float totalDamage, float tickRateFrames)
    {
        if (burning != null)
        {
            StopCoroutine(burning);
        }
        burning = StartCoroutine(TakeDamageOverTime(durationFrames, totalDamage, tickRateFrames));
    }

    /// <summary>
    /// This function will decrease health over time 
    /// by tickRateFrames.
    /// </summary>
    /// <param name="durationFrames">The frames it takes for the dot effect to dissipate.</param>
    /// <param name="totalDamage">The total damage this dot effect will give.</param>
    /// <param name="tickRateFrames">The frames to wait before damage is taken.</param>
    /// <returns>Waits tickRateFrames / 60f seconds before damaging again.</returns>
    public override IEnumerator TakeDamageOverTime(float durationFrames, float totalDamage, float tickRateFrames)
    {
        durationFrames /= 60f;
        tickRateFrames /= 60f;
        float damagePerTick = totalDamage / (durationFrames / tickRateFrames);

        do
        {
            durationFrames -= tickRateFrames;
            TakeDamage(damagePerTick);

            yield return new WaitForSeconds(tickRateFrames);

        } while (durationFrames > 0);
    }

    public IEnumerator Spot(float spotFrames)
    {
        spotFrames /= 60f;
        isSpot = true;
        anim.SetBool("spotted", true);
        ActivateEye();
        yield return new WaitForSeconds(spotFrames);
        anim.SetBool("spotted", false);
        isSpot = false;
    }

    public IEnumerator PlayingAttack()
    {
        anim.SetBool("attacking", true);
        yield return null;
        anim.SetBool("attacking", false);
    }

    private IEnumerator DamageFlash(float frames)
    {
        for (int i = 0; i < propertyBlock.Length; ++i)
        {
            propertyBlock[i].SetFloat("_Alpha", 1f);
            meshParts[i].GetComponent<Renderer>().SetPropertyBlock(propertyBlock[i]);
        }
        for (int i = 0; i < frames; ++i)
        {
            yield return new WaitForEndOfFrame();
        }
        for (int i = 0; i < propertyBlock.Length; ++i)
        {
            propertyBlock[i].SetFloat("_Alpha", 0f);
            meshParts[i].GetComponent<Renderer>().SetPropertyBlock(propertyBlock[i]);
        }
    }

    private IEnumerator OnDeath()
    {
        int i;
        GetComponent<Rigidbody>().AddExplosionForce(1.0f, transform.position, 0.1f);
        for (i = 0; i < meshParts.Length / 3; ++i)
        {
            if (!meshParts[i].gameObject.GetComponent<Rigidbody>()) 
                meshParts[i].gameObject.AddComponent<Rigidbody>();

            meshParts[i].gameObject.GetComponent<Rigidbody>().mass = 0.001f;
            meshParts[i].gameObject.AddComponent<BoxCollider>();
            meshParts[i].gameObject.AddComponent<DestroyAfterOneSecondScript>();
            meshParts[i].parent = null;
        }
        yield return new WaitForEndOfFrame();

        for ( ; i < meshParts.Length * 2 / 3; ++i)
        {
            if (!meshParts[i].gameObject.GetComponent<Rigidbody>())
                meshParts[i].gameObject.AddComponent<Rigidbody>();

            meshParts[i].gameObject.GetComponent<Rigidbody>().mass = 0.001f;
            meshParts[i].gameObject.AddComponent<DestroyAfterOneSecondScript>();
            meshParts[i].gameObject.AddComponent<BoxCollider>();
            meshParts[i].parent = null;


        }
        yield return new WaitForEndOfFrame();

        for ( ; i < meshParts.Length; ++i)
        {
            if (!meshParts[i].gameObject.GetComponent<Rigidbody>())
                meshParts[i].gameObject.AddComponent<Rigidbody>();

            meshParts[i].gameObject.GetComponent<Rigidbody>().mass = 0.001f;
            meshParts[i].gameObject.AddComponent<DestroyAfterOneSecondScript>();
            meshParts[i].gameObject.AddComponent<BoxCollider>();
            meshParts[i].parent = null;
        }
        yield return new WaitForEndOfFrame();
        Destroy(gameObject);
    }

    public void ResetEye()
    {
        for (int i = 0; i < propertyBlock.Length; ++i)
        {
            propertyBlock[i].SetColor("_EmissiveCustom", Color.cyan);
            meshParts[i].GetComponent<Renderer>().SetPropertyBlock(propertyBlock[i]);
        }
    }

    public void ActivateEye()
    {
        for (int i = 0; i < propertyBlock.Length; ++i)
        {
            propertyBlock[i].SetColor("_EmissiveCustom", Color.red);
            meshParts[i].GetComponent<Renderer>().SetPropertyBlock(propertyBlock[i]);
        }
    }
}
