﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticlesScript : MonoBehaviour
{
    public enum HitboxType
    {
        INVALID = -1,
        PLAYER_HITBOX,
        KICK_HITBOX,
        SIZE
    }

    [HideInInspector] public HitboxType type;
    [HideInInspector] public Entity belongsTo;

    private ParticleSystem ps;
    private ParticleSystem.Particle[] particles;
    private GameObject[] enemyObjects;
    private ItemBoxScript[] crateObjects;
    private GeneratorScript[] generatorObjects;
    private int colliderIndex;

    private List<GameObject> enemiesToCheck;
    private List<GameObject> enemiesHit;
    private Dictionary<GameObject, bool> hasDealtDamage;


    private void Start()
    {
        ps = GetComponent<ParticleSystem>();
        particles = new ParticleSystem.Particle[ps.main.maxParticles];
        enemyObjects = GameObject.FindGameObjectsWithTag("Enemy");
        crateObjects = FindObjectsOfType<ItemBoxScript>();
        generatorObjects = FindObjectsOfType<GeneratorScript>();

        enemiesToCheck = new List<GameObject>();
        enemiesHit = new List<GameObject>();
        hasDealtDamage = new Dictionary<GameObject, bool>();

        colliderIndex = 0;

        if(type == HitboxType.KICK_HITBOX)
        {
            PlayerGemScript playerGem = belongsTo.GetComponent<PlayerGemScript>();

            if (playerGem.gem == null)
                AkSoundEngine.SetSwitch("element", "null", gameObject);
            else
                AkSoundEngine.SetSwitch("element", playerGem.gem.soundSwitch, gameObject);
          
            AkSoundEngine.PostEvent("KickMiss", gameObject);
        }

        //Get only enemies that could be in range for later use
        float radius = ps.trigger.radiusScale * 10f;
        foreach (GameObject enemy in enemyObjects)
        {
            float distance = 9999;

            if (enemy)
            {
                distance = Vector3.Distance(belongsTo.transform.position, enemy.transform.position);
                //Distance check to see if they satify the conditions of being in range
                if (distance < radius)
                {
                    enemiesToCheck.Add(enemy);
                    ps.trigger.SetCollider(colliderIndex++, enemy.transform.GetChild(0).GetComponent<CapsuleCollider>());
                }
            }
        }
    }


    private void OnParticleTrigger()
    {
        int particlesAlive = ps.GetParticles(particles);

        enemiesHit.Clear();

        //Get enemy positions and distance check with one of the particles
        for (int i = 0; i < enemiesToCheck.Count; ++i)
        {
            float distance = 9999;
            if(enemiesToCheck[i])
            {
                distance = Vector3.Distance(particles[0].position, enemiesToCheck[i].transform.position);

                if (distance < ps.trigger.radiusScale)
                    enemiesHit.Add(enemiesToCheck[i]);
            }
        }

        //Get box positions and distance check with one of the particles
        foreach (ItemBoxScript box in crateObjects)
        {
            if (box)
            {
                float distance = 9999;
                distance = Vector3.Distance(particles[0].position, box.transform.position);

                //Drop an item if it satifies the conditions of getting hit
                if (distance < ps.trigger.radiusScale)
                    box.GetComponent<ItemBoxScript>().Drop();
            }
        }

        //Get generator positions and distance check with one of the particles
        foreach (GeneratorScript generator in generatorObjects)
        {
            if (generator)
            {
                float distance = 9999;
                distance = Vector3.Distance(particles[0].position, generator.transform.position);

                //Take damage if it satifies the conditions of getting hit
                if (distance < ps.trigger.radiusScale)
                {
                    if (!hasDealtDamage.ContainsKey(generator.gameObject))
                    {
                        hasDealtDamage.Add(generator.gameObject, true);
                        generator.TakeDamage(belongsTo.outgoingAttack.damage);
                    }
                }
            }
        }

        //Check if this particle has already dealt damage to the enemy
        //If not, then add it to the dictionary
        for (int i = 0; i < enemiesHit.Count; ++i)
            if(!hasDealtDamage.ContainsKey(enemiesHit[i]))
                hasDealtDamage.Add(enemiesHit[i], false);

        //If particle has not dealt damage yet, get the type of the hitbox
        for (int i = 0; i < enemiesHit.Count; ++i)
        {
            if (!hasDealtDamage[enemiesHit[i]])
            {
                Rigidbody rb = enemiesHit[i].GetComponent<Rigidbody>();
                EnemyScript enemy = enemiesHit[i].GetComponent<EnemyScript>();
                EnemyHurtboxScript hurtbox = enemiesHit[i].transform.GetChild(0).GetComponent<EnemyHurtboxScript>();

                switch (type)
                {
                    case HitboxType.PLAYER_HITBOX:
                        {
                            //Player hitbox is non-elemental meaning it doesn't care which gem you are holding

                            //Damage and knockback
                            enemy.TakeDamage(belongsTo.outgoingAttack.damage);
                            AkSoundEngine.PostEvent("robot_hit", gameObject);
                            Vector3 pushDir = enemy.transform.position - belongsTo.transform.position;
                            pushDir.Normalize();
                            rb.velocity += pushDir * belongsTo.outgoingAttack.damage * hurtbox.defaultDamageToPushConversion;

                            //Is other a small or large enemy?
                            if (hurtbox.modelSize == EnemyHurtboxScript.Type.SMALL)
                            {
                                Instantiate(hurtbox.universalSmallDamageVFXEffect.gameObject, hurtbox.stunnedSpawn);
                            }
                            else
                            {
                                Instantiate(hurtbox.universalLargeDamageVFXEffect.gameObject, hurtbox.stunnedSpawn);
                            }

                            hasDealtDamage[enemiesHit[i]] = true;
                        }
                        break;
                    case HitboxType.KICK_HITBOX:
                        {
                            //Kick hitbox is elemental meaning it takes the gem you are holding into consideration

                            //get the gem script
                            PlayerGemScript playerGem = belongsTo.GetComponent<PlayerGemScript>();

                            //Don't call OnKick for things that don't move (turrets, generators, etc)
                            if (enemy.gameObject.layer != LayerMask.NameToLayer("PathObstacle"))
                            {
                                //call the gem kick effect
                                playerGem.OnKick(enemy.gameObject);
                            }

                            if (playerGem.gem != null)
                            {
                                switch (playerGem.gem.type)
                                {
                                    case GemScript.GemType.FIRE:
                                        {
                                            //Fire applies a burn effect to the enemy and damage over time.
                                            enemy.burn(playerGem.burnTime * 60, playerGem.burnDamage, playerGem.burnTickTime * 60);

                                            if (hurtbox.modelSize == EnemyHurtboxScript.Type.SMALL)
                                            {
                                                Instantiate(hurtbox.burningSmallVFXEffect, enemy.transform);
                                            }
                                            else
                                            {
                                                Instantiate(hurtbox.burningLargeVFXEffect, enemy.transform);
                                            }
                                        }
                                        break;
                                    case GemScript.GemType.ICE:
                                        {
                                            //Ice slows down the enemy movement
                                            if (enemy.GetComponent<GroundSteeringScript>())
                                            {
                                                GroundSteeringScript gs = enemy.GetComponent<GroundSteeringScript>();
                                                gs.StartCoroutine(gs.GetSlowed(playerGem.frostSlow, playerGem.frostDuration));
                                            }

                                            Instantiate(hurtbox.chilledVFXEffect, enemy.transform);
                                        }
                                        break;
                                    case GemScript.GemType.LIGHTNING:
                                        {
                                            //Lightning stuns the enemy for longer
                                            Instantiate(hurtbox.shockedVFXEffect, enemy.transform);
                                        }
                                        break;
                                    case GemScript.GemType.WIND:
                                        {
                                            //Wind has no vfx
                                        }
                                        break;
                                }
                            }

                            //deal damage
                            enemy.TakeDamage(belongsTo.outgoingAttack.damage);

                            AkSoundEngine.PostEvent("KickHit", gameObject);

                            //then stun for the base amount + the gem bonus (could be 0)
                            if(!enemy.isStunned)
                                StartCoroutine(enemy.Stun(belongsTo.kickStunTime + playerGem.stunKickTimeBonus));

                            //Knockback
                            Vector3 pushDir = enemy.transform.position - belongsTo.transform.position;
                            pushDir.Normalize();
                            rb.velocity += pushDir * belongsTo.outgoingAttack.damage * hurtbox.stunnedDamageToPushConversion * 3f;

                            Instantiate(hurtbox.stunnedVFXEffect, hurtbox.stunnedSpawn);

                            if (hurtbox.modelSize == EnemyHurtboxScript.Type.SMALL)
                            {
                                Instantiate(hurtbox.kickSmallImpactVFXEffect, hurtbox.transform.parent);
                            }
                            else
                            {
                                Instantiate(hurtbox.kickLargeImpactVFXEffect, hurtbox.transform.parent);
                            }

                            hasDealtDamage[enemiesHit[i]] = true;
                        }
                        break;
                }
            }
        }
    }
}
