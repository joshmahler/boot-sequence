﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CapsuleCollider))]
public class EnemyHurtboxScript : MonoBehaviour
{
    private EnemyScript enemy;
    public float defaultDamageToPushConversion = 0.005f;
    public float stunnedDamageToPushConversion = 0.005f;

    public Transform stunnedSpawn;

    public GameObject universalLargeDamageVFXEffect;
    public GameObject universalSmallDamageVFXEffect;
    public GameObject kickLargeImpactVFXEffect;
    public GameObject kickSmallImpactVFXEffect;
    public GameObject stunnedVFXEffect;
    public GameObject burningLargeVFXEffect;
    public GameObject burningSmallVFXEffect;
    public GameObject chilledVFXEffect;
    public GameObject shockedVFXEffect;

    public enum Type
    {
        INVALID = -1,
        SMALL,
        LARGE,
        SIZE
    }

    [HideInInspector] public Type modelSize;


    private void Awake()
    {
        enemy = GetComponentInParent<EnemyScript>();

        if (enemy.tag == "HurtboxSmall")
            modelSize = Type.SMALL;
        else
            modelSize = Type.LARGE;
    }

    private void OnTriggerEnter(Collider other)
    {
        Rigidbody rb = enemy.GetComponent<Rigidbody>();

        switch (other.tag)
        {
            case "PlayerHitbox":
                {
                    AkSoundEngine.PostEvent("robot_hit", gameObject);
                    enemy.TakeDamage(other.GetComponentInParent<HeroScript>().outgoingAttack.damage);
                    

                    Vector3 pushDir = enemy.transform.position - other.transform.parent.position;
                    rb.velocity += pushDir * other.GetComponentInParent<HeroScript>().outgoingAttack.damage * defaultDamageToPushConversion;

                    if (modelSize == Type.SMALL)
                    {
                        Instantiate(universalSmallDamageVFXEffect, stunnedSpawn);
                    }
                    else
                    {
                        Instantiate(universalLargeDamageVFXEffect, stunnedSpawn);
                    }
                }
                break;
            case "KnightBasicHitbox":
                {
                    enemy.TakeDamage(other.GetComponentInParent<HeroScript>().outgoingAttack.damage);
                    //other.GetComponentInParent<KnightScript>().knightAttackEvent.Post(enemy.gameObject);
 

                    Vector3 pushDir = enemy.transform.position - other.transform.parent.position;
                    rb.velocity += pushDir * other.GetComponentInParent<HeroScript>().outgoingAttack.damage * defaultDamageToPushConversion;

                    if (modelSize == Type.SMALL)
                    {
                        Instantiate(universalSmallDamageVFXEffect, stunnedSpawn);
                    }
                    else
                    {
                        Instantiate(universalLargeDamageVFXEffect, stunnedSpawn);
                    }
                }
                break;
            case "KickHitbox":
                {
                    //get the gem script
                    PlayerGemScript playerGemScript = other.GetComponentInParent<PlayerGemScript>();

                    //turrets don't have groundsteeringscript, so we must ignore them
                    if (transform.parent.gameObject.layer != LayerMask.NameToLayer("PathObstacle"))
                    {
                        //call the gem kick effect
                        playerGemScript.OnKick(transform.parent.gameObject);
                    }

                    //deal damage
                    enemy.TakeDamage(other.GetComponentInParent<HeroScript>().outgoingAttack.damage);

                    //then stun for the base amount + the gem bonus (could be 0)
                    StartCoroutine(enemy.Stun(other.GetComponentInParent<HeroScript>().kickStunTime + playerGemScript.stunKickTimeBonus));

                    Vector3 pushDir = enemy.transform.position - other.transform.parent.position;
                    rb.velocity += pushDir * other.GetComponentInParent<HeroScript>().outgoingAttack.damage * stunnedDamageToPushConversion * 3f;

                    Instantiate(stunnedVFXEffect, stunnedSpawn);

                    if (modelSize == Type.SMALL)
                    {
                        Instantiate(kickSmallImpactVFXEffect, transform.parent);
                    }
                    else
                    {
                        Instantiate(kickLargeImpactVFXEffect, transform.parent);
                    }
                }
                break;
            case "MageProjectile":
                {
                    AkSoundEngine.PostEvent("robot_hit", gameObject);
                    enemy.TakeDamage(other.GetComponent<ProjectileScript>().dmg);

            		Vector3 pushDir = enemy.transform.position - other.transform.position;
            		rb.velocity += pushDir * other.GetComponent<ProjectileScript>().dmg * defaultDamageToPushConversion;

                    if (modelSize == Type.SMALL)
                    {
                        Instantiate(universalSmallDamageVFXEffect, stunnedSpawn);
                    }
                    else
                    {
                        Instantiate(universalLargeDamageVFXEffect, stunnedSpawn);
                    }
                }
                break;
            case "VigorProjectile":
                {
                    AkSoundEngine.PostEvent("robot_hit", gameObject);
                    float dmg = other.transform.GetChild(0).GetComponent<ParticlesScript>().belongsTo.outgoingAttack.damage;
                    enemy.TakeDamage(dmg);

                    Vector3 pushDir = enemy.transform.position - other.transform.position;
                    rb.velocity += pushDir * dmg * defaultDamageToPushConversion;

                    if (modelSize == Type.SMALL)
                    {
                        Instantiate(universalSmallDamageVFXEffect, stunnedSpawn);
                    }
                    else
                    {
                        Instantiate(universalLargeDamageVFXEffect, stunnedSpawn);
                    }
                }
                break;
            case "FirePool":
                {
                    //TODO
                    //Object of type GameObject has been destroyed but you are still trying to access it.
                    //get the gem script
                    PlayerGemScript playerGemScript = other.GetComponent<PoolScript>().parent.GetComponentInParent<PlayerGemScript>();
                    enemy.burn(playerGemScript.burnTime * 60, playerGemScript.burnDamage, playerGemScript.burnTickTime * 60);

                    if (modelSize == Type.SMALL)
                    {
                        Instantiate(burningSmallVFXEffect, transform.parent);
                    }
                    else
                    {
                        Instantiate(burningLargeVFXEffect, transform.parent);
                    }
                }
                break;
            case "IcePool":
                {
                    //get the gem script
                    PlayerGemScript playerGemScript = other.GetComponent<PoolScript>().parent.GetComponentInParent<PlayerGemScript>();
                    //get the enemy steering script

                    if (enemy.GetComponent<GroundSteeringScript>())
                    {
                        GroundSteeringScript gs = enemy.GetComponent<GroundSteeringScript>();
                        gs.StartCoroutine(gs.GetSlowed(playerGemScript.frostSlow, playerGemScript.frostDuration));
                    }

                    Instantiate(chilledVFXEffect, transform.parent);
                }
                break;
            case "ShockPool":
                {
                    //get the gem script
                    Instantiate(shockedVFXEffect, transform.parent);
                }
                break;
            case "WindPool":
                {
                    //pushedVFXEffect.Play();
                    //StartCoroutine(StopParticle(pushedVFXEffect, 0.5f));
                }
                break;
            case "DaggerProjectile":
                {
                    enemy.TakeDamage(other.GetComponent<ProjectileScript>().dmg);
                }
                break;
            case "BlinkProjectile":
                {
                    ProjectileScript proj = other.GetComponent<ProjectileScript>();
                    enemy.TakeDamage(proj.dmg);
                    proj.belongTo.transform.position = enemy.transform.position + (-enemy.transform.forward * 3f);
                    proj.belongTo.GetComponent<HeroScript>().ResetFlags();
                    StartCoroutine(Blink(proj));
                }
                break;
            case "MonkHitbox":
                {
                    enemy.TakeDamage(other.GetComponentInParent<HeroScript>().outgoingAttack.damage);

                    Vector2 pushDir = new Vector2(enemy.transform.position.x, enemy.transform.position.z) - 
                                        new Vector2(other.transform.parent.position.x, other.transform.parent.position.z);
                    rb.velocity += new Vector3(pushDir.x, 0f, pushDir.y) * other.GetComponent<MonkPushScript>().pushback;
                }
                break;
        }
    }

    IEnumerator Blink(ProjectileScript proj)
    {
        proj.belongTo.GetComponent<HeroScript>().isBlinking = true;
        yield return null;
        proj.belongTo.transform.rotation *= Quaternion.Euler(0, 180f, 0);
        yield return null;
        proj.belongTo.GetComponent<HeroScript>().isBlinking = false;
    }
    public IEnumerator StopParticle(ParticleSystem toStop, float seconds)
    {
        yield return new WaitForSeconds(seconds);
        if (toStop)
        {
            Utility.Log(toStop.isEmitting);
            toStop.Stop();
            Utility.Log("Stopping" + toStop);
        }
    }
}
