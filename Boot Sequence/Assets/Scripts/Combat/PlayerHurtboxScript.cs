﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CapsuleCollider))]
public class PlayerHurtboxScript : MonoBehaviour
{
    private HeroScript hero;
    private string hurtEvent;

    private void Start()
    {
        hero = GetComponentInParent<HeroScript>();

        if (hero.myClass == HeroScript.Class.Knight)
        {
            hurtEvent = "KnightHurtVO";
        }
        else if (hero.myClass == HeroScript.Class.Mage)
        {
            hurtEvent = "MageHurtVO";

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (hero)
        {
            hero.gameObject.GetComponent<PlayerGemScript>().OnCollission(other.gameObject);
            if (!hero.isInvulnerable)
            {
                switch (other.tag)
                {
                    case "EnemyHitbox":
                        {
                            AkSoundEngine.PostEvent(hurtEvent, hero.gameObject);
                            EnemyScript enemy = other.GetComponentInParent<EnemyScript>();
                            hero.gameObject.GetComponent<PlayerGemScript>().OnDamageTaken(enemy.gameObject);
                            hero.StartCoroutine(hero.InvulnerableFrames(hero.invulnerableFrames));
                            hero.TakeDamage(enemy.outgoingAttack.damage);
                        }
                        break;

                    case "BombExplosion":
                        {
                            AkSoundEngine.PostEvent(hurtEvent, hero.gameObject);
                            EnemyScript enemy = (EnemyScript)other.transform.GetChild(0).GetComponent<ParticlesScript>().belongsTo;
                            hero.gameObject.GetComponent<PlayerGemScript>().OnDamageTaken(enemy.gameObject);
                            hero.StartCoroutine(hero.InvulnerableFrames(hero.invulnerableFrames));
                            hero.TakeDamage(enemy.outgoingAttack.damage);
                        }
                        break;

                    case "ShockHitbox":
                        {
                            AkSoundEngine.PostEvent(hurtEvent, hero.gameObject);
                            EnemyScript enemy = other.GetComponentInParent<EnemyScript>();
                            hero.gameObject.GetComponent<PlayerGemScript>().OnDamageTaken(enemy.gameObject);
                            hero.StartCoroutine(hero.Stun(other.GetComponent<ShockHitboxScript>().stunSeconds));
                            hero.StartCoroutine(hero.InvulnerableFrames(hero.invulnerableFrames));
                            hero.TakeDamage(enemy.outgoingAttack.damage);
                        }
                        break;

                    case "TurretProjectile":
                        AkSoundEngine.PostEvent(hurtEvent, hero.gameObject);
                        hero.StartCoroutine(hero.InvulnerableFrames(hero.invulnerableFrames));
                        hero.TakeDamage(other.GetComponent<ProjectileScript>().dmg);
                        break;
                }
                

                
            }
        }
    }
}
