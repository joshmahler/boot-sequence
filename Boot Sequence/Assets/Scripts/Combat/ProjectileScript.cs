﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ProjectileScript : MonoBehaviour
{
    public float dmg;               // Damage this projectile deals upon impact with enemy
    public float thrust = 500f;      // Thrust (force) this projectile moves by
    public float turnForce = .1f;   // The amount of "aim-assist"  
    public float lifetime = 5f;     // The seconds this projectile "lives" for
    public float velocity;          // The velocity to be applied to this object every FixedUpdate
    public string aimAssistTag = "Enemy";   //Tag the projectile will be colliding with

    [HideInInspector] public GameObject belongTo;

    private Rigidbody rb;       // The Rigidbody component on this object
    private Transform target;   // The target of this object to move towards

    /*
     * Add force once this object gets instantiated and
     * give it a lifetime to be destroy if it does not 
     * hit anything.
     */
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * thrust);
        Destroy(gameObject, lifetime);
    }

    /*
     * If this enters an enemy "aim-assist" box
     * then make the projectile target that enemy
     */
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == aimAssistTag)
        {
            target = other.gameObject.transform;
        }
    }

    /*
     * If this projectile has a target
     * then "aim-assist" it towards that other object
     */
    private void FixedUpdate()
    {
        if(aimAssistTag != "Player" && target != null)
        {
            Utility.Log(target);
            rb.velocity = transform.forward * velocity;

            Quaternion rocketTargetRot = Quaternion.LookRotation(target.position - transform.position);

            rb.MoveRotation(Quaternion.RotateTowards(transform.rotation, rocketTargetRot, turnForce));
        }
    }

    /*
     * If this projectile hits an enemy
     * then lower that enemy's health by dmg and
     * if that enemy's health drops to or below 0,
     * kill it. 
     * If this projectile hits anything, 
     * destroy it.
     */
    private void OnCollisionEnter(Collision other)
    {
		//make the floor not count as a collision!
        if(other.gameObject.tag == aimAssistTag || other.gameObject.tag == "Generator")
        {
                
        }
        if(other.gameObject.layer != LayerMask.NameToLayer("Ground"))
            Destroy(gameObject);
    }
}
