﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillAreaScript : MonoBehaviour
{

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" /*&& !other.gameObject.GetComponent<MovementScript>().canHover*/)
        {
            HeroScript hero = other.GetComponent<HeroScript>();
            if (hero.myClass == HeroScript.Class.Knight)
            {
                AkSoundEngine.PostEvent("KnightPitfallVO", hero.gameObject);
            }
            else if (hero.myClass == HeroScript.Class.Mage)
            {
                AkSoundEngine.PostEvent("MagePitfallVO", hero.gameObject);
            }
        }  

        if(other.gameObject.tag == "Enemy")
            other.gameObject.GetComponent<EnemyScript>().TakeDamage(9999);
    }
}
