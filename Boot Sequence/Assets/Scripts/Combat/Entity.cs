﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    public List<SkillPoolScript.Skill> skills;      // List of skills each player/enemy has
    public SkillPoolScript.Skill outgoingAttack;    // The current outgoing attack

    public float health = 100f;
    public bool isStunned = false;
    public float kickStunTime = 1.0f;

    [HideInInspector] public bool isAttacking = false;      // Is the hero in its attack state?
    [HideInInspector] public bool isAttackWindup = false;   // Is the hero currently winding up an attack?
    [HideInInspector] public bool isAttackActive = false;   // Is the hero's attack hitbox currently active?
    [HideInInspector] public bool isAttackCooldown = false; // Is the hero currently cooling down from an attack?

    [HideInInspector] public GameObject ground;

    [HideInInspector] public Coroutine coroutine;
    [HideInInspector] public Coroutine animCoroutine;
    [HideInInspector] public Coroutine burning;
    private SkillPoolScript skillPool;


    public virtual void Start()
    {
        skillPool = GameObject.FindGameObjectWithTag("SkillPool").GetComponent<SkillPoolScript>();
        skills = new List<SkillPoolScript.Skill>();
    }

    public virtual void Update()
    {
        if(isStunned)
        {
            GetComponent<Rigidbody>().useGravity = true;
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
            }
            ResetFlags();
        }
    }

    public void ResetFlags()
    {
        isAttackActive = false;
        isAttackCooldown = false;
        isAttackWindup = false;
        isAttacking = false;

        if (GetComponent<MovementScript>())
        {
            GetComponent<MovementScript>().canMove = true;
        }
    }

    public IEnumerator ResetAnimatorBool(Animator anim, string name, bool value, float waitSeconds)
    {
        yield return new WaitForSeconds(waitSeconds);
        anim.SetBool(name, value);
    }

    /// <summary>
    /// Adds a skill to the entity based on the SkillType.
    /// </summary>
    /// <param name="theSkill">The index of the dictionary.</param>
    public void AddSkill(SkillPoolScript.Skill.SkillType theSkill)
    {
        skills.Add(skillPool.skillDictionary[(int)theSkill]);
    }

    public bool GetIsGrounded()
    {
        int layer = 1 << 14;

        Collider[] hit = Physics.OverlapSphere(transform.position, .5f, layer);

        if (hit.Length > 0)
        {
            ground = hit[0].gameObject;
            return true;
        }
        return false;
    }

    /// <summary>
    /// This decreases the health 
    /// by the amount defined in damage.
    /// </summary>
    /// <param name="damage">Amount of damage to decrease from health.</param>
    public virtual void TakeDamage(float damage)
    {
        health -= damage;

        if (health <= 0f)
        {
            health = 0f;
        }
    }


    public virtual void burn(float durationFrames, float totalDamage, float tickRateFrames)
    {
        if (burning != null)
        {
            StopCoroutine(burning);
        }
        burning = StartCoroutine(TakeDamageOverTime(durationFrames, totalDamage, tickRateFrames));
    }
    /// <summary>
    /// This function will decrease health over time 
    /// by tickRateFrames.
    /// </summary>
    /// <param name="durationFrames">The frames it takes for the dot effect to dissipate.</param>
    /// <param name="totalDamage">The total damage this dot effect will give.</param>
    /// <param name="tickRateFrames">The frames to wait before damage is taken.</param>
    /// <returns>Waits tickRateFrames / 60f seconds before damaging again.</returns>
    public virtual IEnumerator TakeDamageOverTime(float durationFrames, float totalDamage, float tickRateFrames)
    {
        durationFrames /= 60f;
        tickRateFrames /= 60f;
        float damagePerTick = totalDamage / (durationFrames / tickRateFrames);

        do
        {
            durationFrames -= tickRateFrames;
            TakeDamage(damagePerTick);

            yield return new WaitForSeconds(tickRateFrames);

        } while (durationFrames > 0);
    }


    /// <summary>
    /// Set the appropriate flags of where the attack is.
    /// There are 3 steps to each attack:
    /// Windup - The time it takes to build-up an attack.
    /// Active - The time where the hitbox is actually active during the attack.
    /// Cooldown - The time it takes for the attack to recharge, the "after-effect" this attack has.
    /// </summary>
    /// <param name="windupFrames">Frames the attack takes to build up.</param>
    /// <param name="activeFrames">Frames the attack has an active hitbox.</param>
    /// <param name="cooldownFrames">Frames the attack takes to recharge.</param>
    /// <param name="hitbox">The hitbox getting instantiated</param>
    /// <param name="parent">The parent object of the hitbox (will spawn at parent.position and parent.rotation).</param>
    /// <param name="attackEvent">The sound event that is played when the attack is active.</param>
    /// <returns>Waits for x amount of frames (done internally in seconds) for each portion of the attack.</returns>
    public IEnumerator Attack(float windupFrames, float activeFrames, float cooldownFrames, GameObject hitbox, Transform parent, GameObject windupVisual, GameObject cooldownVisual, string attackEvent = null, string attackSwitchName = null, string attackSwitch = null)
    {
        windupFrames /= 60f;
        activeFrames /= 60f;
        cooldownFrames /= 60f;

        isAttacking = true;

        GameObject windup = null;
        if (windupVisual != null)
        {
            windup = Instantiate(windupVisual, parent.position, parent.rotation, parent);
        }

        isAttackWindup = true;
        yield return new WaitForSeconds(windupFrames);
        isAttackWindup = false;

        if (windup != null)
        {
            Destroy(windup);
        }

        if (hitbox != null)
        {
            hitbox.SetActive(true);
        }
        if (attackEvent != null)
        {
            if (attackSwitch != null && attackSwitchName != null)
            {
                AkSoundEngine.SetSwitch(attackSwitchName, attackSwitch, hitbox);
            }
            AkSoundEngine.PostEvent(attackEvent, hitbox);
        }

        isAttackActive = true;
        yield return new WaitForSeconds(activeFrames);
        isAttackActive = false;

        if (hitbox != null)
        {
            hitbox.SetActive(false);
        }

        GameObject cooldown = null;
        if (cooldownVisual != null)
        {
            cooldown = Instantiate(cooldownVisual, parent.position, parent.rotation, parent);
        }
        if (GetComponent<MovementScript>())
        {
            GetComponent<MovementScript>().canMove = true;
            GetComponent<MovementScript>().canHover = true;
        }

        isAttackCooldown = true;
        yield return new WaitForSeconds(cooldownFrames);
        isAttackCooldown = false;

        if (cooldown != null)
        {
            Destroy(cooldown);
        }

        isAttacking = false;
    }

    /// <summary>
    /// Set the appropriate flags of where the attack is.
    /// There are 3 steps to each attack:
    /// Windup - The time it takes to build-up an attack.
    /// Active - The time where the hitbox is actually active during the attack.
    /// Cooldown - The time it takes for the attack to recharge, the "after-effect" this attack has.
    /// Parent is null always for the projectiles!
    /// </summary>
    /// <param name="windupFrames">Frames the attack takes to build up.</param>
    /// <param name="activeFrames">Frames the attack animates the character.</param>
    /// <param name="cooldownFrames">Frames the attack takes to recharge.</param>
    /// <param name="prefab">The hitbox getting instantiated</param>
    /// <param name="spawn">The spawner object of the projectile.</param>
    /// <param name="attackEvent">The sound event that is played when the projectile is fired.</param>
    /// <returns>Waits for x amount of frames (done internally in seconds) for each portion of the attack.</returns>
    public IEnumerator AttackProjectile(float windupFrames, float activeFrames, float cooldownFrames, GameObject prefab, Transform spawn, GameObject windupVisual, GameObject cooldownVisual, string attackEvent = null)
    {
        windupFrames /= 60f;
        activeFrames /= 60f;
        cooldownFrames /= 60f;

        isAttacking = true;

        GameObject windup = null;
        if (windupVisual != null)
            windup = Instantiate(windupVisual, spawn.position, spawn.rotation, spawn);

        isAttackWindup = true;
        yield return new WaitForSeconds(windupFrames);
        isAttackWindup = false;

        if (windup != null)
            Destroy(windup);

        GameObject hitbox = Instantiate(prefab, spawn.position, spawn.rotation, null);
        
        hitbox.GetComponent<ProjectileScript>().belongTo = spawn.parent.gameObject;

        if (attackEvent != null)
        {
            AkSoundEngine.PostEvent(attackEvent, hitbox);
        }

        isAttackActive = true;
        yield return new WaitForSeconds(activeFrames);
        isAttackActive = false;

        GameObject cooldown = null;
        if(cooldownVisual != null)
            cooldown = Instantiate(cooldownVisual, spawn.position, spawn.rotation, spawn);

        isAttackCooldown = true;
        yield return new WaitForSeconds(cooldownFrames);
        isAttackCooldown = false;

        if(cooldown != null)
            Destroy(cooldown);

        isAttacking = false;
    }

    /// <summary>
    /// If the kick has no other effects, it would call this function.
    /// There are 3 steps to each attack:
    /// Windup - The time it takes to build-up an attack.
    /// Active - The time where the hitbox is actually active during the attack.
    /// Cooldown - The time it takes for the attack to recharge, the "after-effect" this attack has.
    /// </summary>
    /// <param name="windupFrames">Frames the attack takes to build up.</param>
    /// <param name="activeFrames">Frames the attack has an active hitbox.</param>
    /// <param name="cooldownFrames">Frames the attack takes to recharge.</param>
    /// <param name="hitbox">The hitbox getting instantiated</param>
    /// <param name="parent">The parent object of the hitbox (will spawn at parent.position and parent.rotation).</param>
    /// <param name="attackEvent">The sound event that is played when the attack is active.</param>
    public virtual void Kick(float windupFrames, float activeFrames, float cooldownFrames, GameObject hitbox, Transform parent, GameObject windupVisual, GameObject cooldownVisual, string kickEvent = null)
    {
        StartCoroutine(Attack(windupFrames, activeFrames, cooldownFrames, hitbox, parent, windupVisual, cooldownVisual));
    }

    public IEnumerator AttackParticle(float windupFrames, float activeFrames, float cooldownFrames, GameObject hitboxParticle, Transform parent, Entity belongsTo, ParticlesScript.HitboxType hitboxType, string particleSound = null, string particleSwitchName = null, string particleSwitch = null)
    {
        isAttacking = true;
        windupFrames /= 60f;
        activeFrames /= 60f;
        cooldownFrames /= 60f;

        isAttackWindup = true;
        yield return new WaitForSeconds(windupFrames);
        isAttackWindup = false;

        GameObject hitbox = Instantiate(hitboxParticle, parent);
        if(particleSwitch != null && particleSwitchName != null)
        {
            AkSoundEngine.SetSwitch(particleSwitchName, particleSwitch, hitbox);
        }

        AkSoundEngine.PostEvent(particleSound, hitbox);
        hitbox.transform.SetParent(null);

        ParticlesScript particle = hitbox.transform.GetChild(0).GetComponent<ParticlesScript>();
        if (particle)
        {
            particle.belongsTo = belongsTo;
            particle.type = hitboxType;
        }

        isAttackActive = true;
        yield return new WaitForSeconds(activeFrames);
        isAttackActive = false;

        isAttackCooldown = true;
        yield return new WaitForSeconds(cooldownFrames);
        isAttackCooldown = false;

        isAttacking = false;
    }

    /// <summary>
    /// Gets stunned for a number of seconds
    /// </summary>
    /// <param name="seconds">Number of seconds to be stunned for.</param>
    /// <returns>Waits for x seconds then becomes unstunned.</returns>
    public IEnumerator Stun(float seconds)
    {
        isStunned = true;
        yield return new WaitForSeconds(seconds);
        isStunned = false;
    }
}
