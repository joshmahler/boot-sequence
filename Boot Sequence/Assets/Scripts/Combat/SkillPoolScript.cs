﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class SkillPoolScript : MonoBehaviour
{
    public Dictionary<int, Skill> skillDictionary;

    public struct Skill
    {
        public enum SkillType
        {
            INVALID = -1,
            KICK,
            KNIGHT_BASIC,
            KNIGHT_ABILITY,
            MAGE_BASIC,
            MAGE_ABILITY,
            ROGUE_BASIC,
            ROGUE_ABILITY,
            MONK_BASIC,
            MONK_ABILITY,
            ENEMY_BASIC,
            ENEMY_BASIC_SPOT,
            ENEMY_BOMB,
            ENEMY_BOMB_SPOT,
            ENEMY_TURRET,
            ENEMY_TURRET_SPOT,
            ENEMY_SHOCK,
            ENEMY_SHOCK_SPOT,
            SKILL_SIZE
        };

        public SkillType skillType;
        public float windupFrames;
        public float activeFrames;
        public float cooldownFrames;
        public float damage;
    }

    private Skill skill;
    private int id = -1;

    private void Awake()
    {
        skillDictionary = new Dictionary<int, Skill>();
        PopulateDictionary();
    }

    private void PopulateDictionary()
    {
        string filename = "/Data/skillInfo.txt";
        StreamReader reader = new StreamReader(Application.streamingAssetsPath + filename);

        while (!reader.EndOfStream)
        {
            reader.ReadLine();
            int skillIndex;
            if (!int.TryParse(reader.ReadLine(), out skillIndex))
            {
                Utility.Log("Not a valid skill index.");
            }
            else if ((Skill.SkillType)skillIndex < Skill.SkillType.INVALID || (Skill.SkillType)skillIndex > Skill.SkillType.SKILL_SIZE)
            {
                Utility.Log("The skill index is too small or too large.");
            }
            else
            {
                skill.skillType = (Skill.SkillType)skillIndex;
            }

            reader.ReadLine();
            float windupValue;
            if(!float.TryParse(reader.ReadLine(), out windupValue))
            {
                Utility.Log("Not a valid windup value");
            }
            else
            {
                skill.windupFrames = windupValue;
            }


            reader.ReadLine();
            float activeValue;
            if(!float.TryParse(reader.ReadLine(), out activeValue))
            {
                Utility.Log("Not a valid active value");
            }
            else
            {
                skill.activeFrames = activeValue;
            }

            reader.ReadLine();
            float cooldownValue;
            if(!float.TryParse(reader.ReadLine(), out cooldownValue))
            {
                Utility.Log("Not a valid cooldown value");
            }
            else
            {
                skill.cooldownFrames = cooldownValue;
            }

            reader.ReadLine();
            float damageValue;
            if(!float.TryParse(reader.ReadLine(), out damageValue))
            {
                Utility.Log("Not a valid damage value");
            }
            else
            {
                skill.damage = damageValue;
            }

            skillDictionary.Add(++id, skill);
        }

        reader.Close();
    }
}
