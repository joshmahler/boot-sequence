﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndPointScript : MonoBehaviour
{
    private int playersReady = 0;

    public GameObject teleportPS;
    private bool spawnedPs;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (playersReady >= GameControl.control.playersAlive && GameControl.control.playersAlive >= 1)
        {
            //Debug.Log("End point script, dead players length: " + GameControl.control.completelyDeadPlayers.Count);
            //SceneManager.LoadScene("WFC_Test");
            if(!spawnedPs)
                StartCoroutine(LoadNextLevel());
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Gary_Oldmyer" || other.tag == "Player")
            ++playersReady;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
            --playersReady;
    }

    IEnumerator LoadNextLevel()
    {
        spawnedPs = true;
        foreach (var p in GameControl.control.players)
        {
            //p.GetComponent<MovementScript>().canMove = false;
            p.transform.Find("g_mesh").gameObject.SetActive(false);
            var ps = Instantiate(teleportPS, p.transform.position, Quaternion.identity);
            ps.transform.parent = p.transform;
        }
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("WFC_Test");
    }
}
