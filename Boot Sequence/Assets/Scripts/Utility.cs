﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility : MonoBehaviour
{

    public static void Log(string message)
    {
        #if UNITY_EDITOR
                Debug.Log(message);
        #endif
    }

    public static void Log(float message)
    {
#if UNITY_EDITOR
        Debug.Log(message);
#endif
    }

    public static void Log(int message)
    {
#if UNITY_EDITOR
        Debug.Log(message);
#endif
    }

    public static void Log(Transform message)
    {
#if UNITY_EDITOR
        Debug.Log(message);
#endif
    }

    public static void Log(bool message)
    {
#if UNITY_EDITOR
        Debug.Log(message);
#endif
    }

    public static void LogWarning(string message)
    {
#if UNITY_EDITOR
        Debug.LogWarning(message);
#endif
    }

    public static void LogError(string message)
    {
#if UNITY_EDITOR
        Debug.LogError(message);
#endif
    }

}
