﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInfo : MonoBehaviour
{
    /*
     * This class is used in the PlayerSelectScript.
     * Since the player select screen will happen before the HeroScript gets
     * instantiated, it needs to be here.
     * The description is not currently used, but will be in the future 
     * when we decide to do descriptions on the select screen.
     */
    public enum Class { INVALID = -1, Knight, Mage, Rogue, Monk, CLASS_SIZE };
    public string description;
}
