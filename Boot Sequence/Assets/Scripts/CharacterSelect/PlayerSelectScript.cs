﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Rewired;
public class PlayerSelectScript : MonoBehaviour
{
    /*
     * Used to instantiate prefabs by what Class they are, 
     * and to send GameControl the selected Class
     */ 
    private Dictionary<CharacterInfo.Class, string> characterDictionary;

    public Transform spawnPoint;                // Spawn Transform on the pedestal
    public MeshRenderer lockVisual;             // The visual for when a player locks their character
    public Image displayElement;
    public Sprite selectionNeededIcon;
    public Sprite selectionDoneIcon;
    public int playerID = 0;                    // Which player am I?
    public string nextScene = "GemTestScene";    // The next scene to go to

    private string horizAxis = "Horizontal_P1";     // The horizontal axis set to each character for selection
    private string lockButton = "Selection_P1";     // The selection button used to lock in the player's character
    private bool didClassChange = false;            // Internal boolean used in the coroutine
    private bool isLocked = false;                  // Am I currently locked?
    private GameObject model;                       // Internal GameObject used in Instantiate/Destroy functions
    private CharacterInfo.Class currentIndex = 0;   // The current index in terms of CharacterInfo.Class
    private Color originalReadyColor;               // The original lock visual color
    private float changeClassSeconds = .15f;        // The amount of seconds to wait before letting player change class again.
    

    private void Start()
    {
        SetUpPlayer();
        //originalReadyColor = lockVisual.material.color;
        characterDictionary = new Dictionary<CharacterInfo.Class, string>();
        characterDictionary.Add(CharacterInfo.Class.Knight, "Knight3");
        characterDictionary.Add(CharacterInfo.Class.Mage, "Mage3");
        characterDictionary.Add(CharacterInfo.Class.Rogue, "Knight3");
        characterDictionary.Add(CharacterInfo.Class.Monk, "Mage3");
        SetCurrentCharacter(CharacterInfo.Class.INVALID + 1);
    }

    private void Update()
    {
        /* 
         * If the player moves the joystick LEFT, the characters should go "left" as well
         */
        if (!isLocked && !didClassChange && InputManager.input.MainHorizontal(playerID) < 0)
        {
            --currentIndex;
            StartCoroutine(ChangeClass(changeClassSeconds));
        }
        /* 
         * If the player moves the joystick RIGHT, the characters should go "right" as well 
         */
        else if (!isLocked && !didClassChange && InputManager.input.MainHorizontal(playerID) > 0)
        {
            ++currentIndex;
            StartCoroutine(ChangeClass(changeClassSeconds));
        }

        /* 
         * If the player presses the Selection Button and is not currently locked
         * then turn the cube green and increment the lockedAmount for future use 
         */
        if(!isLocked && InputManager.input.SelectionButton(playerID))
        {
            isLocked = true;
            //lockVisual.material.color = Color.green;
            displayElement.sprite = selectionDoneIcon;
            AkSoundEngine.PostEvent("character_select", gameObject);
            ++GameControl.control.lockedAmount;
            /* 
             * If the lockedAmount is equal to the amount of joysticks connected
             * then SetPlayerCharacter is called and attempts to switch the scene to nextScene
             */
            SetPlayerCharacter(characterDictionary[currentIndex]);

            if (GameControl.control.lockedAmount >= ReInput.controllers.Joysticks.Count)
            {
                SceneLoader.loader.LoadScene(nextScene);
                GameControl.control.controllersAmount = ReInput.controllers.Joysticks.Count;
            }
        }
        /* 
         * If the character is locked, and not all players have locked in
         * and the Selection Button is pressed
         * then un-lock the character, set the cube to its original color,
         * and decrement the lockedAmount
         */
        else if(isLocked && InputManager.input.SelectionButton(playerID))
        {
            isLocked = false;
            //lockVisual.material.color = originalReadyColor;
            displayElement.sprite = selectionNeededIcon;
            --GameControl.control.lockedAmount;
            if(GameControl.control.lockedAmount < 0)
            {
                GameControl.control.lockedAmount = 0;
            }
        }
    }

    /// <summary>
    /// Make sure the dictionary is parallel in size with the CLASS_SIZE variable
    /// </summary>
    /// <param name="t">The amount of seconds to wait to prevent the player not able to select a character with precision</param>
    /// <returns></returns>
    IEnumerator ChangeClass(float t)
    {
        didClassChange = true;
        if (currentIndex <= CharacterInfo.Class.INVALID)
        {
            currentIndex = CharacterInfo.Class.CLASS_SIZE - 1;
        }
        else if (currentIndex >= CharacterInfo.Class.CLASS_SIZE)
        {
            currentIndex = CharacterInfo.Class.INVALID + 1;
        }
        SetCurrentCharacter(currentIndex);
        yield return new WaitForSeconds(t);
        didClassChange = false;
    }

    /// <summary>
    /// The Rigidbody.Rotation is frozen for all except y values.  This allows you to rotate 
    /// the model around without it falling over.Not currently implemented.
    /// </summary>
    /// <param name="index">Key for the dictionary to be able to get the correct string value for instantiation</param>
    public void SetCurrentCharacter(CharacterInfo.Class index)
    {
        Destroy(model);
        model = Instantiate(Resources.Load("Prefabs/" + characterDictionary[index]), spawnPoint.position, Quaternion.identity) as GameObject;
        model.transform.rotation = Quaternion.Euler(model.transform.rotation.x, model.transform.rotation.y + 180f, model.transform.rotation.z);
        model.GetComponent<MovementScript>().enabled = false;
        model.GetComponent<HeroScript>().id = playerID;
        model.GetComponent<Entity>().enabled = false;
        model.GetComponent<PlayerGemScript>().enabled = false;
        model.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
    }

    /// <summary>
    /// Uses the playerID assigned in inspector on the pedestal object. 
    ///  If the control setup changes, then this MUST CHANGE AS WELL.
    /// </summary>
    void SetUpPlayer()
    {
        switch (playerID)
        {
            case 1:
                horizAxis = "Horizontal_P1";
                lockButton = "Selection_P1";
                break;
            case 2:
                horizAxis = "Horizontal_P2";
                lockButton = "Selection_P2";
                break;
            case 3:
                horizAxis = "Horizontal_P3";
                lockButton = "Selection_P3";
                break;
            case 4:
                horizAxis = "Horizontal_P4";
                lockButton = "Selection_P4";
                break;
        }
    }

    /// <summary>
    /// This is called AFTER everyone has been locked.
    /// Do NOT call this function beforehand.
    /// </summary>
    /// <param name="key">string to assign the GameControl.control.player<player#>Prefab by the playerID</param>
    public void SetPlayerCharacter(string key)
    {
        switch (playerID)
        {
            case 1:
                GameControl.control.player1Prefab = key;
                break;
            case 2:
                GameControl.control.player2Prefab = key;
                break;
            case 3:
                GameControl.control.player3Prefab = key;
                break;
            case 4:
                GameControl.control.player4Prefab = key;
                break; 
        }

        
    }

}
