﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerSpawnerScript : MonoBehaviour
{

    public Vector3 player1UIPosition;
    public Vector3 player2UIPosition;
    public Vector3 player3UIPosition;
    public Vector3 player4UIPosition;

    public GameObject player1Number;
    public GameObject player2Number;
    public GameObject player3Number;
    public GameObject player4Number;

    public GameObject teleportPS;

    // Hold the players for a hot sec.
    private GameObject p1, p2, p3, p4;

    /*
     * Start()
     * Important: GameControl.control.controllersAmount is used to check how many 
     *              players to instantiate.  GameControl.control.player<player#>Prefab 
     *              MUST be set before this script starts.  This sets up the player
     *              character to use that player's controller.
     *              
     */
    private void Start()
    {
    }

    public void Spawn()
    {
        if (GameControl.control.completelyDeadPlayers.Count == 0 && GameControl.control.firstLevel)
        {
            p1 = Instantiate(Resources.Load("Prefabs/" + GameControl.control.player1Prefab, typeof(GameObject)),
                new Vector3(transform.position.x - 2f, transform.position.y, transform.position.z + 2f), Quaternion.identity) as GameObject;
            Instantiate(teleportPS, p1.transform.position, Quaternion.identity);
            p1.name = "Player 1";
            p1.GetComponent<HeroScript>().id = 1;
            Transform canvas = p1.transform.Find("Canvas");
            canvas.GetChild(0).gameObject.SetActive(true);
            Transform screenSpaceCanvas = p1.transform.Find("ScreenSpaceCanvas");
            screenSpaceCanvas.GetChild(0).gameObject.SetActive(true);
            Transform background = screenSpaceCanvas.Find("Background");
            background.GetComponent<RectTransform>().position = player1UIPosition;
            GameObject p1Number = Instantiate(player1Number, Vector3.zero, Quaternion.identity, background.Find("PlayerNumberHolder"));
            p1Number.transform.localPosition = Vector3.zero;
            p1Number.transform.localRotation = Quaternion.identity;
            background.GetComponent<FadeUIScript>().images.Add(p1Number.GetComponent<Image>());
            p1.GetComponent<MovementScript>().canMove = false;
            Utility.Log("Spawned Player 1");

            if (GameControl.control.controllersAmount >= 2)
            {
                p2 = Instantiate(Resources.Load("Prefabs/" + GameControl.control.player2Prefab, typeof(GameObject)),
                    new Vector3(transform.position.x - 2f, transform.position.y, transform.position.z - 2f), Quaternion.identity) as GameObject;
                Instantiate(teleportPS, p2.transform.position, Quaternion.identity);
                p2.name = "Player 2";

                HeroScript hero = p2.GetComponent<HeroScript>();
                hero.id = 2;

                Transform canvas2 = p2.transform.Find("Canvas");
                canvas2.GetChild(0).gameObject.SetActive(true);
                Transform screenSpaceCanvas2 = p2.transform.Find("ScreenSpaceCanvas");
                screenSpaceCanvas2.GetChild(0).gameObject.SetActive(true);
                Transform background2 = screenSpaceCanvas2.Find("Background");
                background2.GetComponent<RectTransform>().position = player2UIPosition;
                GameObject p2Number = Instantiate(player2Number, Vector3.zero, Quaternion.identity, background2.Find("PlayerNumberHolder"));
                p2Number.transform.localPosition = Vector3.zero;
                p2Number.transform.localRotation = Quaternion.identity;
                background.GetComponent<FadeUIScript>().images.Add(p2Number.GetComponent<Image>());
                p2.GetComponent<MovementScript>().canMove = false;
            }

            if (GameControl.control.controllersAmount >= 3)
            {
                p3 = Instantiate(Resources.Load("Prefabs/" + GameControl.control.player3Prefab, typeof(GameObject)),
                    new Vector3(transform.position.x + 2f, transform.position.y, transform.position.z + 2f), Quaternion.identity) as GameObject;
                Instantiate(teleportPS, p3.transform.position, Quaternion.identity);
                p3.name = "Player 3";

                HeroScript hero = p3.GetComponent<HeroScript>();
                hero.id = 3;

                Transform canvas3 = p3.transform.Find("Canvas");
                canvas3.GetChild(0).gameObject.SetActive(true);
                Transform screenSpaceCanvas3 = p3.transform.Find("ScreenSpaceCanvas");
                screenSpaceCanvas3.GetChild(0).gameObject.SetActive(true);
                Transform background3 = screenSpaceCanvas3.Find("Background");
                background3.GetComponent<RectTransform>().position = player3UIPosition;
                GameObject p3Number = Instantiate(player3Number, Vector3.zero, Quaternion.identity, background3.Find("PlayerNumberHolder"));
                p3Number.transform.localPosition = Vector3.zero;
                p3Number.transform.localRotation = Quaternion.identity;
                background.GetComponent<FadeUIScript>().images.Add(p3Number.GetComponent<Image>());
                p3.GetComponent<MovementScript>().canMove = false;
            }

            if (GameControl.control.controllersAmount >= 4)
            {
                p4 = Instantiate(Resources.Load("Prefabs/" + GameControl.control.player4Prefab, typeof(GameObject)),
                    new Vector3(transform.position.x + 2f, transform.position.y, transform.position.z - 2f), Quaternion.identity) as GameObject;
                Instantiate(teleportPS, p4.transform.position, Quaternion.identity);
                p4.name = "Player 4";

                HeroScript hero = p4.GetComponent<HeroScript>();
                hero.id = 4;

                Transform canvas4 = p4.transform.Find("Canvas");
                canvas4.GetChild(0).gameObject.SetActive(true);
                Transform screenSpaceCanvas4 = p4.transform.Find("ScreenSpaceCanvas");
                screenSpaceCanvas4.GetChild(0).gameObject.SetActive(true);
                Transform background4 = screenSpaceCanvas4.Find("Background");
                background4.GetComponent<RectTransform>().position = player4UIPosition;
                GameObject p4Number = Instantiate(player4Number, Vector3.zero, Quaternion.identity, background4.Find("PlayerNumberHolder"));
                p4Number.transform.localPosition = Vector3.zero;
                p4Number.transform.localRotation = Quaternion.identity;
                background.GetComponent<FadeUIScript>().images.Add(p4Number.GetComponent<Image>());
                p4.GetComponent<MovementScript>().canMove = false;
            }
        }
        else
        {
            GameObject[] players = GameControl.control.players;
            if (players.Length > 3)
            {
                p4 = players[3];
                p4.transform.Find("g_mesh").gameObject.SetActive(true);
                players[3].transform.position = new Vector3(transform.position.x + 2f, transform.position.y, transform.position.z - 2f);
                var ps4 = Instantiate(teleportPS, players[3].transform.position, Quaternion.identity);
                ps4.transform.parent = p4.transform;
            }
            if (players.Length > 2)
            {
                p3 = players[2];
                p3.transform.Find("g_mesh").gameObject.SetActive(true);
                players[2].transform.position = new Vector3(transform.position.x + 2f, transform.position.y, transform.position.z + 2f);
                var ps3 = Instantiate(teleportPS, players[2].transform.position, Quaternion.identity);
                ps3.transform.parent = p3.transform;
            }
            if (players.Length > 1)
            {
                p2 = players[1];
                p2.transform.Find("g_mesh").gameObject.SetActive(true);
                players[1].transform.position = new Vector3(transform.position.x - 2f, transform.position.y, transform.position.z - 2f);
                var ps2 = Instantiate(teleportPS, players[1].transform.position, Quaternion.identity);
                ps2.transform.parent = p2.transform;
            }

            p1 = players[0];
            p1.transform.Find("g_mesh").gameObject.SetActive(true);
            players[0].transform.position = new Vector3(transform.position.x - 2f, transform.position.y, transform.position.z + 2f);
            var ps = Instantiate(teleportPS, players[0].transform.position, Quaternion.identity);
            ps.transform.parent = p1.transform;
        }
    }

    public IEnumerator LetPlayersMoveAgain(float t)
    {
        yield return new WaitForSeconds(t);
        if (p1)
        {
            p1.GetComponent<MovementScript>().ResetValues();
            p1.GetComponent<HeroScript>().isAttacking = false;
        }

        if (p2)
        {
            p2.GetComponent<MovementScript>().ResetValues();
            p2.GetComponent<HeroScript>().isAttacking = false;
        }

        if (p3)
        {
            p3.GetComponent<MovementScript>().ResetValues();
            p3.GetComponent<HeroScript>().isAttacking = false;
        }

        if (p4)
        {
            p4.GetComponent<MovementScript>().ResetValues();
            p4.GetComponent<HeroScript>().isAttacking = false;
        }
    }
}
