﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnScript : MonoBehaviour
{
    public float respawnRange = 8f;
    public int id = -1;
    public GameObject player;

    public Color32 red = new Color32(0xC2, 0x17, 0x16, 0xFF);
    public Color32 blue = new Color32(0x18, 0x27, 0xBF, 0xFF);
    public Color32 green = new Color32(0x4A, 0xC9, 0x1E, 0xFF);
    public Color32 purple = new Color32(0x8B, 0x00, 0xC2, 0xFF);

    private SpriteRenderer spriteRenderer;

    private bool isStarted = false;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        switch(id)
        {
            case 1:
                spriteRenderer.color = red;
                break;
            case 2:
                spriteRenderer.color = blue;
                break;
            case 3:
                spriteRenderer.color = green;
                break;
            case 4:
                spriteRenderer.color = purple;
                break;
        }

        StartCoroutine(LateStart(1.2f));
    }

    private IEnumerator LateStart(float t)
    {
        isStarted = false;
        yield return new WaitForSeconds(t);
        isStarted = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isStarted)
        {
            if (other.transform.parent.gameObject != player && other.tag == "Hurtbox")
            {
                //GameControl.control.RespawnPlayer(id);
                Destroy(other.transform.parent.gameObject);
            }
        }
    }
}
