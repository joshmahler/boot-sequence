﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadPlayerMatsScript : MonoBehaviour
{
    public Material invulnMat;
    // Start is called before the first frame update
    void Start()
    {
        LoadMats();
    }

    /// <summary>
    /// This is really hacky, but load the materials based on the class.
    /// </summary>
    void LoadMats()
    {
        string loader = "Materials/Characters";
        Transform t;
        // I.e, the knight
        if (GetComponent<HeroScript>().myClass == HeroScript.Class.Knight)
        {
            Utility.Log("Loading mats");
            loader += "/matt_knight_P" + GetComponent<HeroScript>().id;
            t = transform.GetChild(1);
            Material[] mats = { Resources.Load<Material>(loader) , invulnMat};
            t.GetChild(0).GetComponent<SkinnedMeshRenderer>().materials = mats;
            t.GetChild(1).GetComponent<SkinnedMeshRenderer>().materials = mats;
            t.GetChild(2).GetComponent<SkinnedMeshRenderer>().materials = mats;
            loader = "Materials/Characters/matt_knightWep_P" + GetComponent<HeroScript>().id;
            Material[] mats2 = { Resources.Load<Material>(loader) , invulnMat};
            t.GetChild(3).GetComponent<SkinnedMeshRenderer>().materials = mats2;
            t.GetChild(4).GetComponent<SkinnedMeshRenderer>().materials = mats2;
        }

        if (GetComponent<HeroScript>().myClass == HeroScript.Class.Mage)
        {
            loader += "/matt_mage_P" + GetComponent<HeroScript>().id;
            t = transform.GetChild(1);
            Material[] mats = { Resources.Load<Material>(loader), invulnMat };
            foreach (Transform c in t)
            {
                //c.GetComponent<SkinnedMeshRenderer>().materials = mats;
                //Utility.Log(c);
                c.gameObject.GetComponent<SkinnedMeshRenderer>().materials = mats;
            }
        }
    }
}
