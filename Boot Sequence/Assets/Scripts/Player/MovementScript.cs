﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour
{
    public float dashSpeed;                 // The multiplier to be applied when player dashes
    public float maxHoverHeight;            // The y-value of the maximum height that can be reached when hover is active
    public float hoverHeightInterval;       // The height that is applied to the transform.position.y after x seconds
    public float fullHoverSpeedSeconds;     // The y-speed applied every x seconds to go full speed while hovering
    public float inAirHoverRechargeSeconds; // The seconds to recharge the full hover y-speed while in the air
    public float hoverSpeed;                // The x/z-speed applied while hovering
    public float groundSpeed;               // The x/z-speed applied while walking

    public Transform hoverParticleSpawner;
    public List<GameObject> idleHoverParticle;
    public List<GameObject> moveHoverParticle;

    
    //public AK.Wwise.Event footstepEvent;

    private Rigidbody rb;               // The Rigidbody on the player character
    private float moveSpeed;            // The current x/z-speed
    private bool isHoverEnding = false; // Is the hover currently ending?
    private PlayerGemScript gemScript;  // The PlayerGemScript component attatched to this object
    private Quaternion prevRotation = Quaternion.identity;
    private HeroScript hero;
    private GameObject idleParticle;        //The idle hover particle that holds the GameObject that will get instantiated
    private GameObject movingParticle;      //The moving hover particle that holds the GameObject that will get instantiated
    private GameObject idleInitParticle;    //A wrapper GameObject that knows if the idle hover particle has been instantiated
    private GameObject movingInitParticle;  //A wrapper GameObject that knows if the moving hover particle has been instantiated
    private Animator anim;

    [HideInInspector] public bool canMove = true;        // Can this player character move currently?
    [HideInInspector] public bool isHovering = false;       // Is the player character currently hovering?
    [HideInInspector] public bool canHover = true;          // Can the player character currently hover?
    [HideInInspector] public bool canDash = false;           // Can the player character currently dash?
    [HideInInspector] public bool isDashing = false;        // Is the player character currently dashing?

    /*
     * Start the moveSpeed to be groundSpeed.
     */
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        gemScript = GetComponent<PlayerGemScript>();
        hero = GetComponent<HeroScript>();
        moveSpeed = groundSpeed;

        switch(hero.id)
        {
            case 1:
                idleParticle = idleHoverParticle[0];
                movingParticle = moveHoverParticle[0];
                break;
            case 2:
                idleParticle = idleHoverParticle[1];
                movingParticle = moveHoverParticle[1];
                break;
            case 3:
                idleParticle = idleHoverParticle[2];
                movingParticle = moveHoverParticle[2];
                break;
            case 4:
                idleParticle = idleHoverParticle[3];
                movingParticle = moveHoverParticle[3];
                break;
        }

        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (!hero.isStunned)
        {

            if (InputManager.input.HoverButtonDown(hero.id) && canHover)
            {
                AkSoundEngine.PostEvent("Hover", gameObject);
            }

            if (InputManager.input.HoverButtonUp(hero.id))
            {
                AkSoundEngine.PostEvent("Hover_Stop", gameObject);
            }
        }
        else
        {
            anim.SetBool("running", false);
            anim.SetBool("hover", false);
            anim.SetBool("kick", false);
            anim.SetBool("super", false);
            anim.SetBool("attack", false);
            anim.SetBool("dashing", false);
        }

        /*
         * If the player can currently hover and the hover button is pressed
         * then shut gravity off for the player and manually change 
         * the y-position of the player character.
         */
        if (canHover && !hero.isStunned && InputManager.input.HoverButton(hero.id))
        {

            rb.useGravity = false;
            float yPos = transform.position.y;

            if (rb.velocity.magnitude > 0)
            {
                if (movingInitParticle == null)
                    movingInitParticle = Instantiate(movingParticle, hoverParticleSpawner);

                movingInitParticle.transform.GetChild(0).rotation = Quaternion.LookRotation(-transform.forward);

                if (idleInitParticle != null)
                    Destroy(idleInitParticle);
            }
            else
            {
                if (idleInitParticle == null)
                    idleInitParticle = Instantiate(idleParticle, hoverParticleSpawner);

                if (movingInitParticle != null)
                    Destroy(movingInitParticle);
            }
            /*
             * If the hover is not currently ending
             * then apply the full hoverHeightInterval to the y-position.
             */
            if (!isHoverEnding)
            {
                yPos = transform.position.y + hoverHeightInterval;
                StartCoroutine(HoverEnding(fullHoverSpeedSeconds, inAirHoverRechargeSeconds));
            }
            /*
             * If the hover is ending, that means the hover has been rapidly pressed.
             * Apply half of the hoverHeightInterval to the y-position.
             */
            else
            {
                yPos = transform.position.y + (hoverHeightInterval / 2);
            }
            /*
             * Check to see if the y-position set above is greater than the maximum hover height.
             */
            if (yPos >= maxHoverHeight - hoverHeightInterval)
            {
                yPos = maxHoverHeight;
            }

            /*
             * Apply the new y-position to the transform.position,
             * set moveSpeed to the hoverSpeed,
             * and change isHovering to true.
             */
            transform.position = new Vector3(transform.position.x, yPos, transform.position.z);
            moveSpeed = hoverSpeed;
            isHovering = true;
            gemScript.OnHover();
        }
            /*
             * If the player cannot currently hover or the hover button is not being pressed
             * set isHovering to false,
             * apply gravity to the player,
             * and set the moveSpeed back to the groundSpeed.
             */
        else
        {
            isHovering = false;
            rb.useGravity = true;
            moveSpeed = groundSpeed;

            if (idleInitParticle != null)
                Destroy(idleInitParticle);
            if (movingInitParticle != null)
                Destroy(movingInitParticle);
        }

        anim.SetBool("hover", isHovering);
     }

    /*
 * Time.deltaTime uses Physics, therefore it has to be in FixedUpdate()
 */
    void FixedUpdate()
    {
        if (!hero.isStunned)
        {
            /*
             * If the character can currently move,
             * move the character and 
             * rotate the character to look in the direction its going
             */
            if (canMove)
            {
                Vector3 movement = InputManager.input.MainMovement(hero.id);

                if (hero.isBlinking)
                {
                    prevRotation = Quaternion.LookRotation(transform.forward);
                    anim.SetBool("running", false);
                }
                else if (movement == Vector3.zero)
                {
                    transform.rotation = prevRotation;
                    anim.SetBool("running", false);
                }
                else
                {
                    transform.rotation = Quaternion.LookRotation(movement);
                    prevRotation = transform.rotation;
                    anim.SetBool("running", true);
                }

                Vector3 move = movement * moveSpeed;
                rb.velocity = new Vector3(move.x, rb.velocity.y, move.z);
            }
            else if (isDashing)
            {
                Vector3 movement = InputManager.input.MainMovement(hero.id);
                transform.rotation = Quaternion.LookRotation(movement);

                Vector3 move = movement * moveSpeed;
                move = new Vector3(move.x, rb.velocity.y, move.z);

                transform.Translate(move * Time.deltaTime, Space.World);

                anim.SetBool("hover", true);
                anim.SetBool("dashing", true);
            }
            else
            {
                anim.SetBool("running", false);
                anim.SetBool("hover", false);
                anim.SetBool("kick", false);
                anim.SetBool("super", false);
                anim.SetBool("attack", false);
                anim.SetBool("dashing", false);
            }
        }

        /*
         * If the character can curretly dash, and the dash is not currently ending,
         * and the dash button is pressed down, then apply a force to the character
         * and set isDashing to true.
         */
        if (canDash && !hero.isStunned && !isDashing && InputManager.input.DashButton(hero.id))
        {
            gemScript.OnDash();
            AkSoundEngine.PostEvent(1942692385, gameObject);
            rb.AddForce(transform.forward * dashSpeed * gemScript.dashSpeedMod);
            StartCoroutine(DashEnding(.75f));
        }
    }


     void FootstepAnimSound() //called from run anim notify
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -Vector3.up, out hit, 1f))
        {
            if (hit.collider.gameObject.layer == 14) //if it's the ground layer
            {

                AkSoundEngine.PostEvent("KnightFootstep", gameObject);

            }
        }
    }

    /*
     * DashEnding(float t)
     *      - t: Seconds to wait for the dash to stop
     * Important: Setting isDashEnding to true means it will freeze the rotation
     *              to prevent the character from falling over. After t seconds, the 
     *              character can look where its going again.
     */
    IEnumerator DashEnding(float t)
    {
        canMove = false;
        isDashing = true;
        yield return new WaitForSeconds(t);
        isDashing = false;
        canMove = true;
        anim.SetBool("hover", false);
        anim.SetBool("dashing", false);
    }

    /*
     * HoverEnding(float fullHoverSeconds, float rechargeSeconds)
     *      - fullHoverSeconds: Seconds to wait before setting isHoverEnding to true. 
     *                          This means the full hover interval will be applied to
     *                          the y-position for t seconds.
     *      - rechargeSeconds: Seconds to wait for the half y-position interval to end
     */
    IEnumerator HoverEnding(float fullHoverSeconds, float rechargeSeconds)
    {
        yield return new WaitForSeconds(fullHoverSeconds);
        isHoverEnding = true;

        yield return new WaitForSeconds(rechargeSeconds);
        isHoverEnding = false;
    }

    public void ResetValues()
    {
        canMove = true;
        transform.GetComponentInChildren<HoverScript>().inactiveStarted = false;
        transform.GetComponentInChildren<DashScript>().inactiveStarted = false;
    }
}



