﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class KnightScript : HeroScript
{
    public GameObject kickHitbox;
    public GameObject swordHitbox;
    public Transform hitboxSpawner;
    public List<GameObject> specialParticleColors;

    public Transform weaponGrip;
    public Transform rightHand;

    private bool canDangerSpeak = true;
    private bool shouldDangerSpeak = false;
    private Animator anim;
    private PlayerGemScript playerGemScript; // The PlayerGemScript attatched to this object
    private MovementScript playerMovementScript; // The MovementScript attatched to this object
    private GameObject specialParticle;

    public override void Start()
    {
        anim = GetComponent<Animator>();
        playerGemScript = GetComponent<PlayerGemScript>();
        playerMovementScript = GetComponent<MovementScript>();
        base.Start();
        AddSkill(SkillPoolScript.Skill.SkillType.KICK);
        AddSkill(SkillPoolScript.Skill.SkillType.KNIGHT_BASIC);
        AddSkill(SkillPoolScript.Skill.SkillType.KNIGHT_ABILITY);

        
        StartCoroutine(DangerVOCheck());
        StartCoroutine(DangerVO());

        switch(id)
        {
            case 1:
                specialParticle = specialParticleColors[0];
                break;
            case 2:
                specialParticle = specialParticleColors[1];
                break;
            case 3:
                specialParticle = specialParticleColors[2];
                break;
            case 4:
                specialParticle = specialParticleColors[3];
                break;
        }
        
        // returns a kick Skill -> attackList[0];
    }

    private void LateUpdate()
    {
        weaponGrip.position = rightHand.position;
    }

    void AttackAnimSound() //called from attack anim notify
    {
        AkSoundEngine.PostEvent("KnightAttack", gameObject);
    }

    void SpecialAnimSound() //called from run anim notify
    {
        AkSoundEngine.PostEvent("KnightSpecial", gameObject);
    }

    void SpecialVOAnimSound()
    {
        AkSoundEngine.PostEvent("KnightSpecialVO", gameObject);
    }

    public override void Update()
    {
        base.Update();

        if (!isStunned)
        {
            //BASIC ATTACK
            if (!isAttacking && InputManager.input.MeleeButton(id))
            {
                outgoingAttack = skills[1];

                anim.SetBool("attack", true);

                //apply gem mod if hovering 
                if (playerGemScript != null && playerMovementScript.isHovering)
                {
                    outgoingAttack.damage *= playerGemScript.attackDamageMult;
                }

                coroutine = StartCoroutine(Attack(outgoingAttack.windupFrames, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames,
                    swordHitbox, hitboxSpawner, null, null));

                if (animCoroutine != null)
                {
                    StopCoroutine(animCoroutine);
                }
                animCoroutine = StartCoroutine(ResetAnimatorBool(anim, "attack", false, .4f));
            }


            //SPECIAL ATTACK
            if (!isAttacking && InputManager.input.SpecialAttackButton(id))
            {
                outgoingAttack = skills[2];

                anim.SetBool("running", false);
                anim.SetBool("super", true);

                //apply gem boost if we're hovering
                if (playerGemScript != null && playerMovementScript.isHovering)
                {
                    outgoingAttack.damage *= playerGemScript.attackDamageMult;
                }
                ms.canMove = false;
                ms.canHover = false;
                coroutine = StartCoroutine(Attack(outgoingAttack.windupFrames, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames,
                    swordHitbox, hitboxSpawner, null, null));

                StartCoroutine(AttackParticle(outgoingAttack.windupFrames, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames, specialParticle, hitboxSpawner, this, ParticlesScript.HitboxType.PLAYER_HITBOX));

                if (animCoroutine != null)
                {
                    StopCoroutine(animCoroutine);
                }
                StartCoroutine(ResetAnimatorBool(anim, "super", false, .2f));
            }

            //KICK
            if (!isAttacking && InputManager.input.KickButton(id))
            {
                outgoingAttack = skills[0];

                anim.SetBool("kick", true);

                if (playerGemScript != null)
                {
                    outgoingAttack.damage *= playerGemScript.kickDamageMult;
                }

                Kick(outgoingAttack.windupFrames - playerGemScript.stunKickWindupMod, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames,
                    kickHitbox, transform, null, null);

                StartCoroutine(AttackParticle(outgoingAttack.windupFrames, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames, kickParticle, hitboxSpawner, this, ParticlesScript.HitboxType.KICK_HITBOX));

                if (animCoroutine != null)
                {
                    StopCoroutine(animCoroutine);
                }
                StartCoroutine(ResetAnimatorBool(anim, "kick", false, .2f));
            }
        }
    }

    IEnumerator DangerVOCheck()
    {
        // first, this checks if the player has low health and if they've already said "I'm in danger" recently.
        // if the player has low health and this would be their first "I'm in danger since having high health, the physics overlap box checks for nearby enemies.
        // if enough enemies are detected nearby, "I'M IN DANGER"


        while (this != null)
        {
           
                if (health >= 100)
                {
                    canDangerSpeak = true;
                }
                else
                {
                    shouldDangerSpeak = true;
                }
            
                
            yield return new WaitForSeconds(15);
        }

    }

    IEnumerator DangerVO()
    {
        while (this != null)
        {
           if (GameControl.control.playersAlive >= 2)
            {

               

                if (canDangerSpeak)
                {
                    int enemyCount = 0;
                    Collider[] cols = Physics.OverlapBox(transform.position, new Vector3(15, 15, 15));
                    for (int i = 0; i < cols.Length; i++)
                    {
                        if (cols[i].tag == "Enemy")
                        {
                            enemyCount++;
                        }
                    }
                    if (enemyCount >= 3)
                    {
                        if (canDangerSpeak)
                        {
                            if (shouldDangerSpeak && health <= 75)
                            {
                                if (myClass == Class.Knight)
                                    AkSoundEngine.PostEvent("KnightDangerVO", gameObject);

                                else if (myClass == Class.Mage)
                                    AkSoundEngine.PostEvent("MageDangerVO", gameObject);

                                canDangerSpeak = false;
                                shouldDangerSpeak = false;
                            }

                        }
                    }


                }
            }
               
            yield return new WaitForSeconds(5);
        }

    }
}
