﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonkScript : HeroScript
{
    public GameObject kickHitbox;
    public GameObject abilityHitbox;
    public GameObject basicHitbox;
    public AK.Wwise.Event monkAttackEvent;
    public AK.Wwise.Event monkHitEvent;
    public AK.Wwise.Event monkSpecialEvent;

    private PlayerGemScript playerGemScript; // The PlayerGemScript attatched to this object
    private MovementScript playerMovementScript; // The MovementScript attatched to this object


    public override void Start()
    {
        playerGemScript = GetComponent<PlayerGemScript>();
        playerMovementScript = GetComponent<MovementScript>();
        base.Start();
        AddSkill(SkillPoolScript.Skill.SkillType.KICK);
        AddSkill(SkillPoolScript.Skill.SkillType.MONK_BASIC);
        AddSkill(SkillPoolScript.Skill.SkillType.MONK_ABILITY);

        // returns a kick Skill -> attackList[0];
    }

    public override void Update()
    {
        base.Update();

        if (!isStunned)
        {
            //BASIC ATTACK
            if (!isAttacking && InputManager.input.MeleeButton(id))
            {
                Utility.Log("monk basic attack");
                outgoingAttack = skills[1];

                //apply gem mod if hovering 
                if (playerGemScript != null && playerMovementScript.isHovering)
                {
                    outgoingAttack.damage *= playerGemScript.attackDamageMult;
                }

                coroutine = StartCoroutine(Attack(outgoingAttack.windupFrames, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames,
                    basicHitbox, transform, (GameObject)Resources.Load("Prefabs/Hitbox/Windup"), (GameObject)Resources.Load("Prefabs/Hitbox/Cooldown")));
            }


            //SPECIAL ATTACK
            if (!isAttacking && InputManager.input.SpecialAttackButton(id))
            {
                Utility.Log("monk ability");
                outgoingAttack = skills[2];

                //apply gem boost if we're hovering
                if (playerGemScript != null && playerMovementScript.isHovering)
                {
                    outgoingAttack.damage *= playerGemScript.attackDamageMult;
                }
                ms.canMove = false;
                coroutine = StartCoroutine(Attack(outgoingAttack.windupFrames, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames,
                    abilityHitbox, transform, (GameObject)Resources.Load("Prefabs/Hitbox/MonkAbilityWindup"), (GameObject)Resources.Load("Prefabs/Hitbox/Cooldown")));
            }

            //KICK
            if (!isAttacking && InputManager.input.KickButton(id))
            {
                Utility.Log("Kick monk");
                outgoingAttack = skills[0];
                if (playerGemScript != null)
                {
                    outgoingAttack.damage *= playerGemScript.kickDamageMult;
                }

                Kick(outgoingAttack.windupFrames - playerGemScript.stunKickWindupMod, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames,
                    kickHitbox, transform, (GameObject)Resources.Load("Prefabs/Hitbox/KickWindup"), (GameObject)Resources.Load("Prefabs/Hitbox/KickCooldown"));
            }
        }
    }
}
