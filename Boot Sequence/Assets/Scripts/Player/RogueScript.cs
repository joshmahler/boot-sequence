﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RogueScript : HeroScript
{
    public GameObject kickHitbox;               // the hitbox for the kick
    public GameObject daggerProjectile;         // The projectile for the basic
    public GameObject blinkProjectile;          // The projectile for the ability
    public Transform daggerSpawner0;            // Spawner for the dagger throw
    public Transform daggerSpawner1;            // Spawner for the dagger throw
    public Transform forwardHitboxSpawner;      // This will be the spawner for both the projectiles
    public Transform daggerSpawner2;            // Spawner for the dagger throw
    public Transform daggerSpawner3;            // Spawner for the dagger throw
    public GameObject windupProjectile;
    public GameObject cooldownProjectile;
    public AK.Wwise.Event rogueAttackEvent;
    public AK.Wwise.Event rogueHitEvent;
    public AK.Wwise.Event rogueSpecialEvent;

    private PlayerGemScript playerGemScript; // The PlayerGemScript attatched to this object
    private MovementScript playerMovementScript; // The MovementScript attatched to this object

    public override void Start()
    {
        playerGemScript = GetComponent<PlayerGemScript>();
        playerMovementScript = GetComponent<MovementScript>();
        base.Start();
        AddSkill(SkillPoolScript.Skill.SkillType.KICK);
        AddSkill(SkillPoolScript.Skill.SkillType.ROGUE_BASIC);
        AddSkill(SkillPoolScript.Skill.SkillType.ROGUE_ABILITY);

        // returns a kick Skill -> attackList[0];
    }

    public override void Update()
    {
        base.Update();

        if (!isStunned)
        {
            //BASIC ATTACK
            if (!isAttacking && InputManager.input.MeleeButton(id))
            {
                Utility.Log("rogue basic attack");
                outgoingAttack = skills[1];

                //apply gem mod if hovering 
                if (playerGemScript != null && playerMovementScript.isHovering)
                {
                    outgoingAttack.damage *= playerGemScript.attackDamageMult;
                }

                // Spawn 5 daggers
                coroutine = StartCoroutine(AttackProjectile(outgoingAttack.windupFrames, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames, 
                    daggerProjectile, forwardHitboxSpawner, windupProjectile, cooldownProjectile));

                StartCoroutine(AttackProjectile(outgoingAttack.windupFrames, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames,
                    daggerProjectile, daggerSpawner0, windupProjectile, cooldownProjectile));

                StartCoroutine(AttackProjectile(outgoingAttack.windupFrames, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames,
                    daggerProjectile, daggerSpawner1, windupProjectile, cooldownProjectile));

                StartCoroutine(AttackProjectile(outgoingAttack.windupFrames, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames,
                    daggerProjectile, daggerSpawner2, windupProjectile, cooldownProjectile));

                StartCoroutine(AttackProjectile(outgoingAttack.windupFrames, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames,
                    daggerProjectile, daggerSpawner3, windupProjectile, cooldownProjectile));
            }


            //SPECIAL ATTACK
            if (!isAttacking && InputManager.input.SpecialAttackButton(id))
            {
                Utility.Log("rogue ability");
                outgoingAttack = skills[2];

                //apply gem boost if we're hovering
                if (playerGemScript != null && playerMovementScript.isHovering)
                {
                    outgoingAttack.damage *= playerGemScript.attackDamageMult;
                }

                coroutine = StartCoroutine(AttackProjectile(outgoingAttack.windupFrames, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames,
                    blinkProjectile, forwardHitboxSpawner, windupProjectile, cooldownProjectile));
            }

            //KICK
            if (!isAttacking && InputManager.input.KickButton(id))
            {
                Utility.Log("Kick rogue");
                outgoingAttack = skills[0];
                if (playerGemScript != null)
                {
                    outgoingAttack.damage *= playerGemScript.kickDamageMult;
                }

                Kick(outgoingAttack.windupFrames - playerGemScript.stunKickWindupMod, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames,
                    kickHitbox, transform, (GameObject)Resources.Load("Prefabs/Hitbox/KickWindup"), (GameObject)Resources.Load("Prefabs/Hitbox/KickCooldown"));
            }
        }
    }
}
