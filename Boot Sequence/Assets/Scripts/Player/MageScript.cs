﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MageScript : HeroScript
{
    public GameObject kickHitbox;
    public List<GameObject> projectileColors;
    public List<GameObject> vigorParticleColors;
    public Transform vigorSpawnTransform;
    public Transform hitboxSpawner;
    public Transform lFireball;
    public Transform rFireball;
    public float specialSelfDamage;

    public GameObject windupProjectile;
    public GameObject cooldownProjectile;

    private PlayerGemScript playerGemScript; // The PlayerGemScript attatched to this object

    private bool fireLeft = true;
    private GameObject vigorSphere;  // The vigor object (ability attack).
    private GameObject projectile;   // The projectile object (basic attack).
    private Animator anim;

    public override void Start()
    {
        playerGemScript = GetComponent<PlayerGemScript>();
        anim = GetComponent<Animator>();
        base.Start();
        AddSkill(SkillPoolScript.Skill.SkillType.KICK);
        AddSkill(SkillPoolScript.Skill.SkillType.MAGE_BASIC);
        AddSkill(SkillPoolScript.Skill.SkillType.MAGE_ABILITY);

        switch(id)
        {
            case 1:
                projectile = projectileColors[0];
                vigorSphere = vigorParticleColors[0];
                break;
            case 2:
                projectile = projectileColors[1];
                vigorSphere = vigorParticleColors[1];
                break;
            case 3:
                projectile = projectileColors[2];
                vigorSphere = vigorParticleColors[2];
                break;
            case 4:
                projectile = projectileColors[3];
                vigorSphere = vigorParticleColors[3];
                break;
        }
    }

    public override void Update()
    {
        base.Update();
        if (!isStunned)
        {
            //BASIC ATTACK
            if (!isAttacking && InputManager.input.MeleeButton(id))
            {
                outgoingAttack = skills[1];

                anim.SetBool("isLeftHand", fireLeft);

                if (fireLeft)
                {
                    coroutine = StartCoroutine(AttackParticle(outgoingAttack.windupFrames, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames,
                        projectile, lFireball, this, ParticlesScript.HitboxType.PLAYER_HITBOX, "FireballCast"));
                    fireLeft = false;
                }
                else
                {
                    coroutine = StartCoroutine(AttackParticle(outgoingAttack.windupFrames, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames,
                        projectile, rFireball, this, ParticlesScript.HitboxType.PLAYER_HITBOX, "FireballCast"));
                    fireLeft = true;
                }

                anim.SetBool("attack", true);

                if (animCoroutine != null)
                {
                    StopCoroutine(animCoroutine);
                }

                StartCoroutine(ResetAnimatorBool(anim, "attack", false, .2f));
            }


            //SPECIAL ATTACK
            if (!isAttacking && health > 25f && InputManager.input.SpecialAttackButton(id))
            {
                outgoingAttack = skills[2];

                anim.SetBool("super", true);

                coroutine = StartCoroutine(AttackParticle(outgoingAttack.windupFrames, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames, 
                    vigorSphere, transform, this, ParticlesScript.HitboxType.INVALID, "MageSpecial"));

                if (animCoroutine != null)
                {
                    StopCoroutine(animCoroutine);
                }
                StartCoroutine(SpecialSelfDamage(outgoingAttack.windupFrames));
                StartCoroutine(ResetAnimatorBool(anim, "super", false, .2f));
            }

            if(!isAttacking && InputManager.input.KickButton(id))
            {

                outgoingAttack = skills[0];

                anim.SetBool("kick", true);

                if (playerGemScript != null)
                {
                    outgoingAttack.damage *= playerGemScript.kickDamageMult;
                }

                Kick(outgoingAttack.windupFrames - playerGemScript.stunKickWindupMod, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames,
                    kickHitbox, transform, null, null);

                StartCoroutine(AttackParticle(outgoingAttack.windupFrames, outgoingAttack.activeFrames, outgoingAttack.cooldownFrames,
                    kickParticle, hitboxSpawner, this, ParticlesScript.HitboxType.KICK_HITBOX));
                

                if (animCoroutine != null)
                {
                    StopCoroutine(animCoroutine);
                }
                StartCoroutine(ResetAnimatorBool(anim, "kick", false, .2f));
            }
        }
    }

    private IEnumerator SpecialSelfDamage(float frames)
    {
        frames /= 60f;
        yield return new WaitForSeconds(frames);
        TakeDamage(specialSelfDamage);
    }
}
