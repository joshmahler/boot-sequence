﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HeroScript : Entity
{
    /*
     * This script must go on each player character.
     * It is the singleton of each player character
     * that is used in almost every other player
     * character script.
     */

    public enum Class { Knight, Mage, Rogue, Monk }; // These are the Classes.
    public Class myClass;                          // What Class am I?
    public int id;
    public float maxHP;                            // Max HP of the character.
    public float invulnerableFrames = 120f;        // The frames the player is invulnerable after hit.  

    public GameObject kickParticle;

    [HideInInspector] public bool isInvulnerable;   // Am I currently invulerable? 
    [HideInInspector] public bool isBlinking;

    // public MeshRenderer healthBar;      // The health bar object
    private Vector3 healthScale;

    private Rigidbody rb;

    [HideInInspector] public MovementScript ms;

    public Transform impactSpawn;
    public GameObject impactVFX;
    public GameObject healVFX;
    public GameObject stunVFX;

    //Brad: health stuff
    public GameObject health1;
    public GameObject health2;
    public GameObject health3;
    public GameObject health4;
    public GameObject health5;
    public GameObject health6;
    public GameObject health7;
    public GameObject healthLow;
    public GameObject healthBorder;

    public GameObject health1Corner;
    public GameObject health2Corner;
    public GameObject health3Corner;
    public GameObject health4Corner;
    public GameObject health5Corner;
    public GameObject health6Corner;
    public GameObject health7Corner;
    public GameObject healthLowCorner;

    private MaterialPropertyBlock[] propertyBlock;
    private Transform[] meshParts;

    private SharedHealthScript sharedHealth;

    private bool isFaded = false;
    private bool startedFade = false;
    private float fadedAfterSeconds = .75f;

    private Coroutine flash;

    private GameObject stunVFXPlaying;

    public Vector3 prevPosition = Vector3.zero;


    public override void Start()
    {
        Transform mesh = transform.Find("g_mesh");
        propertyBlock = new MaterialPropertyBlock[mesh.transform.childCount];
        meshParts = new Transform[mesh.transform.childCount];
        for (int i = 0; i < mesh.transform.childCount; ++i)
        {
            Transform current = mesh.transform.GetChild(i);
            meshParts[i] = current;
            propertyBlock[i] = new MaterialPropertyBlock();
            if (current.GetComponent<Renderer>())
            {
                current.GetComponent<Renderer>().GetPropertyBlock(propertyBlock[i]);
            }

            propertyBlock[i].SetFloat("_Alpha", 0f);
            current.GetComponent<Renderer>().SetPropertyBlock(propertyBlock[i]);

        }

        DontDestroyOnLoad(gameObject);
        ms = GetComponent<MovementScript>();
        rb = GetComponent<Rigidbody>();
        sharedHealth = GameObject.FindGameObjectWithTag("ScreenSpaceUI").transform.Find("SharedHealth").GetComponent<SharedHealthScript>();
        base.Start();
        // healthScale = healthBar.transform.localScale;
    }

    public override void Update()
    {
        if (isStunned)
        {
            if (stunVFXPlaying == null)
            {
                stunVFXPlaying = Instantiate(stunVFX, impactSpawn);
            }
        }
        else
        {
            if (stunVFXPlaying != null)
            {
                Destroy(stunVFXPlaying);
            }
        }

        if (transform.position.y < -10)
        {
            TakeDamage(9999);
        }

        if(GetIsGrounded())
        {
            prevPosition = transform.position;

        }

        rb.angularVelocity = Vector3.zero;
        base.Update();
        UpdateHealth();
    }

    /// <summary>
    /// InvulnerableFrames will wait (iFrames / 60f) seconds
    /// before the player can take damage again.
    /// </summary>
    /// <param name="iFrames">Waits (iFrames / 60f) seconds before setting isInvulnerable to false.</param>
    /// <returns>isInvulnerable is true, waits iFrames frames, and then sets isInvulnerable to false.</returns>
    public IEnumerator InvulnerableFrames(float iFrames)
    {
        flash = StartCoroutine(InvulnFlash(iFrames));
        iFrames /= 60f;
        
        isInvulnerable = true;
        yield return new WaitForSeconds(iFrames);
        isInvulnerable = false;
        StopCoroutine(flash);
        Debug.Log("Stopping flash!");
        for (int i = 0; i < meshParts.Length; ++i)
        {
            propertyBlock[i].SetFloat("_Alpha", 0f);
            meshParts[i].GetComponent<Renderer>().SetPropertyBlock(propertyBlock[i]);
        }
    }

    public IEnumerator InvulnFlash(float iFrames)
    {
        
        int count = 0;
        float numFlashes = 3;
        float modifier = (iFrames / (180 * Mathf.PI * Mathf.PI) * .66f);
         while(count < iFrames)
        {
            count++;
           // Debug.Log(count);
            for (int i = 0; i < meshParts.Length; ++i)
            {
                propertyBlock[i].SetFloat("_Alpha", 1-Mathf.Cos(count * modifier * numFlashes));
                meshParts[i].GetComponent<Renderer>().SetPropertyBlock(propertyBlock[i]);
            }

            yield return new WaitForEndOfFrame();
        }
        for (int i = 0; i < meshParts.Length; ++i)
        {
            propertyBlock[i].SetFloat("_Alpha", 0f);
            meshParts[i].GetComponent<Renderer>().SetPropertyBlock(propertyBlock[i]);
        }
    }


    /// <summary>
    /// TakeDamage decreases health by the damage parameter.
    /// Can be used to heal as well, just put in a negative value.
    /// </summary>
    /// <param name="damage">The amount of health to decrease.</param>
    public override void TakeDamage(float damage)
    {
        if (damage < 0)
        {
            Instantiate(healVFX, transform);
        }
        else
        {
            Instantiate(impactVFX, impactSpawn);
            InputManager.input.Rumble(0.25f, 0.7f, id);
        }

        health -= damage;

        if (health <= 0f)
        {
            //Debug.Log("Stopping flash!");
            if(flash != null)
                StopCoroutine(flash);
            for (int i = 0; i < meshParts.Length; ++i)
            {
                propertyBlock[i].SetFloat("_Alpha", 0);
                meshParts[i].GetComponent<Renderer>().SetPropertyBlock(propertyBlock[i]);
            }
            health = 0f;
            GameControl.control.KillPlayer(gameObject);
        }
        else if(health >= maxHP)
        {
            health = maxHP;
        }
        UpdateHealth();
    }



    public override void burn(float durationFrames, float totalDamage, float tickRateFrames)
    {
        if (burning != null)
        {
            StopCoroutine(burning);
        }
        burning = StartCoroutine(TakeDamageOverTime(durationFrames, totalDamage, tickRateFrames));
    }

    /// <summary>
    /// This function will decrease health over time 
    /// by tickRateFrames.
    /// </summary>
    /// <param name="durationFrames">The frames it takes for the dot effect to dissipate.</param>
    /// <param name="totalDamage">The total damage this dot effect will give.</param>
    /// <param name="tickRateFrames">The frames to wait before damage is taken.</param>
    /// <returns>Waits tickRateFrames / 60f seconds before damaging again.</returns>
    public override IEnumerator TakeDamageOverTime(float durationFrames, float totalDamage, float tickRateFrames)
    {
        durationFrames /= 60f;
        tickRateFrames /= 60f;
        float damagePerTick = totalDamage / (durationFrames / tickRateFrames);

        do
        {
            Utility.Log(damagePerTick);
            Utility.Log(durationFrames);
            durationFrames -= tickRateFrames;
            TakeDamage(damagePerTick);

            yield return new WaitForSeconds(tickRateFrames);

        } while (durationFrames > 0);
    }

    public override void Kick(float windupFrames, float activeFrames, float cooldownFrames, GameObject prefab, Transform parent, GameObject windupVisual, GameObject cooldownVisual, string attackEvent = null)
    {
        ms.canMove = false;
        base.Kick(windupFrames, activeFrames, cooldownFrames, prefab, parent, windupVisual, cooldownVisual, attackEvent = null);
        //OnKick();
    }



    IEnumerator FadeAfterMaxed(float seconds)
    {
        startedFade = true;
        yield return new WaitForSeconds(seconds);
        startedFade = false;
        isFaded = true;
    }

    public void UpdateHealth()
    {
        switch(health)
        {
            case 0f:
                {
                    isFaded = false;

                    healthLow.SetActive(false);
                    health1.SetActive(false);
                    health2.SetActive(false);
                    health3.SetActive(false);
                    health4.SetActive(false);
                    health5.SetActive(false);
                    health6.SetActive(false);
                    health7.SetActive(false);
                    healthBorder.SetActive(false);

                    healthLowCorner.SetActive(false);
                    health1Corner.SetActive(false);
                    health2Corner.SetActive(false);
                    health3Corner.SetActive(false);
                    health4Corner.SetActive(false);
                    health5Corner.SetActive(false);
                    health6Corner.SetActive(false);
                    health7Corner.SetActive(false);
                }
                break;
            case 25f:
                {
                    healthLow.SetActive(true);
                    health1.SetActive(false);
                    health2.SetActive(false);
                    health3.SetActive(false);
                    health4.SetActive(false);
                    health5.SetActive(false);
                    health6.SetActive(false);
                    health7.SetActive(false);
                    healthBorder.SetActive(true);

                    healthLowCorner.SetActive(true);
                    health1Corner.SetActive(false);
                    health2Corner.SetActive(false);
                    health3Corner.SetActive(false);
                    health4Corner.SetActive(false);
                    health5Corner.SetActive(false);
                    health6Corner.SetActive(false);
                    health7Corner.SetActive(false);
                }
                break;
            case 50f:
                {
                    isFaded = false;

                    healthLow.SetActive(false);
                    health1.SetActive(true);
                    health2.SetActive(true);
                    health3.SetActive(false);
                    health4.SetActive(false);
                    health5.SetActive(false);
                    health6.SetActive(false);
                    health7.SetActive(false);
                    healthBorder.SetActive(true);

                    healthLowCorner.SetActive(false);
                    health1Corner.SetActive(true);
                    health2Corner.SetActive(true);
                    health3Corner.SetActive(false);
                    health4Corner.SetActive(false);
                    health5Corner.SetActive(false);
                    health6Corner.SetActive(false);
                    health7Corner.SetActive(false);
                }
                break;
            case 75f:
                {
                    isFaded = false;

                    healthLow.SetActive(false);
                    health1.SetActive(true);
                    health2.SetActive(true);
                    health3.SetActive(true);
                    health4.SetActive(false);
                    health5.SetActive(false);
                    health6.SetActive(false);
                    health7.SetActive(false);
                    healthBorder.SetActive(true);

                    healthLowCorner.SetActive(false);
                    health1Corner.SetActive(true);
                    health2Corner.SetActive(true);
                    health3Corner.SetActive(true);
                    health4Corner.SetActive(false);
                    health5Corner.SetActive(false);
                    health6Corner.SetActive(false);
                    health7Corner.SetActive(false);
                }
                break;
            case 100f:
                {
                    isFaded = false;

                    healthLow.SetActive(false);
                    health1.SetActive(true);
                    health2.SetActive(true);
                    health3.SetActive(true);
                    health4.SetActive(true);
                    health5.SetActive(false);
                    health6.SetActive(false);
                    health7.SetActive(false);
                    healthBorder.SetActive(true);

                    healthLowCorner.SetActive(false);
                    health1Corner.SetActive(true);
                    health2Corner.SetActive(true);
                    health3Corner.SetActive(true);
                    health4Corner.SetActive(true);
                    health5Corner.SetActive(false);
                    health6Corner.SetActive(false);
                    health7Corner.SetActive(false);
                }
                break;
            case 125f:
                {
                    isFaded = false;

                    healthLow.SetActive(false);
                    health1.SetActive(true);
                    health2.SetActive(true);
                    health3.SetActive(true);
                    health4.SetActive(true);
                    health5.SetActive(true);
                    health6.SetActive(false);
                    health7.SetActive(false);
                    healthBorder.SetActive(true);

                    healthLowCorner.SetActive(false);
                    health1Corner.SetActive(true);
                    health2Corner.SetActive(true);
                    health3Corner.SetActive(true);
                    health4Corner.SetActive(true);
                    health5Corner.SetActive(true);
                    health6Corner.SetActive(false);
                    health7Corner.SetActive(false);
                }
                break;
            case 150f:
                {
                    isFaded = false;

                    healthLow.SetActive(false);
                    health1.SetActive(true);
                    health2.SetActive(true);
                    health3.SetActive(true);
                    health4.SetActive(true);
                    health5.SetActive(true);
                    health6.SetActive(true);
                    health7.SetActive(false);
                    healthBorder.SetActive(true);

                    healthLowCorner.SetActive(false);
                    health1Corner.SetActive(true);
                    health2Corner.SetActive(true);
                    health3Corner.SetActive(true);
                    health4Corner.SetActive(true);
                    health5Corner.SetActive(true);
                    health6Corner.SetActive(true);
                    health7Corner.SetActive(false);
                }
                break;
            case 175f:
                {
                    if (!isFaded)
                    {
                        healthLow.SetActive(false);
                        health1.SetActive(true);
                        health2.SetActive(true);
                        health3.SetActive(true);
                        health4.SetActive(true);
                        health5.SetActive(true);
                        health6.SetActive(true);
                        health7.SetActive(true);
                        healthBorder.SetActive(true);

                        healthLowCorner.SetActive(false);
                        health1Corner.SetActive(true);
                        health2Corner.SetActive(true);
                        health3Corner.SetActive(true);
                        health4Corner.SetActive(true);
                        health5Corner.SetActive(true);
                        health6Corner.SetActive(true);
                        health7Corner.SetActive(true);

                        if (!startedFade)
                        {
                            StartCoroutine(FadeAfterMaxed(fadedAfterSeconds));
                        }
                    }
                    else
                    {
                        healthLow.SetActive(false);
                        health1.SetActive(false);
                        health2.SetActive(false);
                        health3.SetActive(false);
                        health4.SetActive(false);
                        health5.SetActive(false);
                        health6.SetActive(false);
                        health7.SetActive(false);
                        healthBorder.SetActive(false);
                    }
                }
                break;
        }
    }
}
