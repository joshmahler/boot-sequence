﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = System.Random;

/*
 Wave Function Collapse as written by Jimmy Griffiths
 Long story short, if you look at this, and use ideas from it, then please give credit where credit is due.
 Whether to me or the github guy who did WFC for computers the first time :) 

        Version 1.0 - February 5th - 2019

 */

/*
Algorithm: according to https://github.com/mxgmn/WaveFunctionCollapse
1. Read the input bitmap and count NxN patterns.
    i. (optional) Augment pattern data with rotations and reflections.

2. Create an array with the dimensions of the output (called "wave" in the source). 
Each element of this array represents a state of an NxN region in the output. 
A state of an NxN region is a superpostion of NxN patterns of the input with boolean coefficients 
(so a state of a pixel in the output is a superposition of input colors with real coefficients). 
False coefficient means that the corresponding pattern is forbidden, true coefficient means that the corresponding pattern is not yet forbidden.

3. Initialize the wave in the completely unobserved state, i.e. with all the boolean coefficients being true.

4. Repeat the following steps:
    i. Observation:
        a. Find a wave element with the minimal nonzero entropy.
        If there is no such elements (if all elements have zero or undefined entropy) 
        then break the cycle (4) and go to step (5).

        b. Collapse this element into a definite state according to its coefficients and the distribution of NxN patterns in the input.

    ii. Propagation: propagate information gained on the previous observation step.

5. By now all the wave elements are either in a completely observed state (all the coefficients except one being zero) 
or in the contradictive state (all the coefficients being zero). In the first case return the output. In the second case finish the work without returning anything.
 */

// Directions are used to determine which we are propagating. 
public enum DIRECTION
{
    INVALID = -1,
    TOP,
    RIGHT,
    DOWN,
    LEFT,
    NUM_DIR
}

public class WFCOutput : MonoBehaviour
{
    private bool[][] wave; // need wave. Not exactly a parallel array, but very similar.
    [Tooltip("The input area that we will WFC on. So, yes, we can have multiple inputs if we want.")]
    public InputTraining input;
    [Tooltip("width is how many in the x will generate, height is how many in the \"y\" (technically z) direction will generate, and gridsize should always be 1.")]
    public int width, height, gridsize;

    // All of this is necessary for entropy calculation.
    private double  startingEntropy;

    // https://github.com/mxgmn/WaveFunctionCollapse/blob/master/OverlappingModel.cs
    // T is such a bullshit arbitrarily named var. But I'm going to use it like it is in the original for right now.
    // Okay, I'm keeping the above comment because it's making me giggle, even if it's not professional.
    // T is the amount of superpositions each slot has before being collapsed. 
    private int T;

    // Random! :D
    System.Random rand = new Random();

    // Remember how many times we've gone through the algorithm.
    // Basically, if we go through it too many times, we might have have a contradiction, and thuse need to reset.
    private int timesThrough = 0;

    // Get our total entropy of the entire output. Use this to check whether we are finished or not.
    private double totalEntropy;

    [Tooltip("When checked, this will force the slots directly adjacent to the start and end nodes to be a certain slot.")]
    public bool mForceFirstCollapse;

    [Tooltip("If Force First Collapse is true, then this will be the index of the module that those adjacent nodes collapse to. " +
             "The index is equivalent to a module's index as a child of InputArea.")]
    public int mFirstCollapseIndex;

    // used to make things on the edge.
    [Tooltip("Make this true if we're using start and end nodes. This should always be true except for extreme circumstances.")]
    public bool mUseStartEnd;
    [Tooltip("The start and end nodes are the unique gameobjects used to represent the starting and ending meshes that will ultimately be placed. " +
             "REMEMBER: In output we should only be using simple shapes and textures.")]
    public GameObject mStartNode, mEndNode;
    [Tooltip("Gen node is, much like start & end, the representation of the generator room objects. Used to help find the transform when doing the conversion, much" +
             " like above.")]
    public GameObject mGenNode;

    /// <summary>
    /// An OutputSlot keeps all superpositions, and whether they are possible at a specific slot or not.
    /// As can be seen in the 3D array.
    /// </summary>
    public class OutputSlot
    {
        public OutputSlot(InputTraining.Module input)
        {
            mModule = input;
            mAlive = true;
        }

        public InputTraining.Module mModule;
        public bool mAlive;
    }

    /// <summary>
    /// Pair keeps track of where we have been when we propagate.
    /// Might not actually be useful, but I'm keeping it for the moment. 
    /// </summary>
    private class Pair
    {
        public Pair(int x, int y)
        {
            mX = x;
            mY = y;
        }

        public override bool Equals(object value)
        {
            Pair p = value as Pair;
            return mX == p.mX && mY == p.mY;
        }

        public int mX; public int mY;
    }

    // our output output contains the location, and an array of all superpositions, that will be
    // collapsed down to one thing.
    private OutputSlot[][][] mOutput;

    public OutputSlot getAt(int x, int y, int m)
    {
        return mOutput[x][y][m];
    }

    public int getT()
    {
        return T;
    }

    // entropy contains the entropy of each slot. How many superpositions are possible at that slot.
    // Needed for the lowest entropy heuristic.
    private double[][] mEntropy;

    // draw the cube output.
    void OnDrawGizmos()
    {
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(new Vector3((width * gridsize / 2f) - gridsize * 0.5f, 0f, (height * gridsize / 2f) - gridsize * 0.5f),
            new Vector3(width * gridsize, gridsize, height * gridsize));
    }

    // Start is called before the first frame update
    void Start()
    {
        width += GameControl.control.xInc;
        height += GameControl.control.yInc;
        // do our WFC. If there are any issues, then it has to be reset.
        bool done = false;
        while (!done)
        {
            if (mUseStartEnd)
            {
                placeStartEnd();
                placeGenRooms();
            }
            initializeOutput();
            reset();
            done = observeWave();
        }
        // once it's done, place the output.
        placeOutput();

    }

    void Update()
    {
        // hit R, reload the scene. It's just easier this way. 
        if (Input.GetKeyUp(KeyCode.R))
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Initialize output with all potential input modules.
    /// Note that output is a 3D array. X, Y, and then our superposition of all possibilities!
    /// </summary>
    void initializeOutput()
    {
        // we need these because the compiler will yell if we try to use public ints for array init.
        int w = width;
        int h = height;

        int i = 0, j = 0;

        // To make a long story short... these are jagged arrays which are kind of weird. 
        // You can only initialize one part of the jagged array at a time.
        mOutput = new OutputSlot[width][][];

        for (i = 0; i < w; ++i)
        {
            mOutput[i] = new OutputSlot[height][];
        }

        // initialize the wave to base values.
        for (i = 0; i < w; ++i)
        {
            for (j = 0; j < h; ++j)
            {
                mOutput[i][j] = new OutputSlot[input.mModules.Count];

                for(int k = 0; k < input.mModules.Count; ++k)
                    mOutput[i][j][k] = new OutputSlot(input.mModules[k]);
            }
        }

        // THIS IS THE WEIGHT. Weight is equivalent to how many distinct possibilities there are per slot.
        T = input.mModules.Count;

    }
    /// <summary>
    /// Initialize the entropy for the entire output.
    /// Entropy is how "easy" a slot is to solve. Lower entropy is better. An entropy of 1 means that
    /// slot is solved.
    /// </summary>
    void initEntropy()
    {
        // This is the Shannon Entropy Formula
        // https://en.wikipedia.org/wiki/Entropy_(information_theory)
        mEntropy = new double[width][];
        for(int i = 0; i < width; ++i)
            mEntropy[i] = new double[height];
        startingEntropy = T - Math.Log(T) / T;
        totalEntropy = startingEntropy;
    }


    /// <summary>
    /// Reset resets all values to default, just in case we run into a contradiction.
    /// </summary>
    private void reset()
    {
        // Get our entropy back.
        initEntropy();

        // reset our times through (yes, it's redundant the first time)
        timesThrough = 0;
        // reset all of our possibilities in the wave to true.
        for (int i = 0; i < width; ++i)
            for (int j = 0; j < height; ++j)
            {
                for (int k = 0; k < T; ++k)
                {
                    mOutput[i][j][k].mAlive = true;
                    //mOutput[i][j][k].mEntropy = startingEntropy;
                }

                mEntropy[i][j] = startingEntropy;
            }
                
    }

    /// <summary>
    /// Function that recalculates the entropy of a singular slot
    /// </summary>
    bool recalcEntropy(int x, int y)
    {
        // s stands for Shannon Entropy. Or slot. Take your pick
        double s = 0;
        // w is our new weight
        int w = 0;

        for (int i = 0; i < T; ++i)
        {
            if (mOutput[x][y][i].mAlive)
                ++w;
        }

        // calculate our entropy
        // https://en.wikipedia.org/wiki/Entropy_(information_theory) is the shannon entropy formula.
        s = w - Math.Log(w) / w;

        // set it
        mEntropy[x][y] = s;

        return s == Double.NegativeInfinity || s == Double.PositiveInfinity;

    }

    /// <summary>
    /// This recalculates the entropy of EVERY SINGLE SLOT. This is not
    /// efficient in the slightest, but it gets the job done for the moment.
    /// </summary>
    /// <returns>if any entropy is undefined, then reset.</returns>
    bool recalcAllEntropy()
    {
        bool badTime = false;
        for (int i = 0; i < width; ++i)
        {
            for(int j = 0; j < height; ++j)
            {
                for (int k = 0; k < T; ++k)
                {
                    badTime = recalcEntropy(i,j);
                }
            }
        }

        return badTime;
    }

    /// <summary>
    /// placeOutput places the output based on what each slot has collapsed to.
    /// </summary>
    void placeOutput()
    {
        for (int i = 0; i < width; ++i)
        {
            for (int j = 0; j < height; ++j)
            {
                for (int k = 0; k < T; ++k)
                {
                    if (mOutput[i][j][k].mAlive)
                    {
                        //Quaternion quat = new Quaternion(mOutput[i][j][k].mModule.mObject.transform.rotation.x, mOutput[i][j][k].mModule.mObject.transform.rotation.y, mOutput[i][j][k].mModule.mObject.transform.rotation.z, 0);
                        var obj = Instantiate(mOutput[i][j][k].mModule.mObject, new Vector3(i + gameObject.transform.position.x, 0, j + gameObject.transform.position.z), Quaternion.identity);
                        obj.transform.SetParent(this.transform);
                        obj.transform.rotation = Quaternion.Euler(mOutput[i][j][k].mModule.mObject.transform.eulerAngles.x, mOutput[i][j][k].mModule.mObject.transform.eulerAngles.y, mOutput[i][j][k].mModule.mObject.transform.eulerAngles.z);
                        //obj.transform.rotation = mOutput[i][j][k].mModule.mObject.transform.eulerAngles.x;
                    }
                }
            }
        }
    }

    int sX = 0, sY = 0, sM = 0;
    [HideInInspector]
    public GameObject startGO, endGO;
    [HideInInspector]
    public GameObject genRoom1, genRoom2;

    private int sX2, sY2;
    private List<DIRECTION> dirPool = new List<DIRECTION>();
    private Pair[] sXsY = new Pair[4];
    /// <summary>
    /// Places the start and end nodes.
    /// </summary>
    void placeStartEnd()
    {
        dirPool.Add(DIRECTION.TOP);
        dirPool.Add(DIRECTION.RIGHT);
        dirPool.Add(DIRECTION.DOWN);
        dirPool.Add(DIRECTION.LEFT);
        placeAlongEdge(ref startGO, ref endGO, "StartNode", "EndNode", mStartNode, mEndNode, false, true, 0, 1);
    }

    /// <summary>
    /// Places the pre-spawned generator rooms
    /// </summary>
    void placeGenRooms()
    {
        placeAlongEdge(ref genRoom1, ref genRoom2, "GenRoom1", "GenRoom2", mGenNode, mGenNode, true, false, 2, 3);
    }

    /// <summary>
    /// Places an object along the edge of the area where we are WFC-ing. Used for start / end, and generators.
    /// </summary>
    /// <param name="obj1">The first object</param>
    /// <param name="obj2">The second object</param>
    /// <param name="name1">The first object's name in editor.</param>
    /// <param name="name2">The second object's name in editor.</param>
    void placeAlongEdge(ref GameObject obj1, ref GameObject obj2, string name1, string name2, GameObject origObj1, GameObject origObj2, bool add180, bool setStarts,
                        int index1, int index2)
    {
        var val = rand.Next(dirPool.Count);
        DIRECTION startDir = dirPool[val];

        if (dirPool.Count > 2)
        {
            if (val < 2)
                val += 2;
            else
                val -= 2;
        }
        
        dirPool.RemoveAt(val);
        dirPool.Remove(startDir);

        int randX = 0, randY = 0;
        int endX = 0, endY = 0;
        Quaternion firstRot = Quaternion.identity;
        Vector3 adder1 = transform.position;
        Vector3 adder2 = transform.position;
        switch (startDir)
        {
            case DIRECTION.TOP:
                randY = height;
                randX = rand.Next(width - 1);
                endX = width - randX - 1;
                endY = 0;
                firstRot.y = 180;
                adder2.z -= 1.0f;
                break;
            case DIRECTION.DOWN:
                randY = 0;
                randX = rand.Next(width - 1);
                endX = width - randX - 1;
                endY = height;
                firstRot.y = 0;
                adder1.z -= 1.0f;
                break;

            case DIRECTION.RIGHT:
                randY = rand.Next(height - 1);
                randX = width;
                endX = 0;
                endY = height - randY - 1;
                firstRot.y = 270;
                adder2.x -= 1.0f;
                break;
            case DIRECTION.LEFT:
                randY = rand.Next(height - 1);
                randX = 0;
                endX = width;
                endY = height - randY - 1;
                firstRot.y = 90;
                adder1.x -= 1.0f;
                break;
        }

        if (setStarts)
        {
            sX = randX;
            sY = randY;
            if (sX == width)
                sX -= 1;

            if (sY == height)
                sY -= 1;

            sX2 = endX;
            sY2 = endY;
            if (sX2 == width)
                sX2 -= 1;
            if (sY2 == height)
                sY2 -= 1;

        }

        int x = randX, y = randY;
        int eX = endX, eY = endY;

        if (x == width)
            --x;
        if (y == height)
            --y;

        if (eX == width)
            --eX;

        if (eY == height)
            --eY;
        
        sXsY[index1] = new Pair(x, y);
        sXsY[index2] = new Pair(eX, eY);

        if (add180)
            firstRot.y -= 90f;

        Quaternion endRot = new Quaternion(firstRot.x, firstRot.y + 180.0f, firstRot.z, firstRot.w);

        obj1 = Instantiate(origObj1, new Vector3(randX + adder1.x, 0, randY + adder1.z), firstRot);
        obj1.name = name1;
        obj1.transform.rotation = Quaternion.Euler(new Vector3(firstRot.x, firstRot.y, firstRot.z));
        obj2 = Instantiate(origObj2, new Vector3(endX + adder2.x, 0, endY + adder2.z), endRot);
        obj2.name = name2;
        obj2.transform.rotation = Quaternion.Euler(new Vector3(endRot.x, endRot.y, endRot.z));

        obj1.transform.SetParent(gameObject.transform);
        obj2.transform.SetParent(gameObject.transform);
    }

    /// <summary>
    /// check the total entropy of the output.
    /// </summary>
    /// <returns>returns the total entropy. If it equals the size * width (i.e, everything is 1),
    /// then we know we are done.</returns>
    double checkTotalEntropy()
    {
        double totalEntropy = 0;

        for (int i = 0; i < width; ++i)
        {
            for (int j = 0; j < height; ++j)
                totalEntropy += mEntropy[i][j];
        }

        return totalEntropy;
    }

    /// <summary>
    /// This is the meat and taters right here.
    /// 1. Pick the lowest entropy slot to collapse (or random if all are equal)
    /// 2. Collapse the wave
    /// 3. Propagate the collapse.
    /// </summary>
    /// <returns>returns a bool whether we have properly collapsed or not.</returns>
    bool observeWave()
    {
        // index vars
        int x = 0, y = 0, m = 0;

        // This might not be necessary. Remember where we've gone so far.
        List<Pair> visited = new List<Pair>();

        // Whether we need to panic and break or not.
        bool quit = false;

        while (timesThrough < (width * height) && ((int) checkTotalEntropy() != (width * height)))
        {
            // if this is our first time through
            if (timesThrough == 0)
            {
                // find a random tile to just collapse.

                if (!mUseStartEnd)
                {
                    x = rand.Next(width);
                    y = rand.Next(height);
                    m = rand.Next(T);
                    sX = x;
                    sY = y;
                    sM = m;
                }

                else
                {
                    x = sX;
                    y = sY;
                    m = rand.Next(T);
                    sM = m;
                }

                if (mForceFirstCollapse)
                    m = mFirstCollapseIndex;

                sM = m;


                // We remove every other theoretical state, one at a time. Once we remove one
                // theoretical state we need to check whether other's theoretical states can still
                // exist or not. I.e, propagate the changes out.
                for (int t = 0; t < T; ++t)
                {
                    if (m != t)
                        mOutput[x][y][t].mAlive = false;

                    if (!mOutput[x][y][t].mAlive)
                    {
                        propogateUWU(x, y, DIRECTION.TOP, visited);
                        propogateUWU(x, y, DIRECTION.RIGHT, visited);
                        propogateUWU(x, y, DIRECTION.DOWN, visited);
                        propogateUWU(x, y, DIRECTION.LEFT, visited);
                    }

                }

                // make sure we recalculate the entropy, to make sure we haven't broken anything.
                quit = recalcAllEntropy();

                if (quit == true)
                    return false;
            }

            if(mForceFirstCollapse && timesThrough < 4)
            {
                for(int i = 1; i < 4; ++i)
                {
                    x = sXsY[i].mX;
                    y = sXsY[i].mY;
                    m = mFirstCollapseIndex;

                    // We remove every other theoretical state, one at a time. Once we remove one
                    // theoretical state we need to check whether other's theoretical states can still
                    // exist or not. I.e, propagate the changes out.
                    for (int t = 0; t < T; ++t)
                    {
                        if (m != t)
                            mOutput[x][y][t].mAlive = false;

                        if (!mOutput[x][y][t].mAlive)
                        {
                            propogateUWU(x, y, DIRECTION.TOP, visited);
                            propogateUWU(x, y, DIRECTION.RIGHT, visited);
                            propogateUWU(x, y, DIRECTION.DOWN, visited);
                            propogateUWU(x, y, DIRECTION.LEFT, visited);
                        }

                    }

                    // make sure we recalculate the entropy, to make sure we haven't broken anything.
                    quit = recalcAllEntropy();

                    if (quit == true)
                        return false;
                }
            }

            else
            {
                // find the lowest entropy location using the lowest entropy heuristic.
                findLowestEntropy(ref x, ref y);

                // for this, we pick a random superposition to collapse to, depending on 
                // because we can't assume every superposition exists anymore.
                int numAlive = 0;

                // sorry, tL stands for temp list, used to pick a random superposition.
                List<int> tL = new List<int>();

                for (int t = 0; t < T; ++t)
                    if (mOutput[x][y][t].mAlive)
                        tL.Add(t);

                m = rand.Next(tL.Count);
                m = tL[m];
                // now that we know which state we want, we remove every other state.
                // Remember, we have to propagate our changes every time we remove a state!
                for (int t = 0; t < T; ++t)
                {
                    if (m != t)
                        mOutput[x][y][t].mAlive = false;

                    if (!mOutput[x][y][t].mAlive)
                    {
                        propogateUWU(x, y, DIRECTION.TOP, visited);
                        propogateUWU(x, y, DIRECTION.RIGHT, visited);
                        propogateUWU(x, y, DIRECTION.DOWN, visited);
                        propogateUWU(x, y, DIRECTION.LEFT, visited);
                    }

                }

                if (mOutput[sX][sY][sM].mAlive == false)
                    Utility.Log("Panic!");

                // again, check whether we've broken our stuff or not.
                quit = recalcAllEntropy();

                if (quit)
                    return false;
            }

            ++timesThrough;
        }
        
        //propogateWave(x, y, m);

        return true;

    }

    /// <summary>
    /// Find lowest entropy finds the location of the lowest entropy.
    /// </summary>
    /// <param name="x"> x location</param>
    /// <param name="y"> y location</param>
    void findLowestEntropy(ref int x, ref int y)
    {
        double lowE = 1000;
        //int lX, lY;

        for (int i = 0; i < width; ++i)
        {
            for (int j = 0; j < height; ++j)
            {
                if (mEntropy[i][j] < lowE && mEntropy[i][j] != 1)
                {
                    lowE = mEntropy[i][j];
                    x = i;
                    y = j;
                }
            }
        }
    }

    /// <summary>
    /// Safety guard to ensure that we don't accidentally remove the last state.
    /// </summary>
    /// <param name="x"> location x to check</param>
    /// <param name="y"> location y to check</param>
    /// <returns>returns true if one state is left, false otherwise.</returns>
    bool hasOneAlive(int x, int y)
    {
        int aliveCount = 0;

        for (int i = 0; i < T; ++i)
        {
            if (mOutput[x][y][i].mAlive)
                ++aliveCount;
        }

        return aliveCount == 1;
    }
    /// <summary>
    /// The REAL meat and taters.
    /// 1. Once we've REMOVED a state, we need to propagate.
    /// 2. This function checks each nearby slot's states, to all of the caller's states,
    /// which will then determine if we have to remove a superposition state, from the neighbor.
    /// 3. If that happens, propagate AGAIN, going back to step 1.
    /// </summary>
    /// <param name="x">x is the CURRENT x location.</param>
    /// <param name="y">y is the CURRENT y location</param>
    /// <param name="dir">dir is the direction that we are looking at.</param>
    /// <param name="visited">List to check to make sure we aren't stepping on any toes.</param>
    void propogateUWU(int x, int y, DIRECTION dir, List<Pair> visited)
    {
        // debug to make sure that things are okay.
        if (mOutput[sX][sY][sM].mAlive == false)
            Utility.Log("Panic!");

        // set up some variables.
        int i = 0;
        bool connected = false;
        bool alive = true;
        // note that we do this 4 times. There's definitely a nicer way to clean this up. 
        // Not sure how though ^
        if (dir == DIRECTION.TOP)
        {
            // Check to make sure we are in bounds.
            if (y + 1 < height)
            {
                // i is THEIR state. j is MY state.
                // check to see if that i superposition has any way to connect to ANY of my superpositions.
                for (i = 0; i < T; ++i)
                {
                    connected = false;
                    alive = true;
                    for (int j = 0; j < T; ++j)
                    {
                        // skip over any already dead states.
                        if (!mOutput[x][y + 1][i].mAlive)
                        {
                            alive = false;
                            break;
                        }

                        // if we have any theoretical connections, break.
                        if (mOutput[x][y + 1][i].mModule.mConnection.mSouthConnection.Equals(mOutput[x][y][j].mModule.mConnection.mNorthConnection)
                             && mOutput[x][y][j].mAlive)
                        {
                            connected = true;
                            break;
                        }
                    }

                    // if the other superposition has no way to connect to any of my superpositions, then we need to remove it.
                    // after removing it, we need to check if that has an effect on any other nearby superpositions.
                    if (!connected && alive)
                    {
                        Pair np = new Pair(x, y + 1);
                        bool beenThere = visited.Contains(np);
                        if (!hasOneAlive(x, y + 1) && !beenThere)
                        {

                            Pair tp = new Pair(x, y);
                            visited.Add(tp);
                            mOutput[x][y + 1][i].mAlive = false;
                            if (mOutput[sX][sY][sM].mAlive == false)
                                Utility.Log("Panic!");
                            // we need to now propagate that new lack of superposition to every other super position.
                            propogateUWU(x, y + 1, DIRECTION.TOP, visited);
                            if (x + 1 < width)
                                propogateUWU(x + 1, y, DIRECTION.RIGHT, visited);
                            if (y - 1 > 0)
                                propogateUWU(x, y - 1, DIRECTION.DOWN, visited);
                            if (x - 1 > 0)
                                propogateUWU(x - 1, y, DIRECTION.LEFT, visited);

                            visited.Remove(tp);
                        }

                    }

                }


            }
        }
        
        // Again, this is kind of gross, but do the same thing as above, but for everything.
        if (dir == DIRECTION.RIGHT)
        {
            if (x + 1 < width)
            {
                for (i = 0; i < T; ++i)
                {
                    connected = false;
                    alive = true;
                    for (int j = 0; j < T; ++j)
                    {
                        if (!mOutput[x + 1][y][i].mAlive)
                        {
                            alive = false;
                            break;
                        }

                        if (mOutput[x + 1][y][i].mModule.mConnection.mWestConnection.Equals(mOutput[x][y][j].mModule.mConnection.mEastConnection)
                             && mOutput[x][y][j].mAlive)
                        {
                            connected = true;
                            break;
                        }
                    }

                    if (!connected && alive)
                    {
                        Pair np = new Pair(x + 1, y);
                        bool beenThere = visited.Contains(np);
                        if (!hasOneAlive(x + 1, y) && !beenThere)
                        {
                            Pair tp = new Pair(x, y);
                            visited.Add(tp);
                            mOutput[x + 1][y][i].mAlive = false;
                            if (mOutput[sX][sY][sM].mAlive == false)
                                Utility.Log("Panic!");
                            if (y + 1 < height)
                                propogateUWU(x, y + 1, DIRECTION.TOP, visited);
                            if (x + 1 < width)
                                propogateUWU(x + 1, y, DIRECTION.RIGHT, visited);
                            if (y - 1 > 0)
                                propogateUWU(x, y - 1, DIRECTION.DOWN, visited);
                            if (x - 1 > 0)
                                propogateUWU(x - 1, y, DIRECTION.LEFT, visited);

                            visited.Remove(tp);
                        }

                    }


                }

                
            }
        }

        if (dir == DIRECTION.DOWN)
        {
            if (y - 1 > 0)
            {
                for (i = 0; i < T; ++i)
                {
                    connected = false;
                    alive = true;
                    for (int j = 0; j < T; ++j)
                    {
                        if (!mOutput[x][y - 1][i].mAlive)
                        {
                            alive = false;
                            break;
                        }

                        if (mOutput[x][y - 1][i].mModule.mConnection.mNorthConnection.Equals(mOutput[x][y][j].mModule.mConnection.mSouthConnection)
                             && mOutput[x][y][j].mAlive)
                        {
                            connected = true;
                            break;
                        }
                    }
                    if (!connected && alive)
                    {
                        Pair np = new Pair(x, y - 1);
                        bool beenThere = visited.Contains(np);
                        if (!hasOneAlive(x, y - 1) && !beenThere)
                        {
                            Pair tp = new Pair(x, y);
                            visited.Add(tp);
                            mOutput[x][y - 1][i].mAlive = false;
                            if (mOutput[sX][sY][sM].mAlive == false)
                                Utility.Log("Panic!");
                            if (y + 1 < height)
                                propogateUWU(x, y + 1, DIRECTION.TOP, visited);
                            if (x + 1 < width)
                                propogateUWU(x + 1, y, DIRECTION.RIGHT, visited);
                            if (y - 1 > 0)
                                propogateUWU(x, y - 1, DIRECTION.DOWN, visited);
                            if (x - 1 > 0)
                                propogateUWU(x - 1, y, DIRECTION.LEFT, visited);

                            visited.Remove(tp);
                        }

                    }

                }

            }
        }


        if (dir == DIRECTION.LEFT)
        {
            if (x - 1 > 0)
            {
                for (i = 0; i < T; ++i)
                {
                    connected = false;
                    alive = true;
                    for (int j = 0; j < T; ++j)
                    {
                        if (!mOutput[x - 1][y][i].mAlive)
                        {
                            alive = false;
                            break;
                        }

                        if (mOutput[x - 1][y][i].mModule.mConnection.mEastConnection.Equals(mOutput[x][y][j].mModule.mConnection.mWestConnection)
                             && mOutput[x][y][j].mAlive)
                        {
                            connected = true;
                            break;
                        }
                    }

                    if (!connected && alive)
                    {
                        Pair np = new Pair(x - 1, y);
                        bool beenThere = visited.Contains(np);
                        if (!hasOneAlive(x - 1, y) && !beenThere)
                        {
                            Pair tp = new Pair(x, y);
                            visited.Add(tp);
                            mOutput[x - 1][y][i].mAlive = false;
                            if (mOutput[sX][sY][sM].mAlive == false)
                                Utility.Log("Panic!");
                            if (y + 1 < height)
                                propogateUWU(x, y + 1, DIRECTION.TOP, visited);
                            if (x + 1 < width)
                                propogateUWU(x + 1, y, DIRECTION.RIGHT, visited);
                            if (y - 1 > 0)
                                propogateUWU(x, y - 1, DIRECTION.DOWN, visited);
                            if (x - 1 > 0)
                                propogateUWU(x - 1, y, DIRECTION.LEFT, visited);

                            visited.Remove(tp);
                        }

                    }
                }



            }


        }


    }

    // And that's it... Everything past this point is old stuff from the github.
    // For posterity reasons, of course.
    /*
    /// <summary>
    /// After observing, propogate that shit back DJ
    /// </summary>
    void propogateWave(int x, int y, int m)
    {
        // Let's start by just doing direct adjacencies...
        // in a clockwise direction.
        // Starting with up

        // I'm so sorry this is recursive.
        propogateDir(x,y,m,DIRECTION.TOP);
        propogateDir(x, y, m, DIRECTION.RIGHT);
        propogateDir(x, y, m, DIRECTION.DOWN);
        propogateDir(x, y, m, DIRECTION.LEFT);

        // now that we've figured THAT shit out, we need to propogate further.

    }

    void propogateFirst(int y, int x, int m, DIRECTION dir)
    {
        // n for next module
        int n;
        // go ahead and check our shit. Make sure we're not OOB tho.
        if (dir == DIRECTION.TOP)
        {
            if (y + 1 < height)
            {
                for (n = 0; n < T; ++n)
                {
                    if (mOutput[x][y][m].mModule.mConnection.mNorthConnection !=
                        mOutput[x][y + 1][n].mModule.mConnection.mSouthConnection)
                        mOutput[x][y + 1][n].mAlive = false;
                }

                recalcEntropy(x, y + 1, m);
            }
        }

        if (dir == DIRECTION.RIGHT)
        {
            if (x + 1 < width)
            {
                for (n = 0; n < T; ++n)
                {
                    if (mOutput[x][y][m].mModule.mConnection.mEastConnection !=
                        mOutput[x + 1][y][n].mModule.mConnection.mWestConnection)
                        mOutput[x + 1][y][n].mAlive = false;
                }

                recalcEntropy(x + 1, y, m);
            }
        }

        if (dir == DIRECTION.DOWN)
        {
            if (y - 1 > 0)
            {
                for (n = 0; n < T; ++n)
                {
                    if (mOutput[x][y][m].mModule.mConnection.mSouthConnection !=
                        mOutput[x][y - 1][n].mModule.mConnection.mNorthConnection)
                        mOutput[x][y - 1][n].mAlive = false;
                }


                recalcEntropy(x, y - 1, m);
            }
        }

        if (dir == DIRECTION.LEFT)
        {
            if (x - 1 > 0)
            {
                for (n = 0; n < T; ++n)
                {
                    if (mOutput[x][y][m].mModule.mConnection.mWestConnection !=
                        mOutput[x - 1][y][n].mModule.mConnection.mEastConnection)
                        mOutput[x - 1][y][n].mAlive = false;
                }

                recalcEntropy(x - 1, y, m);
            }
        }
    }

    void propogateDir(int x, int y, int m, DIRECTION dir)
    {
        // n for next module
        int n;
        // go ahead and check our shit. Make sure we're not OOB tho.
        if (dir == DIRECTION.TOP)
        {
            if (y + 1 < height)
            {
                for (n = 0; n < T; ++n)
                {
                    if (mOutput[x][y][m].mModule.mConnection.mNorthConnection !=
                        mOutput[x][y + 1][n].mModule.mConnection.mSouthConnection)
                        mOutput[x][y + 1][n].mAlive = false;
                    
                    if(mOutput[x][y][m].mModule.mConnection.mNorthConnection ==
                       mOutput[x][y + 1][n].mModule.mConnection.mSouthConnection && mOutput[x][y + 1][n].mAlive == false)
                        mOutput[x][y + 1][n].mAlive = true;
                }


                recalcEntropy(x, y + 1, m);
                if(mOutput[x][y + 1][m].mEntropy < startingEntropy && mOutput[x][y+1][m].mEntropy > 1)
                    propogateWave(x, y+1, m);
            }
        }

        if (dir == DIRECTION.RIGHT)
        {
            if (x + 1 < width)
            {
                for (n = 0; n < T; ++n)
                {
                    if (mOutput[x][y][m].mModule.mConnection.mEastConnection !=
                        mOutput[x + 1][y][n].mModule.mConnection.mWestConnection)
                            mOutput[x + 1][y][n].mAlive = false;
                    
                    if(mOutput[x][y][m].mModule.mConnection.mEastConnection ==
                       mOutput[x + 1][y][n].mModule.mConnection.mWestConnection && mOutput[x + 1][y][n].mAlive == false)
                            mOutput[x + 1][y][n].mAlive = true;
                }

                recalcEntropy(x + 1, y, m);
                if (mOutput[x + 1][y][m].mEntropy < startingEntropy && mOutput[x + 1][y][m].mEntropy > 1)
                    propogateWave(x = 1, y, m);
            }
        }

        if (dir == DIRECTION.DOWN)
        {
            if (y - 1 > 0)
            {
                for (n = 0; n < T; ++n)
                {
                    if (mOutput[x][y][m].mModule.mConnection.mSouthConnection !=
                        mOutput[x][y - 1][n].mModule.mConnection.mNorthConnection)
                        mOutput[x][y - 1][n].mAlive = false;

                    if (mOutput[x][y][m].mModule.mConnection.mSouthConnection ==
                        mOutput[x][y - 1][n].mModule.mConnection.mNorthConnection &&
                        mOutput[x][y - 1][n].mAlive == false)
                        mOutput[x][y - 1][n].mAlive = true;
                }
                    

                recalcEntropy(x, y - 1, m);
                if (mOutput[x][y - 1][m].mEntropy < startingEntropy && mOutput[x][y - 1][m].mEntropy > 1)
                    propogateWave(x, y - 1, m);
            }
        }

        if (dir == DIRECTION.LEFT)
        {
            if (x - 1 > 0)
            {
                for (n = 0; n < T; ++n)
                {
                    if (mOutput[x][y][m].mModule.mConnection.mWestConnection !=
                        mOutput[x - 1][y][n].mModule.mConnection.mEastConnection)
                        mOutput[x - 1][y][n].mAlive = false;

                    if (mOutput[x][y][m].mModule.mConnection.mWestConnection ==
                        mOutput[x - 1][y][n].mModule.mConnection.mEastConnection && mOutput[x - 1][y][n].mAlive == false)
                        mOutput[x - 1][y][n].mAlive = true;
                }

                recalcEntropy(x - 1, y, m);

                if (mOutput[x - 1][y][m].mEntropy < startingEntropy && mOutput[x - 1][y][m].mEntropy > 1)
                    propogateWave(x - 1, y, m);
            }
        }
    }

    void observe()
    {
        double min = 1E+3;
        int argmin = -1;

        for (int i = 0; i < wave.Length; ++i)
        {
            int amount = sumsOfOnes[i];
            if (amount == 0)
                return;

            double entropy = entropies[i];

            if (amount > 1 && entropy <= min)
            {
                double noise = 1E-6 * rand.NextDouble();
                if (entropy + noise < min)
                {
                    min = entropy + noise;
                    argmin = i;
                }
            }
        }

        if (argmin == -1)
        {
            observed = new int[width * height];
            for(int i = 0; i < wave.Length; ++i)
                for(int t = 0; t < T; ++t)
                    if (wave[i][t])
                    {
                        observed[i] = t;
                        break;
                    }
        }

        double[] distribution = new double[T];
        for (int t = 0; t < T; ++t)
            distribution[t] = wave[argmin][t] ? weights[t] : 0;

        bool[] w = wave[argmin];
        //for(int t = 0; t < T; ++t)
        //    if(w[t] != (t == r))
    }

    void propogate()
    {

    }
    */
}
