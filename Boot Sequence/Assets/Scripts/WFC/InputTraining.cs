﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 WFC Input as written by Jimmy Griffiths, with the help of Josh Mahler.
    Long story short, if you look at this, and use ideas from it, then please give credit where credit is due.
    Whether to me or the github guy who did WFC for computers the first time :) 

    Version 1.0 - February 5th - 2019
*/

/*
Algorithm: according to https://github.com/mxgmn/WaveFunctionCollapse
1. Read the input bitmap and count NxN patterns.
    i. (optional) Augment pattern data with rotations and reflections.

2. Create an array with the dimensions of the output (called "wave" in the source). 
Each element of this array represents a state of an NxN region in the output. 
A state of an NxN region is a superpostion of NxN patterns of the input with boolean coefficients 
(so a state of a pixel in the output is a superposition of input colors with real coefficients). 
False coefficient means that the corresponding pattern is forbidden, true coefficient means that the corresponding pattern is not yet forbidden.

3. Initialize the wave in the completely unobserved state, i.e. with all the boolean coefficients being true.

4. Repeat the following steps:
    i. Observation:
        a. Find a wave element with the minimal nonzero entropy.
        If there is no such elements (if all elements have zero or undefined entropy) 
        then break the cycle (4) and go to step (5).

        b. Collapse this element into a definite state according to its coefficients and the distribution of NxN patterns in the input.

    ii. Propagation: propagate information gained on the previous observation step.

5. By now all the wave elements are either in a completely observed state (all the coefficients except one being zero) 
or in the contradictive state (all the coefficients being zero). In the first case return the output. In the second case finish the work without returning anything.
 */

public enum CONNECTION_TYPE
{
    INVALID_TYPE = -1,
    FLOOR,
    PIT,
    WALL,
    ANY_NON_WALL,
    ANY,
    NUM_TYPES
}

// TODO: Set connections to be okay on certain things, but not allow it to connect to other things. 
// I.e, Pit can connect to floor or pit, but no walls allowed.

public class InputTraining : MonoBehaviour
{

    private const int MAX_CONNECTION_SIZE = 3;

    public class ConnectionType: IEquatable<ConnectionType>
    {
        public CONNECTION_TYPE[] mConnection = new CONNECTION_TYPE[MAX_CONNECTION_SIZE];

        public bool Equals(ConnectionType other)
        {
            if (other is null)
                return false;

            for (int i = 0; i < MAX_CONNECTION_SIZE; ++i)
            {
                if (mConnection[i] != other.mConnection[i] && other.mConnection[i] != CONNECTION_TYPE.ANY_NON_WALL)
                    return false;
            }

            return true;
        }

        public override bool Equals(object obj) => Equals(obj as ConnectionType);
        public override int GetHashCode() => (mConnection).GetHashCode();
    }

    /// <summary>
    /// A module is the input object. Output will be made up of hundreds of modules.
    /// </summary>
    public class Module
    {
        public Module()
        {
            mObject = null;
            mPossibleConnections = null;
        }

        public Module(GameObject obj)
        {
            mObject = obj;
            mPossibleConnections = new Module[4];
            mMainTexture = mObject.GetComponent<MeshRenderer>().materials[0].GetTexture("_MainTex") as Texture2D;
            mConnection = new ModuleConnection();
        }
        public GameObject mObject; // The actual gameObject that this module is defined is.
        public Module[] mPossibleConnections; // What this specific module can be connected to.
        public ModuleConnection mConnection; // These are my connections at a specific point. How I go out, and how others get to me.
        public Texture2D mMainTexture;
    }

    /// <summary>
    /// ModuleConnection stores the cardinal connections. So that we can, well, connect.
    /// </summary>
    public class ModuleConnection
    {
        // Commenting these out because I don't think tuples are necessary
        /*
        public Tuple<CONNECTION_TYPE, CONNECTION_TYPE, CONNECTION_TYPE> mNorthConnection;
        public Tuple<CONNECTION_TYPE, CONNECTION_TYPE, CONNECTION_TYPE> mEastConnection;
        public Tuple<CONNECTION_TYPE, CONNECTION_TYPE, CONNECTION_TYPE> mSouthConnection;
        public Tuple<CONNECTION_TYPE, CONNECTION_TYPE, CONNECTION_TYPE> mWestConnection;
        */

        // arrays for connections. each one SHOULD only be size 3. 
        public ConnectionType mNorthConnection = new ConnectionType();
        public ConnectionType mEastConnection = new ConnectionType();
        public ConnectionType mSouthConnection = new ConnectionType();
        public ConnectionType mWestConnection = new ConnectionType();
    }

    /// <summary>
    /// Struct for a public "dictionary". Because Unity doesn't like public
    /// dictionaries appearing the inspector. 
    /// </summary>
    [Serializable]
    public struct ColorToConnection
    {
        public Color32 mColorType;
        public CONNECTION_TYPE mConnectionFromColorType;
    }

    // public vars for InputTraining
    [Tooltip("width is how many in the x will generate, height is how many in the \"y\" (technically z) direction will generate, and gridsize should always be 1.")]
    public int width, gridsize, height;

    [Tooltip("This is our dictionary, where we tell the algorithm which color type can connect to which.")]
    public ColorToConnection[] mColorToConnection; // Have to do this to make a faux dictionary. 

    // private vars for InputTraining
    private Dictionary<Color32, CONNECTION_TYPE> mColorToConnectionDict = new Dictionary<Color32, CONNECTION_TYPE>();

    [HideInInspector]
    public List<Module> mModules = new List<Module>(); // Our literal input in the square.

    [HideInInspector]
    public List<Module> mOrigModules = new List<Module>(); // original modules.


    // draws the matrix cube thing for placing things.
    void OnDrawGizmos()
    {
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(new Vector3((width * gridsize / 2f) - gridsize * 0.5f, 0f, (height * gridsize / 2f) - gridsize * 0.5f),
            new Vector3(width * gridsize, gridsize, height * gridsize));
    }

    /// <summary>
    /// First we get our colors to connections, then get our input modules,
    /// and then set each one's connection.
    /// </summary>
    void Start()
    {
        // getPixelValues();
        setUpColorDict();
        getModules();
        setPossibleConnections();
        doAllRotations();

    }

    /// <summary>
    /// Get each thing that is inside of the input square.
    /// </summary>
    void getModules()
    {
        int count = transform.childCount;
        int i;
        for (i = 0; i < count; ++i)
        {
            Module newMod = new Module(transform.GetChild(i).gameObject);
            Vector3 tempPos = newMod.mObject.transform.position; // save position so I don't pull my hair out.

            // check to make sure we're inside the bounds of the rectangular input area
            if ((tempPos.x > -0.1) && (tempPos.x <= width * gridsize - 0.1f + transform.position.x) &&
                (tempPos.z > -0.1) && (tempPos.z <= height * gridsize - 0.1f + transform.position.z))
            {
                mOrigModules.Add(newMod); // leaving it as this for now, new funky things can happen later
            }
        }
    }

    /// <summary>
    /// Set the connections for every single input module.
    /// </summary>
    void setPossibleConnections()
    {
        int i;
        for (i = 0; i < mOrigModules.Count; ++i)
        {
            mOrigModules[i] = getPixelValues(mOrigModules[i]);
        }
    }

    /// <summary>
    /// Get the values for our pixel texture connections. After that, turn those pixel values
    /// into actual connections using the dictionary we set up.
    /// </summary>
    /// <param name="mod">the module that we are trying to find the connections for.</param>
    /// <returns>a module that has proper connections.</returns>
    Module getPixelValues(Module mod)
    {
        // gives us a 1D array
        var colorArray = mod.mMainTexture.GetPixels32(0);
        //Color32 testColor = colorArray[0];
        //Color32 testColor2 = colorArray[colorArray.Length - 1];
        // From 2D to 1D: index = x + (y * width)

        int w = mod.mMainTexture.width - 1;
        int h = mod.mMainTexture.height - 1;
        /*
        Color32 col1 = colorArray[0]; // Top Left corner
        Color32 col2 = colorArray[w / 2]; // top middle
        Color32 col3 = colorArray[w]; // top right corner 
        Color32 col4 = colorArray[((h + 1) / 2) * (w + 1)]; // middle left
        Color32 col5 = colorArray[w + (((h + 1) / 2) * (w + 1))]; // middle right
        Color32 col6 = colorArray[(h + 1) * (w)]; // bot left corner
        Color32 col7 = colorArray[w / 2 + ((h + 1) * (w)) - 1]; // bot middle
        Color32 col8 = colorArray[colorArray.Length - 1]; // bottom right corner
        */
        Color32 _col1 = colorArray[w / 8]; // bot left 8th
        Color32 _col2 = colorArray[w / 2]; // bot middle
        Color32 _col3 = colorArray[7 * (w + 1) / 8]; // bot right 8th
        Color32 _col4 = colorArray[((h + 1) / 8) * (w + 1)]; // left bot 8th
        Color32 _col5 = colorArray[((h + 1) / 2) * (w + 1)]; // left middle
        Color32 _col6 = colorArray[7 * (h + 1) / 8 * (w + 1)]; // left top 8th (Yes for now???)
        Color32 _col7 = colorArray[colorArray.Length - (7 * w / 8)]; // top left 8th
        Color32 _col8 = colorArray[(w) / 2 + ((h + 1) * (w))]; // top middle
        Color32 _col9 = colorArray[colorArray.Length - 1 - (w / 8)]; // top right 8th
        Color32 _col10 = colorArray[w + ((h+1) / 8 * (w + 1))]; // right bot 8th
        Color32 _col11 = colorArray[w + (((h + 1) / 2) * (w + 1))]; // right middle
        Color32 _col12 = colorArray[w + 7 * (h + 1) / 8 * (w + 1)]; // right top 8th
        //Color32 _col12 = colorArray[7 * w / 8 + h * (w + 1)]; // REAL BOTTOM RIGHT 8TH

        // Colors are read top to bottom, left to right order
        /*
        Color32[] northSide = { _col1, _col2, _col3 };      // Top left, top middle, top right
        Color32[] eastSide = { _col10, _col11, _col12 };    // top right, middle right, bottom right
        Color32[] southSide = { _col7, _col8, _col9 };      // bottom left, bottom middle, bottom right
        Color32[] westSide = { _col4, _col5, _col6 };       // top left, middle left, bottom left
        */
        Color32[] northSide = { _col7, _col8, _col9 };      // Top left, top middle, top right
        Color32[] eastSide = { _col12, _col11, _col10 };    // top right, middle right, bottom right
        Color32[] southSide = { _col1, _col2, _col3 };      // bottom left, bottom middle, bottom right
        Color32[] westSide = { _col6, _col5, _col4 };       // top left, middle left, bottom left

        // convert each color to a connection.
        mod.mConnection.mNorthConnection.mConnection = convertColorToConnectionType(northSide);
        mod.mConnection.mEastConnection.mConnection  = convertColorToConnectionType(eastSide);
        mod.mConnection.mSouthConnection.mConnection = convertColorToConnectionType(southSide);
        mod.mConnection.mWestConnection.mConnection  = convertColorToConnectionType(westSide);

        return mod;

    }

    /// <summary>
    /// Does the horse-work of converting a specific color array to a connection type that can be put in a module.
    /// </summary>
    /// <param name="colorArray"></param>
    /// <returns>returns the finished CONNECTION_TYPE array that can go in a module.</returns>
    CONNECTION_TYPE[] convertColorToConnectionType(Color32[] colorArray)
    {
        CONNECTION_TYPE[] theArray = new CONNECTION_TYPE[colorArray.Length]; // Should always be 3, but making it modular to be safe

        for (int i = 0; i < theArray.Length; ++i)
            theArray[i] = mColorToConnectionDict[colorArray[i]];

        return theArray;
    }

    /// <summary>
    /// This function converts our faux dictionary to a real dictionary, to make converting color to connection easier.
    /// Probably not the most efficient thing, but it works. 
    /// </summary>
    void setUpColorDict()
    {
        for(int i = 0; i < mColorToConnection.Length; ++i)
            mColorToConnectionDict.Add(mColorToConnection[i].mColorType, mColorToConnection[i].mConnectionFromColorType);
    }

    /// <summary>
    /// Create a new module for each rotation that does not exist yet. 
    /// </summary>
    /// <param name="mod">the first input module</param>
    void doRotations(Module mod, float addAmt)
    {
        //GameObject newObj = mod.mObject;
        //GameObject newObj = new GameObject(mod.mObject.name + "_" + addAmt);
        var newObj = Instantiate(mod.mObject);
        Vector3 rot = mod.mObject.transform.rotation.eulerAngles;
        rot.y += addAmt;
        newObj.name = mod.mObject.name + "_" + rot.y;
        newObj.transform.rotation = Quaternion.Euler(rot.x, rot.y, rot.z);
        //newObj.transform.rotation.eulerAngles = new Vector3();
        Module newMod = new Module(newObj);
        newMod.mConnection.mNorthConnection = mod.mConnection.mWestConnection;
        newMod.mConnection.mEastConnection = mod.mConnection.mNorthConnection;
        newMod.mConnection.mSouthConnection = mod.mConnection.mEastConnection;
        newMod.mConnection.mWestConnection = mod.mConnection.mSouthConnection;
        mModules.Add(newMod);
    }

    /// <summary>
    /// Does the rotations for every single module.
    /// </summary>
    void doAllRotations()
    {
        int counter = 0;
        // Kind of roundabout, but get the original module + new modules into the list.
        for (int i = 0; i < mOrigModules.Count; ++i)
        {
            mModules.Add(mOrigModules[i]);
            //doRotations(mOrigModules[i], 0);
            doRotations(mModules[counter], 90);
            ++counter;
            doRotations(mModules[counter], 90);
            ++counter;
            doRotations(mModules[counter], 90);
            counter += 2;
        }
    }
}
