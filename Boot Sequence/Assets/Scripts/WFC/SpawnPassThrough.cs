﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using UnityEngine;

public class SpawnPassThrough : MonoBehaviour
{
    // Start is called before the first frame update
    List<Transform> mSpawnPoints = new List<Transform>();

    [Tooltip("The amount of generators that will be spawned.")]
    public int generatorCount = 5;
    [Tooltip("The generator object that will be spawned in the level.")]
    public GameObject generatorObj;

    [Tooltip("The total amount of enemies that will be spawned in the level.")]
    public int enemyCount;
    [Tooltip("All enemy types that can be spawned in the level.")]
    public GameObject[] enemyObjList;

    [Tooltip("The amount of crates that will be cr(e)ated in the level.")]
    public int crateCount;
    [Tooltip("The crate objects that will be spawned in the level.")]
    public GameObject[] crateObjList;

    [Tooltip("The amount of turrets that will be spawned.")]
    private int turretCount;
    [Tooltip("All the variations of turrets that can be spawned.")]
    public GameObject[] turretEnemies;

    System.Random rand = new System.Random();

    [Tooltip("The exact name of a spawn point. The code will look through the scene for these specific names as the spawn point type.")]
    public string generatorSpawnName, crateSpawnName, enemySpawnName, turretSpawnName;
    List<Transform> mGenSpawnPoints = new List<Transform>();
    List<Transform> mCrateSpawnPoints = new List<Transform>();
    List<Transform> mEnemySpawnPoints = new List<Transform>();
    List<Transform> mTurretSpawnPoints = new List<Transform>();

    private int basicEnWeight, bombEnWeight, shockEnWeight;
    private int weightTotal;
    private int[] weights = new int[3];

    public GameObject mPillar;
    public int numPillars = 12;

    public GameObject wallObj;
    public float wallWidth;

    void Start()
    {
        GetNumbersFromGC();
        startGetPoints();
        spawnGeneratorPass();
        spawnEnemyPass();
        spawnCratePass();
        turretSpawnPass();
        SpawnPillarPass();
        SpawnWallsPass();

        DoneSpawning();
    }

    void startGetPoints()
    {
        for(int i = 0; i < transform.childCount; ++i)
        {
            getPoints(transform.GetChild(i).gameObject);
        }
    }

    void getPoints(GameObject obj)
    {
        //int counter = 0;
        //foreach(Transform t in obj.transform)
        //{
        //    if(t.name.ToLower().Contains("spawn"))
        //            mSpawnPoints.Add(t);

        //    if(t.name.ToLower().Contains(generatorSpawnName))
        //            mGenSpawnPoints.Add(t);

        //   if (t.name.ToLower().Contains(crateSpawnName))
        //            mCrateSpawnPoints.Add(t);

        //    if (t.name.ToLower().Contains(enemySpawnName))
        //            mEnemySpawnPoints.Add(t);

        //    if(t.childCount > 0)
        //        getPoints(t.GetChild(counter).gameObject);

        //    ++counter;
        //}

        if(obj.transform.childCount > 0)
            for(int i = 0; i < obj.transform.childCount; ++i)
            {
                if (obj.gameObject.activeInHierarchy == false)
                    break;

                if (obj.name.ToLower().Contains("spawn"))
                    mSpawnPoints.Add(obj.transform);

                if (obj.name.ToLower().Contains(generatorSpawnName.ToLower()))
                    mGenSpawnPoints.Add(obj.transform);

                if (obj.name.ToLower().Contains(crateSpawnName.ToLower()))
                    mCrateSpawnPoints.Add(obj.transform);

                if (obj.name.ToLower().Contains(enemySpawnName.ToLower()))
                    mEnemySpawnPoints.Add(obj.transform);

                if (obj.name.ToLower().Contains(turretSpawnName.ToLower()))
                    mTurretSpawnPoints.Add(obj.transform);

                if (obj.transform.childCount > 0)
                    getPoints(obj.transform.GetChild(i).gameObject);
            }

        else
        {
            if (obj.gameObject.activeInHierarchy == false)
                return;

            if (obj.name.ToLower().Contains("core"))
                return;

            if (obj.name.ToLower().Contains("spawn"))
                mSpawnPoints.Add(obj.transform);

            if (obj.name.ToLower().Contains(generatorSpawnName.ToLower()))
                mGenSpawnPoints.Add(obj.transform);

            if (obj.name.ToLower().Contains(crateSpawnName.ToLower()))
                mCrateSpawnPoints.Add(obj.transform);

            if (obj.name.ToLower().Contains(enemySpawnName.ToLower()))
                mEnemySpawnPoints.Add(obj.transform);

            if (obj.name.ToLower().Contains(turretSpawnName.ToLower()))
                mTurretSpawnPoints.Add(obj.transform);
        }        
        /*
        for (int i = 0; i < transform.childCount; ++i)
        {
            for (int j = 0; j < transform.GetChild(i).childCount; ++j)
            {
                if(transform.GetChild(i).GetChild(j).name.ToLower().Contains("spawn"))
                    mSpawnPoints.Add(transform.GetChild(i).GetChild(j));

                if(transform.GetChild(i).GetChild(j).name.ToLower().Contains(generatorSpawnName))
                    mGenSpawnPoints.Add(transform.GetChild(i).GetChild(j));

                if (transform.GetChild(i).GetChild(j).name.ToLower().Contains(crateSpawnName))
                    mCrateSpawnPoints.Add(transform.GetChild(i).GetChild(j));

                if (transform.GetChild(i).GetChild(j).name.ToLower().Contains(enemySpawnName))
                    mEnemySpawnPoints.Add(transform.GetChild(i).GetChild(j));
            }
        }
        */
    }

    void spawnGeneratorPass()
    {
        for (int i = 0; i < generatorCount; ++i)
        {
            instantiateObj(generatorObj, "Generator_", i, generatorSpawnName);
        }
    }

    void spawnEnemyPass()
    {
        for (int i = 0; i < enemyCount; ++i)
        {
            if (mEnemySpawnPoints.Count <= 0)
                break;
            int en = GetRandomWeighted();
            var o = enemyObjList[en];
            instantiateObj(o, o.name + "_", i, enemySpawnName);
        }
    }

    void turretSpawnPass()
    {
        for(int i = 0; i < turretCount; ++i)
        {
            if (mTurretSpawnPoints.Count <= 0)
                break;
            int tn = rand.Next(turretEnemies.Length);
            var t = turretEnemies[tn];
            instantiateObj(t, t.name + "_", i, turretSpawnName);
        }
    }

    void spawnCratePass()
    {
        for (int i = 0; i < crateCount; ++i)
        {
            if (mCrateSpawnPoints.Count <= 0)
                break;
            int cr = rand.Next(crateObjList.Length);
            var c = crateObjList[cr];
            instantiateObj(c, c.name + "_", i, crateSpawnName);
        }
    }



    void instantiateObj(GameObject obj, string objName, int nameNum, string spName)
    {
        int loc;
        Vector3 pos;
        if (spName.Equals(generatorSpawnName))
        {
            loc = rand.Next(mGenSpawnPoints.Count - 1);
            pos = mGenSpawnPoints[loc].position;
            mGenSpawnPoints.RemoveAt(loc);
        }
        else if (spName.Equals(enemySpawnName))
        {
            loc = rand.Next(mEnemySpawnPoints.Count - 1);
            pos = mEnemySpawnPoints[loc].position;
            mEnemySpawnPoints.RemoveAt(loc);
        }
        else if (spName.Equals(crateSpawnName))
        {
            loc = rand.Next(mCrateSpawnPoints.Count - 1);
            pos = mCrateSpawnPoints[loc].position;
            mCrateSpawnPoints.RemoveAt(loc);
        }
        else if (spName.Equals(turretSpawnName))
        {
            loc = rand.Next(mTurretSpawnPoints.Count - 1);
            pos = mTurretSpawnPoints[loc].position;
            pos.y += 0.5f;
            mTurretSpawnPoints.RemoveAt(loc);
        }
        else
        {
            loc = rand.Next(mSpawnPoints.Count - 1);
            pos = mSpawnPoints[loc].position;
            mSpawnPoints.RemoveAt(loc);
        }

        var go = Instantiate(obj);
        pos.y += 0.5f;
        go.transform.position = pos;
        go.name = objName + (nameNum + 1);
        go.transform.SetParent(gameObject.transform);
    }

    public void DoneSpawning()
    {
        GameControl.control.doneSpawn = true;
    }

    private void GetNumbersFromGC()
    {
        enemyCount = GameControl.control.totalEnemies;
        generatorCount = GameControl.control.totalGenerators;
        turretCount = GameControl.control.totalTurrets;
        basicEnWeight = GameControl.control.basicEnemyWeight;
        bombEnWeight = GameControl.control.bomberEnemyWeight;
        shockEnWeight = GameControl.control.shockerEnemyWeight;
        weightTotal = basicEnWeight + bombEnWeight + shockEnWeight;
        weights[0] = basicEnWeight;
        weights[1] = bombEnWeight;
        weights[2] = shockEnWeight;
    }

    int GetRandomWeighted()
    {
        int result, total = 0;
        int randVal = Random.Range(0, weightTotal);

        for (result = 0; result < 3; ++result)
        {
            total += weights[result];
            if (total > randVal)
                break;
        }

        return result;
    }

    // Used to find where we can and cannot spawn pillars. Also walls.
    // absMin is the negative wall, absMax is the wall max, 
    // edgeMin is the left / bottom transform of the level
    // edgeMax is the right / top transform of the level.
    private float absMin = -42, absMaxX = 99, edgeMin = -35, edgeMaxX = 92;
    private float absMaxY, edgeMaxY;

    private void SpawnPillarPass()
    {
        edgeMin = -35;
        absMin = edgeMin - 7f;
        edgeMaxX = GetComponent<ConvertTextureToMesh>().getOScript().width *
                  GetComponent<ConvertTextureToMesh>().mSizeOfModule + 21f; // The 21 is for the gen room / start area.
        absMaxX = edgeMaxX + 7f;
        edgeMaxY = GetComponent<ConvertTextureToMesh>().getOScript().height *
                   GetComponent<ConvertTextureToMesh>().mSizeOfModule + 21f;
        absMaxY = edgeMaxY + 7f;
        // top
        InstantiatePillars(absMin, absMaxX, edgeMaxY, absMaxY);
        // right
        InstantiatePillars(edgeMaxX, absMaxX, absMin, absMaxY);
        // bottom
        InstantiatePillars(absMin, absMaxX, absMin, edgeMin);
        // left
        InstantiatePillars(absMin, edgeMin, absMin, absMaxY);
    }

    private void InstantiatePillars(float xMin, float xMax, float yMin, float yMax)
    {
        for (int i = 0; i < numPillars; ++i)
        {
            float x = Random.Range(xMin, xMax);
            float y = Random.Range(yMin, yMax);
            var obj = Instantiate(mPillar);
            obj.transform.SetParent(transform);
            obj.transform.localPosition = new Vector3(x,-11,y);
            obj.name = "PillarLad";
        }
    }

    private void SpawnWallsPass()
    {
        // Move the transform of the walls back, because the prefab is fucked right now.
        absMaxX += 5f;
        absMaxY += 5f;
        absMin -= 5f;

        // Top
        InstantiateWalls(absMin, absMaxY, absMaxX, absMaxY, true, 90);

        // Right
        InstantiateWalls(absMaxX, absMin, absMaxX, absMaxY, false, 180);

        // Bottom
        InstantiateWalls(absMin, absMin, absMaxX, absMaxY, true, 270);

        // Left
        InstantiateWalls(absMin, absMin, absMaxX, absMaxY, false, 0);
    }

    private void InstantiateWalls(float xLine, float yLine, float xMax, float yMax, bool horizontal, float rotation)
    {
        while (xLine <= xMax && yLine <= yMax)
        {
            var obj = Instantiate(wallObj);
            obj.transform.SetParent(transform);
            obj.name = "WallLad";
            obj.transform.localPosition = new Vector3(xLine, 0, yLine);
            obj.transform.rotation = Quaternion.Euler(0, rotation, 0);

            if (horizontal)
                xLine += wallWidth;
            else
                yLine += wallWidth;
        }

    }
}
