﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlayerWFC : MonoBehaviour
{
    public WFCOutput mWfcOutput;
    public Transform mWFCOutObj;

    public GameObject mOkaySpawnLoc;
    public GameObject spawnObj;

    List<Vector3> mPossibilities = new List<Vector3>();

    private bool done = false;

    public GameObject mPlayerToSpawn;

    private GameObject mSpawner;

    // Start is called before the first frame update
    void Start()
    {
    }

    void Update()
    {
        if (!done)
        {
            findLocs();
            placeSpawn();
            spawnPlayer();
        }
    }

    public void findLocs()
    {
        for (int i = 0; i < mWFCOutObj.childCount; ++i)
        {
            string strComp = mWFCOutObj.GetChild(i).gameObject.name
                .Substring(0, mWFCOutObj.GetChild(i).gameObject.name.IndexOf("("));
            if (strComp.Equals(mOkaySpawnLoc.gameObject.name))
                mPossibilities.Add(new Vector3(mWFCOutObj.GetChild(i).position.x, mWFCOutObj.GetChild(i).position.y + 1, mWFCOutObj.GetChild(i).position.z));
        }
    }

    public void placeSpawn()
    {
        System.Random rand = new System.Random();

        int loc = rand.Next(mPossibilities.Count);

        mSpawner = Instantiate(spawnObj, mPossibilities[loc], Quaternion.identity);

        done = true;
    }

    public void spawnPlayer()
    {
        var play = Instantiate(mPlayerToSpawn, mSpawner.transform.position, Quaternion.identity);

        GameObject.Find("Main Camera").transform.position = new Vector3(play.transform.position.x, play.transform.position.y + 2, play.transform.position.z - 10);
        GameObject.Find("Main Camera").transform.SetParent(play.transform);

    }

}
