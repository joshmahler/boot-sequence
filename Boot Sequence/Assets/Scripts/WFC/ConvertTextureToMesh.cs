﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class ConvertTextureToMesh : MonoBehaviour
{
    /// <summary>
    /// Much like with Input, creating a faux dictionary because Unity dumm.
    /// </summary>
    [Serializable]
    public class TextureToObject
    {
        [Tooltip("The simple object.")]
        public GameObject mTextureObj; // The simple object
        [Tooltip("The variations of real objects that can be placed for this simple object.")]
        public List<GameObject> mREEEEalObjs = new List<GameObject>(); // The real object that will be placed.
    }

    [Tooltip("TTO stands for \"Texture to Object\". In this list, place all the simple objects, and then the actual \"Room\" variations that those simple objects can be.")]
    public TextureToObject[] mTTOList;
    public float mSizeOfModule = 16f;
    private float mWidth, mHeight;

    [Tooltip("The output script.")]
    public WFCOutput mOScript;
    [Tooltip("The input script.")]
    public InputTraining mIScript;

    Dictionary<GameObject, List<GameObject>> mDict = new Dictionary<GameObject, List<GameObject>>();

    private List<GameObject> mAllRealObjects = new List<GameObject>();

    [Tooltip("RGO standing for \"Real GameObject\". These are the start and end nodes, turned into meshes.")]
    public GameObject startRGO, finishRGO;

    [Tooltip("Like above, the special generator room variants.")]
    public List<GameObject> generatorVariants = new List<GameObject>();

    System.Random rand = new System.Random();

    public GameObject killArea;

    void Start()
    {
        InitAll();
    }

    private void InitAll()
    {
        mWidth = mOScript.width;
        mHeight = mOScript.height;
        RotateAllRObjects();

        //ConvertDict();
        ConvertDict2();
        PlaceOutput();
        PlaceStartAndEnd();
        PlaceSpecialGenRooms();
    }

    //private void ConvertDict()
    //{
    //    for (int i = 0; i < mIScript.mModules.Count; ++i)
    //    {
    //        mDict.Add(mIScript.mModules[i].mObject, mAllRealObjects[i]);
    //    }
    //}

    private void ConvertDict2()
    {
        int i, j;
        // go through the entire TTO list. 
        for (i = 0; i < mTTOList.Length; ++i)
        {
            // Find the actual texture object, and it's location in the list.
            // tmo for Temp Module Object. Toduleject for short.
            var tmo = mIScript.mModules.Find(o => o.mObject == mTTOList[i].mTextureObj);
            int spot = mIScript.mModules.FindIndex(o => o.mObject == mTTOList[i].mTextureObj);
            for (j = 0; j < mTTOList.Length; ++j)
            {
                if (mTTOList[j].mTextureObj == tmo.mObject)
                    break;
            }

            // use that location to also get the real object spot that we're currently looking at. 
            var curRoSpot = j;

            // for each rot
            for (j = 0; j < 4; ++j)
            {
                // List of GO for the dictionary.
                List<GameObject> tL = new List<GameObject>();

                // Go through and grab the variation. adding our j to get the proper rotation.
                foreach (var obj in mTTOList[curRoSpot].mREEEEalObjs)
                {
                    int tRO = mAllRealObjects.FindIndex(o => o.name == obj.name);
                    tL.Add(mAllRealObjects[tRO + j]);
                }
                
                // Add the variations to the dictionary, used for output.
                mDict.Add(mIScript.mModules[spot].mObject, tL);
                ++spot;
            }


            //var tro = mTTOList[j].mREEEEalObj;
            //mAllRealObjects.IndexOf(mTTOList[j].mREEEEalObj);
            // The 4 is for rotations.
            //for (j = 0; j < 4; ++j)
            //{
            //    List<GameObject> tL = new List<GameObject>();
            //    int troSpot = mAllRealObjects.FindIndex(o => o == obj);
            //    foreach (var obj in mTTOList[curRoSpot].mREEEEalObjs)
            //    {
            //        tL.Add(mAllRealObjects[troSpot]);
            //        ++troSpot;
            //    }
            //    mDict.Add(mIScript.mModules[spot].mObject, tL);
            //    ++spot;
            //    ++curRoSpot;
            //}
            


            //mDict.Add(mIScript.mModules[spot].mObject, mAllRealObjects[troSpot]);
            //mDict.Add(mIScript.mModules[++spot].mObject, mAllRealObjects[++troSpot]);
            //mDict.Add(mIScript.mModules[++spot].mObject, mAllRealObjects[++troSpot]);
            //mDict.Add(mIScript.mModules[++spot].mObject, mAllRealObjects[++troSpot]);
        }
    }

    private void RotateAllRObjects()
    {
        for (int i = 0; i < mTTOList.Length; ++i)
        {
            for (int j = 0; j < mTTOList[i].mREEEEalObjs.Count; ++j)
            {
                var go = Instantiate(mTTOList[i].mREEEEalObjs[j]);
                go.name = mTTOList[i].mREEEEalObjs[j].name;
                mAllRealObjects.Add(go);
                go.transform.position = new Vector3(0f, 0f, 0f);
                //RotateOneObject(mTTOList[i].mREEEEalObj, 0);
                RotateOneObject(mTTOList[i].mREEEEalObjs[j], 90);
                RotateOneObject(mTTOList[i].mREEEEalObjs[j], 180);
                RotateOneObject(mTTOList[i].mREEEEalObjs[j], 270);
            }
        }
    }

    private void RotateOneObject(GameObject origObj, float adder)
    {
        GameObject obj = Instantiate(origObj);
        obj.name = origObj.name + "_" + adder;
        obj.transform.position = new Vector3(0f, 0f, 0f);
        Quaternion quat = obj.transform.rotation;
        quat.y += adder;
        //quat = Quaternion.Normalize(quat);
        obj.transform.rotation = Quaternion.Euler(quat.x, quat.y, quat.z);
        mAllRealObjects.Add(obj);
    }

    private void PlaceOutput()
    {
        for (int i = 0; i < mWidth; ++i)
        {
            for (int j = 0; j < mHeight; ++j)
            {
                for (int k = 0; k < mOScript.getT(); ++k)
                {
                    if (mOScript.getAt(i, j, k).mAlive)
                    {
                        var tempObjList = mDict[mOScript.getAt(i, j, k).mModule.mObject];
                        var tgo = tempObjList[rand.Next(tempObjList.Count)];
                        var rgo = Instantiate(tgo);
                        rgo.transform.position = new Vector3(i * mSizeOfModule + transform.position.x, -4.5f, j * mSizeOfModule + transform.position.z);
                        rgo.transform.SetParent(gameObject.transform);
                        rgo.name += "_Level_Piece";
                        break;
                    }
                }
            }
        }

        var kz = Instantiate(killArea, gameObject.transform.position + new Vector3(28, 0, 28), Quaternion.identity);
        Vector3 v = kz.transform.position;

        v.y -= 7.5f;
        kz.transform.position = v;
        kz.transform.SetParent(gameObject.transform);

    }

    private void PlaceStartAndEnd()
    {
        //Utility.Log("Start: " + mOScript.startGO.transform.localPosition);
        //Utility.Log("End: " + mOScript.endGO.transform.localPosition);

        var go = Instantiate(startRGO);
        var go2 = Instantiate(finishRGO);
        go.transform.SetParent(gameObject.transform);
        go2.transform.SetParent(gameObject.transform);
        go.transform.rotation = mOScript.startGO.transform.rotation;
        go2.transform.rotation = mOScript.endGO.transform.rotation;

        go.name = "env_start";
        go2.name = "env_end";

        go.transform.localPosition = mOScript.startGO.transform.localPosition * mSizeOfModule;
        Vector3 v1, v2;
        v1 = go.transform.localPosition;
        v1.y -= 4.5f;
        go.transform.localPosition = v1;
        go2.transform.localPosition = mOScript.endGO.transform.localPosition * mSizeOfModule;
        v2 = go2.transform.localPosition;
        v2.y -= 4.5f;
        go2.transform.localPosition = v2;
    }

    private void PlaceSpecialGenRooms()
    {
        var go = Instantiate(generatorVariants[rand.Next(generatorVariants.Count)]);
        var go2 = Instantiate(generatorVariants[rand.Next(generatorVariants.Count)]);

        go.transform.SetParent(gameObject.transform);
        go2.transform.SetParent(gameObject.transform);
        go.transform.rotation = mOScript.genRoom1.transform.rotation;
        go2.transform.rotation = mOScript.genRoom2.transform.rotation;
        
        go.name = "Generator_Room_1";
        go2.name = "Generator_Room_2";

        go.transform.localPosition = mOScript.genRoom1.transform.localPosition * mSizeOfModule;
        Vector3 v1, v2;
        v1 = go.transform.localPosition;
        v1.y -= 4.5f;
        go.transform.localPosition = v1;
        go2.transform.localPosition = mOScript.genRoom2.transform.localPosition * mSizeOfModule;
        v2 = go2.transform.localPosition;
        v2.y -= 4.5f;
        go2.transform.localPosition = v2;
    }

    /// <summary>
    /// getOScript allows us to get the output script that CTTM is using.
    /// </summary>
    /// <returns>A copy of WFCOutput.</returns>
    public WFCOutput getOScript()
    {
        return mOScript;
    }
}
