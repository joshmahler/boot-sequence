%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: lower_half_mask
  m_Mask: 01000000000000000000000001000000010000000000000000000000000000000000000001000000010000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: g_controls
    m_Weight: 0
  - m_Path: g_controls/ctrl_root
    m_Weight: 0
  - m_Path: g_controls/ctrl_root/ctrl_finger_options_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrl_finger_options_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrL_foot_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrL_foot_l/t_heel_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrL_foot_l/t_heel_l/t_toe_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrL_foot_l/t_heel_l/t_toe_l/ik_toe_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrL_foot_l/t_heel_l/t_toe_l/t_ball_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrL_foot_l/t_heel_l/t_toe_l/t_ball_l/ik_foot_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrL_foot_l/t_heel_l/t_toe_l/t_ball_l/ik_leg_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrL_foot_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrL_foot_r/t_heel_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrL_foot_r/t_heel_r/t_toe_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrL_foot_r/t_heel_r/t_toe_r/ik_toe_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrL_foot_r/t_heel_r/t_toe_r/t_ball_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrL_foot_r/t_heel_r/t_toe_r/t_ball_r/ik_foot_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrL_foot_r/t_heel_r/t_toe_r/t_ball_r/ik_leg_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrl_headSwitch
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrl_hips
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrl_hips/ctrl_body
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrl_hips/ctrl_body/sp_chest
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/ctrl_hips/ctrl_body/sp_chest/ctrl_chest
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_arms
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_arms/ctrl_clav_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_arms/ctrl_clav_l/ctrl_pauldron_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_arms/ctrl_clav_l/grp_ctrl_fk_shoulder_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_arms/ctrl_clav_l/grp_ctrl_fk_shoulder_l/ctrl_fk_shoulder_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_arms/ctrl_clav_l/grp_ctrl_fk_shoulder_l/ctrl_fk_shoulder_l/grp_ctrl_fk_elbow_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_arms/ctrl_clav_l/grp_ctrl_fk_shoulder_l/ctrl_fk_shoulder_l/grp_ctrl_fk_elbow_l/ctrl_fk_elbow_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_arms/ctrl_clav_l/grp_ctrl_fk_shoulder_l/ctrl_fk_shoulder_l/grp_ctrl_fk_elbow_l/ctrl_fk_elbow_l/grp_ctrl_fk_wrist_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_arms/ctrl_clav_l/grp_ctrl_fk_shoulder_l/ctrl_fk_shoulder_l/grp_ctrl_fk_elbow_l/ctrl_fk_elbow_l/grp_ctrl_fk_wrist_l/ctrl_fk_wrist_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_arms/ctrl_clav_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_arms/ctrl_clav_r/ctrl_pauldron_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_arms/ctrl_clav_r/grp_ctrl_fk_shoulder_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_arms/ctrl_clav_r/grp_ctrl_fk_shoulder_r/ctrl_fk_shoulder_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_arms/ctrl_clav_r/grp_ctrl_fk_shoulder_r/ctrl_fk_shoulder_r/grp_ctrl_fk_elbow_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_arms/ctrl_clav_r/grp_ctrl_fk_shoulder_r/ctrl_fk_shoulder_r/grp_ctrl_fk_elbow_r/ctrl_fk_elbow_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_arms/ctrl_clav_r/grp_ctrl_fk_shoulder_r/ctrl_fk_shoulder_r/grp_ctrl_fk_elbow_r/ctrl_fk_elbow_r/grp_ctrl_fk_wrist_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_arms/ctrl_clav_r/grp_ctrl_fk_shoulder_r/ctrl_fk_shoulder_r/grp_ctrl_fk_elbow_r/ctrl_fk_elbow_r/grp_ctrl_fk_wrist_r/ctrl_fk_wrist_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_ctrl_hand_ik_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_ctrl_hand_ik_l/ctrl_hand_ik_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_ctrl_hand_ik_l/ctrl_hand_ik_l/ik_arm_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_ctrl_hand_ik_l/ctrl_hand_ik_l/t_hand_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_ctrl_hand_ik_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_ctrl_hand_ik_r/ctrl_hand_ik_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_ctrl_hand_ik_r/ctrl_hand_ik_r/ik_arm_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_ctrl_hand_ik_r/ctrl_hand_ik_r/t_r_hand
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_head
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_head/sp_head_aim
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_head/sp_head_aim/ctrl_head_aim
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_head/sp_head_aim/ctrl_head_aim/t_head_aim
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_head/sp_head_fk
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_head/sp_head_fk/ctrl_head_fk
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/grp_head/sp_head_fk/ctrl_head_fk/t_head_fk
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/sp_elbow_pvc_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/sp_elbow_pvc_l/ctrl_elbow_pvc_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/sp_elbow_pvc_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/sp_elbow_pvc_r/ctrl_elbow_pvc_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/sp_knee_pvc_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/sp_knee_pvc_l/ctrl_knee_pvc_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/sp_knee_pvc_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/sp_knee_pvc_r/ctrl_knee_pvc_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/sp_neck
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/sp_neck/ctrl_neck
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/sp_weapon
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/sp_weapon/ctrl_weapon_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_l/ctrl_middle1_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_l/ctrl_middle1_l/ctrl_middle2_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_l/ctrl_middle1_l/ctrl_middle2_l/ctrl_middle3_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_l/ctrl_pinky1_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_l/ctrl_pinky1_l/ctrl_pinky2_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_l/ctrl_pinky1_l/ctrl_pinky2_l/ctrl_pinky3_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_l/ctrl_pointer1_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_l/ctrl_pointer1_l/ctrl_pointer2_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_l/ctrl_pointer1_l/ctrl_pointer2_l/ctrl_pointer3_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_l/ctrl_thumb1_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_l/ctrl_thumb1_l/ctrl_thumb2_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_l/ctrl_thumb1_l/ctrl_thumb2_l/ctrl_thumb3_l
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_r/ctrl_middle1_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_r/ctrl_middle1_r/ctrl_middle2_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_r/ctrl_middle1_r/ctrl_middle2_r/ctrl_middle3_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_r/ctrl_pinky1_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_r/ctrl_pinky1_r/ctrl_pinky2_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_r/ctrl_pinky1_r/ctrl_pinky2_r/ctrl_pinky3_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_r/ctrl_pointer1_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_r/ctrl_pointer1_r/ctrl_pointer2_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_r/ctrl_pointer1_r/ctrl_pointer2_r/ctrl_pointer3_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_r/ctrl_thumb1_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_r/ctrl_thumb1_r/ctrl_thumb2_r
    m_Weight: 1
  - m_Path: g_controls/ctrl_root/t_fingers_r/ctrl_thumb1_r/ctrl_thumb2_r/ctrl_thumb3_r
    m_Weight: 1
  - m_Path: g_mesh_seperate
    m_Weight: 0
  - m_Path: g_mesh_seperate/g_hand_l
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_l/m_gaurd_l
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_l/m_hand_l
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_l/m_middle1_l
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_l/m_middle2_l
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_l/m_middle3_l
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_l/m_pinky1_l
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_l/m_pinky2_l
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_l/m_pinky3_l
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_l/m_pointer1_l
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_l/m_pointer2_l
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_l/m_pointer3_l
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_l/m_thumb1_l
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_l/m_thumb2_l
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_l/m_thumb3_l
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_r
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_r/m_gaurd_r
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_r/m_hand_r
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_r/m_middle1_r
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_r/m_middle2_r
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_r/m_middle3_r
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_r/m_pinky1_r
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_r/m_pinky2_r
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_r/m_pinky3_r
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_r/m_pointer1_r
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_r/m_pointer2_r
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_r/m_pointer3_r
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_r/m_thumb1_r
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_r/m_thumb2_r
    m_Weight: 1
  - m_Path: g_mesh_seperate/g_hand_r/m_thumb3_r
    m_Weight: 1
  - m_Path: g_mesh_seperate/m_arm_l
    m_Weight: 1
  - m_Path: g_mesh_seperate/m_arm_lower_r
    m_Weight: 1
  - m_Path: g_mesh_seperate/m_arm_r
    m_Weight: 1
  - m_Path: g_mesh_seperate/m_belt
    m_Weight: 1
  - m_Path: g_mesh_seperate/m_boot_l
    m_Weight: 1
  - m_Path: g_mesh_seperate/m_boot_r
    m_Weight: 1
  - m_Path: g_mesh_seperate/m_chest
    m_Weight: 1
  - m_Path: g_mesh_seperate/m_gauntlet_l
    m_Weight: 1
  - m_Path: g_mesh_seperate/m_helm
    m_Weight: 1
  - m_Path: g_mesh_seperate/m_pants
    m_Weight: 1
  - m_Path: g_mesh_seperate/m_pauldron_l
    m_Weight: 1
  - m_Path: g_mesh_seperate/m_pauldron_r
    m_Weight: 1
  - m_Path: g_mesh_seperate/m_pouch1
    m_Weight: 1
  - m_Path: g_mesh_seperate/m_pouch2
    m_Weight: 1
  - m_Path: g_mesh_seperate/m_pouch3
    m_Weight: 1
  - m_Path: g_mesh_seperate/m_sword
    m_Weight: 1
  - m_Path: g_mesh_seperate/m_tabard
    m_Weight: 1
  - m_Path: g_skel_export
    m_Weight: 0
  - m_Path: g_skel_export/skel_root
    m_Weight: 1
  - m_Path: g_skel_export/skel_root/skel_hips
    m_Weight: 1
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_l
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_l/skel_forearm_l
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_l/skel_forearm_l/skel_bicep_l
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_l/skel_forearm_l/skel_bicep_l/skel_hand_l
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_l/skel_forearm_l/skel_bicep_l/skel_hand_l/skel_middle1_l
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_l/skel_forearm_l/skel_bicep_l/skel_hand_l/skel_middle1_l/skel_middle2_l
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_l/skel_forearm_l/skel_bicep_l/skel_hand_l/skel_middle1_l/skel_middle2_l/skel_middle3_l
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_l/skel_forearm_l/skel_bicep_l/skel_hand_l/skel_pinky1_l
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_l/skel_forearm_l/skel_bicep_l/skel_hand_l/skel_pinky1_l/skel_pinky2_l
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_l/skel_forearm_l/skel_bicep_l/skel_hand_l/skel_pinky1_l/skel_pinky2_l/skel_pinky3_l
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_l/skel_forearm_l/skel_bicep_l/skel_hand_l/skel_pointer1_l
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_l/skel_forearm_l/skel_bicep_l/skel_hand_l/skel_pointer1_l/skel_pointer2_l
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_l/skel_forearm_l/skel_bicep_l/skel_hand_l/skel_pointer1_l/skel_pointer2_l/skel_pointer3_l
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_l/skel_forearm_l/skel_bicep_l/skel_hand_l/skel_thumb1_l
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_l/skel_forearm_l/skel_bicep_l/skel_hand_l/skel_thumb1_l/skel_thumb2_l
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_l/skel_forearm_l/skel_bicep_l/skel_hand_l/skel_thumb1_l/skel_thumb2_l/skel_thumb3_l
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_l/skel_forearm_l/skel_shoulderPad_end_l
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_r
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_r/skel_forearm_r
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_r/skel_forearm_r/skel_bicep_r
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_r/skel_forearm_r/skel_bicep_r/skel_hand_r
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_r/skel_forearm_r/skel_bicep_r/skel_hand_r/skel_middle1_r
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_r/skel_forearm_r/skel_bicep_r/skel_hand_r/skel_middle1_r/skel_middle2_r
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_r/skel_forearm_r/skel_bicep_r/skel_hand_r/skel_middle1_r/skel_middle2_r/skel_middle3_r
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_r/skel_forearm_r/skel_bicep_r/skel_hand_r/skel_pinky1_r
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_r/skel_forearm_r/skel_bicep_r/skel_hand_r/skel_pinky1_r/skel_pinky2_r
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_r/skel_forearm_r/skel_bicep_r/skel_hand_r/skel_pinky1_r/skel_pinky2_r/skel_pinky3_r
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_r/skel_forearm_r/skel_bicep_r/skel_hand_r/skel_pointer1_r
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_r/skel_forearm_r/skel_bicep_r/skel_hand_r/skel_pointer1_r/skel_pointer2_r
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_r/skel_forearm_r/skel_bicep_r/skel_hand_r/skel_pointer1_r/skel_pointer2_r/skel_pointer3_r
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_r/skel_forearm_r/skel_bicep_r/skel_hand_r/skel_thumb1_r
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_r/skel_forearm_r/skel_bicep_r/skel_hand_r/skel_thumb1_r/skel_thumb2_r
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_r/skel_forearm_r/skel_bicep_r/skel_hand_r/skel_thumb1_r/skel_thumb2_r/skel_thumb3_r
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_clav_r/skel_forearm_r/skel_shoulderPad_end_r
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_neck
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_spine/skel_chest/skel_neck/skel_head_end
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_hips/skel_thigh_l
    m_Weight: 1
  - m_Path: g_skel_export/skel_root/skel_hips/skel_thigh_l/skel_shin_l
    m_Weight: 1
  - m_Path: g_skel_export/skel_root/skel_hips/skel_thigh_l/skel_shin_l/skel_ankle_l
    m_Weight: 1
  - m_Path: g_skel_export/skel_root/skel_hips/skel_thigh_l/skel_shin_l/skel_ankle_l/skel_ball_l
    m_Weight: 1
  - m_Path: g_skel_export/skel_root/skel_hips/skel_thigh_l/skel_shin_l/skel_ankle_l/skel_ball_l/skel_toe_end_l
    m_Weight: 1
  - m_Path: g_skel_export/skel_root/skel_hips/skel_thigh_r
    m_Weight: 1
  - m_Path: g_skel_export/skel_root/skel_hips/skel_thigh_r/skel_shin_r
    m_Weight: 1
  - m_Path: g_skel_export/skel_root/skel_hips/skel_thigh_r/skel_shin_r/skel_ankle_r
    m_Weight: 1
  - m_Path: g_skel_export/skel_root/skel_hips/skel_thigh_r/skel_shin_r/skel_ankle_r/skel_ball_r
    m_Weight: 1
  - m_Path: g_skel_export/skel_root/skel_hips/skel_thigh_r/skel_shin_r/skel_ankle_r/skel_ball_r/skel_toe_end_r
    m_Weight: 1
  - m_Path: g_skel_export/skel_root/skel_wep_grip
    m_Weight: 0
  - m_Path: g_skel_export/skel_root/skel_wep_grip/skel_wep_end
    m_Weight: 0
  - m_Path: g_wiring
    m_Weight: 0
  - m_Path: g_wiring/g_skel_fk
    m_Weight: 0
  - m_Path: g_wiring/g_skel_fk/skel_fk_forearm_l
    m_Weight: 0
  - m_Path: g_wiring/g_skel_fk/skel_fk_forearm_l/skel_fk_bicep_l
    m_Weight: 0
  - m_Path: g_wiring/g_skel_fk/skel_fk_forearm_l/skel_fk_bicep_l/skel_fk_hand_l
    m_Weight: 0
  - m_Path: g_wiring/g_skel_fk/skel_fk_forearm_r
    m_Weight: 0
  - m_Path: g_wiring/g_skel_fk/skel_fk_forearm_r/skel_fk_bicep_r
    m_Weight: 0
  - m_Path: g_wiring/g_skel_fk/skel_fk_forearm_r/skel_fk_bicep_r/skel_fk_hand_r
    m_Weight: 0
  - m_Path: g_wiring/g_skel_ik
    m_Weight: 0
  - m_Path: g_wiring/g_skel_ik/g_ik_arms
    m_Weight: 0
  - m_Path: g_wiring/g_skel_ik/skel_ik_forearm_l
    m_Weight: 0
  - m_Path: g_wiring/g_skel_ik/skel_ik_forearm_l/skel_ik_bicep_l
    m_Weight: 0
  - m_Path: g_wiring/g_skel_ik/skel_ik_forearm_l/skel_ik_bicep_l/skel_ik_hand_l
    m_Weight: 0
  - m_Path: g_wiring/g_skel_ik/skel_ik_forearm_r
    m_Weight: 0
  - m_Path: g_wiring/g_skel_ik/skel_ik_forearm_r/skel_ik_bicep_r
    m_Weight: 0
  - m_Path: g_wiring/g_skel_ik/skel_ik_forearm_r/skel_ik_bicep_r/skel_ik_hand_r
    m_Weight: 0
  - m_Path: g_wiring/g_spaces
    m_Weight: 0
  - m_Path: g_wiring/g_spaces/spc_chest
    m_Weight: 0
  - m_Path: g_wiring/g_spaces/spc_hand_l
    m_Weight: 0
  - m_Path: g_wiring/g_spaces/spc_hand_r
    m_Weight: 0
  - m_Path: g_wiring/g_spaces/spc_hips
    m_Weight: 0
  - m_Path: g_wiring/g_spaces/spc_neck
    m_Weight: 0
  - m_Path: g_wiring/g_spaces/spc_thigh_l
    m_Weight: 0
  - m_Path: g_wiring/g_spaces/spc_thigh_r
    m_Weight: 0
  - m_Path: g_wiring/g_spaces/spc_world
    m_Weight: 0
  - m_Path: m_proxy_player1
    m_Weight: 0
