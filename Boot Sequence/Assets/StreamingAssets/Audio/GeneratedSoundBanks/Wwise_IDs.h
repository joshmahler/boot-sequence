/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID BOMBERCHARGE = 2241161048U;
        static const AkUniqueID BOMBERCHARGESTOP = 2979898000U;
        static const AkUniqueID BOMBEREXPLOSION = 4047202833U;
        static const AkUniqueID BOMBERFOOTSTEP = 603035814U;
        static const AkUniqueID CHARACTER_SELECT = 3311442969U;
        static const AkUniqueID DASH = 1942692385U;
        static const AkUniqueID DASH_STOP = 3615040804U;
        static const AkUniqueID DUNGEON_AMBIENCE = 1845925700U;
        static const AkUniqueID ELECTRICLOOP = 332551302U;
        static const AkUniqueID ELEMENT = 4135068155U;
        static const AkUniqueID ENEMY_HIT = 1010055213U;
        static const AkUniqueID FIREBALLCAST = 534402839U;
        static const AkUniqueID FIREBALLHIT = 128707349U;
        static const AkUniqueID GEMSWAP = 2464846003U;
        static const AkUniqueID GENERATOR_DESTROY = 1156214555U;
        static const AkUniqueID GENERATOR_HIT = 188383770U;
        static const AkUniqueID GENERATOR_HUM = 4147798687U;
        static const AkUniqueID HEALTHPICKUP = 3065686491U;
        static const AkUniqueID HOVER = 3753593413U;
        static const AkUniqueID HOVER_STOP = 1797625800U;
        static const AkUniqueID KICKHIT = 3946660102U;
        static const AkUniqueID KICKMISS = 1140079467U;
        static const AkUniqueID KNIGHTATTACK = 1119039034U;
        static const AkUniqueID KNIGHTATTACKHIT = 1619645461U;
        static const AkUniqueID KNIGHTDANGERVO = 3813662282U;
        static const AkUniqueID KNIGHTFOOTSTEP = 2108997344U;
        static const AkUniqueID KNIGHTHURTVO = 4130224508U;
        static const AkUniqueID KNIGHTPITFALLVO = 1741235859U;
        static const AkUniqueID KNIGHTSPECIAL = 2695097747U;
        static const AkUniqueID KNIGHTSPECIALVO = 1704594082U;
        static const AkUniqueID MAGEATTACKVO = 2838812426U;
        static const AkUniqueID MAGEDANGERVO = 1130859619U;
        static const AkUniqueID MAGEFOOTSTEP = 3597534269U;
        static const AkUniqueID MAGEHURTVO = 2951716849U;
        static const AkUniqueID MAGEPITFALLVO = 579567824U;
        static const AkUniqueID MAGESPECIAL = 469094660U;
        static const AkUniqueID MONK_ATTACK = 3935031979U;
        static const AkUniqueID MONK_SPECIAL = 3003637796U;
        static const AkUniqueID PLAYMUSIC = 417627684U;
        static const AkUniqueID PROJECTILESIZZLE = 1256276641U;
        static const AkUniqueID PROJECTILESIZZLESTOP = 2055504377U;
        static const AkUniqueID RAILING_BREAK = 203632331U;
        static const AkUniqueID ROBOT_HIT = 3346201765U;
        static const AkUniqueID ROGUE_ATTACK = 3675485020U;
        static const AkUniqueID ROGUE_SPECIAL = 789736001U;
        static const AkUniqueID TELEPORT = 530129416U;
        static const AkUniqueID TURRET_GUNSHOT = 3048402288U;
        static const AkUniqueID WALKERAGGRO = 1794676631U;
        static const AkUniqueID WALKERATTACK = 819226263U;
        static const AkUniqueID WALKERDEATH = 1455463447U;
        static const AkUniqueID WALKERFOOTSTEP = 1526193385U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace MUSIC_STATE
        {
            static const AkUniqueID GROUP = 3826569560U;

            namespace STATE
            {
                static const AkUniqueID CS = 1853303367U;
                static const AkUniqueID DUNGEON = 608898761U;
                static const AkUniqueID GAME_OVER = 1432716332U;
                static const AkUniqueID MAIN_MENU = 2005704188U;
            } // namespace STATE
        } // namespace MUSIC_STATE

    } // namespace STATES

    namespace SWITCHES
    {
        namespace ELEMENT
        {
            static const AkUniqueID GROUP = 4135068155U;

            namespace SWITCH
            {
                static const AkUniqueID ELECTRIC = 3250089732U;
                static const AkUniqueID FIRE = 2678880713U;
                static const AkUniqueID ICE = 344481046U;
                static const AkUniqueID NULL = 784127654U;
                static const AkUniqueID WIND = 1537061107U;
            } // namespace SWITCH
        } // namespace ELEMENT

        namespace PLAYBACK
        {
            static const AkUniqueID GROUP = 3413458270U;

            namespace SWITCH
            {
                static const AkUniqueID LOOP = 691006007U;
                static const AkUniqueID ONESHOT = 1360693519U;
            } // namespace SWITCH
        } // namespace PLAYBACK

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID DASHENERGY = 1083842821U;
        static const AkUniqueID HOVERENERGY = 3078178729U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AGGRO = 456574329U;
        static const AkUniqueID ATTACK = 180661997U;
        static const AkUniqueID DEATH = 779278001U;
        static const AkUniqueID ENEMY = 2299321487U;
        static const AkUniqueID ENEMYFOOTSTEP = 534259801U;
        static const AkUniqueID GAME = 702482391U;
        static const AkUniqueID GENERATOR = 334231652U;
        static const AkUniqueID HIT = 1116398592U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID PLAYER = 1069431850U;
        static const AkUniqueID PLAYERFOOTSTEP = 3542290436U;
        static const AkUniqueID PLAYERVO = 3423020503U;
        static const AkUniqueID RAILING_BREAK = 203632331U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID REVERB = 348963605U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
