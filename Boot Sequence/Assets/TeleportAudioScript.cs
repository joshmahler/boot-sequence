﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportAudioScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        AkSoundEngine.PostEvent("Teleport", gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
