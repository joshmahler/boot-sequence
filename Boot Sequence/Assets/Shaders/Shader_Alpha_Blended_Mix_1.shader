// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Champlain/Shader_Alpha_Blended_Mix_1"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,0)
		_SecondaryColor("Secondary Color", Color) = (1,0,0,0)
		[Toggle(_COLORLERP_ON)] _ColorLerp("Color Lerp", Float) = 0
		_ColorBoost("Color Boost", Float) = 1
		_Mask1("Mask 1", 2D) = "white" {}
		_Mask1UTiling("Mask 1 U Tiling", Float) = 1
		_Mask1VTiling("Mask 1 V Tiling", Float) = 1
		_Mask1UOffset("Mask 1 U Offset", Float) = 0
		_Mask1VOffset("Mask 1 V Offset", Float) = 0
		_Mask1USpeed("Mask 1 U Speed", Float) = 0
		_Mask1VSpeed("Mask 1 V Speed", Float) = 0
		_DiffuseAttenuation("Diffuse Attenuation", Range( 0 , 1)) = 0
		_AlphaAttenuation("Alpha Attenuation", Range( 0 , 1)) = 0
		_Alpha("Alpha", 2D) = "white" {}
		_AlphaBoost("Alpha Boost", Float) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma shader_feature _COLORLERP_ON
		#pragma surface surf Unlit alpha:fade keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
			float4 vertexColor : COLOR;
		};

		uniform float _ColorBoost;
		uniform float4 _Color;
		uniform sampler2D _Mask1;
		uniform float _Mask1USpeed;
		uniform float _Mask1VSpeed;
		uniform float _Mask1UTiling;
		uniform float _Mask1VTiling;
		uniform float _Mask1UOffset;
		uniform float _Mask1VOffset;
		uniform float _DiffuseAttenuation;
		uniform float _AlphaAttenuation;
		uniform sampler2D _Alpha;
		uniform float4 _Alpha_ST;
		uniform float _AlphaBoost;
		uniform float4 _SecondaryColor;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float4 appendResult35 = (float4(_Mask1USpeed , _Mask1VSpeed , 0.0 , 0.0));
			float4 appendResult38 = (float4(_Mask1UTiling , _Mask1VTiling , 0.0 , 0.0));
			float4 appendResult41 = (float4(_Mask1UOffset , _Mask1VOffset , 0.0 , 0.0));
			float2 uv_TexCoord31 = i.uv_texcoord * appendResult38.xy + appendResult41.xy;
			float2 panner20 = ( 1.0 * _Time.y * appendResult35.xy + uv_TexCoord31);
			float4 tex2DNode19 = tex2D( _Mask1, panner20 );
			float2 uv_Alpha = i.uv_texcoord * _Alpha_ST.xy + _Alpha_ST.zw;
			float3 desaturateInitialColor4 = ( saturate( ( tex2DNode19 + _AlphaAttenuation ) ) * tex2D( _Alpha, uv_Alpha ) ).rgb;
			float desaturateDot4 = dot( desaturateInitialColor4, float3( 0.299, 0.587, 0.114 ));
			float3 desaturateVar4 = lerp( desaturateInitialColor4, desaturateDot4.xxx, 0.0 );
			float3 temp_output_42_0 = saturate( ( ( i.vertexColor.a * desaturateVar4 ) * _AlphaBoost ) );
			float4 temp_output_43_0 = ( saturate( ( tex2DNode19 + _DiffuseAttenuation ) ) * float4( temp_output_42_0 , 0.0 ) );
			float4 lerpResult15 = lerp( _SecondaryColor , _Color , temp_output_43_0);
			#ifdef _COLORLERP_ON
				float4 staticSwitch14 = lerpResult15;
			#else
				float4 staticSwitch14 = ( _Color * temp_output_43_0 );
			#endif
			o.Emission = ( _ColorBoost * ( staticSwitch14 * i.vertexColor ) ).rgb;
			o.Alpha = temp_output_42_0.x;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16200
-1913;1;1906;1010;2295.668;746.7563;1.634909;True;True
Node;AmplifyShaderEditor.RangedFloatNode;40;-4040.539,-279.1373;Float;False;Property;_Mask1UOffset;Mask 1 U Offset;7;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;37;-4052.675,-536.9706;Float;False;Property;_Mask1UTiling;Mask 1 U Tiling;5;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;36;-4050.076,-418.6704;Float;False;Property;_Mask1VTiling;Mask 1 V Tiling;6;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;39;-4037.94,-160.8371;Float;False;Property;_Mask1VOffset;Mask 1 V Offset;8;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;41;-3816.29,-236.8871;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;34;-3606.124,-12.42047;Float;False;Property;_Mask1VSpeed;Mask 1 V Speed;10;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;33;-3610.024,-104.7205;Float;False;Property;_Mask1USpeed;Mask 1 U Speed;9;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;38;-3828.426,-494.7204;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;35;-3386.424,-81.32047;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;31;-3574.927,-271.121;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;20;-3184.717,-162.31;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;22;-2522.018,65.14867;Float;False;Property;_AlphaAttenuation;Alpha Attenuation;12;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;19;-2892.766,-191.3279;Float;True;Property;_Mask1;Mask 1;4;0;Create;True;0;0;False;0;dd5a9a32a8b487f4db1d600326e62d68;dd5a9a32a8b487f4db1d600326e62d68;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;21;-2121.289,46.83472;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;27;-1861.094,66.87488;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;3;-2018.423,171.3857;Float;True;Property;_Alpha;Alpha;13;0;Create;True;0;0;False;0;51b909d6d869b1e4eaddddcbd8acd68e;51b909d6d869b1e4eaddddcbd8acd68e;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;23;-1596.899,155.1725;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.VertexColorNode;6;-1311.605,-27.75859;Float;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DesaturateOpNode;4;-1217.448,160.2462;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;11;-1001.73,329.4385;Float;False;Property;_AlphaBoost;Alpha Boost;14;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;7;-975.45,137.2462;Float;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;25;-2702.4,-15.20219;Float;False;Property;_DiffuseAttenuation;Diffuse Attenuation;11;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;-751.1292,137.6521;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;24;-2319.368,-181.0691;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;28;-2109.038,-186.2001;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;42;-594.4407,133.7489;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;5;-1833.267,-546.0078;Float;False;Property;_Color;Color;0;0;Create;True;0;0;False;0;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;43;-866.7596,-105.8724;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;16;-1836.534,-333.8079;Float;False;Property;_SecondaryColor;Secondary Color;1;0;Create;True;0;0;False;0;1,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;18;-1116.146,-534.0823;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;15;-1196.697,-326.9527;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;14;-746.6393,-355.0819;Float;False;Property;_ColorLerp;Color Lerp;2;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;17;-294.0222,43.83376;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;12;-361.7874,-155.6237;Float;False;Property;_ColorBoost;Color Boost;3;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;-84.33638,20.81981;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;2;161.1007,-25.57152;Float;False;True;2;Float;ASEMaterialInspector;0;0;Unlit;Champlain/Shader_Alpha_Blended_Mix_1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;False;0;False;Transparent;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;41;0;40;0
WireConnection;41;1;39;0
WireConnection;38;0;37;0
WireConnection;38;1;36;0
WireConnection;35;0;33;0
WireConnection;35;1;34;0
WireConnection;31;0;38;0
WireConnection;31;1;41;0
WireConnection;20;0;31;0
WireConnection;20;2;35;0
WireConnection;19;1;20;0
WireConnection;21;0;19;0
WireConnection;21;1;22;0
WireConnection;27;0;21;0
WireConnection;23;0;27;0
WireConnection;23;1;3;0
WireConnection;4;0;23;0
WireConnection;7;0;6;4
WireConnection;7;1;4;0
WireConnection;9;0;7;0
WireConnection;9;1;11;0
WireConnection;24;0;19;0
WireConnection;24;1;25;0
WireConnection;28;0;24;0
WireConnection;42;0;9;0
WireConnection;43;0;28;0
WireConnection;43;1;42;0
WireConnection;18;0;5;0
WireConnection;18;1;43;0
WireConnection;15;0;16;0
WireConnection;15;1;5;0
WireConnection;15;2;43;0
WireConnection;14;1;18;0
WireConnection;14;0;15;0
WireConnection;17;0;14;0
WireConnection;17;1;6;0
WireConnection;13;0;12;0
WireConnection;13;1;17;0
WireConnection;2;2;13;0
WireConnection;2;9;42;0
ASEEND*/
//CHKSM=C7577AB8817CFA977E895D7A3F7C958F006CCAFD