// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Champlain/Shader_Masked_Mix_2_Fresnel"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.333
		_Color("Color", Color) = (1,1,1,0)
		_SecondaryColor("Secondary Color", Color) = (1,0,0,0)
		[Toggle(_COLORLERP_ON)] _ColorLerp("Color Lerp", Float) = 0
		_ColorBoost("Color Boost", Float) = 1
		_Mask1("Mask 1", 2D) = "white" {}
		_Mask1UTiling("Mask 1 U Tiling", Float) = 1
		_Mask1VTiling("Mask 1 V Tiling", Float) = 1
		_Mask1UOffset("Mask 1 U Offset", Float) = 0
		_Mask1VOffset("Mask 1 V Offset", Float) = 0
		_Mask1USpeed("Mask 1 U Speed", Float) = 0
		_Mask1VSpeed("Mask 1 V Speed", Float) = 0
		_Mask1DiffuseAttenuation("Mask 1 Diffuse Attenuation", Range( 0 , 1)) = 0
		_Mask1AlphaAttenuation("Mask 1 Alpha Attenuation", Range( 0 , 1)) = 0
		_Mask2("Mask 2", 2D) = "white" {}
		_Mask2UTiling("Mask 2 U Tiling", Float) = 1
		_Mask2VTiling("Mask 2 V Tiling", Float) = 1
		_Mask2UOffset("Mask 2 U Offset", Float) = 0
		_Mask2VOffset("Mask 2 V Offset", Float) = 0
		_Mask2USpeed("Mask 2 U Speed", Float) = 0
		_Mask2VSpeed("Mask 2 V Speed", Float) = 0
		_Mask2DiffuseAttenuation("Mask 2 Diffuse Attenuation", Range( 0 , 1)) = 0
		_Mask2AlphaAttenuation("Mask 2 Alpha Attenuation", Range( 0 , 1)) = 0
		_Alpha("Alpha", 2D) = "white" {}
		_AlphaBoost("Alpha Boost", Float) = 1
		_AlphaDiffuseAttenuation("Alpha Diffuse Attenuation", Range( 0 , 1)) = 0
		_FresnelPower("Fresnel Power", Float) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Off
		ZWrite On
		ZTest Less
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma shader_feature _COLORLERP_ON
		#pragma surface surf Unlit keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
			float4 vertexColor : COLOR;
			float3 viewDir;
			float3 worldNormal;
		};

		uniform float _ColorBoost;
		uniform float4 _Color;
		uniform sampler2D _Mask2;
		uniform float _Mask2USpeed;
		uniform float _Mask2VSpeed;
		uniform float _Mask2UTiling;
		uniform float _Mask2VTiling;
		uniform float _Mask2UOffset;
		uniform float _Mask2VOffset;
		uniform float _Mask2DiffuseAttenuation;
		uniform sampler2D _Mask1;
		uniform float _Mask1USpeed;
		uniform float _Mask1VSpeed;
		uniform float _Mask1UTiling;
		uniform float _Mask1VTiling;
		uniform float _Mask1UOffset;
		uniform float _Mask1VOffset;
		uniform float _Mask1DiffuseAttenuation;
		uniform float _AlphaDiffuseAttenuation;
		uniform float _Mask2AlphaAttenuation;
		uniform float _Mask1AlphaAttenuation;
		uniform sampler2D _Alpha;
		uniform float4 _Alpha_ST;
		uniform float _FresnelPower;
		uniform float _AlphaBoost;
		uniform float4 _SecondaryColor;
		uniform float _Cutoff = 0.333;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float4 appendResult53 = (float4(_Mask2USpeed , _Mask2VSpeed , 0.0 , 0.0));
			float4 appendResult50 = (float4(_Mask2UTiling , _Mask2VTiling , 0.0 , 0.0));
			float4 appendResult51 = (float4(_Mask2UOffset , _Mask2VOffset , 0.0 , 0.0));
			float2 uv_TexCoord52 = i.uv_texcoord * appendResult50.xy + appendResult51.xy;
			float2 panner54 = ( 1.0 * _Time.y * appendResult53.xy + uv_TexCoord52);
			float4 tex2DNode56 = tex2D( _Mask2, panner54 );
			float4 appendResult35 = (float4(_Mask1USpeed , _Mask1VSpeed , 0.0 , 0.0));
			float4 appendResult38 = (float4(_Mask1UTiling , _Mask1VTiling , 0.0 , 0.0));
			float4 appendResult41 = (float4(_Mask1UOffset , _Mask1VOffset , 0.0 , 0.0));
			float2 uv_TexCoord31 = i.uv_texcoord * appendResult38.xy + appendResult41.xy;
			float2 panner20 = ( 1.0 * _Time.y * appendResult35.xy + uv_TexCoord31);
			float4 tex2DNode19 = tex2D( _Mask1, panner20 );
			float2 uv_Alpha = i.uv_texcoord * _Alpha_ST.xy + _Alpha_ST.zw;
			float3 desaturateInitialColor4 = ( ( saturate( ( tex2DNode56 + _Mask2AlphaAttenuation ) ) * saturate( ( tex2DNode19 + _Mask1AlphaAttenuation ) ) ) * tex2D( _Alpha, uv_Alpha ) ).rgb;
			float desaturateDot4 = dot( desaturateInitialColor4, float3( 0.299, 0.587, 0.114 ));
			float3 desaturateVar4 = lerp( desaturateInitialColor4, desaturateDot4.xxx, 0.0 );
			float3 ase_worldNormal = i.worldNormal;
			float3 ase_vertexNormal = mul( unity_WorldToObject, float4( ase_worldNormal, 0 ) );
			float dotResult79 = dot( i.viewDir , ( ase_vertexNormal * -1.0 ) );
			float dotResult75 = dot( i.viewDir , ase_vertexNormal );
			float3 temp_output_42_0 = saturate( ( ( ( i.vertexColor.a * desaturateVar4 ) * pow( max( saturate( dotResult79 ) , saturate( dotResult75 ) ) , _FresnelPower ) ) * _AlphaBoost ) );
			float4 temp_output_43_0 = ( ( saturate( ( tex2DNode56 + _Mask2DiffuseAttenuation ) ) * saturate( ( tex2DNode19 + _Mask1DiffuseAttenuation ) ) ) * float4( saturate( ( _AlphaDiffuseAttenuation + temp_output_42_0 ) ) , 0.0 ) );
			float4 lerpResult15 = lerp( _SecondaryColor , _Color , temp_output_43_0);
			#ifdef _COLORLERP_ON
				float4 staticSwitch14 = lerpResult15;
			#else
				float4 staticSwitch14 = ( _Color * temp_output_43_0 );
			#endif
			o.Emission = ( _ColorBoost * ( staticSwitch14 * i.vertexColor ) ).rgb;
			o.Alpha = 1;
			clip( temp_output_42_0.x - _Cutoff );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16200
7;1;1906;1010;1707.715;286.7533;1.045067;True;True
Node;AmplifyShaderEditor.RangedFloatNode;37;-4037.568,-292.7391;Float;False;Property;_Mask1UTiling;Mask 1 U Tiling;6;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;40;-4025.432,-34.90615;Float;False;Property;_Mask1UOffset;Mask 1 U Offset;8;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;36;-4034.969,-174.4389;Float;False;Property;_Mask1VTiling;Mask 1 V Tiling;7;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;39;-4022.833,83.39405;Float;False;Property;_Mask1VOffset;Mask 1 V Offset;9;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;47;-3968.625,-695.5293;Float;False;Property;_Mask2UOffset;Mask 2 U Offset;17;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-3966.026,-577.2292;Float;False;Property;_Mask2VOffset;Mask 2 V Offset;18;0;Create;True;0;0;False;0;0;0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;45;-3978.162,-835.0629;Float;False;Property;_Mask2VTiling;Mask 2 V Tiling;16;0;Create;True;0;0;False;0;1;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;46;-3980.761,-953.3632;Float;False;Property;_Mask2UTiling;Mask 2 U Tiling;15;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;49;-3538.109,-521.1127;Float;False;Property;_Mask2USpeed;Mask 2 U Speed;19;0;Create;True;0;0;False;0;0;0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;41;-3801.183,7.344025;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;33;-3610.024,-104.7205;Float;False;Property;_Mask1USpeed;Mask 1 U Speed;10;0;Create;True;0;0;False;0;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;50;-3756.511,-911.1129;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;38;-3813.319,-250.4889;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;48;-3532.742,-428.8123;Float;False;Property;_Mask2VSpeed;Mask 2 V Speed;20;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;51;-3744.375,-653.2792;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;34;-3606.124,-12.42047;Float;False;Property;_Mask1VSpeed;Mask 1 V Speed;11;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;52;-3503.012,-687.5131;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;53;-3314.509,-497.7126;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DynamicAppendNode;35;-3386.424,-81.32047;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;31;-3574.927,-271.121;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;20;-3184.717,-162.31;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;54;-3112.802,-578.7021;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;19;-2892.766,-191.3279;Float;True;Property;_Mask1;Mask 1;5;0;Create;True;0;0;False;0;dd5a9a32a8b487f4db1d600326e62d68;dd5a9a32a8b487f4db1d600326e62d68;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;55;-2519.035,-83.84421;Float;False;Property;_Mask2AlphaAttenuation;Mask 2 Alpha Attenuation;22;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;56;-2820.851,-607.7201;Float;True;Property;_Mask2;Mask 2;14;0;Create;True;0;0;False;0;dd5a9a32a8b487f4db1d600326e62d68;e3cc5194de1837742b7d1510913627b9;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;22;-2522.018,65.14867;Float;False;Property;_Mask1AlphaAttenuation;Mask 1 Alpha Attenuation;13;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.NormalVertexDataNode;78;-2805.858,410.1225;Float;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;21;-2121.289,46.83472;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;61;-2197.574,-186.8211;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;85;-2618.378,526.7761;Float;False;Constant;_Float1;Float 1;26;0;Create;True;0;0;False;0;-1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;84;-2470.524,414.4558;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;27;-1948.917,47.52388;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;80;-2455.858,249.1226;Float;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;74;-2449.524,560.4557;Float;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.NormalVertexDataNode;76;-2455.524,741.4557;Float;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;62;-1968.34,-152.5849;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;63;-1712.311,23.06262;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;3;-2018.423,171.3857;Float;True;Property;_Alpha;Alpha;23;0;Create;True;0;0;False;0;51b909d6d869b1e4eaddddcbd8acd68e;4e3b32866c3e4534ba5d863ac10b0d8c;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DotProductOpNode;79;-2208.858,383.1225;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;75;-2211.524,654.4557;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;23;-1510.564,150.7069;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;90;-2043.876,383.2644;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;91;-2042.244,654.501;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;92;-1841.326,504.1041;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;71;-1625.623,558.7008;Float;False;Property;_FresnelPower;Fresnel Power;26;0;Create;True;0;0;False;0;1;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DesaturateOpNode;4;-1321.864,152.5684;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.VertexColorNode;6;-1416.021,-35.43633;Float;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PowerNode;70;-1318.623,385.6558;Float;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;7;-1079.866,129.5684;Float;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;11;-1014.146,319.7607;Float;False;Property;_AlphaBoost;Alpha Boost;24;0;Create;True;0;0;False;0;1;5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;68;-923.6226,128.6558;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;-763.5456,127.9743;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;57;-2725.751,-385.4492;Float;False;Property;_Mask2DiffuseAttenuation;Mask 2 Diffuse Attenuation;21;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;25;-2708.23,-236.7362;Float;False;Property;_Mask1DiffuseAttenuation;Mask 1 Diffuse Attenuation;12;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;58;-2441.613,-602.4393;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;64;-1193.706,-161.5469;Float;False;Property;_AlphaDiffuseAttenuation;Alpha Diffuse Attenuation;25;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;24;-2325.198,-402.6031;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;42;-590.9572,127.3712;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;59;-2260.281,-600.0494;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;65;-828.3342,-78.51469;Float;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;28;-2186.769,-398.0177;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;60;-1904.331,-521.7422;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;66;-556.6343,-95.41467;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;16;-1801.368,-951.1669;Float;False;Property;_SecondaryColor;Secondary Color;2;0;Create;True;0;0;False;0;1,0,0,0;1,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;43;-912.8258,-376.1291;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;5;-1798.101,-1163.366;Float;False;Property;_Color;Color;1;0;Create;True;0;0;False;0;1,1,1,0;1,0.7737833,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;18;-1080.98,-1151.441;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;15;-1161.531,-944.3116;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;14;-711.4732,-972.4409;Float;False;Property;_ColorLerp;Color Lerp;3;0;Create;True;0;0;False;0;0;0;1;True;;Toggle;2;Key0;Key1;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;12;-234.799,-233.7704;Float;False;Property;_ColorBoost;Color Boost;4;0;Create;True;0;0;False;0;1;3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;17;-294.0222,43.83376;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;-84.33638,20.81981;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;2;161.1007,-25.57152;Float;False;True;2;Float;ASEMaterialInspector;0;0;Unlit;Champlain/Shader_Masked_Mix_2_Fresnel;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;Off;1;False;-1;1;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.333;True;False;0;True;TransparentCutout;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;41;0;40;0
WireConnection;41;1;39;0
WireConnection;50;0;46;0
WireConnection;50;1;45;0
WireConnection;38;0;37;0
WireConnection;38;1;36;0
WireConnection;51;0;47;0
WireConnection;51;1;44;0
WireConnection;52;0;50;0
WireConnection;52;1;51;0
WireConnection;53;0;49;0
WireConnection;53;1;48;0
WireConnection;35;0;33;0
WireConnection;35;1;34;0
WireConnection;31;0;38;0
WireConnection;31;1;41;0
WireConnection;20;0;31;0
WireConnection;20;2;35;0
WireConnection;54;0;52;0
WireConnection;54;2;53;0
WireConnection;19;1;20;0
WireConnection;56;1;54;0
WireConnection;21;0;19;0
WireConnection;21;1;22;0
WireConnection;61;0;56;0
WireConnection;61;1;55;0
WireConnection;84;0;78;0
WireConnection;84;1;85;0
WireConnection;27;0;21;0
WireConnection;62;0;61;0
WireConnection;63;0;62;0
WireConnection;63;1;27;0
WireConnection;79;0;80;0
WireConnection;79;1;84;0
WireConnection;75;0;74;0
WireConnection;75;1;76;0
WireConnection;23;0;63;0
WireConnection;23;1;3;0
WireConnection;90;0;79;0
WireConnection;91;0;75;0
WireConnection;92;0;90;0
WireConnection;92;1;91;0
WireConnection;4;0;23;0
WireConnection;70;0;92;0
WireConnection;70;1;71;0
WireConnection;7;0;6;4
WireConnection;7;1;4;0
WireConnection;68;0;7;0
WireConnection;68;1;70;0
WireConnection;9;0;68;0
WireConnection;9;1;11;0
WireConnection;58;0;56;0
WireConnection;58;1;57;0
WireConnection;24;0;19;0
WireConnection;24;1;25;0
WireConnection;42;0;9;0
WireConnection;59;0;58;0
WireConnection;65;0;64;0
WireConnection;65;1;42;0
WireConnection;28;0;24;0
WireConnection;60;0;59;0
WireConnection;60;1;28;0
WireConnection;66;0;65;0
WireConnection;43;0;60;0
WireConnection;43;1;66;0
WireConnection;18;0;5;0
WireConnection;18;1;43;0
WireConnection;15;0;16;0
WireConnection;15;1;5;0
WireConnection;15;2;43;0
WireConnection;14;1;18;0
WireConnection;14;0;15;0
WireConnection;17;0;14;0
WireConnection;17;1;6;0
WireConnection;13;0;12;0
WireConnection;13;1;17;0
WireConnection;2;2;13;0
WireConnection;2;10;42;0
ASEEND*/
//CHKSM=7DEFEE8732052449E64CB166D6BC36DFADAF1AFB